﻿using Ori.Core.Helpers;
using System;
using Xunit;
using Ori.StreamManager;
using Ori.Codec;
using Ori.RTPConnection;
using System.Threading;

namespace Ori.StreamManagerSharpTest
{
public class StreamManagerSharp
{
    static StreamManagerSharp()
    {
        var anOriHome = Environment.GetEnvironmentVariable ("ORI_HOME");
        DllLoader.Load (@$"{anOriHome}\DLLs\StreamManager\shared\debug");
        DllLoader.Load (@$"{anOriHome}\ThirdParty\ffmpeg\vc142-x86_64\debug\bin");
        DllLoader.Load (@$"{anOriHome}\ThirdParty\opencv\build\bin\Debug");
    }

    [Fact]
    public void Run()
    {
        var aParams = new Parameters();
        aParams.SetCameraId (0);
        aParams.SetPortBase (8888);
        
        var anEncParams = new EncoderParameters();
        anEncParams.SetWidth (320);
        anEncParams.SetHeight (240);
        anEncParams.SetVideoCodecID (AVCodecID.AV_CODEC_ID_VP8);
        aParams.SetEncoderParameters (anEncParams);

        var aPoint = new DestinationPoint ("127.0.0.1", 8890);
        aParams.SetDestinationPoint (aPoint);

        var aManager = new Manager (aParams);
        
        var aThread = new Thread (new ThreadStart (() => {
            aManager.Start();
        }));

        aThread.Start();
        aThread.Join();
    }
}}
