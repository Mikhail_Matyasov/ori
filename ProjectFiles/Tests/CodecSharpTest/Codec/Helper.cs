﻿using Ori.Codec;
using System;
using System.Collections.Generic;

namespace CodecCSharpTest.CodecTest
{
public class Helper
{
    public static void FillFrame (Frame theFrame, int theIndex)
    {
        int i, j;
        int aWidth = theFrame.Width();
        int aHeight = theFrame.Height();
        
        var aData0 = new std.ByteArray();
        var aData1 = new std.ByteArray();
        var aData2 = new std.ByteArray();

        for (i = 0; i < aHeight; i++) {
            for (j = 0; j < aWidth; j++) {
                aData0.Add (Convert.ToByte ((i + j + theIndex) % 256));
            }
        }

        /* Cb and Cr */
        for (i = 0; i < aHeight / 2; i++) {
            for (j = 0; j < aWidth / 2; j++) {
                aData1.Add (Convert.ToByte ((i + j + i * j * theIndex) % 256));
                aData2.Add (Convert.ToByte ((i + j + i + theIndex) % 256)); 
            }
        }

        theFrame.Set (0, aData0);
        theFrame.Set (1, aData1);
        theFrame.Set (2, aData2);
    }
}}
