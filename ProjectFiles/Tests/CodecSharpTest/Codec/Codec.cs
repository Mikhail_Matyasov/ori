﻿using Xunit;
using Ori.Codec;
using Ori.Core.Helpers;
using System.Collections.Generic;
using System;

namespace CodecCSharpTest.CodecTest
{
public class Codec
{
static Codec()
{
    string anOriHome = Environment.GetEnvironmentVariable ("ORI_HOME");
	DllLoader.Load (@$"{anOriHome}\\DLLs\\Codec\\debug");
	DllLoader.Load (@$"{anOriHome}\\ThirdParty\\ffmpeg\\vc142-x86_64\\debug\\bin");
}

[Fact]
public void EncodeData()
{
	var aPackets = CreatePackets();

	Assert.True (aPackets[0].GetData().Count == 76925);
	Assert.True (aPackets[2].GetData().Count == 5449);
	Assert.True (aPackets[4].GetData().Count == 5475);

	Assert.True (aPackets[0].GetData()[500] == 221);
	Assert.True (aPackets[0].GetData()[1000] == 241);
}

[Fact]
public void MuxData()
{
    MuxerParameters aParameters = new MuxerParameters();
	aParameters.SetWidth (1366);
	aParameters.SetHeight (768);
	Muxer aMuxer = new Muxer (aParameters);
	var aPackets = CreatePackets();

	foreach (var aPacket in aPackets) {
		Assert.True (aMuxer.MuxPacket (aPacket));

		Fragment aFragment = new Fragment();
		if (aMuxer.GetFragment (aFragment)) {
			Assert.True (aFragment.Data().Count != 0);

			Fragment aNewFragment = new Fragment();
			Assert.False (aMuxer.GetFragment (aNewFragment));
		}
	}
}

[Fact]
public void CreatePacketFromBytes()
{
	byte[] aBuffer = new byte[] 
	{ 
		1, 2, 0, 2, 3,  9, 9, 12, 34, 12
	};

	MuxerParameters aParams = new MuxerParameters();
	Muxer aMuxer = new Muxer (aParams);

	for (int i = 0; i < 10; i++) {
		var aPacket = aMuxer.MakePacket (new std.ByteArray (aBuffer));
		Assert.True (aPacket.GetData().Count != 0);
	}
}

[Fact]
public void ScaleFrame()
{
	var aFrame = new Frame (AVPixelFormat.AV_PIX_FMT_YUV420P, 320, 240);
	Helper.FillFrame (aFrame, 0);

	ScalerParameters aParams = new ScalerParameters();
	aParams.SetSourceWidth (aFrame.Width());
	aParams.SetSourceHeight (aFrame.Height());
	aParams.SetSourcePixelFormat (AVPixelFormat.AV_PIX_FMT_YUV420P);
	aParams.SetTargetWidth (aFrame.Width());
	aParams.SetTargetHeight (aFrame.Height());
	aParams.SetTargetPixelFormat (AVPixelFormat.AV_PIX_FMT_RGBA);

	Scaler aScaler = new Scaler (aParams);
	var aConvertedFrame = aScaler.ConvertPixelFormat (aFrame);
	
	var aData = aConvertedFrame.GetData();
	Assert.True (aData.Count == 1);
	Assert.True (aData[0].Count == aFrame.Width() * aFrame.Height() * 4);
}

[Fact]
public void DecodePacket()
{
	var aPackets = CreatePackets();

	int aNumberOfFrames = 0;
	int aNumberOfBytes = 0;

	DecoderParameters aDecParameters = new DecoderParameters();
	aDecParameters.SetWidth (1366);
	aDecParameters.SetHeight (768);
	Decoder aDecoder = new Decoder (aDecParameters);
	
	for (int i = 0; i < aPackets.Count; i++) {

		var aPacket = aDecoder.MakePacket (aPackets[i].GetData());
		Assert.True (aDecoder.DecodePacket (aPacket));

		var aFrame = aDecoder.GetFrame();
		if (aFrame != null) {
			aNumberOfFrames++;
			aNumberOfBytes += aFrame.GetData()[0].Count;
		}
	}
	
	Assert.True (aNumberOfFrames == 20);
	Assert.True (aNumberOfBytes == 20981760);
}


List <Packet> CreatePackets()
{
	EncoderParameters aParameters = new EncoderParameters();
	aParameters.SetWidth (1366);
	aParameters.SetHeight (768);
	aParameters.SetVideoCodecID (AVCodecID.AV_CODEC_ID_VP8);

	Encoder anEncoder = new Encoder (aParameters);

	int aNumberOfFrames = 20;
	List<Packet> aPackets = new List <Packet>();
	for (int i = 0; i < aNumberOfFrames; i++)
	{
		var aFrame = anEncoder.MakeFrame();
		Helper.FillFrame (aFrame, i);
		anEncoder.EncodeFrame(aFrame);

		var aPacket = new Packet (new Packet.Initialized());
		if (anEncoder.GetPacket (aPacket)) {
			aPackets.Add (aPacket);
		}
	}

	return aPackets;
}

}}

