using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using System.Linq;

namespace Ori.PCTestLib {
public class PCTestLib
{
private readonly PeerConnectionInit anInit = new PeerConnectionInit (rtc.LoggingSeverity.LS_NONE);
static PCTestLib()
{
    string anOriHome = Environment.GetEnvironmentVariable ("ORI_HOME");

    Core.Helpers.DllHelper.AddDllDirrectory ($@"{anOriHome}/DLLs/PeerConnection/debug");
    Core.Helpers.DllHelper.AddDllDirrectory ($@"{anOriHome}/ThirdParty/tbb/vc142-x86_64/debug/lib");
    Core.Helpers.DllHelper.AddDllDirrectory ($@"{anOriHome}/DLLs/Core/debug");
}

[Fact]
public void SetIceCandidateProperty()
{
    IceCandidate aCandidate = new IceCandidate();
    aCandidate.SetSdpMlineIndex (5);
    Assert.True (aCandidate.SdpMlineIndex() == 5);
}

[Fact]
public void CreatePeerConnection()
{
    DataChannelObserver aPeer1ChannelObserver = new DataChannelObserver();
    PeerConnectionObserver aPeer1ConnectionObserver = new PeerConnectionObserver();
    PeerConnection aPeerConnection = new PeerConnection (aPeer1ConnectionObserver, aPeer1ChannelObserver);
    Assert.True (aPeerConnection.Ready());
}

[Fact]
public void EstablishConnection()
{
    DataChannelObserver aPeer1ChannelObserver = new DataChannelObserver();
    PeerConnectionObserver aPeer1ConnectionObserver = new PeerConnectionObserver();
    PeerConnection aPeer1 = new PeerConnection (aPeer1ConnectionObserver, aPeer1ChannelObserver);

    DataChannelObserver aPeer2ChannelObserver = new DataChannelObserver();
    PeerConnectionObserver aPeer2ConnectionObserver = new PeerConnectionObserver();
    PeerConnection aPeer2 = new PeerConnection (aPeer2ConnectionObserver, aPeer2ChannelObserver);

    aPeer1ConnectionObserver.SetOtherPeer (aPeer2);
    aPeer1ConnectionObserver.SetOtherChannelObserver (aPeer2ChannelObserver);
    aPeer2ConnectionObserver.SetOtherPeer (aPeer1);
    aPeer2ConnectionObserver.SetOtherChannelObserver (aPeer1ChannelObserver);

    Task.Run (() => {
        while (!aPeer1ChannelObserver.Openned) {}
        aPeer1.StopListening();
    });

    aPeer1.CreateOffer (false);
    aPeer1.Listen();

    Assert.True (aPeer1ConnectionObserver.Offer.Sdp().Lenght() != 0);
    Assert.True (aPeer1ConnectionObserver.Offer.Type() == SessionDescription.DescriptionType.OFFER);
    Assert.True (aPeer1ConnectionObserver.Candidate.Candidate().Lenght() != 0);

    Assert.True (aPeer2ConnectionObserver.Answer.Sdp().Lenght() != 0);
    Assert.True (aPeer2ConnectionObserver.Answer.Type() == SessionDescription.DescriptionType.ANSWER);
    Assert.True (aPeer2ConnectionObserver.Candidate.Candidate().Lenght() != 0);

    Task.Run (() => {
        while (aPeer2ChannelObserver.Message == null) {}
        aPeer1.StopListening();
    });

    var aMessage = Message ("My message");
    aPeer1.Send (aMessage, true);
    aPeer1.Listen();
    
    bool anIsEqual = aPeer2ChannelObserver.Message.IsEqual (aMessage);
    Assert.True (anIsEqual);
}

[Fact]
public void CreateOfferFromOtherThread()
{
    Thread aT1 = new Thread (new ThreadStart (()=> {
        DataChannelObserver aPeer1ChannelObserver = new DataChannelObserver();
        PeerConnectionObserver aPeer1ConnectionObserver = new PeerConnectionObserver();
        PeerConnection aPeer1 = new PeerConnection (aPeer1ConnectionObserver, aPeer1ChannelObserver);

        Task.Run (() => {
            while (aPeer1ConnectionObserver.Candidate == null) {}
            aPeer1.StopListening();
        });

        aPeer1.CreateOffer();
        aPeer1.Listen();

        Assert.True (aPeer1ConnectionObserver.Offer.Sdp().Lenght() != 0);
        Assert.True (aPeer1ConnectionObserver.Offer.Type() == Ori.SessionDescription.DescriptionType.OFFER);
        Assert.True (aPeer1ConnectionObserver.Candidate.Candidate().Lenght() != 0);
    }));

    aT1.Start();
    aT1.Join();
}

[Fact]
public void CreateMessage()
{
    Message ("Message");
}

Base.ByteArray Message (string theMessage)
{
    var aByteArray = Encoding.UTF8.GetBytes (theMessage);
    var aMessage = new Base.ByteArray();
    aMessage.Insert (aByteArray, aByteArray.Count());

    Assert.True (theMessage.Length == aMessage.Lenght());
    return aMessage;
}

}}
