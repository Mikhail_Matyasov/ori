﻿
namespace Ori.PCTestLib {
class DataChannelObserver : Ori.DataChannelObserver
{

public DataChannelObserver()
{
    Openned = false;    
}

public Base.ByteArray Message { get; set; }
public bool Openned { get; set; }
public override void OnMessage (Base.ByteArray theMessage)
{
    Message = new Base.ByteArray (theMessage);
}

public override void OnOpenned()
{
    Openned = true;
}

}}
