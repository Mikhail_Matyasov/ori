﻿using System.Threading.Tasks;

namespace Ori.PCTestLib {
class PeerConnectionObserver : Ori.PeerConnectionObserver
{
public SessionDescription Offer { get; set; }
public SessionDescription Answer { get; set; }
public IceCandidate Candidate { get; set; }
private PeerConnection myOtherPeer = null;
private DataChannelObserver myOtherChannelObserver = null;

public void SetOtherPeer (PeerConnection theOtherPeer)
{
    myOtherPeer = theOtherPeer;
}

public void SetOtherChannelObserver (DataChannelObserver theObserver)
{
    myOtherChannelObserver = theObserver;
}

public override void OnCreateAnswer (Base.String theAnswer)
{
    Answer = PeerConnectionLib.JsonReader.ParseSdp (theAnswer);
    if (myOtherPeer != null) {
        myOtherPeer.SetRemoteDescription (Answer, false);   
    }
}

public override void OnCreateOffer (Base.String theOffer)
{
    Offer = PeerConnectionLib.JsonReader.ParseSdp (theOffer);

    if (myOtherPeer != null) {
        myOtherPeer.SetRemoteDescription (Offer, false);
        myOtherPeer.CreateAnswer (false);

        Task.Run (() => {
            while (!myOtherChannelObserver.Openned) {}
            myOtherPeer.StopListening();
        });

        myOtherPeer.Listen();
    }
}
public override void OnIceCandidate (Base.String theCandidate)
{
    Candidate = PeerConnectionLib.JsonReader.ParseCandidate (theCandidate);

    if (myOtherPeer != null) {
        myOtherPeer.AddIceCandidate (Candidate, false); 
    }
}

}}
