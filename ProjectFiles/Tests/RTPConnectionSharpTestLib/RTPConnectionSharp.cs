using Ori.Core.Helpers;
using Ori.RTPConnection;
using Ori.Base;
using System;
using System.Threading;
using Xunit;

namespace RTPConnectionSharpTestLib
{
public class RTPConnectionSharp
{

static RTPConnectionSharp()
{
    var anOriHome = Environment.GetEnvironmentVariable ("ORI_HOME");
    DllHelper.AddDllDirrectory (@$"{anOriHome}\DLLs\RTPConnection\debug");
    DllHelper.AddDllDirrectory (@$"{anOriHome}\DLLs\PeerConnection\debug");
    DllHelper.AddDllDirrectory ($@"{anOriHome}\DLLs\Core\debug");
}

class RTPObserver : RTPListener.Observer
{
    public override void OnPacket (ByteArray thePacket)
    {
        Packet = new ByteArray (thePacket);
    }

    public ByteArray Packet { get; set; }
}

private void SendData (byte[] theData, int thePortBase)
{
    RTPObserver anObserver = new RTPObserver();
    RTPListener aListener = new RTPListener (thePortBase, anObserver);
    Assert.True (aListener.SessionStatus().IsEqual ("Success"));

    var aThread = new Thread (new ThreadStart (() => {

        aListener.Start();
    }));
    aThread.Start();

    DestinationPoint aPoint = new DestinationPoint (new Ori.Base.String ("127.0.0.1"), thePortBase);
    var anArray = new DestinationPointArray();
    anArray.Push (aPoint);

    RTPClient aClient = new RTPClient (thePortBase + 2, anArray);
    Assert.True (aClient.SessionStatus().IsEqual ("Success"));

    ByteArray aPacket = new ByteArray();
    aPacket.Insert (theData, theData.Length);
    Assert.True (aClient.Send (aPacket));

    while (anObserver.Packet == null) {}
    var aTargetData = anObserver.Packet;
    Assert.True (aPacket.At (2) == aTargetData.At (2));
    Assert.True (aPacket.Lenght() == aTargetData.Lenght());

    aListener.Stop();
}

[Fact]
public void SendPacketOnce()
{
    int aPortBase = 8000;
    SendData (new byte[] { 0, 1, 2, 3 }, aPortBase);
}

[Fact]
public void SendBigData()
{
    int aDataSize = 40000;
    byte[] aData = new byte[aDataSize];
    for (int i = 0; i < aDataSize; i++) {
        aData[i] = Convert.ToByte (i % 256);
    }

    int aPortBase = 8004;
    SendData (aData, aPortBase);
}

}

}
