#include <PCTestLib/Pch.hxx>
#include <Ori/PeerConnectionInit.hxx>

#include <rtc_base/logging.h>

int main()
{
    Ori::PeerConnectionInit anInit (rtc::LoggingSeverity::LS_NONE);
    //testing::GTEST_FLAG(filter) = "PeerConnection.CreateMessage";
    //testing::GTEST_FLAG(filter) = "PeerConnection.CreateTenConnectionFromOneFactoryFromDifferentThreads";
    //testing::GTEST_FLAG(filter) = "PeerConnection.EstablishConnectionTest";
    //testing::GTEST_FLAG(filter) = "PeerConnection.CreateTenConnection";
    //testing::GTEST_FLAG(filter) = "PeerConnection.EstablishBrowserConnection";
    //testing::GTEST_FLAG(filter) = "PeerConnection.EstablishMultyConnection";
    //testing::GTEST_FLAG(filter) = "PeerConnection.Send100Messages";
    testing::InitGoogleMock();
    testing::InitGoogleTest();

    RUN_ALL_TESTS();
}