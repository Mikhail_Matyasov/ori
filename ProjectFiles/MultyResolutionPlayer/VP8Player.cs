﻿using Ori.Core.RTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Ori.RTPConnection;
using std;
using System.Threading;
using System.Linq;
using Xunit;

namespace Ori.MultyResolutionPlayer
{
    class VP8Player
    {
        private static int IndexOf (int theStartIndex, byte[] theArray)
        {
            byte[] aPattern = { 45, 45, 45, 45, 45, 62 };
            for (int i = theStartIndex;  i < theArray.Length; i++) {
                
                if (theArray[i    ] == aPattern[0] &&
                    theArray[i + 1] == aPattern[1] &&
                    theArray[i + 2] == aPattern[2] &&
                    theArray[i + 3] == aPattern[3] &&
                    theArray[i + 4] == aPattern[4] &&
                    theArray[i + 5] == aPattern[5]) {
                    
                    return i;
                }
            }

            return -1;
        }
        private static List <byte[]> ReadFile (string theFileName)
        {
            var anOriHome = Environment.GetEnvironmentVariable ("ORI_HOME");
            var aFileStream =  File.OpenRead ($@"{anOriHome}\ProjectFiles\MultyResolutionPlayer\Resources\vp8\{theFileName}");
            BinaryReader aReader = new BinaryReader (aFileStream);
            var aFile = aReader.ReadBytes ((int)aFileStream.Length);

            List <byte[]> aRes = new List <byte[]>();
            int aStartIndex = 0;
            int aPatternLenght = 6;
            while (true) {
                
                int anEnd = IndexOf (aStartIndex + aPatternLenght, aFile);
                if (anEnd == -1) {
                    break;
                }
                int aFrameCapacity = anEnd - aStartIndex;
                byte[] aFrame = new byte[aFrameCapacity];
                Array.Copy (aFile, aStartIndex + aPatternLenght, aFrame, 0, aFrameCapacity);
                aRes.Add (aFrame);
                
                aStartIndex = anEnd;
            }


            return aRes;
        }

        public static async void Play (string theFileName, int thePacketWidth, int thePacketHeight)
        {
            var aPackets = ReadFile (theFileName);

            var aDescriptor = new RTPConnectionDescriptor();
            aDescriptor.Description = "Multy resolution player";
            aDescriptor.Password = "";
            aDescriptor.Host = "oritube.ru";
            aDescriptor.PacketWidth = thePacketWidth;
            aDescriptor.PacketHeight = thePacketHeight;

            int aPort = await RTPConnectionBuilder.Build (aDescriptor);
            var anIps = Dns.GetHostAddresses ("oritube.ru");
            string anIpv4 = "";
            foreach (var anIp in anIps) {
                if (anIp.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                   anIpv4 = anIp.ToString();
                   break;
                }
            }

            var aPoint = new DestinationPoint (anIpv4, aPort);
            int aPortBase = 222222;
            var aClient = new RTPClient (aPortBase, new DestinationPointVector { aPoint });
            Assert.True (aClient.SessionStatus() == "Success", "RTP session is not valid.");

            int aFps = 30;
            while (true)
            {
                foreach (var aPacketData in aPackets) {

                    var anRTPPacket = new RTPPacket (aPacketData);
                    Assert.True (aClient.Send (anRTPPacket), "Can not send packet via network");
                    Thread.Sleep (1000 / aFps);
                }
            }
        }
    }
}
