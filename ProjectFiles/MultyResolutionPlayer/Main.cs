using System;
using Xunit;
using Ori.Core.Helpers;

namespace Ori.MultyResolutionPlayer
{
    public class Main
    {

        static Main()
        {
            var anOriHome = Environment.GetEnvironmentVariable ("ORI_HOME");
            DllLoader.Load (@$"{anOriHome}\DLLs\RTPConnection\debug");
        }

        [Fact]
        public void vp8_320x240()
        {
            VP8Player.Play ("320x240.vp8", 320, 240);
        }

        [Fact]
        public void vp8_640x480()
        {
            VP8Player.Play ("640x480.vp8", 640, 480);
        }

        [Fact]
        public void vp8_1280x720()
        {
            VP8Player.Play ("1280x720.vp8", 1280, 720);
        }

        [Fact]
        public void vp8_1920x1080()
        {
            VP8Player.Play ("1920x1080.vp8", 1920, 1080);
        }

        [Fact]
        public void vp8_240x320()
        {
            VP8Player.Play ("240x320.vp8", 240, 320);
        }

        [Fact]
        public void vp8_480x640()
        {
            VP8Player.Play ("480x640.vp8", 480, 640);
        }

        [Fact]
        public void vp8_720x1280()
        {
            VP8Player.Play ("720x1280.vp8", 720, 1280);
        }

        [Fact]
        public void vp8_1080x1920()
        {
            VP8Player.Play ("1080x1920.vp8", 1080, 1920);
        }
    }
}
