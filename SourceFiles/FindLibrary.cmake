
function (FindLibrary TARGET_LIBRARY TARGET_PROJECT)

    if (${TARGET_LIBRARY} STREQUAL "libvpx")
        FindLIBVPX (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "ffmpeg")
        FindFFMPEG (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "jrtplib")
        FindJRTPLIB (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "signalr")
        FindSIGNALR (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "cpprest")
        FindCPPREST (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "zlib")
        FindZLIB (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} MATCHES "boost*")
        FindBOOST (${TARGET_LIBRARY} ${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "googletest")
        FindGOOGLETEST (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "webrtc")
        FindWEBRTC (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "tbb")
        FindTBB (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "sdl2")
        FindSDL2 (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "rapidjson")
        FindRAPIDJSON (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "openssl")
        FindOPENSSL (${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} MATCHES "Qt5*")
        FindQT (${TARGET_LIBRARY} ${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "lora")
        FindLoRa (${TARGET_LIBRARY} ${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "arduino")
        FindArduino (${TARGET_LIBRARY} ${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} STREQUAL "nrf")
        FindNRF24L01 (${TARGET_LIBRARY} ${TARGET_PROJECT})
    elseif (${TARGET_LIBRARY} MATCHES "opencv*")
        FindOpenCV (${TARGET_LIBRARY} ${TARGET_PROJECT})    
    else()
        target_link_libraries (${TARGET_PROJECT} ${TARGET_LIBRARY})
    endif()
endfunction()

function (ShowMessage CONDITION LIB)
    if (EXISTS ${CONDITION})
        
        string(FIND "$ENV{FOUNDED_LIBRARIES}" "${LIB}" RES)
        if (${RES} EQUAL -1)
            message ("-- Found: ${LIB}")
            set (ENV{FOUNDED_LIBRARIES} "$ENV{FOUNDED_LIBRARIES} ${LIB}")
        endif()
    else()
        string(FIND "$ENV{NOTFOUNDED_LIBRARIES}" "${LIB}" RES)
        if (${RES} EQUAL -1)
            message ("-- Not Found: ${LIB}")
            set (ENV{NOTFOUNDED_LIBRARIES} "$ENV{NOTFOUNDED_LIBRARIES} ${LIB}")
        endif()
    endif()
endfunction()

function (FindLIBVPX TARGET_PROJECT)
    set (LIBVPX_DIR ${THIRD_PARTY_HOME}/libvpx/${COMPILER}-${ARCH}/${MODE})
    set (LIBVPX_INCLUDE ${LIBVPX_DIR}/include)
    set (LIBVPX_LIB_DIR ${LIBVPX_DIR}/lib)

    if (EXISTS ${LIBVPX_DIR})
        if (${COMPILER} STREQUAL "and")
            set (LIBVPX_LIB libvpx.a)
        else()
            set (LIBVPX_LIB vpx.lib)
        endif()
        
        include_directories (${LIBVPX_INCLUDE})
        target_link_libraries (${TARGET_PROJECT} ${LIBVPX_LIB_DIR}/${LIBVPX_LIB})
    endif()
    
    ShowMessage (${LIBVPX_DIR} libvpx)
endfunction()

function (FindFFMPEG TARGET_PROJECT)
    set (FFMPEG_DIR ${THIRD_PARTY_HOME}/ffmpeg/${COMPILER}-${ARCH}/${MODE})
    set (FFMPEG_INCLUDE ${FFMPEG_DIR}/include)
    
    if (EXISTS ${FFMPEG_DIR})
        if (${COMPILER} STREQUAL "and")
            set (FFMPEG_LIB lib/libavformat.a
                            lib/libavcodec.a
                            lib/libswscale.a
                            lib/libavutil.a)
        else()
            set (FFMPEG_LIB lib/avformat.lib
                            lib/avcodec.lib
                            lib/avutil.lib
                            lib/swscale.lib)
        endif()
        
        include_directories (${FFMPEG_INCLUDE})
        foreach (LIB ${FFMPEG_LIB})
            target_link_libraries (${TARGET_PROJECT} ${FFMPEG_DIR}/${LIB})
        endforeach()
        
        if (${COMPILER} STREQUAL "vc142")
            file (GLOB FFMPEG_DLL ${FFMPEG_DIR}/lib/*.dll)
            foreach (DLL ${FFMPEG_DLL})
                file (COPY ${DLL} DESTINATION ${PROJECT_LIBRARY_DIR})
            endforeach()
        endif()
    endif()
    
    ShowMessage (${FFMPEG_DIR} ffmpeg)
endfunction()

function (FindJRTPLIB TARGET_PROJECT)
    set (JRTP_DIR ${THIRD_PARTY_HOME}/jrtplib/${COMPILER}-${ARCH}/${MODE})
    set (JRTP_INCLUDE_DIR ${JRTP_DIR}/include)
    set (JRTP_LIB_DIR ${JRTP_DIR}/lib)
    
    if(EXISTS ${JRTP_DIR})
      include_directories (${JRTP_INCLUDE_DIR})
      
      if (${COMPILER} STREQUAL "vc142")
          set (LINK_LIB ${JRTP_LIB_DIR}/jrtplib_d.lib
                        WS2_32.lib)
      else()
          set (LINK_LIB ${JRTP_LIB_DIR}/libjrtp.a)
      endif()
      
      target_link_libraries (${TARGET_PROJECT} ${LINK_LIB})
    endif()
    
    ShowMessage (${JRTP_DIR} jrtplib)
endfunction()

function (FindSIGNALR TARGET_PROJECT)
    set (SIGNALR_DIR ${THIRD_PARTY_HOME}/signalr-cpp/${COMPILER}-${ARCH}/${MODE})
    set (SIGNALR_INCLUDE_DIR ${SIGNALR_DIR}/include)
    set (SIGNALR_LIB_DIR ${SIGNALR_DIR}/lib)
    
    if(EXISTS ${SIGNALR_DIR})
      include_directories (${SIGNALR_INCLUDE_DIR})
      
      if (${COMPILER} STREQUAL "vc142")
          target_link_libraries (${TARGET_PROJECT} ${SIGNALR_LIB_DIR}/signalr.lib)
          file (COPY ${SIGNALR_LIB_DIR}/signalr.dll DESTINATION ${PROJECT_LIBRARY_DIR})
      else()
          target_link_libraries (${TARGET_PROJECT} ${SIGNALR_LIB_DIR}/signalr.a)
      endif()
    endif()
    
    ShowMessage (${SIGNALR_DIR} signalr)
endfunction()

function (FindCPPREST TARGET_PROJECT)
    set (CPPREST_DIR ${THIRD_PARTY_HOME}/cpprest/${COMPILER}-${ARCH}/${MODE})
    set (CPPREST_INCLUDE_DIR ${CPPREST_DIR}/include)
    set (CPPREST_LIB_DIR ${CPPREST_DIR}/lib)
    
    if(EXISTS ${CPPREST_DIR})
      include_directories (${CPPREST_INCLUDE_DIR})
      
      if (${COMPILER} STREQUAL "vc142")
          target_link_libraries (${TARGET_PROJECT} ${CPPREST_LIB_DIR}/cpprest142_2_10d.lib)
          file (COPY ${CPPREST_LIB_DIR}/cpprest142_2_10d.dll DESTINATION ${PROJECT_LIBRARY_DIR})
      else()
          target_link_libraries (${TARGET_PROJECT} ${CPPREST_LIB_DIR}/cpprest.a)
      endif()
    endif()
    
    ShowMessage (${CPPREST_DIR} cpprest)
endfunction()

function (FindZLIB TARGET_PROJECT)
    set (ZLIB_DIR ${THIRD_PARTY_HOME}/zlib/${COMPILER}-${ARCH}/${MODE})
    set (ZLIB_INCLUDE_DIR ${ZLIB_DIR}/include)
    set (ZLIB_LIB_DIR ${ZLIB_DIR}/lib)
    
    if(EXISTS ${ZLIB_DIR})
      include_directories (${ZLIB_INCLUDE_DIR})
      
      if (${COMPILER} STREQUAL "vc142")
          target_link_libraries (${TARGET_PROJECT} ${ZLIB_LIB_DIR}/zlib.lib)
      else()
          target_link_libraries (${TARGET_PROJECT} ${ZLIB_LIB_DIR}/zlib.a)
      endif()
    endif()
    
    ShowMessage (${ZLIB_DIR} zlib)
endfunction()

function (FindOPENSSL TARGET_PROJECT)
    set (OPENSSL_DIR ${THIRD_PARTY_HOME}/openssl/${COMPILER}-${ARCH}/debug)
    set (OPENSSL_INCLUDE_DIR ${OPENSSL_DIR}/include)
    set (OPENSSL_LIB_DIR ${OPENSSL_DIR}/lib)
    
    if(EXISTS ${OPENSSL_DIR})
      include_directories (${OPENSSL_INCLUDE_DIR})
      
      if (${COMPILER} STREQUAL "vc142")
          target_link_libraries (${TARGET_PROJECT} ${OPENSSL_LIB_DIR}/libssl.lib
                                                   ${OPENSSL_LIB_DIR}/libcrypto.lib)
          set  (LIB_EXT .dll)
      else()
          target_link_libraries (${TARGET_PROJECT} ${OPENSSL_LIB_DIR}/libssl_1_1.so
                                                   ${OPENSSL_LIB_DIR}/libcrypto_1_1.so)
           set  (LIB_EXT .so)
      endif()
            
          file (GLOB LIBS ${OPENSSL_LIB_DIR}/*${LIB_EXT})
          foreach (LIB ${LIBS})
              file (COPY ${LIB} DESTINATION ${PROJECT_LIBRARY_DIR})
          endforeach()
    endif()
    
    ShowMessage (${OPENSSL_DIR} openssl)
endfunction()

function (FindBOOST TARGET_LIBRARY TARGET_PROJECT)
    
    set (BOOST_DIR ${THIRD_PARTY_HOME}/boost/${COMPILER}-${ARCH}/${MODE})
    set (BOOST_INCLUDE_DIR ${BOOST_DIR}/include)
    set (BOOST_LIB_DIR ${BOOST_DIR}/lib)
    
    if(EXISTS ${BOOST_DIR})
      include_directories (${BOOST_INCLUDE_DIR})
      
      
      if (${COMPILER} STREQUAL "vc142")
          set (LINK_LIB ${BOOST_LIB_DIR}/lib${TARGET_LIBRARY}-vc142-mt-gd-x64-1_71.lib)
      else()
          set (LINK_LIB ${BOOST_LIB_DIR}/lib${TARGET_LIBRARY}.a)
      endif()
      if (EXISTS ${LINK_LIB})
          target_link_libraries (${TARGET_PROJECT} ${LINK_LIB})
      endif()
    endif()
    
    ShowMessage (${BOOST_DIR} boost)
endfunction()

function (FindGOOGLETEST TARGET_PROJECT)
    set (TEST_DIR ${THIRD_PARTY_HOME}/googletest/${COMPILER}-${ARCH}/${MODE})
    set (TEST_INCLUDE ${TEST_DIR}/include)
    
    if (EXISTS ${TEST_DIR})
        
        # windows only
        set (TEST_LIB gmockd.lib
                      gtestd.lib)
        
        include_directories (${TEST_INCLUDE})
        foreach (LIB ${TEST_LIB})
            target_link_libraries (${TARGET_PROJECT} ${TEST_DIR}/lib/${LIB})
        endforeach()
    endif()
    
    ShowMessage (${TEST_DIR} googletest)
endfunction()

function (FindWEBRTC TARGET_PROJECT)
    set (WEBRTC_DIR ${THIRD_PARTY_HOME}/webrtc/${COMPILER}-${ARCH}/${MODE})
    set (WEBRTC_INCLUDE ${WEBRTC_DIR}/include)
    
    if (EXISTS ${WEBRTC_DIR})
        
        # windows only
        include_directories (${WEBRTC_INCLUDE})
        target_link_libraries (${TARGET_PROJECT} ${WEBRTC_DIR}/lib/rtc_json.lib
                                                 ${WEBRTC_DIR}/lib/jsoncpp.lib
                                                 ${WEBRTC_DIR}/lib/webrtc.lib
                                                 WindowsApp_downlevel.lib
                                                 WinMM.lib
                                                 msdmo.lib
                                                 security.lib
                                                 wmcodecdspuuid.lib)
        file (COPY ${WEBRTC_DIR}/lib/jsoncpp.dll DESTINATION ${PROJECT_LIBRARY_DIR})
    endif()
    
    ShowMessage (${WEBRTC_DIR} webrtc)
endfunction()

function (FindTBB TARGET_PROJECT)
    set (TBB_DIR ${THIRD_PARTY_HOME}/tbb/${COMPILER}-${ARCH}/${MODE})
    set (TBB_INCLUDE ${TBB_DIR}/include)
    
    if (EXISTS ${TBB_DIR})
        
        # windows only
        include_directories (${TBB_INCLUDE})
        target_link_libraries (${TARGET_PROJECT} ${TBB_DIR}/lib/tbb_debug.lib)
        file (COPY ${TBB_DIR}/lib/tbb_debug.dll DESTINATION ${PROJECT_LIBRARY_DIR})
    endif()
    
    ShowMessage (${TBB_DIR} tbb)
endfunction()

function (FindSDL2 TARGET_PROJECT)
    set (SDL_DIR ${THIRD_PARTY_HOME}/sdl2/${COMPILER}-${ARCH}/${MODE})
    set (SDL_INCLUDE ${SDL_DIR}/include)
    
    if (EXISTS ${SDL_DIR})
        
        # windows only
        include_directories (${SDL_INCLUDE})
        target_link_libraries (${TARGET_PROJECT} ${SDL_DIR}/lib/SDL2.lib)
        file (COPY ${SDL_DIR}/lib/SDL2.dll DESTINATION ${PROJECT_LIBRARY_DIR})
    endif()
    
    ShowMessage (${SDL_DIR} sdl2)
endfunction()

function (FindRAPIDJSON TARGET_PROJECT)
    
    set (JSON_INCLUDE ${THIRD_PARTY_HOME}/rapidjson/1.1.0/include)
    if (EXISTS ${JSON_INCLUDE})
        include_directories (${JSON_INCLUDE})
    endif()
    
    ShowMessage (${JSON_INCLUDE} rapidjson)
endfunction()

function (FindQT TARGET_LIBRARY TARGET_PROJECT)
    set (QT_INCLUDE_DIR ${QT_DIR}/include)
    set (QT_LIB_DIR ${QT_DIR}/lib)
    
    if(EXISTS ${QT_DIR})
      include_directories (${QT_INCLUDE_DIR})
      
      if (${COMPILER} STREQUAL "vc142")
      
          set_target_properties (${TARGET_PROJECT} PROPERTIES VS_DEBUGGER_ENVIRONMENT "PATH=%PATH%;${QT_DIR}/bin")
          if (${MODE} STREQUAL "debug")
              set (LIBNAME ${QT_LIB_DIR}/${TARGET_LIBRARY}d.lib)
          else()
              set (LIBNAME ${QT_LIB_DIR}/${TARGET_LIBRARY}.lib)
          endif()
          if (EXISTS ${LIBNAME})
              target_link_libraries (${TARGET_PROJECT} ${LIBNAME})
          endif()
          if (NOT ${TARGET_LIBRARY} STREQUAL "Qt5")
              ShowMessage (${LIBNAME} ${TARGET_LIBRARY})
          endif()
      elseif (${COMPILER} STREQUAL "and")
          if (${ARCH} STREQUAL "x86")
              set (LIBNAME ${QT_LIB_DIR}/lib${TARGET_LIBRARY}_x86.so)
          elseif(${ARCH} STREQUAL "x86_64")
              set (LIBNAME ${QT_LIB_DIR}/lib${TARGET_LIBRARY}_x86_64.so)
          elseif(${ARCH} STREQUAL "arm")
              set (LIBNAME ${QT_LIB_DIR}/lib${TARGET_LIBRARY}_armeabi-v7a.so)
          elseif(${ARCH} STREQUAL "arm64")
              set (LIBNAME ${QT_LIB_DIR}/lib${TARGET_LIBRARY}_arm64-v8a.so)
          endif()
          
          if (EXISTS ${LIBNAME})
              target_link_libraries (${TARGET_PROJECT} ${LIBNAME})
          endif()
          if (NOT ${TARGET_LIBRARY} STREQUAL "Qt5")
              ShowMessage (${LIBNAME} ${TARGET_LIBRARY})
          endif()
      endif()
    endif()
endfunction()

function (FindLoRa TARGET_LIBRARY TARGET_PROJECT)
    set (LORA_INCLUDE ${THIRD_PARTY_HOME}/lora/0.7.2/src)
    if (EXISTS ${LORA_INCLUDE})
        include_directories (${LORA_INCLUDE})
    endif()
    
    ShowMessage (${LORA_INCLUDE} lora)
endfunction()

function (FindArduino TARGET_LIBRARY TARGET_PROJECT)

    set (ARDUINO_HARDWARE D:/ProgramFiles/Arduino/hardware)
    if (EXISTS ${ARDUINO_HARDWARE})
        include_directories (${ARDUINO_HARDWARE}/arduino/avr/cores/arduino)
        include_directories (${ARDUINO_HARDWARE}/arduino/avr/variants/standard)
        include_directories (${ARDUINO_HARDWARE}/tools/avr/lib/gcc/avr/7.3.0/include)
        include_directories (${ARDUINO_HARDWARE}/tools/avr/avr/include)
        include_directories (${ARDUINO_HARDWARE}/tools/avr/avr/include-fixed)
        include_directories (${ARDUINO_HARDWARE}/tools/avr/avr/include/avr)
        include_directories (${ARDUINO_HARDWARE}/tools/avr/lib/gcc/avr/4.9.2/include)
        include_directories (${ARDUINO_HARDWARE}/tools/avr/lib/gcc/avr/4.9.3/include)
        include_directories (${ARDUINO_HARDWARE}/arduino/avr/libraries/SPI/src)
    endif()
    
    ShowMessage (${ARDUINO_HARDWARE} arduino)
endfunction()

function (FindNRF24L01 TARGET_LIBRARY TARGET_PROJECT)
    set (NRF_INCLUDE ${THIRD_PARTY_HOME}/nRF24L01/1.3.6)
    if (EXISTS ${NRF_INCLUDE})
        include_directories (${NRF_INCLUDE})
    endif()
    
    ShowMessage (${NRF_INCLUDE} nrf24L01)
endfunction()

function (FindOpenCV TARGET_LIBRARY TARGET_PROJECT)

    set (CV_DIR ${THIRD_PARTY_HOME}/opencv/${COMPILER}-${ARCH}/${MODE})
    set (CV_INCLUDE_DIR ${CV_DIR}/include)
    set (CV_LIB_DIR ${CV_DIR}/lib)
    
    if(EXISTS ${CV_DIR})
      include_directories (${CV_INCLUDE_DIR})
      
      if (${COMPILER} STREQUAL "vc142")
          if (${MODE} STREQUAL "debug")
              set (LINK_LIB ${CV_LIB_DIR}/${TARGET_LIBRARY}450d.lib)
          endif()
      elseif (${COMPILER} STREQUAL "and")
          set (LINK_LIB ${CV_LIB_DIR}/lib${TARGET_LIBRARY}.so)
      endif()
      
      if (EXISTS ${LINK_LIB})
          target_link_libraries (${TARGET_PROJECT} ${LINK_LIB})
      endif()
      
      if (${COMPILER} STREQUAL "vc142")
          file (GLOB CV_DLL ${CV_LIB_DIR}/${TARGET_LIBRARY}450d.dll)
          file (COPY ${CV_DLL} DESTINATION ${PROJECT_LIBRARY_DIR})
      endif()
    endif()
    
    ShowMessage (${CV_DIR} opencv)

endfunction()