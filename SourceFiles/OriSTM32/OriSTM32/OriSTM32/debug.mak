#Generated by VisualGDB (http://visualgdb.com)
#DO NOT EDIT THIS FILE MANUALLY UNLESS YOU ABSOLUTELY NEED TO
#USE VISUALGDB PROJECT PROPERTIES DIALOG INSTEAD

BINARYDIR := Debug

#Additional flags
PREPROCESSOR_MACROS := DEBUG=1 __ORI_STM32LIB
INCLUDE_DIRS := ../../../OriCore/include ../../../OriSTM32Lib/include ../../../OriSTM32Lib/include/FreeRTOS ../../../OriMobileLib/include ../../../../../cygwin64/home/Ori/ThirdParty/freertos/10.4.1/FreeRTOS/Source/include ../../../../../cygwin64/home/Ori/ThirdParty/freertos/10.4.1/FreeRTOS/Source/portable/GCC/ARM_CM3 C:\Users\Mikhail\AppData\Local\VisualGDB\EmbeddedBSPs\arm-eabi\com.sysprogs.arm.stm32\STM32F1xxxx\STM32F1xx_HAL_Driver\Inc ../../../OriSTM32Lib/include/HAL
LIBRARY_DIRS := 
LIBRARY_NAMES := 
ADDITIONAL_LINKER_INPUTS := 
MACOS_FRAMEWORKS := 
LINUX_PACKAGES := 

CFLAGS := -ggdb -ffunction-sections -O0
CXXFLAGS := -ggdb -ffunction-sections -fno-exceptions -O0
ASFLAGS := 
LDFLAGS := -Wl,-gc-sections
COMMONFLAGS := 
LINKER_SCRIPT := ../../../OriSTM32Lib/src/STM32System/STM32System_LinkerScript.lds

START_GROUP := -Wl,--start-group
END_GROUP := -Wl,--end-group

#Additional options detected from testing the toolchain
USE_DEL_TO_CLEAN := 1
CP_NOT_AVAILABLE := 1

ADDITIONAL_MAKE_FILES := stm32.mak
GENERATE_BIN_FILE := 1
GENERATE_IHEX_FILE := 0
