#include <Ori/WebRTCTestLib_Pch.hxx>
#include <Ori/WebRTC_Init.hxx>
#include <Ori/Base_Logger.hxx>

int main()
{
    //testing::GTEST_FLAG(filter) = "WebRTC.CreateOfferFromOtherThread";

    Ori::Base::Logger::SetDefaultLevel (Ori::Base::LogLevel::Fail);
    Ori::WebRTC::Init Init;
    testing::InitGoogleTest();
    RUN_ALL_TESTS();
}