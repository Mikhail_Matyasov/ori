#include <Ori/WebRTCTestLib_Pch.hxx>
#include <Ori/WebRTCTestLib_MultyObserver.hxx>

#include <Ori/Base_String.hxx>
#include <Ori/WebRTC_Connection.hxx>
#include <Ori/WebRTCTestLib_JsonReader.hxx>
#include <Ori/WebRTCTestLib_DataChannelObserver.hxx>

namespace Ori {
namespace WebRTCTestLib {

void MultyObserver::OnCreateOffer (const Base::String& theOffer)
{
    /*myMainThread = std::thread ([this, theOffer]() -> void {

        JsonReader aReader;
        Ori::SessionDescription anOffer;
        aReader.ParseSdp (theOffer.Data(), anOffer);

        myOtherConnection->SetRemoteDescription (anOffer);
        myOtherConnection->CreateAnswer();

    });
    myMainThread.join();*/
}

void MultyObserver::OnCreateAnswer (const Base::String& theAnswer)
{
    /*myAnswerThread = std::thread ([this, theAnswer]() -> void {
        JsonReader aReader;
        aReader.ParseSdp (theAnswer.Data(), myAnswer);
        myOtherConnection->SetRemoteDescription (myAnswer);
    });
    myAnswerThread.join();*/
}

void MultyObserver::OnIceCandidate (const Base::String& theCandidate)
{
    /*myCandidateThread = std::thread ([this, theCandidate]() -> void {
        JsonReader aReader;
        aReader.ParseCandidate (theCandidate.Data(), myCandidate);

        myOtherConnection->AddIceCandidate (myCandidate);
    });
    myCandidateThread.join();*/
}

}}
