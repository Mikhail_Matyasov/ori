#include <Ori/WebRTCTestLib_Pch.hxx>
#include <Ori/WebRTCTestLib_ConnectionObserver.hxx>

#include <Ori/WebRTC_Connection.hxx>
#include <Ori/WebRTCTestLib_JsonReader.hxx>
#include <thread>

namespace Ori {
namespace WebRTCTestLib {

void ConnectionObserver::OnCreateAnswer (const Base::String& theAnswer)
{
    JsonReader aReader;
    aReader.ParseSdp (theAnswer, myAnswer);

    if (myOtherPeer) {
        myOtherPeer->SetRemoteDescription (myAnswer, false);
    }
}

void ConnectionObserver::OnIceCandidate (const Base::String& theCandidate)
{
    JsonReader aReader;
    aReader.ParseCandidate (theCandidate, myCandidate);

    if (myOtherPeer) {
        myOtherPeer->AddIceCandidate (myCandidate, false);
    }
}

void ConnectionObserver::OnCreateOffer (const Base::String& theOffer)
{
    JsonReader aReader;
    aReader.ParseSdp (theOffer, myOffer);

    if (myOtherPeer) {

        myOtherPeer->SetRemoteDescription (myOffer, false);
        myOtherPeer->CreateAnswer (false);

        std::thread aT1 ([&]() -> void {
            while (!myOtherObserver->Openned()) {}
            myOtherPeer->StopListening();
        });
        aT1.detach();

        myOtherPeer->Listen();
    }
}

}}
