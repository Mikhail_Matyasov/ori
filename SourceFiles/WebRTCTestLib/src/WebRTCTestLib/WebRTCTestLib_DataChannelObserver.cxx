#include <Ori/WebRTCTestLib_Pch.hxx>
#include <Ori/WebRTCTestLib_DataChannelObserver.hxx>

namespace Ori {
namespace WebRTCTestLib {

void DataChannelObserver::OnMessage (const Base::ByteArray& theMessage)
{
    myMessage = theMessage;
}

void DataChannelObserver::OnOpenned()
{
    myOpenned = true;
}

}}
