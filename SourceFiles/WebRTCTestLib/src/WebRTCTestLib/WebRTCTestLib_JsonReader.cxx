#include <Ori/WebRTCTestLib_Pch.hxx>
#include <Ori/WebRTCTestLib_JsonReader.hxx>

using namespace Ori::WebRTC;

namespace Ori {
namespace WebRTCTestLib {

bool JsonReader::Int (int i)
{
    myObjects.emplace (myCurrentKey, Base::String (i));
    return true;
}

bool JsonReader::Uint (unsigned u)
{
    myObjects.emplace (myCurrentKey, Base::String (u));
    return true;
}

bool JsonReader::String (const char* str, SizeType length, bool copy)
{
    myObjects.emplace (myCurrentKey, Base::String (str));
    return true;
}

bool JsonReader::Key (const char* str, SizeType length, bool copy)
{
    myCurrentKey = str;
    return true;
}

void JsonReader::ParseSdp (const Base::String& theSource, SessionDescription& theTarget)
{
    Reader aReader;
    auto aData = theSource.Data();
    StringStream aJsonString (aData);
    auto aRes = aReader.Parse (aJsonString, *this);

    theTarget.Sdp() = myObjects.find("sdp")->second;
    auto aType = myObjects.find ("type")->second;
    if (aType == "offer") {
        theTarget.Type() = SessionDescription::DescriptionType::OFFER;
    } else if (aType == "answer") {
        theTarget.Type() = SessionDescription::DescriptionType::ANSWER;
    }
}

void JsonReader::ParseCandidate (const Base::String& theSource, IceCandidate& theTarget)
{
    Reader aReader;
    StringStream aJsonString (theSource.Data());
    aReader.Parse (aJsonString, *this);

    theTarget.Candidate() = myObjects.find ("candidate")->second;
    theTarget.SdpMid() = myObjects.find ("sdpMid")->second;

    auto aNumber = myObjects.find ("sdpMLineIndex")->second.Data();
    theTarget.SdpMlineIndex() = std::stoi (aNumber);
}

}}
