#include <Ori/WebRTCTestLib_Pch.hxx>

#include <Ori/Base_String.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/WebRTC_Connection.hxx>
#include <Ori/WebRTC_ConnectionObserver.hxx>
#include <Ori/WebRTCTestLib_ConnectionObserver.hxx>
#include <Ori/WebRTCTestLib_DataChannelObserver.hxx>
#include <Ori/WebRTCTestLib_MultyObserver.hxx>

#include <rtc_base/logging.h>
#include <thread>

namespace Ori {
namespace WebRTCTestLib {
namespace {

Base::ByteArray Message (const std::string& theMessage)
{
    auto aData = reinterpret_cast <const uint8_t*> (theMessage.c_str());
    Base::ByteArray anArray (aData,
                             static_cast <int> (theMessage.size()));
    return anArray;
}


void EstablishConnection ()
{
    auto aPeer1Observer = std::make_shared <ConnectionObserver>();
    auto aPeer2Observer = std::make_shared <ConnectionObserver>();
    auto aPeer1DataChannelObserver = std::make_shared <DataChannelObserver>();
    auto aPeer2DataChannelObserver = std::make_shared <DataChannelObserver>();

    Connection aPeer1 (*aPeer1Observer, *aPeer1DataChannelObserver);
    Connection aPeer2 (*aPeer2Observer, *aPeer2DataChannelObserver);

    aPeer1Observer->SetOtherPeer (&aPeer2);
    aPeer2Observer->SetOtherPeer (&aPeer1);
    aPeer1Observer->SetOtherObserver (aPeer1DataChannelObserver.get());
    aPeer2Observer->SetOtherObserver (aPeer2DataChannelObserver.get());

    aPeer1.CreateOffer (false);

    std::thread aT1 ([&]() -> void {
        while (!aPeer1DataChannelObserver->Openned()) {}

        EXPECT_TRUE (!aPeer1Observer->Offer().Sdp().Empty());
        EXPECT_TRUE (aPeer1Observer->Offer().Type() == SessionDescription::DescriptionType::OFFER);
        EXPECT_TRUE (!aPeer1Observer->Candidate().Candidate().Empty());

        EXPECT_TRUE (!aPeer2Observer->Answer().Sdp().Empty());
        EXPECT_TRUE (aPeer2Observer->Answer().Type() == SessionDescription::DescriptionType::ANSWER);
        EXPECT_TRUE (!aPeer2Observer->Candidate().Candidate().Empty());

        auto aMessage = Message ("Hello");
        auto aF = aMessage[0];
        aPeer1.Send (aMessage);
        while (aPeer2DataChannelObserver->Message().Empty()) {}

        auto aRes = aPeer2DataChannelObserver->Message();
        auto aResC = aRes[0];

        EXPECT_TRUE (aMessage == aPeer2DataChannelObserver->Message());
        
        aPeer1.StopListening();
    });
    aT1.detach();

    aPeer1.Listen();
}

}

TEST (WebRTC, CreateConnection)
{
    auto aPeer1Observer = std::make_shared <ConnectionObserver>();
    auto aPeer1DataChannelObserver = std::make_shared <DataChannelObserver>();

    Connection aConnection (*aPeer1Observer, *aPeer1DataChannelObserver);
    EXPECT_TRUE (aConnection.Ready());
}

TEST (WebRTC, SendData)
{
    auto aPeer1Observer = std::make_shared <ConnectionObserver>();
    auto aPeer1DataChannelObserver = std::make_shared <DataChannelObserver>();

    Connection aConnection (*aPeer1Observer, *aPeer1DataChannelObserver);
    EXPECT_TRUE (aConnection.Ready());

    auto aMessage = Message ("Hello");
    EXPECT_NO_FATAL_FAILURE (aConnection.Send (aMessage));
}

TEST(WebRTC, EstablishConnectionTest)
{
    EstablishConnection();
}

TEST(WebRTC, CreateOfferFromOtherThread)
{
    std::thread t1 ([&]() -> void {

        auto aPeer1Observer = std::make_shared <ConnectionObserver>();
        auto aPeer1ChannelObserver = std::make_shared <DataChannelObserver>();
        Connection aPeer1 (*aPeer1Observer, *aPeer1ChannelObserver);

        std::thread aT1 ([&]() -> void {
            while (aPeer1Observer->Candidate().Candidate().Empty()) {
                std::this_thread::sleep_for (std::chrono::microseconds (1000));
            }
            aPeer1.StopListening();
        });
        aT1.detach();

        aPeer1.CreateOffer();
        aPeer1.Listen();

        EXPECT_TRUE (!aPeer1Observer->Offer().Sdp().Empty());
        EXPECT_TRUE (aPeer1Observer->Offer().Type() == SessionDescription::DescriptionType::OFFER);
        EXPECT_TRUE (!aPeer1Observer->Candidate().Candidate().Empty());

    });

    t1.join();
}

TEST (WebRTC, EstablishMultyConnection)
{

    std::thread t1 ([&]() -> void {
        EstablishConnection();
    });

    std::thread t2 ([&]() -> void {
        EstablishConnection();
    });

    t1.join();
    t2.join();
}

TEST(WebRTC, CreateTenConnection)
{
    for (int i = 0; i < 10; i++) {
        EstablishConnection();
    }
}

TEST(WebRTC, CreateTenConnectionFromDifferentThreads)
{
    for (int i = 0; i < 10; i++) {
        auto aThread = std::make_shared <std::thread> ([&]() -> void {
            EstablishConnection();
        });
        aThread->join();
    }
}

TEST(WebRTC, Send100Messages)
{
    auto aPeer1Observer = std::make_shared <ConnectionObserver>();
    auto aPeer2Observer = std::make_shared <ConnectionObserver>();
    auto aPeer1DataChannelObserver = std::make_shared <DataChannelObserver>();
    auto aPeer2DataChannelObserver = std::make_shared <DataChannelObserver>();

    Connection aPeer1 (*aPeer1Observer, *aPeer1DataChannelObserver);
    Connection aPeer2 (*aPeer2Observer, *aPeer2DataChannelObserver);

    aPeer1Observer->SetOtherPeer (&aPeer2);
    aPeer2Observer->SetOtherPeer (&aPeer1);
    aPeer1Observer->SetOtherObserver (aPeer2DataChannelObserver.get());
    aPeer2Observer->SetOtherObserver (aPeer1DataChannelObserver.get());

    std::thread aT1 ([&]() -> void {
        while (!aPeer1DataChannelObserver->Openned()) {}
        aPeer1.StopListening();
    });
    aT1.detach();

    aPeer1.CreateOffer();
    aPeer1.Listen();

    EXPECT_TRUE (!aPeer1Observer->Offer().Sdp().Empty());
    EXPECT_TRUE (aPeer1Observer->Offer().Type() == SessionDescription::DescriptionType::OFFER);
    EXPECT_TRUE (!aPeer1Observer->Candidate().Candidate().Empty());

    EXPECT_TRUE (!aPeer2Observer->Answer().Sdp().Empty());
    EXPECT_TRUE (aPeer2Observer->Answer().Type() == SessionDescription::DescriptionType::ANSWER);
    EXPECT_TRUE (!aPeer2Observer->Candidate().Candidate().Empty());

    bool anIsT2Break = false;
    std::thread aT2 ([&]() -> void {
        while (!anIsT2Break)
        {
            while (aPeer2DataChannelObserver->Message().Empty()) {}
            aPeer1.StopListening();
        }
    });
    aT2.detach();

    auto aMessage = Message ("Hello");
    int aNumberOfReceivedMessages = 0;
    while (aNumberOfReceivedMessages < 100) {
        aPeer1.Send (aMessage);
        aPeer1.Listen();
        aNumberOfReceivedMessages++;
    }
    anIsT2Break = true;
}

}}
