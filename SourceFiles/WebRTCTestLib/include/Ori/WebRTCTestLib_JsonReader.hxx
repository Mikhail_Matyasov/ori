#ifndef _Ori_WebRTCTestLib_JsonReader_HeaderFile
#define _Ori_WebRTCTestLib_JsonReader_HeaderFile

#include <Ori/WebRTC_SessionDescription.hxx>
#include <Ori/WebRTC_IceCandidate.hxx>

#include <rapidjson/reader.h>
#include <unordered_map>

using namespace rapidjson;
using namespace Ori::WebRTC;

namespace Ori {
namespace WebRTCTestLib {

class JsonReader
{
public:
    bool Null() { return true; }
    bool Bool (bool b) { return true; }
    bool Int (int i);
    bool Uint (unsigned u);
    bool Int64 (int64_t i) { return true; }
    bool Uint64 (uint64_t u) { return true; }
    bool Double (double d) { return true; }
    bool RawNumber (const char* str, SizeType length, bool copy) { return true; }
    bool String (const char* str, SizeType length, bool copy);
    bool StartObject() { return true; };
    bool Key (const char* str, SizeType length, bool copy);
    bool EndObject (SizeType memberCount) { return true; };
    bool StartArray() { return true; }
    bool EndArray (SizeType elementCount) { return true; }

    void ParseSdp (const Base::String& theSource, SessionDescription& theTarget);
    void ParseCandidate (const Base::String& theSource, IceCandidate& theTarget);

private:
    std::unordered_map <Base::String, Base::String> myObjects;
    Base::String myCurrentKey;
};

}}

#endif