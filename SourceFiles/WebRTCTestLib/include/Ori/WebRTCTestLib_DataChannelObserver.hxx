#ifndef _Ori_WebRTCTestLib_DataChannelObserver_HeaderFile
#define _Ori_WebRTCTestLib_DataChannelObserver_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/WebRTC_DataChannelObserver.hxx>

namespace Ori {
namespace WebRTCTestLib {

class DataChannelObserver : public Ori::WebRTC::DataChannelObserver
{
public:
    DataChannelObserver() : myOpenned (false) {}
    void OnMessage (const Ori::Base::ByteArray& theMessage) override;
    void OnOpenned() override;

    __ORI_PROPERTY (Ori::Base::ByteArray, Message);
    __ORI_PRIMITIVE_PROPERTY (bool, Openned);

};

}}

#endif
