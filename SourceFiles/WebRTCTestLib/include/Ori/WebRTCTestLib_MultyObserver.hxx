#ifndef _Ori_WebRTCTestLib_MultyObserver_HeaderFile
#define _Ori_WebRTCTestLib_MultyObserver_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/WebRTC_ConnectionObserver.hxx>
#include <Ori/WebRTC_SessionDescription.hxx>
#include <Ori/WebRTC_IceCandidate.hxx>

#include <thread>

using namespace Ori::WebRTC;

namespace Ori {
namespace Base {
class String;
}

namespace WebRTC {
class Connection;
}

namespace WebRTCTestLib {
class MultyObserver : public ConnectionObserver
{
public:
    MultyObserver()
    {}

    void OnCreateOffer (const Base::String& theOffer) override;
    void OnCreateAnswer (const Base::String& theAnswer) override;
    void OnIceCandidate (const Base::String& theCandidate) override;

    __ORI_PROPERTY (SessionDescription, Offer);
    __ORI_PROPERTY (SessionDescription, Answer);
    __ORI_PROPERTY (IceCandidate, Candidate);

    void SetOtherPeer (std::shared_ptr <WebRTC::Connection> thePeer)
    { myOtherConnection = thePeer; }

private:
    std::shared_ptr <WebRTC::Connection> myOtherConnection;
    std::thread myAnswerThread;
    std::thread myCandidateThread;
    std::thread myMainThread;
};

}}

#endif
