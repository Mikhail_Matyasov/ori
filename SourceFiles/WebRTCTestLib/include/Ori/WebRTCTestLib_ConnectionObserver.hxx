#ifndef _Ori_WebRTCTestLib_ConnectionObserver_HeaderFile
#define _Ori_WebRTCTestLib_ConnectionObserver_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/WebRTC_ConnectionObserver.hxx>
#include <Ori/WebRTC_SessionDescription.hxx>
#include <Ori/WebRTC_IceCandidate.hxx>
#include <Ori/WebRTCTestLib_DataChannelObserver.hxx>

using namespace Ori::WebRTC;

namespace Ori {
namespace WebRTC {
class Connection;
}
namespace WebRTCTestLib {

class ConnectionObserver : public Ori::WebRTC::ConnectionObserver
{
public:
    ConnectionObserver() :
        myOtherPeer (nullptr),
        myOtherObserver (nullptr)
    {}
    
    void OnCreateOffer (const Base::String& theOffer) override;
    void OnCreateAnswer (const Base::String& theAnswer) override;
    void OnIceCandidate (const Base::String& theCandidate) override;

    void SetOtherPeer (Connection* thePeer)
    { myOtherPeer = thePeer; }
    void SetOtherObserver (WebRTCTestLib::DataChannelObserver* theObserver)
    { myOtherObserver = theObserver; }

    __ORI_PROPERTY (SessionDescription, Offer);
    __ORI_PROPERTY (SessionDescription, Answer);
    __ORI_PROPERTY (IceCandidate, Candidate);

private:
    Connection* myOtherPeer;
    DataChannelObserver* myOtherObserver;
};

}}

#endif
