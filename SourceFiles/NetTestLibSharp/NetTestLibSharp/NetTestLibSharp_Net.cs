using Xunit;
using Ori.Net;
using Ori.Base;
using System.Collections.Generic;

namespace Ori.OriNetSharpTestLib
{

public class Observer : RTPListener.Observer
{
    public Observer()
    {
        Packets = new List <ByteArray>();
    }
    public override void OnPacket (ByteArray thePacket)
    {
        Packets.Add (thePacket.Clone());
    }

    public List <ByteArray> Packets { get; set; }
}
public class OriNetSharpTestLib_Net
{
    static OriNetSharpTestLib_Net()
    {
       bool aRes = DllHelper.Load ("OriCore.dll");
       aRes &= DllHelper.Load ("RTP.dll");
       Assert.True (aRes);
    }

    private void SendData (byte[] theData, int theNumberOfPackets)
    {
        int aUdpPort = 8000;
        int aTcpPort = 8002;
        Observer anObserver = new Observer();
        RTPListener aListener = new RTPListener (aUdpPort, aTcpPort, anObserver);

        Assert.True(aListener.Status() == RTPSessionStatus.Success);
        aListener.Start();

        RTPEndPoint aPoint = new RTPEndPoint (new String ("127.0.0.1"), aTcpPort, aUdpPort);

        RTPClient aClient = new RTPClient (aUdpPort + 4, aPoint);
        Assert.True(aClient.Status() == RTPSessionStatus.Success);

        for (int i = 0; i < theNumberOfPackets; i++)
        {
            Assert.True(aClient.Send(theData, theData.Length));
        }

        while (anObserver.Packets.Count != theNumberOfPackets) { }
        aListener.Stop();
    }

    [Fact]
    public void Send10Packets()
    {
        byte[] aData = new byte[] { 1, 2, 3, 4 };
        SendData (aData, 10);
    }
}

}
