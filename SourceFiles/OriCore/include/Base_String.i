%feature("nspace") Ori::Base::String;

%{
#include <Ori/Base_String.hxx>
%}

%rename(IsEqual) Ori::Base::String::operator== (const String& theOther) const;
%rename(IsEqual) Ori::Base::String::operator== (const char* theOther) const;
%ignore Ori::Base::String::UTF16;
%ignore Ori::Base::String::String (const wchar_t* theString);

typedef unsigned long long uint64_t;
typedef long long int64_t;
%include <Ori/Base_String.hxx>