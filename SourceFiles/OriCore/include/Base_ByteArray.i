%feature("nspace") Ori::Base::Vector;

%{
#include <Ori/Base_Vector.hxx>
%}

namespace Ori {
namespace Base {

%extend Vector {
public:	 
	 void GetData (unsigned char* theData)
	 {
	     uint8_t* aData = $self->Data();
	     int aLenght = $self->Lenght();
		 std::copy (aData, aData + aLenght, theData);
	 }
}

}}

%apply unsigned char OUTPUT[] { unsigned char* theData }
%csmethodmodifiers Ori::Base::Vector::GetData "private unsafe";

typedef unsigned char uint8_t;
%csmethodmodifiers "public unsafe";
%apply unsigned char FIXED[] { unsigned char* theData }

%rename(IsEqual) Ori::Base::Vector <unsigned char>::operator== (const Vector <unsigned char>& theOther) const;
%rename(At) Ori::Base::Vector <unsigned char>::operator[] (int theIndex) const;

%include <Ori/Base_Vector.hxx>
%include <Ori/Base_ByteArray.hxx>

%ignore Vector (const unsigned char* theData, int theLenght);
%ignore Vector (const unsigned char* theFirst, const unsigned char* theLast);
%ignore Vector (const std::initializer_list <unsigned char>& theList);
%ignore Insert (const unsigned char* theStart, const unsigned char* theEnd, int theStartPos);
%ignore Insert (const unsigned char* theStart, const unsigned char* theEnd);
%ignore Push (const unsigned char* theFirst, const unsigned char* theLast);
%ignore begin() const;
%ignore end() const;
%ignore Data() const;
%ignore Data();
%ignore Add;

%typemap(csclassmodifiers) Ori::Base::Vector "public partial class"

%template(ByteArray) Ori::Base::Vector <unsigned char>;
%csmethodmodifiers "public";