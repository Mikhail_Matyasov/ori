%module(directors="1") OriCore

%include <Base_Headers.i>
%include <Base_String.i>
%include <Base_ByteArrayView.i>
%include <Base_ByteArray.i>
%include <Base_LogLevel.i>
%include <Base_ILogger.i>
%include <Base_LoggerBuffer.i>
%include <Base_Logger.i>
%include <Base_PixelFormat.i>
%include <Base_Frame.i>
