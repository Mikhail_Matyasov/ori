%feature("nspace") Ori::Base::VectorView;

%{
#include <Ori/Base_VectorView.hxx>
%}

%include <Ori/Base_VectorView.hxx>
%include <Ori/Base_ByteArray.hxx>

%ignore VectorView (const unsigned char* theData, int theLenght);
%ignore VectorView (const unsigned char* theFirst, const unsigned char* theLast);
%ignore begin() const;
%ignore end() const;
%ignore Data() const;
%ignore Data();

%template(ByteArrayView) Ori::Base::VectorView <unsigned char>;