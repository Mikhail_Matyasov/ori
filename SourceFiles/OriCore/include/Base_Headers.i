%include <arrays_csharp.i>
%include <shared_ptr.i>
%include <std_string.i>
%include <std_vector.i>
%include <typemaps.i>
%include <windows.i>
%include <csharp.swg>

SWIG_CSBODY_PROXY(public, public, SWIGTYPE)
SWIG_CSBODY_TYPEWRAPPER(public, public, public, SWIGTYPE)

%include <Ori/Base_Macros.hxx>