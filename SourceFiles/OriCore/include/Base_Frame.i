%feature("nspace") Ori::Base::Frame;

%{
#include <Ori/Base_Frame.hxx>
%}

%feature("nspace") Ori::Base::Vector <Ori::Base::ByteArray>;

%ignore Vector (const std::initializer_list <Ori::Base::Vector <unsigned char>>& theList);
%ignore Vector (const Ori::Base::Vector <unsigned char>* theData, int theLenght);
%ignore Vector (const Ori::Base::Vector <unsigned char>* theFirst, const Ori::Base::Vector <unsigned char>* theLast);
%ignore Insert (const Ori::Base::Vector <unsigned char>* theData, int theLenght, int theStartPos);
%ignore Insert (const Ori::Base::Vector <unsigned char>* theData, int theLenght);
%ignore Insert (const Ori::Base::Vector <unsigned char>* theStart, const Ori::Base::Vector <unsigned char>* theEnd, int theStartPos);
%ignore Insert (const Ori::Base::Vector <unsigned char>* theStart, const Ori::Base::Vector <unsigned char>* theEnd);
%ignore GetData (unsigned char* theData);

%template(ByteArrayVector) Ori::Base::Vector <Ori::Base::ByteArray>;

%apply uint8_t FIXED[] { uint8_t* theValue }
%csmethodmodifiers Set "public unsafe";
%include <Ori/Base_Frame.hxx>