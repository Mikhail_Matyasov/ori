#ifndef _Ori_Embed_Macros_HeaderFile
#define _Ori_Embed_Macros_HeaderFile

#define __ORI_EMBED_SX1278_TX_BUFFER_LENGTH 128
#define __ORI_EMBED_SX1278_RX_BUFFER_LENGTH __ORI_EMBED_SX1278_TX_BUFFER_LENGTH
#define __ORI_EMBED_SX1278_PACKET_HEADER_LENGTH 4 // ( 1 (sender id) + 1 (command id) + 1 (packet id) + 1 (data length))
#define __ORI_EMBED_SX1278_MAX_DATA_LENGTH (__ORI_EMBED_SX1278_TX_BUFFER_LENGTH - __ORI_EMBED_SX1278_PACKET_HEADER_LENGTH)


#endif
