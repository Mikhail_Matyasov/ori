#ifndef _Ori_Base_Helper_HeaderFile
#define _Ori_Base_Helper_HeaderFile

#include <stddef.h>
#include <sstream>
#include <string.h>

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_Allocator.hxx>

namespace Ori {
namespace Base {

class Helper
{
public:
    template <typename T>
    static int Find (const T* theBegin, const T* theEnd, const T* theSubBegin, const T* theSubEnd)
    {
		if (theEnd - theBegin < theSubEnd - theSubBegin) {
			return -1;
		}
	    
	    int aSubTailLength = static_cast <int> ((theSubEnd - theSubBegin) - 1);
	    
        for (auto i = theBegin; i != theEnd; i++) {
		    if (*i == *theSubBegin) {
				
			    int aMainTailLength = static_cast <int> ((theEnd - i) - 1);
				if (aMainTailLength < aSubTailLength) {
					return -1;
				}
			    
                int aCounter = 0;
			    int aStart = static_cast <int> (i - theBegin + 1);
                for (int j = aStart, k = 1; k < aSubTailLength + 1; k++, j++) {
					if (theBegin [j] != theSubBegin [k]) {
						break;
					}
	                aCounter++;
                }
			    
			    
                if (aCounter == aSubTailLength) {
                    return static_cast <int> (i - theBegin);
                }
		    }
        }
	    return -1;
    }
	
	static int Find (const uint8_t* theBegin, const uint8_t* theEnd, const char* theString)
	{
		const uint8_t* aSubBegin = reinterpret_cast <const uint8_t*> (theString);
		const uint8_t* aSubEnd = reinterpret_cast <const uint8_t*> (theString + strlen (theString));
		return Find (theBegin, theEnd, aSubBegin, aSubEnd);
	}
	
	template <typename T>
	static T NumberOfDigits (T theNumber, uint8_t theRadix = 10)
	{
		T aRes = 1;
		while ((theNumber /= theRadix) > 0) {
			aRes++;
		}
		return aRes;
	}

	template <typename T>
	static String ToString (T theNumber)
	{
		T aNum = NumberOfDigits (theNumber);
		char* aBuffer = Base::Allocator::ConstructArray <char> (static_cast <int> (aNum + 1));
		aBuffer[aNum] = '\0';

		T i = aNum - 1;
		while ((theNumber / 10) > 0) {
			T aRemainder = theNumber % 10;
			aBuffer [i--] = static_cast <char> (aRemainder + '0');
			theNumber /= 10;
		}

		T aRemainder = theNumber % 10;
		aBuffer [i--] = static_cast <char> (aRemainder + '0');
		String aRes (aBuffer);
		Base::Allocator::DeallocateArray (aBuffer);
	
		return aRes;
	}

	ORI_EXPORT static String ToString (double theNumber);
	ORI_EXPORT static String ToString (float theNumber);
};

}}

#endif
