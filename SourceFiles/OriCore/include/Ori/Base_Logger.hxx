#ifndef _Ori_Base_Logger_HeaderFile
#define _Ori_Base_Logger_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_LogLevel.hxx>
#include <Ori/Base_LoggerBuffer.hxx>

#include <memory>

namespace Ori {
namespace Base {
class ILogger;
class LoggerImpl;

class Logger
{
public:

    ORI_EXPORT Logger (const ILogger* theLogger = nullptr);

    ORI_EXPORT LoggerBuffer Verbose (const char* theFile = __FILE__, int theLine = __LINE__);
    ORI_EXPORT LoggerBuffer Info (const char* theFile = __FILE__,  int theLine = __LINE__);
    ORI_EXPORT LoggerBuffer Warning (const char* theFile = __FILE__,  int theLine = __LINE__);
    ORI_EXPORT LoggerBuffer Fail (const char* theFile = __FILE__, int theLine = __LINE__);
    ORI_EXPORT static LoggerBuffer Instance (LogLevel theLevel,
                                             const char* theFile = __FILE__,
                                             int theLine = __LINE__);
    ORI_EXPORT static void SetDefaultLevel (LogLevel theLevel);

    __ORI_PRIMITIVE_UNIMPLEMENTED_REFERENCE (LogLevel, Level);
    __ORI_PRIMITIVE_UNIMPLEMENTED_REFERENCE (bool, IsStrict);

private:
    Logger (const std::shared_ptr <LoggerImpl>& theImpl);

private:
    std::shared_ptr <LoggerImpl> myImpl;

};


#define ORI_LOG_VERBOSE(LOGGER) LOGGER.Verbose (__FILE__, __LINE__)
#define ORI_LOG_INFO(LOGGER) LOGGER.Info (__FILE__, __LINE__)
#define ORI_LOG_WARNING(LOGGER) LOGGER.Warning (__FILE__, __LINE__)
#define ORI_LOG_FAIL(LOGGER) LOGGER.Fail (__FILE__, __LINE__)
#define ORI_LOG_DEBUG(LOGGER) LOGGER.Fail (__FILE__, __LINE__)

#define ORI_LOG_VERBOSE_DEFAULT() Ori::Base::Logger::Instance (Ori::Base::LogLevel::Verbose, __FILE__, __LINE__)
#define ORI_LOG_INFO_DEFAULT() Ori::Base::Logger::Instance (Ori::Base::LogLevel::Info, __FILE__, __LINE__)
#define ORI_LOG_WARNING_DEFAULT() Ori::Base::Logger::Instance (Ori::Base::LogLevel::Warning, __FILE__, __LINE__)
#define ORI_LOG_FAIL_DEFAULT() Ori::Base::Logger::Instance (Ori::Base::LogLevel::Fail, __FILE__, __LINE__)
#define ORI_LOG_DEBUG_DEFAULT() Ori::Base::Logger::Instance (Ori::Base::LogLevel::Debug, __FILE__, __LINE__)

}}

#endif