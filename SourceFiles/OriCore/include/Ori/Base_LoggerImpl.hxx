#ifndef _Ori_Base_LoggerImpl_HeaderFile
#define _Ori_Base_LoggerImpl_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Base_ILogger.hxx>

namespace Ori {
namespace Base {
class ILogger;

class LoggerImpl
{
public:
    LoggerImpl (const ILogger* theLogger = nullptr);
    ~LoggerImpl();

    LoggerBuffer Verbose (const char* theFile = __FILE__, int theLine = __LINE__);
    LoggerBuffer Info (const char* theFile = __FILE__, int theLine = __LINE__);
    LoggerBuffer Warning (const char* theFile = __FILE__, int theLine = __LINE__);
    LoggerBuffer Fail (const char* theFile = __FILE__, int theLine = __LINE__);
    static LoggerBuffer Buffer (LogLevel theCurrentLevel,
                                const char* theFile = __FILE__,
                                int theLine = __LINE__);
	static LoggerImpl* Instance();
    static void SetDefaultLevel (LogLevel theLevel);
    bool IsRightLevel();

    LogLevel& Level();
    bool& IsStrict();

private:
    LoggerBuffer MakeBuffer (LogLevel theLevel, const char* theFile, int theLine);

private:
    const ILogger* myLogger;
    String myFile;
    String myLine;
    LogLevel myCurrentLevel;
    bool myIsNeedDestroyLogger;

    Base::LogLevel myLevel;
    bool myIsStrict;
};

}}

#endif
