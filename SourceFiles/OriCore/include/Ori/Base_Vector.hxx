#ifndef _Ori_Base_Vector_HeaderFile
#define _Ori_Base_Vector_HeaderFile

#include <Ori/Base_Allocator.hxx>
#include <Ori/Base_Exception.hxx>
#include <Ori/Base_VectorView.hxx>

namespace Ori {
namespace Base {

template <typename T>
class Vector : public VectorView <T>
{
public:
    Vector() :
        VectorView <T>(),
		myCapacity (10)
    {
	    Allocate (myCapacity);
        this->myLast = this->myFirst;
    }
	
	Vector (int theCapacity) :
        VectorView <T>(),
		myCapacity (0) // should be 0
    {
	    this->myFirst = this->myLast;
        Reserve (theCapacity);
    }

    ~Vector()
    {
        Destroy();
    }

    // Clang requires explicitly invoke inherited protected member through a pointer
    // on this->
    Vector (const T* theData, int theLenght) :
        VectorView <T>()
    {
        myCapacity = theLenght;
        Allocate (myCapacity);
        std::copy (theData, theData + theLenght, myData);
        this->myLast = this->myFirst + theLenght;
    }

    Vector (const T* theFirst, const T* theLast) :
        VectorView <T>()
    {
        myCapacity = static_cast <int> (theLast - theFirst);
        Allocate (myCapacity);
        std::copy (theFirst, theLast, myData);
        this->myLast = this->myFirst + myCapacity;
    }

    Vector (const Vector& theOther) :
        VectorView <T>()
    {
        myCapacity = theOther.myCapacity;
        Allocate (myCapacity);
        std::copy (theOther.myData, theOther.myData + theOther.Lenght(), myData);
        this->myLast = this->myFirst + theOther.Lenght();
    }

    Vector (const std::initializer_list <T>& theList) :
        VectorView <T>()
    {
        myCapacity = static_cast <int> (theList.size() > 10 ? theList.size() : 10);
        Allocate (myCapacity);
        for (auto i = theList.begin(); i != theList.end(); i++) {

            int anIndex = this->Lenght();
            Assign (*i, anIndex);
        }
    }

    Vector (int theNumberOfElements, const T& theElem) :
        VectorView <T>()
    {
        myCapacity = static_cast <int> (theNumberOfElements > 10 ? theNumberOfElements : 10);
        Allocate (myCapacity);
        for (int i = 0; i < theNumberOfElements; i++) {
            Assign (theElem, i);
        }
    }

    Vector <T>& operator= (const VectorView <T>& theOther)
    {
        Destroy();
        myCapacity = theOther.Lenght() > 10 ? theOther.Lenght() : 10;

        Allocate (myCapacity);

        std::copy (theOther.begin(), theOther.end(), myData);
        this->myLast = this->myFirst + theOther.Lenght();
        return *this;
    }

    T& operator[] (int theIndex)
    {
        this->CheckIndex (theIndex);
        return myData[theIndex];
    }

    const T& operator[] (int theIndex) const override
    {
        this->CheckIndex (theIndex);
        return myData[theIndex];
    }

    T* Data()
    {
        return myData;
    }

    const T* Data() const
    {
        return myData;
    }

    bool Reserve (int theCapacity)
    {
        if (myCapacity >= theCapacity) {
            return false;
        }

        Reallocate (theCapacity);
        return true;
    }

    void Resize (int theCapacity)
    {
        Reserve (theCapacity);
        this->myLast = this->myFirst + theCapacity;
    }

    void Push (const T& theElem)
    {
        int aLenght = this->Lenght();
        if (aLenght >= myCapacity) {
            Reallocate (aLenght * 2);
        }
        Assign (theElem, aLenght);
    }

    void Push (const T* theData, int theLenght)
    {
        int aNewLenght = this->Lenght() + theLenght;
        if (aNewLenght >= myCapacity) {
            Reallocate (aNewLenght * 2);
        }

        for (int i = this->Lenght(), j = 0; i < aNewLenght; i++, j++) {
            Assign (theData[j], i);
        }
    }

    void Push (const T* theFirst, const T* theLast)
    {
        int aLenght = static_cast <int> (theLast - theFirst);
        Push (theFirst, aLenght);
    }

    void Push (const VectorView <T>& theOther)
    {
        Push (theOther.begin(), theOther.end());
    }

    void Insert (const T* theData, int theLenght, int theStartPos = 0)
    {
        if (theStartPos + theLenght > myCapacity) {
            Reallocate (theStartPos + theLenght);
        }

        std::copy (theData, theData + theLenght, myData + theStartPos);
        this->myLast += theLenght;
    }

    void Insert (const VectorView <T>& theData, int theStartPos = 0)
    {
        Insert (theData.Data(), theData.Lenght(), theStartPos);
    }

    void Insert (const T* theStart, const T* theEnd, int theStartPos = 0)
    {
        auto aLenght = static_cast <int> (theEnd - theStart);
        Insert (theStart, aLenght, theStartPos);
    }

    VectorView <T> Clone() const override
    {
        return Vector <T> (this->myFirst, this->Lenght());
    }

    void Reset()
    {
        this->myLast = this->myFirst;
    }

    void Clear()
    {
        Reset();
        Destroy();
        this->myCapacity = 0;
    }
	
	void Remove (int theElementIndex)
	{
		int aCurrentLength = this->Lenght();
		this->myLast--;
		if (theElementIndex == aCurrentLength - 1) { // last element
			return;
		}
		
		for (int i = theElementIndex + 1; i < aCurrentLength; i++) {
			myData[i - 1] = myData[i];
		}
	}
	
	__ORI_PRIMITIVE_PROPERTY (int, Capacity);

private:
    void Reallocate (int theNewCapacity)
    {
        int aLenght = this->Lenght();
        if (aLenght > 0) {
            
            T* aTmp = Allocator::ConstructArray <T> (aLenght);
            std::copy (this->myFirst, this->myLast, aTmp);
            Destroy();

            Allocate (theNewCapacity);
            std::copy (aTmp, aTmp + aLenght, myData);
            this->myLast = this->myFirst + aLenght;
	        Allocator::DeallocateArray <T> (aTmp);
        } else {

            Destroy();
            Allocate (theNewCapacity);
        }
        myCapacity = theNewCapacity;
    }

    void Allocate (int theSize)
    {
	    if (myCapacity > 0 && myData) {
			if (myCapacity >= theSize) {
				return;
			}
			Destroy();
		}
	    
		if (theSize > 0) {
			myData = Allocator::ConstructArray <T> (theSize);
			this->myFirst = myData;
			this->myLast = this->myFirst;	
		}
    }
    void Assign (const T& theElem, int theIndex)
    {
        myData[theIndex] = theElem;
        this->myLast++;
    }

    void Destroy()
    {
	    Allocator::DeallocateArray <T> (myData);
	    myData = nullptr;
    }

private:
    T* myData = nullptr;
};

}}

#endif
