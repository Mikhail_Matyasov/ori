#ifndef _Ori_Base_ByteArray_HeaderFile
#define _Ori_Base_ByteArray_HeaderFile

#include <Ori/Base_Vector.hxx>
#include <stdint.h>

namespace Ori { 
namespace Base { 

typedef Vector <uint8_t> ByteArray;
typedef VectorView <uint8_t> ByteArrayView;

}}


#endif