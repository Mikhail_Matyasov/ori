#ifndef _Ori_Base_String_HeaderFile
#define _Ori_Base_String_HeaderFile

#include <Ori/Base_Macros.hxx>

#include <string>
#include <string.h>

namespace Ori {
namespace Base {
__ORI_DECLARE_BYTEARRAY

class String
{
public:
    ORI_EXPORT String();
    ORI_EXPORT ~String();
    ORI_EXPORT String (const char* theString);
    ORI_EXPORT String (const String& theOther);
    ORI_EXPORT String (uint64_t theOther);
    ORI_EXPORT String (int64_t theOther);
    ORI_EXPORT String (int theOther);
    ORI_EXPORT String (unsigned int theOther);
    ORI_EXPORT String (float theOther);
    ORI_EXPORT String (double theOther);
    ORI_EXPORT String (bool theOther);
    ORI_EXPORT String (const char* theBegin, const char* theEnd);

    ORI_EXPORT String& operator<< (const char* theString);
    ORI_EXPORT String& operator<< (const String& theOther);
	ORI_EXPORT String& operator<< (char theOther);
	ORI_EXPORT String& operator<< (uint8_t theOther);
	ORI_EXPORT String& operator<< (int8_t theOther);
	
	template <typename T>
    String& operator<< (T theOther)
	{
	    (*this) += theOther;
		return *this;
    }
	
    ORI_EXPORT String& operator<< (float theOther);
    ORI_EXPORT String& operator<< (double theOther);
    ORI_EXPORT String& operator<< (bool theOther);

    ORI_EXPORT String& operator= (const String& theOther);
    ORI_EXPORT String& operator= (const char* theOther);
    ORI_EXPORT String& operator+= (const String& theOther);
    ORI_EXPORT String& operator+= (const char* theOther);
	ORI_EXPORT String& operator+= (char theOther);
	ORI_EXPORT String& operator+= (uint8_t theOther);
	ORI_EXPORT String& operator+= (int8_t theOther);
	
	template <typename T>
    String& operator+= (T theOther)
	{
		AppendNumber (theOther);
		return *this;
	}
	
    ORI_EXPORT String& operator+= (double theOther);
    ORI_EXPORT String& operator+= (bool theOther);
    ORI_EXPORT String operator+ (const char* theOther) const;
    ORI_EXPORT String operator+ (const String& theOther) const;
    ORI_EXPORT bool operator== (const String& theOther) const;
    ORI_EXPORT bool operator== (const char* theOther) const;
    ORI_EXPORT bool operator!= (const String& theOther) const;
    ORI_EXPORT bool operator!= (const char* theOther) const;
    ORI_EXPORT char& operator[] (int theIndex);
    ORI_EXPORT char operator[] (int theIndex) const;
	
	ORI_EXPORT int Find (const char* theSubString, int theStartIndex = 0);
	ORI_EXPORT int ToInt();
	ORI_EXPORT String Substring (int theStartIndex, int theLength);
    ORI_EXPORT bool Empty() const;
    ORI_EXPORT size_t Hash() const;
    ORI_EXPORT const char* Data() const;
    ORI_EXPORT char* Data();
    ORI_EXPORT int Length() const;

private:
    void Construct (const char* theData);
    void Construct (const uint8_t* theData, int theLenght);
    ORI_EXPORT void Append (const uint8_t* theData, int theLenght);
    void AppendRaw (const uint8_t* theData, int theLenght);
    bool CompareRange (const uint8_t* theOtherData, int theLenght) const;

    void Reallocate (int theNewCapacity);
    void Allocate (int theSize);
    void Assign (uint8_t theElem, int theIndex);
    void Destroy();

    template <typename T>
    void AppendNumber (T theNumber)
    {
        auto aNumber = std::to_string (theNumber);
        const char* aString = aNumber.c_str();
        const uint8_t* aData = reinterpret_cast <const uint8_t*> (aString);
        int aLenght = static_cast <int> (strlen (aString));
        Append (aData, aLenght);
    }

private:
    uint8_t* myData = nullptr;
    int myLenght;
    int myCapacity;
};

}}
#ifndef __ORI_STM32LIB
namespace std
{
template <>
struct hash <Ori::Base::String>
{
    ORI_EXPORT size_t operator() (const Ori::Base::String& theString) const
    {
        return theString.Hash();
    }
};
}
#endif

#endif