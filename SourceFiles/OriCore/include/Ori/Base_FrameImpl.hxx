#ifndef _Ori_Base_FrameImpl_HeaderFile
#define _Ori_Base_FrameImpl_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_PixelFormat.hxx>

namespace Ori {
namespace Base {

class FrameImpl
{
public:

	FrameImpl();
	FrameImpl (int theWidth, int theHeight, PixelFormat thePixelFormat = PixelFormat::YUV420);

	operator bool() const;
	std::shared_ptr <FrameImpl> Clone() const;

	__ORI_PROPERTY (Base::Vector <std::shared_ptr <Base::ByteArrayView>>, Data);
	__ORI_PROPERTY (Base::Vector <int>, Stride);
	__ORI_PRIMITIVE_PROPERTY (int, Width);
	__ORI_PRIMITIVE_PROPERTY (int, Height);
	__ORI_PRIMITIVE_PROPERTY (Base::PixelFormat, PixelFormat);

};

}}

#endif
