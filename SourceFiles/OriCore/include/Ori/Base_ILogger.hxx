#ifndef _Ori_Base_ILogger_HeaderFile
#define _Ori_Base_ILogger_HeaderFile

#include <Ori/Base_Macros.hxx>

namespace Ori {
namespace Base {
class String;

class ILogger
{
public:
    virtual ~ILogger() {}
    ORI_EXPORT virtual void Print (const String& theMessage) const;
};

}}

#endif