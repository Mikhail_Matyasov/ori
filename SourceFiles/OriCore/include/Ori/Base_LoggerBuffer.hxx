#ifndef _Ori_Base_LoggerBuffer_HeaderFile
#define _Ori_Base_LoggerBuffer_HeaderFile

#include <Ori/Base_String.hxx>
#include <Ori/Base_LogLevel.hxx>
#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace Base {
class ILogger;

class LoggerBuffer
{
public:
    ORI_EXPORT LoggerBuffer();
    ORI_EXPORT LoggerBuffer (bool theIsUseBuffer, const ILogger* theIfs, const char* theLevel, const char* theFile, int theLine);
    ORI_EXPORT ~LoggerBuffer();
    ORI_EXPORT LoggerBuffer& operator<< (const String& theMessage);
    ORI_EXPORT LoggerBuffer& operator<< (const char* theMessage);
    ORI_EXPORT LoggerBuffer& operator<< (uint64_t theOther);
    ORI_EXPORT LoggerBuffer& operator<< (int64_t theOther);
    ORI_EXPORT LoggerBuffer& operator<< (int theOther);
    ORI_EXPORT LoggerBuffer& operator<< (unsigned int theOther);
    ORI_EXPORT LoggerBuffer& operator<< (float theOther);
    ORI_EXPORT LoggerBuffer& operator<< (double theOther);
    ORI_EXPORT LoggerBuffer& operator<< (bool theOther);
    ORI_EXPORT LoggerBuffer& operator<< (const ByteArrayView& theArray);

private:
    String myBuffer;
    const ILogger* myLoggerInterface;
    bool myIsUseBuffer;
};

}}


#endif