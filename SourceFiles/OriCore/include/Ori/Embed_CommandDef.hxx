#ifndef _Ori_Embed_CommandDef_HeaderFile
#define _Ori_Embed_CommandDef_HeaderFile

namespace Ori { 
namespace Embed {

enum class CommandType
{
	Failed,
	Success,
	Blink,
	Reply,
	TcpClientSendVideoData,
	TcpClientSendPacket,
	UdpClientSendPacket,
	TcpClientFreeBufferSpaceAvaliable,
	SX1278ListenerOnVideoData,
	SX1278ListenerOnPacket,
	SX1278ListenerOnAcknowledgement,
	ConfigureAsServer,
	ConfigureAsClient,
	InvalidCommand
};

enum class BlinkType
{
	Simple
};

}}


#endif
