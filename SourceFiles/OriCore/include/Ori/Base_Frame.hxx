#ifndef _Ori_Codec_Frame_HeaderFile
#define _Ori_Codec_Frame_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_PixelFormat.hxx>

#include <memory>

namespace Ori {
namespace Base {
class FrameImpl;

class Frame
{
public:

	ORI_EXPORT Frame();
	ORI_EXPORT Frame (int theWidth, int theHeight, PixelFormat thePixelFormat = PixelFormat::YUV420);

	__ORI_UNIMPLEMENTED_REFERENCE (Base::Vector <std::shared_ptr <Base::ByteArrayView>>, Data);
	__ORI_UNIMPLEMENTED_REFERENCE (Base::Vector <int>, Stride);
	__ORI_PRIMITIVE_UNIMPLEMENTED_REFERENCE (int, Width);
	__ORI_PRIMITIVE_UNIMPLEMENTED_REFERENCE (int, Height);
	__ORI_PRIMITIVE_UNIMPLEMENTED_REFERENCE (Base::PixelFormat, PixelFormat);

	ORI_EXPORT operator bool() const;
	ORI_EXPORT Frame Clone() const;

private:
	Frame (const std::shared_ptr <FrameImpl>& theImpl);

private:
	std::shared_ptr <FrameImpl> myImpl;
};

}}

#endif
