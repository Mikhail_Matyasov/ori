#ifndef _Ori_Base_TypeBuffer_HeaderFile
#define _Ori_Base_TypeBuffer_HeaderFile

#include <stdint.h>
#include <algorithm>

namespace Ori {
namespace Base {

template <typename T>
union TypeBuffer
{
public:
    TypeBuffer (const T& theValue) : Value (theValue) {}
    TypeBuffer (const uint8_t* theData, int theLength = sizeof (T))
    { std::copy (theData, theData + theLength, Bytes); }
    TypeBuffer() {}

public:
    T Value;
    uint8_t Bytes[sizeof (T)];
};

}}


#endif
