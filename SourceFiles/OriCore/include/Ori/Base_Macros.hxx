#ifndef _Ori_Base_Macros_HeaderFile
#define _Ori_Base_Macros_HeaderFile

namespace Ori {
namespace Base {

#ifdef _WIN32
    #define ORI_EXPORT __declspec (dllexport)
#else
    #define ORI_EXPORT
#endif

#ifdef SWIGCSHARP
    #define SWIG_SETTER(TYPE,NAME) \
    void Set ## NAME (const TYPE& the ## NAME) \
    { \
        my ## NAME = the ## NAME; \
    }

    #define SWIG_PRIMITIVE_SETTER(TYPE,NAME) \
    void Set ## NAME (TYPE the ## NAME) \
    { \
        my ## NAME = the ## NAME; \
    }

    #define SWIG_REFERENCE_SETTER(TYPE,NAME) \
    void Set ## NAME (const TYPE& the ## NAME) \
    { \
        NAME() = the ## NAME; \
    }

    #define SWIG_PRIMITIVE_REFERENCE_SETTER(TYPE,NAME) \
    void Set ## NAME (TYPE the ## NAME) \
    { \
        NAME() = the ## NAME; \
    }
#else
    #define SWIG_SETTER(TYPE,NAME)
    #define SWIG_PRIMITIVE_SETTER(TYPE,NAME)
    #define SWIG_REFERENCE_SETTER(TYPE,NAME)
    #define SWIG_PRIMITIVE_REFERENCE_SETTER(TYPE,NAME)
#endif

//----------------------------------------
#define __ORI_PROPERTY(TYPE,NAME) \
    \
    public: \
        const TYPE & NAME() const { return my ## NAME ; } \
        TYPE & NAME() { return my ## NAME ; } \
        SWIG_SETTER(TYPE,NAME); \
    protected: \
        TYPE   my ## NAME; \
    public:

//----------------------------------------
#define __ORI_PRIMITIVE_PROPERTY(TYPE,NAME) \
    \
    public: \
        TYPE NAME() const { return my ## NAME ; } \
        TYPE& NAME() { return my ## NAME ; } \
        SWIG_PRIMITIVE_SETTER(TYPE,NAME); \
    protected: \
        TYPE   my ## NAME; \
    public:

//----------------------------------------
#define __ORI_READONLY_PROPERTY(TYPE,NAME) \
    \
    public: \
        const TYPE & NAME() const { return my ## NAME ; } \
    protected: \
        TYPE   my ## NAME; \
    public:

//----------------------------------------
#define __ORI_READONLY_PRIMITIVE_PROPERTY(TYPE,NAME) \
    \
    public: \
        TYPE NAME() const { return my ## NAME ; } \
    protected: \
        TYPE   my ## NAME; \
    public:

//----------------------------------------
#define __ORI_UNIMPLEMENTED_REFERENCE(TYPE,NAME) \
    \
    public: \
        ORI_EXPORT const TYPE & NAME() const; \
        ORI_EXPORT TYPE & NAME(); \
        SWIG_REFERENCE_SETTER(TYPE,NAME);

//----------------------------------------
#define __ORI_PRIMITIVE_UNIMPLEMENTED_REFERENCE(TYPE,NAME) \
    \
    public: \
        ORI_EXPORT TYPE NAME() const; \
        ORI_EXPORT TYPE& NAME(); \
        SWIG_PRIMITIVE_REFERENCE_SETTER(TYPE,NAME);

//----------------------------------------
#define __ORI_DECLARE_BYTEARRAY \
    template <typename T> \
    class Vector; \
    typedef Vector <uint8_t> ByteArray;

//----------------------------------------
#define __ORI_DECLARE_BYTEARRAYVIEW \
    template <typename T> \
    class VectorView; \
    typedef VectorView <uint8_t> ByteArrayView;

}}
//----------------------------------------

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#else

#ifndef SWIGCSHARP

void emscripten_run_script (const char*);
#ifndef EMSCRIPTEN_KEEPALIVE
#define EMSCRIPTEN_KEEPALIVE
#endif

#endif
#endif
#endif

