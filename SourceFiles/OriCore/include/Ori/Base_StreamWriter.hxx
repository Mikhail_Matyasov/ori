#ifndef _Ori_Base_StreamWriter_HeaderFile
#define _Ori_Base_StreamWriter_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_ByteArray.hxx>

#include <memory>
#include <fstream>

namespace Ori {
namespace Base {
    
class StreamWriter
{
public:
    ORI_EXPORT StreamWriter();
    ORI_EXPORT StreamWriter (const String& theFileName);

    ORI_EXPORT void Write (const void* theData, int theLenght, const String& theFileName = "");
    ORI_EXPORT void Write (const ByteArrayView& theArray, const String& theFileName = "");

private:
    void InitIfNeed (const String& theFileName);

private:
    std::shared_ptr <std::ofstream> myStream;
    String myFileName;

};

}}

#endif