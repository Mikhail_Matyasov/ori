#ifndef _Ori_Base_Assert_HeaderFile
#define _Ori_Base_Assert_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>

namespace Ori {
namespace Base {

class Assert
{
public:
    ORI_EXPORT static void Raise (const char* theMessage);
    ORI_EXPORT static void True (bool theExpression);
    ORI_EXPORT static void False (bool theExpression);
};

}}


#endif