#ifndef _Ori_Base_Exception_HeaderFile
#define _Ori_Base_Exception_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>

namespace Ori {
namespace Base {

class Exception
{
public:
    ORI_EXPORT Exception (const Base::String& theMessage);
    ORI_EXPORT const Base::String& What() const;


private:
    Base::String myMessage;
};

}}


#endif