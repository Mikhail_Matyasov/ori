#ifndef _Ori_Base_VectorView_HeaderFile
#define _Ori_Base_VectorView_HeaderFile

#include <Ori/Base_Exception.hxx>
#include <initializer_list>
#include <algorithm>

namespace Ori {
namespace Base {

template <typename T>
class VectorView
{
public:

    VectorView() :
        myFirst (nullptr),
        myLast (nullptr)
    {}

    VectorView (const T* theFirst, const T* theLast) :
        myFirst (theFirst),
        myLast (theLast + 1)
    {}

    VectorView (const T* theFirst, int theLenght) :
        myFirst (theFirst),
        myLast (theFirst + theLenght)
    {}

    VectorView (const VectorView& theOther) :
        myFirst (theOther.myFirst),
        myLast (theOther.myLast)
    {}

    // Base class must have at least one virtual function to use dynamic_cast
    virtual ~VectorView() {}

    VectorView& operator= (const VectorView& theOther)
    {
        myFirst = theOther.myFirst;
        myLast = theOther.myLast;
        return *this;
    }

    virtual const T& operator[] (int theIndex) const
    {
        CheckIndex (theIndex);
        return myFirst[theIndex];
    }

    bool operator== (const VectorView& theOther) const
    {
        if (Lenght() != theOther.Lenght()) {
            return false;
        }
        bool aRes = std::equal (myFirst, myLast, theOther.myFirst);
        return aRes;
    }

    bool operator!= (const VectorView& theOther) const
    {
        return !((*this) == theOther);
    }

    const T* Data() const
    {
        return myFirst;
    }

    int Lenght() const
    {
        int aRes = static_cast <int> (myLast - myFirst);
        return aRes;
    }

    bool Empty() const
    {
        return myLast == myFirst;
    }

    void SetBegin (const T* theBegin)
    { myFirst = theBegin; }

    void SetEnd (const T* theEnd)
    { myLast = theEnd; }

    const T* begin() const
    {
        return myFirst;
    }

    const T* end() const
    {
        return myLast;
    }

    bool Contains (const T& theElem) const
    {
        auto anIt = std::find (myFirst, myLast, theElem);
        if (anIt == myLast) {
            return false;
        }
        return true;
    }
	
	int Find (const T& theElem) const
    {
        const T* anElement = std::find (myFirst, myLast, theElem);
		if (anElement == myLast) {
			return -1;
		}
	    return static_cast <int> (anElement - myFirst);
    }

    virtual VectorView <T> Clone() const
    {
        return VectorView <T> (this->myFirst, this->Lenght());
    }

protected:

    void CheckIndex (int theIndex) const
    {
        int aLenght = Lenght();
        if (aLenght <= theIndex) {
#ifndef __ORI_STM32LIB
            throw Base::Exception ("Index out of range");
#endif
        }
    }

protected:
    const T* myFirst;
    const T* myLast;
};

}}


#endif