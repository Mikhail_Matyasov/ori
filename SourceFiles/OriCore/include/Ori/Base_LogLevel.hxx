#ifndef _Ori_Base_LogLevel_HeaderFile
#define _Ori_Base_LogLevel_HeaderFile


namespace Ori {
namespace Base {

enum class LogLevel
{
    None,
    Debug,
    Fail,
    Warning,
    Info,
    Verbose
};

}}


#endif