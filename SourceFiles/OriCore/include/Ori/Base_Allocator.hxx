#ifndef _Ori_Base_Allocator_HeaderFile
#define _Ori_Base_Allocator_HeaderFile

#include <Ori/Base_Pch.hxx>

namespace Ori {
namespace Base {
	
#ifdef __ORI_USE_USER_ALLOCATOR
#define __ORI_USE_USER_ALLOCATOR_CONSTRUCT
#define __ORI_USE_USER_ALLOCATOR_ALLOCATE
#define __ORI_USE_USER_ALLOCATOR_DEALLOCATE
#define __ORI_USE_USER_ALLOCATOR_DEALLOCATEARRAY			  
#endif

class Allocator
{
public:
	
	template <typename T, class... Types>
	static T* Construct (Types&&... theArgs);
	
	template <typename T>
	static T* ConstructArray (int theSize);
	
	template <typename T>
	static T* Allocate (int theSize);
	
	template <typename T>
	static bool Deallocate (T* thePointer);
	
	template <typename T>
	static bool DeallocateArray (T* thePointer);
};

#ifndef __ORI_USE_USER_ALLOCATOR

#ifndef __ORI_USE_USER_ALLOCATOR_CONSTRUCT
template <typename T, class ...Types>
inline T* Allocator::Construct (Types&& ...theArgs)
{
	return new T (std::forward <Types> (theArgs)...);
}
#endif
	
#ifndef __ORI_USE_USER_ALLOCATOR_CONSTRUCT_ARRAY
template <typename T>
inline T* Allocator::ConstructArray (int theSize)
{
	return new T [theSize];
}
#endif

#ifndef __ORI_USE_USER_ALLOCATOR_ALLOCATE
template<typename T>
inline T* Allocator::Allocate (int theSize)
{
	return malloc (sizeof (T) * theSize);
}
#endif

#ifndef __ORI_USE_USER_ALLOCATOR_DEALLOCATE
template<typename T>
inline bool Allocator::Deallocate (T* thePointer)
{
	delete thePointer;
	return true;
}
#endif

#ifndef __ORI_USE_USER_ALLOCATOR_DEALLOCATEARRAY
template<typename T>
inline bool Allocator::DeallocateArray (T* thePointer)
{
	delete[] thePointer;
	return true;
}
	
#endif
#endif

}}


#endif
