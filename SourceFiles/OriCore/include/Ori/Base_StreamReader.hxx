#ifndef _Ori_Base_StreamReader_HeaderFile
#define _Ori_Base_StreamReader_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_ByteArray.hxx>

#include <fstream>

namespace Ori {
namespace Base {

class StreamReader
{
public:
    ORI_EXPORT StreamReader();
    ORI_EXPORT StreamReader (const String& theFileName);

    ORI_EXPORT std::shared_ptr <ByteArray> Read (const String& theFileName = "");

private:
    void InitIfNeed (const String& theFileName);

private:
    std::shared_ptr <std::ifstream> myStream;
    String myFileName;
};

}}

#endif