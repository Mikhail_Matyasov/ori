#ifndef _Ori_Embed_ESP8266Packet_HeaderFile
#define _Ori_Embed_ESP8266Packet_HeaderFile

#include <Ori/Base_ByteArray.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <stdint.h>

#define __ORI_MAX_ESP8266_PACKET_SIZE 2048
#define __ORI_MAX_ESP8266_DATA_SIZE 2016

namespace Ori { 
namespace Embed { 

class ESP8266Packet
{
public:
    ORI_EXPORT ESP8266Packet();
    ORI_EXPORT ESP8266Packet (uint8_t theID);

    __ORI_PROPERTY (uint8_t, ID);
    __ORI_PROPERTY (CommandType, Command);
    __ORI_PROPERTY (Base::ByteArrayView, Data);

private:
    static uint8_t GetID();

private:
    static uint8_t myCurrentID;
};

}}


#endif
