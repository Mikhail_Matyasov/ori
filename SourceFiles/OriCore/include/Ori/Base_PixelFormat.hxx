#ifndef _Ori_Base_PixelFormat_HeaderFile
#define _Ori_Base_PixelFormat_HeaderFile


namespace Ori {
namespace Base {

enum class PixelFormat
{
    YUV420,
    RGBA,
    BGRA
};

}}


#endif