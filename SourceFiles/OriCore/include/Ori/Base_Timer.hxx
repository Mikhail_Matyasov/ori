#ifndef _Ori_Base_Timer_HeaderFile
#define _Ori_Base_Timer_HeaderFile

#include <Ori/Base_Macros.hxx>

namespace Ori {
namespace Base {

class Timer
{
public:
    typedef long long TimeType;
    typedef unsigned long long DurationType;

    class Duration
    {
    public:
        ORI_EXPORT Duration (DurationType theValue);
        ORI_EXPORT Duration();
        ORI_EXPORT DurationType Seconds() const;
        ORI_EXPORT DurationType Milliseconds() const;
        ORI_EXPORT DurationType Microseconds() const;
        ORI_EXPORT DurationType Nanoseconds() const;
        ORI_EXPORT Duration& operator+= (const Duration& theDuration);

    private:
        DurationType myDuration;
    };

    ORI_EXPORT static TimeType Seconds();
    ORI_EXPORT static TimeType Milliseconds();
    ORI_EXPORT static TimeType Microseconds();
    ORI_EXPORT static TimeType Nanoseconds();
    ORI_EXPORT static void Sleep (int theMicroseconds);

    ORI_EXPORT void Timestamp();
    ORI_EXPORT Timer::Duration DurationSinceLastTimestamp();

private:
    TimeType myStartTime;
};

}}


#endif
