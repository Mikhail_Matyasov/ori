%feature("nspace") Ori::Base::ILogger;

%{
#include <Ori/Base_ILogger.hxx>
%}

%csmethodmodifiers Ori::Base::ILogger::Print "public virtual";
%feature("director") Ori::Base::ILogger;

%include <Ori/Base_ILogger.hxx>