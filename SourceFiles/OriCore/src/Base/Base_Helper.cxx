#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Helper.hxx>

#include <stdlib.h>

namespace Ori {
namespace Base {

String Helper::ToString (double theNumber)
{
    uint64_t anIntegerPart = static_cast <uint64_t> (theNumber);
    String aRes = ToString (anIntegerPart);

    const uint64_t aMaxMantisaLength = 10000000000;
    double aMantisa = theNumber - static_cast <double> (anIntegerPart);
    uint64_t anIntegerMantisa = static_cast <uint64_t> (aMantisa * aMaxMantisaLength);
    while (anIntegerMantisa / 10 > 0) {
        uint64_t aRemainder = anIntegerMantisa % 10;
        if (aRemainder != 0) {
            break;
        }
        anIntegerMantisa /= 10;
    }

    aRes += '.';
    aRes += ToString (anIntegerMantisa);
    return aRes;
}

String Helper::ToString (float theNumber)
{
    return ToString (static_cast <double> (theNumber));
}

}}
