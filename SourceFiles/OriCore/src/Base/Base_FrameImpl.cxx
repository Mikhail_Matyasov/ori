#include <Ori/Base_Pch.hxx>
#include <Ori/Base_FrameImpl.hxx>

namespace Ori {
namespace Base {

FrameImpl::FrameImpl() :
    myWidth (0),
    myHeight (0),
    myPixelFormat (Base::PixelFormat::YUV420)
{}

FrameImpl::FrameImpl (int theWidth, int theHeight, Base::PixelFormat thePixelFormat) :
    myWidth (theWidth),
    myHeight (theHeight),
    myPixelFormat (thePixelFormat)
{
    switch (thePixelFormat)
    {
    case PixelFormat::YUV420:
        myStride.Resize (3);
        myStride[0] = theWidth;
        myStride[1] = theWidth / 2;
        myStride[2] = theWidth / 2;
        break;
    default:
        myStride.Resize (1);
        myStride[0] = theWidth * 4;
        break;
    }
}

FrameImpl::operator bool() const
{
    return !(myWidth == 0 && myHeight == 0);
}

namespace {
std::shared_ptr <ByteArray> Copy (const std::shared_ptr <ByteArrayView>& theSource)
{
   const uint8_t* aFirst = theSource->begin();
   const uint8_t* aLast = theSource->end();
   return std::make_shared <ByteArray> (aFirst, aLast);
}
}

std::shared_ptr <FrameImpl> FrameImpl::Clone() const
{
    auto aClone = std::make_shared <FrameImpl> (myWidth, myHeight, myPixelFormat);
    aClone->Data().Resize (myData.Lenght());
    for (int i = 0; i < myData.Lenght(); i++) {
        aClone->Data()[i] = Copy (myData[i]);
    }

    return aClone;
}

}}