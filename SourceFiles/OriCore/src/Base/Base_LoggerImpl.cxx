#include <Ori/Base_Pch.hxx>
#include <Ori/Base_LoggerImpl.hxx>

#include <Ori/Base_Allocator.hxx>

namespace Ori {
namespace Base {
namespace {
const char* FileName (const char* theData, size_t theLenght)
{
    auto anEnd = theData + theLenght;
    for (auto i = anEnd; i != theData; i--) {
        if (*i == '\\' || *i == '/') {

            return i + 1;
        }
    }

    return "";
}

const char* ToString (LogLevel theLevel)
{
    switch (theLevel)
    {
        case LogLevel::None:
            return "";
        case LogLevel::Fail:
            return "[FAIL]";
        case LogLevel::Warning:
            return "[WARNING]";
        case LogLevel::Info:
            return "[INFO]";
        case LogLevel::Verbose:
            return "[VERBOSE]";
    }
    return "";
}

}

LoggerImpl::LoggerImpl (const ILogger* theLogger) :
    myCurrentLevel (LogLevel::Info),
    myLevel (LogLevel::Info),
    myIsStrict (false)
{
    if (theLogger) {
        myLogger = theLogger;
        myIsNeedDestroyLogger = false;
    } else {
        myLogger = new ILogger();
        myIsNeedDestroyLogger = true;
    }
}
	
LoggerImpl* LoggerImpl::Instance()
{
	static LoggerImpl* Instance = Allocator::Construct <LoggerImpl>();
	return Instance;
}	

LoggerImpl::~LoggerImpl()
{
    if (myIsNeedDestroyLogger) {
        delete myLogger;
    }
}

LoggerBuffer LoggerImpl::Verbose (const char* theFile, int theLine)
{
    return MakeBuffer (LogLevel::Verbose, theFile, theLine);
}

LoggerBuffer LoggerImpl::Info (const char* theFile, int theLine)
{
    return MakeBuffer (LogLevel::Info, theFile, theLine);
}

LoggerBuffer LoggerImpl::Warning (const char* theFile, int theLine)
{
    return MakeBuffer (LogLevel::Warning, theFile, theLine);
}

LoggerBuffer LoggerImpl::Fail (const char* theFile, int theLine)
{
    return MakeBuffer (LogLevel::Fail, theFile, theLine);
}

LoggerBuffer LoggerImpl::Buffer (LogLevel theLevel, const char* theFile, int theLine)
{
    return Instance()->MakeBuffer (theLevel, theFile, theLine);
}
void LoggerImpl::SetDefaultLevel (LogLevel theLevel)
{
    Instance()->Level() = theLevel;
}

bool LoggerImpl::IsRightLevel()
{
    if (myIsStrict) {
        return myCurrentLevel == myLevel;
    } else {
        return myCurrentLevel <= myLevel;
    }
}

LogLevel& LoggerImpl::Level()
{
    return myLevel;
}

bool& LoggerImpl::IsStrict()
{
    return myIsStrict;
}

LoggerBuffer LoggerImpl::MakeBuffer (LogLevel theLevel, const char* theFile, int theLine)
{
    myCurrentLevel = theLevel;
    bool anIsRight = IsRightLevel();
    if (anIsRight) {
        auto aFileName = FileName (theFile, strlen (theFile));
        return LoggerBuffer (anIsRight, myLogger, ToString (myCurrentLevel), aFileName, theLine);
    }

    return LoggerBuffer();
}

}}
