#include <Ori/Base_Pch.hxx>
#include <Ori/Base_StreamWriter.hxx>

#include <Ori/Base_Exception.hxx>

namespace Ori {
namespace Base {

StreamWriter::StreamWriter()
{}

StreamWriter::StreamWriter (const String& theFileName)
{
    InitIfNeed (theFileName);
}

void StreamWriter::Write (const void* theData, int theLenght, const String& theFileName)
{
    InitIfNeed (theFileName);

    auto aData = reinterpret_cast <const char*> (theData);
    myStream->write (aData, theLenght);
    myStream->flush();
}

void StreamWriter::Write (const ByteArrayView& theArray, const String& theFileName)
{
    InitIfNeed (theFileName);

    auto aData = reinterpret_cast <const char*> (theArray.Data());
    int aLenght = theArray.Lenght();
    myStream->write (aData, aLenght);
    myStream->flush();
}

void StreamWriter::InitIfNeed (const String& theFileName)
{
    if (theFileName.Empty() && myFileName.Empty()) {
        throw Exception ("Filename must be provided.");
    }

    if (!theFileName.Empty() && theFileName != myFileName) {

        if (myStream) {
            myStream->close();
        }

        myStream = std::make_shared <std::ofstream> (theFileName.Data(), std::ofstream::binary);
        myFileName = theFileName;
    }
}

}}
