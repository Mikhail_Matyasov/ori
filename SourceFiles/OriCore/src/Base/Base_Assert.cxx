#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Assert.hxx>

#include <Ori/Base_Exception.hxx>

namespace Ori {
namespace Base {

void Assert::Raise (const char* theMessage)
{
    throw Exception (theMessage);
}

void Assert::True (bool theExpression)
{
    if (!theExpression) {
        Raise ("Expression must be true");
    }
}

void Assert::False (bool theExpression)
{
    if (theExpression) {
        Raise ("Expression must be false");
    }
}

}}
