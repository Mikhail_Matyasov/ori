#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Timer.hxx>


namespace Ori {
namespace Base {

Timer::Duration::Duration (DurationType theValue) :
    myDuration (theValue)
{}

Timer::Duration::Duration () :
    myDuration (0)
{}

Timer::DurationType Timer::Duration::Seconds() const
{
    return static_cast <Timer::DurationType> (myDuration / 1e+9);
}

Timer::DurationType Timer::Duration::Milliseconds() const
{
    return static_cast <Timer::DurationType> (myDuration / 1e+6);
}

Timer::DurationType Timer::Duration::Microseconds() const
{
    return static_cast <Timer::DurationType> (myDuration / 1e+3);
}

Timer::DurationType Timer::Duration::Nanoseconds() const
{
    return myDuration;
}

Timer::Duration& Timer::Duration::operator+= (const Timer::Duration& theDuration)
{
    myDuration += theDuration.Nanoseconds();
    return *this;
}

namespace {
template <typename T>
Timer::TimeType GetTimeSinceEpoch()
{
    auto aTime = std::chrono::duration_cast <T>
        (std::chrono::system_clock::now().time_since_epoch());
    
    return aTime.count();
}
}

Timer::TimeType Timer::Seconds()
{
    return GetTimeSinceEpoch <std::chrono::seconds>();
}

Timer::TimeType Timer::Milliseconds()
{
    return GetTimeSinceEpoch <std::chrono::milliseconds>();
}

Timer::TimeType Timer::Microseconds()
{
    return GetTimeSinceEpoch <std::chrono::microseconds>();
}

Timer::TimeType Timer::Nanoseconds()
{
    return GetTimeSinceEpoch <std::chrono::nanoseconds>();
}

void Timer::Sleep (int theMicroseconds)
{
    std::this_thread::sleep_for (std::chrono::microseconds (theMicroseconds));
}

void Timer::Timestamp()
{
    myStartTime = Nanoseconds();
}

Timer::Duration Timer::DurationSinceLastTimestamp()
{
    DurationType aDurationTime = static_cast <DurationType> (Nanoseconds() - myStartTime);
    Timer::Duration aDuration (aDurationTime);

    return aDuration ;
}

}}
