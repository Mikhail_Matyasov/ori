#include <Ori/Base_Pch.hxx>
#include <Ori/Base_ILogger.hxx>

#include <Ori/Base_String.hxx>

#ifdef __ANDROID__
#include <android/log.h>
#endif

namespace Ori {
namespace Base {

void ILogger::Print (const String& theMessage) const
{
#ifdef __ANDROID__
    __android_log_print (ANDROID_LOG_VERBOSE, "", "%s", theMessage.Data());
#elif __EMSCRIPTEN__
    String aMessage = String ("console.log ('") + theMessage.Data() + "')";
    emscripten_run_script (aMessage.Data());
#else
#ifndef __ORI_STM32LIB
    std::cout << theMessage.Data() << std::endl;
#endif	
#endif
}

}}
