#include <Ori/Base_Pch.hxx>
#include <Ori/Base_String.hxx>

#include <Ori/Base_Allocator.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Helper.hxx>

#ifndef __ORI_STM32LIB
#include <boost/functional/hash.hpp>
#endif

namespace Ori {
namespace Base {

String::String() :
    myLenght (1),
    myCapacity (10)
{
    Allocate (myCapacity);
    myData[0] = '\0';
}
	
String::~String()
{
	Destroy();
}

String::String (const char* theString)
{
    Construct (theString);
}

String::String (const String& theOther)
{
    Construct (theOther.myData, theOther.myLenght);
}

String::String (uint64_t theOther)
{
    *this = Helper::ToString (theOther);
}

String::String (int64_t theOther)
{
   *this = Helper::ToString (theOther);
}

String::String (int theOther)
{
    *this = Helper::ToString (theOther);
}

String::String (unsigned int theOther)
{
    *this = Helper::ToString (theOther);
}

String::String (float theOther)
{
    *this = Helper::ToString (theOther);
}

String::String (double theOther)
{
    *this = Helper::ToString (theOther);
}

String::String (bool theOther)
{
    if (theOther) {
        Construct ("true");
    } else {
        Construct ("false");
    }
}

String::String (const char* theBegin, const char* theEnd)
{
    auto aData = reinterpret_cast <const uint8_t*> (theBegin);
    int aLenght = static_cast <int> (theEnd - theBegin);
    
    myLenght = aLenght + 1;
    Allocate (myLenght);
    std::copy (aData, aData + aLenght, myData);
    myData[aLenght] = '\0';
}

String& String::operator<< (const char* theString)
{
    (*this) += theString;
    return *this;
}

String& String::operator<< (const String& theString)
{
    (*this) += theString;
    return *this;
}
	
String& String::operator<< (char theOther)
{
	(*this) += theOther;
    return *this;
}
	
String& String::operator<< (uint8_t theOther)
{
	(*this) += theOther;
    return *this;
}	
	
String& String::operator<< (int8_t theOther)
{
	(*this) += theOther;
    return *this;
}	

String& String::operator<< (float theOther)
{
    (*this) += theOther;
    return *this;
}

String& String::operator<< (double theOther)
{
    (*this) += theOther;
    return *this;
}

String& String::operator<< (bool theOther)
{
    (*this) += theOther;
    return *this;
}

String& String::operator= (const String& theOther)
{
    Construct (theOther.myData, theOther.myLenght);
    return *this;
}

String& String::operator= (const char* theOther)
{
    Construct (theOther);
    return *this;
}

String& String::operator+= (const String& theOther)
{
    AppendRaw (theOther.myData, theOther.myLenght);
    return *this;
}

String& String::operator+= (const char* theOther)
{
    const uint8_t* aData = reinterpret_cast <const uint8_t*> (theOther);
    int aLenght = static_cast <int> (strlen (theOther));
    Append (aData, aLenght);

    return *this;
}

String& String::operator+= (char theOther)
{
    uint8_t* aData = reinterpret_cast <uint8_t*> (&theOther);
    Append (aData, 1);

    return *this;
}
	
String& String::operator+= (uint8_t theOther)
{
    uint8_t* aData = reinterpret_cast <uint8_t*> (&theOther);
    Append (aData, 1);

    return *this;
}
	
String& String::operator+= (int8_t theOther)
{
    uint8_t* aData = reinterpret_cast <uint8_t*> (&theOther);
    Append (aData, 1);

    return *this;
}	

String& String::operator+= (double theOther)
{
    AppendNumber (theOther);
    return *this;
}

String& String::operator+= (bool theOther)
{
    const char* aString = theOther ? "true" : "false";
    (*this) += aString;
    return *this;
}

String String::operator+ (const char* theOther) const
{
    String aCopy (*this);
    aCopy += theOther;
    return aCopy;
}

String String::operator+ (const String& theOther) const
{
    String aCopy (*this);
    aCopy += theOther;

    return aCopy;
}

bool String::operator== (const String& theOther) const
{
    return CompareRange (theOther.myData, theOther.myLenght);
}

bool String::operator== (const char* theOther) const
{
    auto aData = reinterpret_cast <const uint8_t*> (theOther);
    int aLenght = static_cast <int> (strlen (theOther));
    
    return CompareRange (aData, aLenght + 1);
}

bool String::operator!= (const String& theOther) const
{
    return !(*this == theOther);
}

bool String::operator!= (const char* theOther) const
{
    return !(*this == theOther);
}

char& String::operator[] (int theIndex)
{
    return reinterpret_cast <char&> (myData [theIndex]);
}

char String::operator[] (int theIndex) const
{
    return reinterpret_cast <char&> (myData [theIndex]);
}

bool String::Empty() const
{
    return myLenght == 1;
}

size_t String::Hash() const
{
#ifndef __ORI_STM32LIB
    boost::hash <std::string> aHash;
    size_t aHashValue = aHash (Data());

    return aHashValue;
#else
    return 0;
#endif
}

const char* String::Data() const
{
    return reinterpret_cast <const char*> (myData);
}

char* String::Data()
{
    return reinterpret_cast <char*> (myData);
}

ORI_EXPORT int String::Length() const
{
    return myLenght - 1;
}

// private members =========================

void String::Construct (const char* theData)
{
    auto aData = reinterpret_cast <const uint8_t*> (theData);
    int aLenght = static_cast <int> (strlen (theData));
    
    myLenght = aLenght + 1;
    Allocate (myLenght);
    std::copy (aData, aData + aLenght, myData);
    myData[aLenght] = '\0';
}

void String::Construct (const uint8_t* theData, int theLenght)
{
    Allocate (theLenght);
    myLenght = theLenght;
    std::copy (theData, theData + theLenght, myData);
}

void String::Append (const uint8_t* theData, int theLenght)
{
    AppendRaw (theData, theLenght);
    int aNewLength = myLenght + 1;
    if (myCapacity < aNewLength) {
        Reallocate ((myCapacity + 1) * 2);
    }

    myData [myLenght] = '\0';
    myLenght++;
}

void String::AppendRaw (const uint8_t* theData, int theLenght)
{
    int aNewLength = myLenght + theLenght;
    if (myCapacity < aNewLength) {
        Reallocate ((myCapacity + theLenght) * 2);
    }

    std::copy (theData, theData + theLenght, &myData[myLenght - 1]);
    myLenght += (theLenght - 1);
}

bool String::CompareRange (const uint8_t* theOtherData, int theLenght) const
{
    if (myLenght != theLenght) {
        return false;
    }

    bool aRes = std::equal (theOtherData, theOtherData + theLenght, myData);
    return aRes;   
}

void String::Reallocate (int theNewCapacity)
{
    if (myLenght > 0) {
            
        if (myLenght >= theNewCapacity) {
            return;
        }
        uint8_t* aTmp = Allocator::ConstructArray <uint8_t> (myLenght);
        std::copy (myData, myData + myLenght, aTmp);
        Destroy();

        Allocate (theNewCapacity);
        std::copy (aTmp, aTmp + myLenght, myData);
	    Allocator::DeallocateArray <uint8_t> (aTmp);
    } else {

        Destroy();
        Allocate (theNewCapacity);
    }
    myCapacity = theNewCapacity;
}

void String::Allocate (int theSize)
{
	if (myCapacity > 0 && myData) {
		if (myCapacity >= theSize) {
			return;
		}
		Destroy();
	}
	myData = Allocator::ConstructArray <uint8_t> (theSize);
    myCapacity = theSize;
}

void String::Assign (uint8_t theElem, int theIndex)
{
    myData[theIndex] = theElem;
    myLenght++;
}

void String::Destroy()
{
	Allocator::DeallocateArray <uint8_t> (myData);
	myData = nullptr;
}
	

int String::Find (const char* theSubString, int theStartIndex)
{
	if (theStartIndex >= Length()) {
		return -1;	
	}
	
	return Helper::Find (myData + theStartIndex, myData + Length(), theSubString);
}
	
String String::Substring (int theStartIndex, int theLength)
{
	String aSubstring;
	aSubstring.Append (&myData[theStartIndex], theLength);
	return aSubstring;
}
	
int String::ToInt()
{
	return atoi (Data());
}	

}}
