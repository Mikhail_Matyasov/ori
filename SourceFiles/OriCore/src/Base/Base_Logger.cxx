#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Logger.hxx>

#include <Ori/Base_ILogger.hxx>
#include <Ori/Base_LoggerImpl.hxx>

namespace Ori {
namespace Base {

Logger::Logger (const ILogger* theLogger) :
    myImpl (std::make_shared <LoggerImpl> (theLogger))
{}

LoggerBuffer Logger::Verbose (const char* theFile, int theLine)
{
    return myImpl->Verbose (theFile, theLine);
}

LoggerBuffer Logger::Info (const char* theFile, int theLine)
{
    return myImpl->Info (theFile, theLine);
}

LoggerBuffer Logger::Warning (const char* theFile, int theLine)
{
    return myImpl->Warning (theFile, theLine);
}

LoggerBuffer Logger::Fail (const char* theFile, int theLine)
{
    return myImpl->Fail (theFile, theLine);
}

LoggerBuffer Logger::Instance (LogLevel theLevel, const char* theFile, int theLine)
{
    return LoggerImpl::Buffer (theLevel, theFile, theLine);
}

void Logger::SetDefaultLevel (LogLevel theLevel)
{
    LoggerImpl::SetDefaultLevel (theLevel);
}

LogLevel& Logger::Level()
{
    return myImpl->Level();
}

LogLevel Logger::Level() const
{
    return myImpl->Level();
}

bool Logger::IsStrict() const
{
    return myImpl->IsStrict();
}

bool& Logger::IsStrict()
{
    return myImpl->IsStrict();
}

Logger::Logger (const std::shared_ptr <LoggerImpl>& theImpl) :
    myImpl (theImpl)
{}

}}