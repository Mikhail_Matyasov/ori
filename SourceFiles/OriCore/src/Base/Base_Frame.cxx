#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Base_FrameImpl.hxx>

namespace Ori {
namespace Base {

Frame::Frame (int theWidth, int theHeight, Base::PixelFormat thePixelFormat) :
    myImpl  (std::make_shared <FrameImpl> (theWidth, theHeight, thePixelFormat))
{}

Frame::Frame() :
    myImpl (std::make_shared <FrameImpl>())
{}

Frame::Frame (const std::shared_ptr <FrameImpl>& theImpl) :
    myImpl (theImpl)
{}

Base::Vector <std::shared_ptr <ByteArrayView>>& Frame::Data()
{
    return myImpl->Data();
}

const Base::Vector <std::shared_ptr <ByteArrayView>>& Frame::Data() const
{
    return myImpl->Data();
}

Base::Vector <int>& Frame::Stride()
{
    return myImpl->Stride();
}

const Base::Vector <int>& Frame::Stride() const
{
    return myImpl->Stride();
}

int& Frame::Width()
{
   return myImpl->Width();
}

int Frame::Width() const
{
    return myImpl->Width();
}

int& Frame::Height()
{
    return myImpl->Height();
}

int Frame::Height() const
{
    return myImpl->Height();
}

Base::PixelFormat& Frame::PixelFormat()
{
    return myImpl->PixelFormat();
}

Base::PixelFormat Frame::PixelFormat() const
{
    return myImpl->PixelFormat();
}

Frame::operator bool() const
{
    return myImpl->operator bool();
}

Frame Frame::Clone() const
{
    return Frame (myImpl->Clone());
}

}}
