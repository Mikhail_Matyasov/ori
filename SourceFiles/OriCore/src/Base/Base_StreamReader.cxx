#include <Ori/Base_Pch.hxx>
#include <Ori/Base_StreamReader.hxx>

namespace Ori {
namespace Base {

StreamReader::StreamReader()
{}

StreamReader::StreamReader (const String& theFileName)
{
    InitIfNeed (theFileName);
}

std::shared_ptr <ByteArray> StreamReader::Read (const String& theFileName)
{
    InitIfNeed (theFileName);

    myStream->seekg (0, myStream->end);
    int aLenght = static_cast <int> (myStream->tellg());
    myStream->seekg (0, myStream->beg);

    auto anArray = std::make_shared <ByteArray>();
    anArray->Resize (aLenght);
    char* aData = reinterpret_cast <char*> (anArray->Data());
    myStream->read (aData, aLenght);

    return anArray;
}

void StreamReader::InitIfNeed (const String& theFileName)
{
    
    if (theFileName.Empty() && myFileName.Empty()) {
        throw Exception ("Filename must be provided.");
    }

    if (!theFileName.Empty() && theFileName != myFileName) {

        if (myStream) {
            myStream->close();
        }

        auto aFileName = theFileName.Data();
        myStream = std::make_shared <std::ifstream> (aFileName, std::ifstream::binary);
        myFileName = theFileName;
    }
}

}}