#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Exception.hxx>

namespace Ori {
namespace Base {

Exception::Exception (const Base::String& theMessage) :
    myMessage (theMessage)
{}

const Base::String& Exception::What() const
{
    return myMessage;
}

}}
