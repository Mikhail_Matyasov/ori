#include <Ori/Base_Pch.hxx>
#include <Ori/Base_LoggerBuffer.hxx>


#include <Ori/Base_ILogger.hxx>

namespace Ori {
namespace Base {

LoggerBuffer::LoggerBuffer() :
    myIsUseBuffer (false)
{}

LoggerBuffer::LoggerBuffer (bool theIsUseBuffer,
                            const ILogger* theIfs,
                            const char* theLevel,
                            const char* theFile,
                            int theLine) :
    myLoggerInterface (theIfs),
    myIsUseBuffer (theIsUseBuffer)
{
    if (myIsUseBuffer) {
        myBuffer += theLevel;
        myBuffer += theFile;
        myBuffer += ":";
        myBuffer += theLine;
        myBuffer += " ";
    }
}

LoggerBuffer::~LoggerBuffer()
{
    if (myIsUseBuffer) {
        myLoggerInterface->Print (myBuffer);
    }
}

LoggerBuffer& LoggerBuffer::operator<< (const String& theMessage)
{
    if (myIsUseBuffer) {
        myBuffer += theMessage;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (const char* theMessage)
{
    if (myIsUseBuffer) {
        myBuffer += theMessage;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (uint64_t theOther)
{
    if (myIsUseBuffer) {
        myBuffer += theOther;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (int64_t theOther)
{
    if (myIsUseBuffer) {
        myBuffer += theOther;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (int theOther)
{
    if (myIsUseBuffer) {
        myBuffer += theOther;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (unsigned int theOther)
{
    if (myIsUseBuffer) {
        myBuffer += theOther;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (float theOther)
{
    if (myIsUseBuffer) {
        myBuffer += theOther;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (double theOther)
{
    if (myIsUseBuffer) {
        myBuffer += theOther;
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (bool theOther)
{
    if (myIsUseBuffer) {
        myBuffer += theOther ? "true" : "false";
    }
    return *this;
}

LoggerBuffer& LoggerBuffer::operator<< (const ByteArrayView& theArray)
{
    if (myIsUseBuffer) {
        for (uint8_t aByte : theArray) {
            myBuffer += static_cast <char> (aByte);
        }
    }
    return *this;
}

}}
