#include <Ori/Base_Pch.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>

namespace Ori {
namespace Embed {

ESP8266Packet::ESP8266Packet() :
    myID (GetID())
{}

ESP8266Packet::ESP8266Packet (uint8_t theID) :
    myID (theID),
    myCommand (CommandType::InvalidCommand)
{}

uint8_t ESP8266Packet::myCurrentID = 0;

uint8_t ESP8266Packet::GetID()
{
    return myCurrentID++;
}

}}
