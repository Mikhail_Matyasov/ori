#ifndef _Ori_ImageRenderer_Image_HeaderFile
#define _Ori_ImageRenderer_Image_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/ImageRenderer_PixelFormat.hxx>

#include <vector>

namespace Ori {
namespace ImageRenderer {

class Image
{
public:
    ORI_EXPORT Image (const uint8_t* theY, int theYPitch,
                      const uint8_t* theU, int theUPitch,
                      const uint8_t* theV, int theVPitch,
                      PixelFormat theFormat,
                      int theWidth,
                      int theHeight);

    ORI_EXPORT Image (const uint8_t* theData,
                      PixelFormat theFormat,
                      int theWidth,
                      int theHeight);

    PixelFormat PixFormat() const { return myPixFormat; }
    int Width() const { return myWidth; }
    int Height() const { return myHeight; }
    const Base::Vector <Base::ByteArray>& Buffer() const { return myBuffer; }
    bool SupportedFormat() const { return myIsSupportedFormat; }
    bool IsPlanar() const { return myIsPlanarFormat; }
    const Base::Vector <int>& Linesize() const { return myLinesize; }

private:
    Image (PixelFormat theFormat,
           int theWidth,
           int theHeight,
           bool theIsPlanar);

private:
    Base::Vector <Base::ByteArray> myBuffer;
    PixelFormat myPixFormat;
    int myWidth;
    int myHeight;
    Base::Vector <int> myLinesize;
    bool myIsSupportedFormat;
    bool myIsPlanarFormat;
};

}}


#endif
