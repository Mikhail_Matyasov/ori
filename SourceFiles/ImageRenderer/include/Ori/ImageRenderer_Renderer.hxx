#ifndef _Ori_ImageRenderer_Renderer_HeaderFile
#define _Ori_ImageRenderer_Renderer_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/ImageRenderer_PixelFormat.hxx>

#include <functional>

struct SDL_Texture;
struct SDL_Renderer;
struct SDL_Window;

namespace boost {
class mutex;
}

namespace Ori {
namespace ImageRenderer {
class Image;

class Renderer
{
public:
    ORI_EXPORT Renderer (int theWidth, int theHeight, PixelFormat theFormat);
    ORI_EXPORT void Render (const Image& theImage);

private:
    void WaitEvent();

private:
    SDL_Texture* myTexture;
    SDL_Renderer* myRenderer;
    SDL_Window* myWindow;

};

}}


#endif
