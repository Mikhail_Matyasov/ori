#ifndef _Ori_ImageRenderer_PixelFormat_HeaderFile
#define _Ori_ImageRenderer_PixelFormat_HeaderFile

namespace Ori  {
namespace ImageRenderer {

enum class PixelFormat {
    Undefined,
    RGBA,
    YUV420
};

}}

#endif