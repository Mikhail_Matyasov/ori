#include <Ori/ImageRendererBase_Pch.hxx>
#include <Ori/ImageRenderer_Renderer.hxx>

#include <Ori/ImageRenderer_Image.hxx>

#include <boost/thread.hpp>

namespace Ori {
namespace ImageRenderer {
namespace {

SDL_PixelFormatEnum ToSDLFormat (PixelFormat theFormat)
{
    switch (theFormat) {
    case PixelFormat::YUV420:
        return SDL_PixelFormatEnum::SDL_PIXELFORMAT_IYUV;
    case PixelFormat::RGBA:
        return SDL_PixelFormatEnum::SDL_PIXELFORMAT_RGBA32;
    default:
        break;
    }

    return SDL_PixelFormatEnum::SDL_PIXELFORMAT_UNKNOWN;
}

}


Renderer::Renderer (int theWidth, int theHeight, PixelFormat theFormat) :
    myTexture (nullptr),
    myRenderer (nullptr),
    myWindow (nullptr)
{
    int aRes = SDL_Init (SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER);
    if (aRes < 0) {
        throw Base::Exception ("Can not init SDL");
    }

    myWindow = SDL_CreateWindow ("window",
                                 SDL_WINDOWPOS_CENTERED,
                                 SDL_WINDOWPOS_CENTERED,
                                 theWidth,
                                 theHeight,
                                 SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (!myWindow) {
        throw Base::Exception ("Can not allocate window");
    }

    myRenderer = SDL_CreateRenderer (myWindow, -1, SDL_RendererFlags::SDL_RENDERER_SOFTWARE);
    if (!myRenderer) {
        throw Base::Exception ("Can not allocate renderer");
    }

    auto aFormat = ToSDLFormat (theFormat);
    myTexture = SDL_CreateTexture (myRenderer,
                                   aFormat,
                                   SDL_TextureAccess::SDL_TEXTUREACCESS_STREAMING,
                                   theWidth,
                                   theHeight);
}

void Renderer::Render (const Image& theImage)
{
    if (!theImage.SupportedFormat()) {
        return;
    }

    if (theImage.IsPlanar()) {

        auto& aBuf1 = const_cast <Base::ByteArray&> (theImage.Buffer()[0]);
        auto& aBuf2 = const_cast <Base::ByteArray&> (theImage.Buffer()[1]);
        auto& aBuf3 = const_cast <Base::ByteArray&> (theImage.Buffer()[2]);

        SDL_UpdateYUVTexture (myTexture, nullptr,
                              aBuf1.Data(), theImage.Linesize()[0],
                              aBuf1.Data(), theImage.Linesize()[1],
                              aBuf1.Data(), theImage.Linesize()[2]);
    } else {

        auto& aBuf1 = const_cast <Base::ByteArray&> (theImage.Buffer()[0]);
        SDL_UpdateTexture (myTexture, nullptr, aBuf1.Data(), theImage.Width());
    }

    SDL_Rect aRect;
    aRect.x = 0;
    aRect.y = 0;
    aRect.w = theImage.Width();
    aRect.h = theImage.Height();
    
    SDL_RenderClear (myRenderer);
    SDL_RenderCopy (myRenderer, myTexture, nullptr, &aRect);
    SDL_RenderPresent (myRenderer);

    WaitEvent();
}

void Renderer::WaitEvent()
{
    SDL_Event anEvent;
    while (SDL_WaitEvent (&anEvent)) {

        if (anEvent.type == SDL_EventType::SDL_QUIT) {
            SDL_Quit();
            return;
        }
    }
}


}}