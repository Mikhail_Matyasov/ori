#include <Ori/ImageRendererBase_Pch.hxx>
#include <Ori/ImageRenderer_Image.hxx>

#include <sdl2/SDL_pixels.h>

namespace Ori {
namespace ImageRenderer {

Image::Image (const uint8_t* theY, int theYPitch,
              const uint8_t* theU, int theUPitch,
              const uint8_t* theV, int theVPitch,
              PixelFormat theFormat,
              int theWidth,
              int theHeight) : Image (theFormat, theWidth, theHeight, true)
{
    if (!myIsSupportedFormat) {
        return;
    }

    myLinesize[0] = theYPitch;
    myLinesize[1] = theUPitch;
    myLinesize[2] = theVPitch;
    int aYLenght = myWidth * myHeight;
    int aULenght = aYLenght / 4;

    auto Insert = [] (Base::ByteArray& theBuf, const uint8_t* theData, int theLenght) -> void {
        theBuf.Insert (theData, theLenght);
    };

    Insert (myBuffer[0], theY, aYLenght);
    Insert (myBuffer[1], theU, aULenght);
    Insert (myBuffer[2], theV, aULenght);
}

Image::Image (const uint8_t* theData,
              PixelFormat theFormat,
              int theWidth,
              int theHeight) :
    Image (theFormat, theWidth, theHeight, false)
{
    if (!myIsSupportedFormat) {
        return;
    }

    myBuffer[0].Insert (theData, myLinesize[0]);
}

Image::Image (PixelFormat theFormat, int theWidth, int theHeight, bool theIsPlanar) :
    myPixFormat (theFormat),
    myWidth (theWidth),
    myHeight (theHeight),
    myIsSupportedFormat (true),
    myIsPlanarFormat (theIsPlanar)
{
    switch (myPixFormat) {

    case PixelFormat::YUV420:
        if (theIsPlanar) {
            myLinesize.Resize (3);
            myBuffer.Resize (3);
        } else {
            myLinesize.Push (static_cast <int> (myWidth * myHeight * 1.5));
            myBuffer.Resize (1);
        }
        break;
    case PixelFormat::RGBA:

        myLinesize.Push (myWidth * myHeight * 4);
        myBuffer.Resize (1);
        break;
    default:
        break;
    }
}

}}
