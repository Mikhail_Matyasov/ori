#include <Ori/OriCoreTestLib_Pch.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Exception.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Base_ILogger.hxx>
#include <Ori/Base_Helper.hxx>

namespace Ori {
namespace OriCoreTestLib {
using namespace Ori::Base;

TEST (Base_String, CreateString)
{
    String aString ("String");
    EXPECT_TRUE (aString.Length() == 6);
    EXPECT_TRUE (aString == "String");
}

TEST (Base_String, HashFunction)
{
    String aString ("String");
    std::unordered_map <String, String> aMap;

    auto aKey = "Key";
    auto aValue = "Value";
    aMap.emplace (aKey, aValue);
    auto anIt = aMap.find (aKey);

    EXPECT_TRUE (anIt->second == aValue);
}

TEST (Base_String, GetData)
{
    String aString ("String");
    std::string aCopy = aString.Data();

    EXPECT_TRUE (aCopy == "String");
}

TEST (Base_String, IncreaseString)
{
    String aString ("String");
    std::string aCopy = aString.Data();
    EXPECT_TRUE (aCopy == "String");

    aString += "_Sufix";
    std::string aIncreased = aString.Data();

    EXPECT_TRUE (aIncreased == "String_Sufix");
}

TEST (Base_String, GetByIndex)
{
    String aString ("String");
    String aCopy;
    aCopy += aString[0];
    aCopy += aString[1];

    EXPECT_TRUE (aCopy == "St");
}

TEST (Base_String, GetDataTwice)
{
    String aString ("String");

    auto aData = aString.Data();
    auto aData2 = aString.Data();
    std::string aBuf = aData;
    std::string aBuf2 = aData2;

    EXPECT_TRUE (aBuf == aBuf2);

}

TEST (Base_String, AssignData)
{
    String aString ("String");
    aString = "New string";

    EXPECT_TRUE (aString == "New string");
}

TEST (Base_String, CreateFromInteger)
{
    String aString (12345);
    EXPECT_TRUE (aString == "12345");
}

TEST (Base_String, AssignWChar)
{
    String aString (L"String");
    aString = L"New string";

    EXPECT_TRUE (aString == L"New string");
}

TEST (Base_String, AppendStringTwice)
{
    String aString1 ("String1");
    String aString2 ("String2");
    aString1 += aString2;
    aString1 += aString2;

    EXPECT_TRUE (aString1 == "String1String2String2");
}

TEST (Base_ByteArray, CreateFromLocal)
{
    auto GetString = [&](const std::string& theString) -> Base::ByteArray {

        return Base::ByteArray (reinterpret_cast <const uint8_t*> (theString.c_str()),
                                static_cast <int> (theString.size()));
    };

    auto aString = GetString ("String");
    auto aSample = ByteArray (reinterpret_cast <const uint8_t*> ("String"), 6);
    EXPECT_TRUE (aString == aSample);
    EXPECT_TRUE (!aSample.Empty());
    EXPECT_TRUE (aSample.Lenght() == 6);

}

TEST (Base_ByteArray, CreateFromInitializerList)
{
    ByteArray anArray {1, 2, 3, 4};
    EXPECT_TRUE (anArray.Lenght() == 4);
    EXPECT_TRUE (anArray[0] == 1);
}

TEST (Base_Vector, AddElement)
{
    Vector <ByteArray> aVec;

    {
        ByteArray anArr1 = { 1, 2, 3, 4 };
        ByteArray anArr2 = { 5, 6, 7, 8 };

        aVec.Push (anArr1);
        aVec.Push (anArr2);
    }

    EXPECT_TRUE (aVec[0].Lenght() == 4);
    EXPECT_TRUE (aVec[1].Lenght() == 4);
    EXPECT_TRUE (aVec[0] ==  ByteArray ( {1, 2, 3, 4} ));
    EXPECT_TRUE (aVec[1] ==  ByteArray ( {5, 6, 7, 8} ));
}

TEST (Base_Vector, ResetSize)
{
    Vector <int> aVec {
        1, 2, 3, 4
    };
    aVec.Reset();
    aVec.Push (5);
    aVec.Push (6);
    aVec.Push (7);
    aVec.Push (8);

    EXPECT_TRUE (aVec[2] == 7);
}

TEST (Base_Vector, MakeCopy)
{
    ByteArray anArr = { 1, 2, 3, 4 };
    auto aCopy = anArr;
    EXPECT_TRUE (aCopy == anArr);
}

TEST (Base_Vector, Remove)
{
    ByteArray anArr = { 1, 2, 3, 4, 5, 6 };
    anArr.Remove (0);
    anArr.Remove (2);
    anArr.Remove (3);

    ByteArray anExpectedArray = { 2, 3, 5 };
    EXPECT_TRUE (anArr == anExpectedArray);
}

TEST (Base_VectorView, SimpleInheritance)
{
    VectorView <int>* aView = new VectorView <int>();
    auto aNull = dynamic_cast <Vector <int>*> (aView);

    EXPECT_TRUE (!aNull);

    Vector <int> aVec;
    aView = &aVec;
    auto aNotNull = dynamic_cast <Vector <int>*> (aView);

    EXPECT_TRUE (aNotNull);

    VectorView <ByteArray>* anArrayView = new VectorView <ByteArray>();
    Vector <ByteArray> anArray;
    anArrayView = &anArray;

    auto anNotNullArray = dynamic_cast <Vector <ByteArray>*> (anArrayView);
    EXPECT_TRUE (anNotNullArray);
}

TEST (Base_VectorView, SharedInheritance)
{
    std::shared_ptr <ByteArrayView> aBase = std::make_shared <ByteArray>();
    auto aDerived = dynamic_cast <ByteArray*> (aBase.get());
    EXPECT_TRUE (aDerived);

    aDerived->Push (5);
    EXPECT_TRUE ((*aBase)[0] == 5);

    std::shared_ptr <VectorView <ByteArrayView>> aBase2 = std::make_shared <Vector <ByteArrayView>>();
    auto aVec = dynamic_cast <Vector <ByteArrayView>*> (aBase2.get());
    EXPECT_TRUE (aVec->Empty());
}

TEST (Base_VectorView, CreateFromPointers)
{
    ByteArray anArray { 0, 4, 5 };
    ByteArrayView aView (anArray.begin(), anArray.end());

    EXPECT_TRUE (aView[1] == 4);

    ByteArrayView* aViewP = &anArray;

    EXPECT_TRUE ((*aViewP)[2] == 5);
}

TEST (Base_VectorView, CreateFromArrays)
{
    int aLenght = 10;
    int** anArray = new int* [aLenght];
    for (int i = 0; i < aLenght; i++) {

        anArray[i] = new int [aLenght];
        for (int j = 0; j < aLenght; j++) {
            anArray[i][j] = j;
        }
    }

    VectorView <int*> aVec (&anArray[0], &anArray[9]);
    EXPECT_TRUE (aVec[5][4] == 4);
}

TEST (Base_VectorView, CreateFromVector)
{
    int aLenght = 10;
    int** anArray = new int* [aLenght];
    for (int i = 0; i < aLenght; i++) {

        anArray[i] = new int [aLenght];
        for (int j = 0; j < aLenght; j++) {
            anArray[i][j] = j;
        }
    }

    VectorView <int> aSub [2] {
        VectorView <int> (&anArray[0][0], &anArray[0][9]),
        VectorView <int> (&anArray[3][0], &anArray[3][9])
    };

    VectorView <VectorView <int>> aVec (&aSub[0], &aSub[1]);
    auto aRes = aVec[1][2];
    EXPECT_TRUE (aRes == 2);
}

TEST (Base_VectorView, Contains)
{
    Vector <int> aVec { 1, 2, 3, 4, 5 };

    EXPECT_TRUE (aVec.Contains (2));
    EXPECT_TRUE (aVec.Contains (4));
}

TEST (Base_Exception, ThrowAndGetMessage)
{
    String aSample ("exception");
    String aMessage;

    try {
        throw Exception (aSample);
    } catch  (const Exception& e) {
        aMessage = e.What();
    }

    EXPECT_TRUE (aMessage == aSample);
}

class TestLogger : public ILogger
{
public:
    void Print (const String& theMessage) const override
    {
        myMessage += theMessage;
    }

    const String& Message() const
    {
        return myMessage;
    }

private:
    mutable String myMessage;
};

TEST (Base_Logger, PrintMessage)
{
    TestLogger aLogI1;
    Logger aLogger1 (&aLogI1);
    
    int aLine = __LINE__;
    ORI_LOG_INFO (aLogger1) << "INFO";

    String anExpectedMessage = String ("[INFO]OriCoreTestLib_OriCoreBase.cxx:") + String (aLine + 1) + " INFO";
    EXPECT_TRUE (aLogI1.Message() == anExpectedMessage);

    TestLogger aLogI2;
    Logger aLogger2 (&aLogI2);
    aLogger2.Level() = LogLevel::Fail;
    ORI_LOG_INFO (aLogger2) << "INFO";
    EXPECT_TRUE (aLogI2.Message().Empty());

    aLine = __LINE__;
    ORI_LOG_FAIL (aLogger2) << "INFO" << "::" << "FAIL";
    anExpectedMessage = String ("[FAIL]OriCoreTestLib_OriCoreBase.cxx:") + String (aLine + 1) + " INFO" + "::" + "FAIL";
    EXPECT_TRUE (aLogI2.Message() == anExpectedMessage);
}

TEST (Base_Helper, ToString)
{
    int aNumber = 12345;
    String aString = Helper::ToString (aNumber);

    EXPECT_TRUE (aString == "12345");
}

}}
