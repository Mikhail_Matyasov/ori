#include <Ori/OriCoreTestLib_Pch.hxx>
#include <Ori/Base_Allocator.hxx>

int main()
{
    testing::InitGoogleTest();
    //testing::GTEST_FLAG(filter) = "Base_String.AppendStringTwice";
    //testing::GTEST_FLAG(filter) = "Base_Logger.PrintMessage";
    
    RUN_ALL_TESTS();
}