#ifndef _Ori_Media_MotionDetectorImpl_HeaderFile
#define _Ori_Media_MotionDetectorImpl_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Media_FrameBuffer.hxx>
#include <Ori/Base_PixelFormat.hxx>
#include <opencv2/core.hpp>
#include <memory>

namespace Ori {
namespace Base {
class Frame;
}
namespace Codec {
class Scaler;
}
namespace Media {

// algorith: https://bitworks.software/en/high-speed-movement-detector-opencv-numba-numpy-python.html
class MotionDetectorImpl
{
public:
    MotionDetectorImpl (int theFPS);
    bool Detect (const Base::Frame& theFrame);

private:
    void InitScaler (int theWidth, int theHeight, Base::PixelFormat theFormat);
    bool ComputeDifference (const cv::Mat& theCurrentFrame, cv::Mat& theDif);
    bool FindGroups (cv::Mat& theSource, std::vector <std::vector <cv::Point>>& theTarget);

private:
    int myFPS;
    FrameBuffer myFrameBuffer;
    std::shared_ptr <Codec::Scaler> myScaler;
};

}}

#endif
