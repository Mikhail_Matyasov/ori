#ifndef _Ori_Media_MotionDetector_HeaderFile
#define _Ori_Media_MotionDetector_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <memory>

namespace Ori {
namespace Base {
class Frame;
}
namespace Media {
class MotionDetectorImpl;

// algorith: https://bitworks.software/en/high-speed-movement-detector-opencv-numba-numpy-python.html
class MotionDetector
{
public:
    ORI_EXPORT MotionDetector (int theFPS = 30);
    ORI_EXPORT bool Detect (const Base::Frame& theFrame);

private:
    std::shared_ptr <MotionDetectorImpl> myImpl;
};

}}

#endif
