#ifndef _Ori_Media_FrameBuffer_HeaderFile
#define _Ori_Media_FrameBuffer_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Vector.hxx>
#include <memory>
#include <opencv2/core.hpp>

namespace Ori {
namespace Media {

class FrameBuffer
{
public:
    FrameBuffer (int theLength);
    void Push (const cv::Mat& theFrame);
    const cv::Mat* LastFrame (int theIndex = 1) const;

    __ORI_PROPERTY (Base::Vector <cv::Mat>, DataBuffer)

private:
    int myCurrentIndex;
    int myLength;
};

}}

#endif
