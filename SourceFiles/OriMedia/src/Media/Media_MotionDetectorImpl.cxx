#include <Ori/MediaBase_Pch.hxx>
#include <Ori/Media_MotionDetectorImpl.hxx>

#include <Ori/Base_Frame.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/Codec_Scaler.hxx>
#include <Ori/Codec_ScalerParameters.hxx>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace Ori::Base;
using namespace Ori::Codec;

namespace Ori {
namespace Media {

namespace {

void Smooth (cv::Mat& theImage)
{
    cv::Size aGaussianKernel (5, 5);
    cv::GaussianBlur (theImage, theImage, aGaussianKernel, 0, 0);
}

void ConvertToGrayImage (cv::Mat& theImage, const PixelFormat& thePixelFormat)
{
    switch (thePixelFormat) {
    case PixelFormat::BGRA:
        cv::cvtColor (theImage, theImage, cv::COLOR_BGRA2GRAY);
        break;
    case PixelFormat::RGBA:
        cv::cvtColor (theImage, theImage, cv::COLOR_RGBA2GRAY);
        break;
    default:
        ORI_LOG_FAIL_DEFAULT() << "Unsupported pixel format";
        break;
    }
}

}

MotionDetectorImpl::MotionDetectorImpl (int theFPS) :
    myFPS (theFPS),
    myFrameBuffer (theFPS / 3)
{}

void MotionDetectorImpl::InitScaler (int theWidth, int theHeight, PixelFormat theFormat)
{
    if (myScaler) {
        return;
    }

    float aScale = 0.4;
    ScalerParameters aParams;
    aParams.SourceWidth() = theWidth;
    aParams.TargetWidth() = theWidth * aScale;
    aParams.SourceHeight() = theHeight;
    aParams.TargetHeight() = theHeight * aScale;
    aParams.SourcePixelFormat() = theFormat;
    aParams.TargetPixelFormat() = theFormat;
    myScaler = std::make_shared <Scaler> (aParams);
}

bool MotionDetectorImpl::ComputeDifference (const cv::Mat& theCurrentFrame, cv::Mat& theDif)
{
    const auto aLastFrame = myFrameBuffer.LastFrame (2);
    if (!aLastFrame) {
        myFrameBuffer.Push (theCurrentFrame);
        return false;
    }

    cv::absdiff (*aLastFrame, theCurrentFrame, theDif);

    // With large sensetivity value small object won't be shown on thresholded image
    const int aSensativity = 60;
    cv::absdiff (*aLastFrame, theCurrentFrame, theDif);
    Smooth (theDif);
    cv::threshold (theDif, theDif, aSensativity, 255, cv::ThresholdTypes::THRESH_BINARY);
    myFrameBuffer.Push (theCurrentFrame);
    return true;
}

void DeleteSmallContours (const std::vector <std::vector <cv::Point>>& theSource,
                          cv::Mat& theTarget)
{
    const double aMinContourArea = 15;
    cv::Scalar aBlack (0, 0, 0);
    for (const auto& aContour : theSource) {
        std::vector <cv::Point> anAprox;
        cv::approxPolyDP (aContour, anAprox, 5, true);
        double anArea = cv::contourArea (anAprox);
        if (anArea <= aMinContourArea) {
            cv::fillPoly (theTarget, aContour, aBlack);
        }
    }
}

bool MotionDetectorImpl::FindGroups (cv::Mat& theSource, std::vector <std::vector <cv::Point>>& theTarget)
{
    std::vector <std::vector <cv::Point>> aContours;
    std::vector <cv::Vec4i> aHirarchy;
    cv::findContours (theSource, aContours, aHirarchy,
                      cv::RetrievalModes::RETR_TREE,
                      cv::ContourApproximationModes::CHAIN_APPROX_SIMPLE);

    if (aContours.empty()) {
        return false;
    }
    DeleteSmallContours (aContours, theSource);

    cv::Mat aKernel = cv::getStructuringElement (cv::MORPH_ELLIPSE, cv::Point (10, 10));
    cv::morphologyEx (theSource, theSource, cv::MorphTypes::MORPH_CLOSE, aKernel);

    aHirarchy.clear();
    std::vector <std::vector <cv::Point>> aMorphContours;
    cv::findContours (theSource, aMorphContours, aHirarchy,
                      cv::RetrievalModes::RETR_TREE,
                      cv::ContourApproximationModes::CHAIN_APPROX_SIMPLE);

    if (aMorphContours.empty()) {
        return false;
    }

    std::sort (aMorphContours.begin(), aMorphContours.end(),
        [] (const std::vector <cv::Point>& theFirst, const std::vector <cv::Point>& theSecond) -> bool {
        double anArea1 = cv::contourArea (theFirst);
        double anArea2 = cv::contourArea (theSecond);
        if (anArea1 > anArea2) {
            return true;
        }
        return false;
    });


    for (size_t i = 0; i < aMorphContours.size(); i++) {
        if (i == 10) {
            break;
        }
        theTarget.push_back (aMorphContours [i]);
    }
    return true;
}

bool MotionDetectorImpl::Detect (const Frame& theFrame)
{
    InitScaler (theFrame.Width(), theFrame.Height(), theFrame.PixelFormat());

    auto aScaledFrame = myScaler->Scale (theFrame);
    uint8_t* aData = const_cast <uint8_t*> (aScaledFrame.Data()[0]->Data());
    cv::Mat anImage (aScaledFrame.Height(), aScaledFrame.Width(), CV_8UC4, aData, aScaledFrame.Stride()[0]);

    Smooth (anImage);
    ConvertToGrayImage (anImage, theFrame.PixelFormat());

    cv::Mat aDif;
    if (!ComputeDifference (anImage, aDif)) {
        return false;
    }

    std::vector <std::vector <cv::Point>> aGroups;
    if (!FindGroups (aDif, aGroups)) {
        return false;
    }

    return !aGroups.empty();
}

}}
