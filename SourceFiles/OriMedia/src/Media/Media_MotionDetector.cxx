#include <Ori/MediaBase_Pch.hxx>
#include <Ori/Media_MotionDetector.hxx>

#include <Ori/Media_MotionDetectorImpl.hxx>

namespace Ori {
namespace Media {

MotionDetector::MotionDetector (int theFPS) :
    myImpl (std::make_shared <MotionDetectorImpl> (theFPS))
{}

bool MotionDetector::Detect (const Base::Frame& theFrame)
{
    return myImpl->Detect (theFrame);  
}

}}
