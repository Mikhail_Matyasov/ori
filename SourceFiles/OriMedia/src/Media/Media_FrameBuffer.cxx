#include <Ori/MediaBase_Pch.hxx>
#include <Ori/Media_FrameBuffer.hxx>

namespace Ori {
namespace Media {

FrameBuffer::FrameBuffer (int theLength) :
    myDataBuffer (theLength),
    myCurrentIndex (0),
    myLength (0)
{
    myDataBuffer.Resize (theLength);    
}

void FrameBuffer::Push (const cv::Mat& theFrame)
{
    myDataBuffer [myCurrentIndex++] = theFrame;
    if (myCurrentIndex == myDataBuffer.Capacity()) {
        myCurrentIndex = 0;
    }
    if (myLength < myDataBuffer.Capacity()) {
        myLength++;
    }
}

const cv::Mat* FrameBuffer::LastFrame (int theIndex) const
{
    if (myLength < theIndex) {
        return nullptr;
    }

    int aLastIndex = myCurrentIndex - theIndex;
    if (aLastIndex < 0) {
        // plus negative value
        aLastIndex = myDataBuffer.Capacity() + aLastIndex;
    }

    return &myDataBuffer [aLastIndex];
}

}}
