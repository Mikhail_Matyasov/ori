﻿namespace Ori.Core
{
    public class Core_Base
    {
        public enum ErrorCodes {
            OK,
            SERVER_UNDEFINED,
            ACCESSPOINT_UNDEFINED
        }

        public static readonly char END_OF_FRAME = '#';
        public static readonly string BASE64_PREFIX = "data:image/jpeg;base64,";
        public static readonly string HUB_FRAME_MANAGER = REMOTEHOST + "/hub_frame_manager";
        public static readonly string JPEGS = ".jpegs";
        public static readonly string LOCALHOST = "https://localhost:44311";
        public static readonly string LOCALHOST_IIS = "http://localhost";
        public static readonly string PANASONICHCV380_SERVER = "192.168.43.226";
        public static readonly string REMOTEHOST = "http://51.104.193.142";
        public static readonly string VIRTUALMACHINE_IIS = "http://192.168.0.106";

        public static readonly string HOST_IP = "192.168.56.1";
        public static readonly string HOST_ACCESSPOINT = "192.168.56.0";
    }
}
