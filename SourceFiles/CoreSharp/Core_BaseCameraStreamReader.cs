﻿namespace Ori.Core {
    public abstract class Core_BaseCameraStreamReader
    {
        public delegate void OnReceivePacketDelegate (byte[] thePacket);
        public event OnReceivePacketDelegate OnReceivePacketEvent; 

        public delegate void OnReceiveFrameDelegate (byte[] theFrame);
        public event OnReceiveFrameDelegate OnReceiveFrameEvent;
        protected void OnReceiveFrameEvent_Run (byte[] theFrame)
        {
            OnReceiveFrameEvent?.Invoke (theFrame);
        }
        protected void OnReceivePacketEvent_Run (byte[] thePacket)
        {
            
            OnReceivePacketEvent?.Invoke (thePacket);
        }
        public abstract void ReadStream();

    }

}
