COMPILER=$1
MODE=$2
ARCH=$3
BUILD_TYPE=$4 # server, android, all

BUILD_DIR=D:/Ori/SourceFiles/win-build
QT_DIR=D:/programs/qt/5.14.2/msvc2017_64
THIRD_PARTY_HOME=D:/cygwin64/home/Ori/ThirdParty
SWIG=D:/cygwin64/home/Ori/DevTools/swig/swigwin-4.0.1/swig.exe
ARDUINO_HARDWARE="D:/ProgramFiles/Arduino/hardware"
QT_ANDROID_PACKAGE_SOURCE_DIR="\"android-package-source-directory\":\"D:/Ori/SourceFiles/OriMobile/android_sources\","

if [ $COMPILER == "vc142" ]; then
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    
    cmake -G "Visual Studio 16 2019" -DCOMPILER=$COMPILER \
                                     -DMODE=$MODE \
                                     -DARCH=$ARCH \
                                     -DBUILD_TYPE=$BUILD_TYPE \
                                     -DBUILD_DIR=$BUILD_DIR \
                                     -DQT_DIR=$QT_DIR \
                                     -DTHIRD_PARTY_HOME=$THIRD_PARTY_HOME \
                                     -DSWIG=$SWIG \
                                     -DARDUINO_HARDWARE=$ARDUINO_HARDWARE ../
fi
