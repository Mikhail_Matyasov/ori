using Xunit;
using Ori.Base;

namespace Ori.OriCoreSharpTestLib
{
public class OriCoreBase_ByteArray
{
    static OriCoreBase_ByteArray()
    {
       bool aRes =  DllHelper.Load ("OriCore.dll");
       Assert.True (aRes);
    }

    [Fact]
    public void GetData()
    {
        var anAray = new ByteArray();
        anAray.Push (1);
        anAray.Push (2);
        anAray.Push (3);
        anAray.Push (4);

        var aData = anAray.Data();
        Assert.True (aData[0] == 1);
        Assert.True (aData[1] == 2);
        Assert.True (aData[2] == 3);
        Assert.True (aData[3] == 4);
    }

    [Fact]
    public void CreateFromData()
    {
        byte[] aData = new byte[4];
        aData[0] = 1;
        aData[1] = 2;
        aData[2] = 3;
        aData[3] = 4;

        var anArray = new ByteArray (aData);
        var aStoredData = anArray.Data();

        for (int i = 0; i < aStoredData.Length; i++) {
            Assert.True (aStoredData[i] == aData[i]);
        }
    }
}

}
