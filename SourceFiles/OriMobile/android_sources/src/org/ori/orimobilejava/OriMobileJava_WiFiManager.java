package org.ori.orimobilejava;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import java.lang.String;
import java.util.List;

public class OriMobileJava_WiFiManager {

    private android.content.Context myAppContext;
    private WifiManager myManager;
    private android.app.Activity myMainActivity;

    public OriMobileJava_WiFiManager (android.content.Context theContext, android.app.Activity theActivity)
    {
        myAppContext = theContext;
        myManager = (WifiManager) myAppContext.getSystemService (theContext.WIFI_SERVICE);
        myMainActivity = theActivity;
    }

    public String AvaliableAccessPoints()
    {
        String anAPs = "";
        return anAPs;
    }

    public boolean IsCurrentSSID (String theSSID)
    {
        WifiInfo anInfo = myManager.getConnectionInfo();
        if (anInfo != null) {
            if (anInfo.getSSID() != null) {
                String anSSID = anInfo.getSSID();
                if (anSSID.contains (theSSID)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean IsConnected()
    { return false; }

    public void OpenWiFiSettings()
    {
        myMainActivity.startActivity (new android.content.Intent (android.provider.Settings.ACTION_WIFI_SETTINGS));
    }
}
