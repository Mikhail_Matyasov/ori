#include <Ori/MobileGui_Application.hxx>
#include <Ori/Base_Logger.hxx>

#include <QtCore/qcoreapplication.h>
#include <QtWidgets/qapplication.h>
#include <QtQml/qqmlapplicationengine.h>

int main (int argc, char** argv)
{
    QCoreApplication::setAttribute ( Qt::AA_EnableHighDpiScaling );
    QApplication app (argc, argv);

    Ori::Base::Logger::SetDefaultLevel (Ori::Base::LogLevel::Warning);
    QQmlApplicationEngine anEngine;
    Ori::MobileGui::Application anApp;
    anApp.Load (anEngine);

    return app.exec();
}
