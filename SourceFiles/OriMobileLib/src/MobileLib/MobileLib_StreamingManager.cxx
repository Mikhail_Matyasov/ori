#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_StreamingManager.hxx>

#include <Ori/Base_String.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Base_PixelFormat.hxx>
#include <Ori/MobileLib_FrameConverter.hxx>

#include <QtGui/qguiapplication.h>
#include <QtGui/qscreen.h>
#ifdef __ANDROID__
#include <QtAndroidExtras/qandroidjniobject.h>
#endif

#define MAX_NUMBER_IDLE_FRAMES 20
#define MIN_NUMBER_MOTION_FRAMES 5

using namespace Ori::Base;

namespace Ori {
namespace MobileLib {

void StreamingManager::ProcessFrame (QVideoFrame* theFrame)
{
    if (!theFrame) {
        return;
    }
    Frame aFrame = FrameConverter::Convert (*theFrame, myScreeenOrientation);
    bool anIsDetected = myDetector.Detect (aFrame);
    //if (!anIsDetected) {
    //    myNumberOfIdleFrames++;

    //    // idle detected
    //    if (myNumberOfIdleFrames == MAX_NUMBER_IDLE_FRAMES && myNumberOfMotionFrames != 0) {
    //        myNumberOfMotionFrames = 0;

    //        auto aState = myFrameEncoder.State();
    //        myFrameEncoder.StopEncoding(); // change state
    //        if (aState == FrameEncoder::StateType::Enabled) {
    //            myPacketTransmitter.StartTransmitting();
    //        }
    //        return;
    //    }

    //    if (myNumberOfMotionFrames == 0) {
    //        // returns if no motion frames was detected before
    //        return;
    //    }
    //    // go next, if at least one motion frame was detected before
    //} else {
    //    myNumberOfMotionFrames++;
    //}

    //myFrameEncoder.AddFrame (aFrame);

    //// motion detected
    //if (myNumberOfMotionFrames == MIN_NUMBER_MOTION_FRAMES) {
    //    myNumberOfIdleFrames = 0;
    //    myFrameEncoder.StartEncoding();
    //}
}

namespace {

void LockOrientation (Qt::ScreenOrientation theOrientation)
{
#ifdef __ANDROID__
    auto anActivity = QAndroidJniObject::callStaticObjectMethod ("org/qtproject/qt5/android/QtNative",
                                                                 "activity",
                                                                 "()Landroid/app/Activity;");
    if (anActivity.isValid()) {
        anActivity.callMethod <void> ("setRequestedOrientation"
                                       , "(I)V"
                                       , theOrientation);
    }
#endif
}
}

StreamingManager::StreamingManager (const StreamingManagerParameters& theParams) :
    myFrameEncoder (theParams.Width(), theParams.Height()),
    myPacketTransmitter (myFrameEncoder.Frames(), myFrameEncoder.Packets()),
    myNumberOfMotionFrames (0),
    myNumberOfIdleFrames (0)
{
    myParameters = theParams;
    
    auto aScreen = QGuiApplication::primaryScreen();
    myScreeenOrientation = aScreen->orientation();
    LockOrientation (myScreeenOrientation);
}

}}
