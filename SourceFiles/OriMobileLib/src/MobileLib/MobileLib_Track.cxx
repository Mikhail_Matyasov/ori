#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_Track.hxx>

#include <Ori/Codec_Decoder.hxx>
#include <Ori/Codec_DecoderParameters.hxx>
#include <Ori/Codec_CodecType.hxx>
#include <Ori/Base_Timer.hxx>

using namespace Ori::Codec;
using namespace Ori::Base;

namespace Ori {
namespace MobileLib {

void Track::RegisterObserver (Observer* theObserver)
{
    myObserver = theObserver;
}

void Track::Play (int theFps)
{
    std::thread T ([&, theFps]() -> void {
        int aDelayInMks = 1000000 / theFps;
        DecoderParameters aParams;
        aParams.Width() = myWidth;
        aParams.Height() = myHeigth;
        aParams.VideoCodecType() = CodecType::VP9;
        Decoder aDecoder (aParams);

        Timer aTimer;
        for (const auto& aPacket : myPackets) {

            aTimer.Timestamp();
            if (auto aFrame = aDecoder.Decode (aPacket)) {
                auto aDuration = aTimer.DurationSinceLastTimestamp();
                int anAdditionalDelay = aDelayInMks - aDuration.Microseconds();
                if (anAdditionalDelay > 0) {
                    Timer::Sleep (anAdditionalDelay);
                }
                myObserver->OnFrame (aFrame);
            }
        }
    });
}

}}
