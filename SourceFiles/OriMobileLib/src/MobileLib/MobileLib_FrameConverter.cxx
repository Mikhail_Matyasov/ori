#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_FrameConverter.hxx>

#include <QtGui/qopenglcontext.h>
#include <QtGui/qopenglfunctions.h>
#include <QtMultimedia/5.14.2/QtMultimedia/private/qvideoframe_p.h>
#include <QtGui/qguiapplication.h>
#include <QtGui/qscreen.h>
#include <QtGui/qimage.h>

using namespace Ori::Base;

namespace Ori {
namespace MobileLib {
namespace {

Frame CreateFrame (const QImage& theImage, PixelFormat theFormat)
{
    Frame aFrame (theImage.width(), theImage.height(), theFormat);
    aFrame.Data().Resize (1);
    aFrame.Data()[0] = std::make_shared <Base::ByteArray> (theImage.bits(), theImage.sizeInBytes());
    return aFrame;
}

Frame FinalizeConvertion (const QImage& theImage, const Base::PixelFormat& theFormat)
{
    auto aFormat = theImage.format();
    if (aFormat == QImage::Format::Format_RGB32) {
        return CreateFrame (theImage.mirrored(), theFormat);
    } else if (aFormat == QImage::Format::Format_ARGB32) {
        return CreateFrame (theImage, theFormat);
    }
    auto aRes = theImage.convertToFormat (QImage::Format::Format_ARGB32);
    return CreateFrame (aRes, theFormat);
}

Frame DoConvert (const QImage& theImage,
                 const Base::PixelFormat& theFormat,
                 Qt::ScreenOrientation theOrientation)
{

    if (theOrientation == Qt::ScreenOrientation::InvertedPortraitOrientation ||
        theOrientation == Qt::ScreenOrientation::PortraitOrientation) {

        QMatrix aRotation;
        aRotation.rotate (90);
        return FinalizeConvertion (theImage.transformed (aRotation), theFormat);       
    }

    return FinalizeConvertion (theImage, theFormat);
}
}

Frame FrameConverter::Convert (const QVideoFrame& theFrame, Qt::ScreenOrientation theOrientation)
{
    if (theFrame.handleType() == QAbstractVideoBuffer::NoHandle) {

        // Desktop
        QImage aRes = qt_imageFromVideoFrame (theFrame);
        if (aRes.isNull()) {
            return Frame();
        }
        return DoConvert (aRes, Base::PixelFormat::BGRA, theOrientation);
    }

    if (theFrame.handleType() == QAbstractVideoBuffer::GLTextureHandle ) {

        // Android
        QImage aRes (theFrame.width(), theFrame.height(), QImage::Format_ARGB32);
        GLuint aTextureID = static_cast <GLuint> (theFrame.handle().toInt());
        auto aGlFunc = QOpenGLContext::currentContext()->functions();

        GLuint aFrameBuffers;
        aGlFunc->glGenFramebuffers (1, &aFrameBuffers);

        aGlFunc->glBindFramebuffer (GL_FRAMEBUFFER, aFrameBuffers);
        aGlFunc->glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, aTextureID, 0);
        aGlFunc->glReadPixels (0, 0, theFrame.width(),
                               theFrame.height(),
                               GL_RGBA, GL_UNSIGNED_BYTE,
                               aRes.bits());

        GLint aParams;
        aGlFunc->glGetIntegerv (GL_FRAMEBUFFER_BINDING, &aParams);
        aGlFunc->glBindFramebuffer (GL_FRAMEBUFFER, static_cast <GLuint> (aParams));
        
        auto aFrame = DoConvert (aRes, PixelFormat::RGBA, theOrientation);
        aGlFunc->glDeleteFramebuffers (1, &aFrameBuffers);
        return aFrame;
    }
}

}}
