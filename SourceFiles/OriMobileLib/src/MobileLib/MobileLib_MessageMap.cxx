#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_MessageMap.hxx>


namespace Ori {
namespace MobileLib {

MessageMap* MessageMap::Instance()
{
    static auto aMap = new MessageMap();
    return aMap;
}

void MessageMap::Remove (const Base::String& theKey)
{
    myMap.erase (theKey);
}


}}
