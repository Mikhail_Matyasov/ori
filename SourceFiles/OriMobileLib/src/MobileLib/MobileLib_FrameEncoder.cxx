#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_FrameEncoder.hxx>

#include <Ori/Base_Assert.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/Codec_CodecType.hxx>
#include <Ori/Codec_CodecMode.hxx>
#include <Ori/Codec_EncoderParameters.hxx>
#include <Ori/Codec_ScalerParameters.hxx>
#include <Ori/Codec_Scaler.hxx>
#include <Ori/Codec_Encoder.hxx>

using namespace Ori::Base;
using namespace Ori::Codec;

namespace Ori {
namespace MobileLib {

FrameEncoder::FrameEncoder (int theFrameWidth, int theFrameHeigth) :
    myState (StateType::Disabled)
{
    EncoderParameters aParams;
    aParams.Width() = theFrameWidth;
    aParams.Height() = theFrameHeigth;
    aParams.VideoCodecType() = CodecType::VP9;
    aParams.Mode() = CodecMode::MaximumCompression;

    myEncoder = std::make_shared <Encoder> (aParams);
}

void FrameEncoder::AddFrame (const Base::Frame& theFrame)
{
    myFrames.PushBack (theFrame);
}

void FrameEncoder::StartEncoding()
{
    myState = StateType::Enabled;
    Assert::True (!myFrames.Container().empty());

    const auto& aFirstFrame = myFrames.Container().front();
    InitScaler (aFirstFrame.Width(), aFirstFrame.Height(), aFirstFrame.PixelFormat());

    std::thread T ([&] () -> void {

        while (true) {
            while (myFrames.Container().empty()) {
                if (myState == StateType::ShutDown) {
                    // stop encoding if all frames processed
                    myState = StateType::Disabled;
                    std::terminate();
                }
                Timer::Sleep (10000);
            }

            const auto& aFrame = myFrames.Container().front();
            const auto& aYuvFrame = myScaler->Scale (aFrame);
            if (auto aPacket = myEncoder->Encode (aYuvFrame)) {
                myPackets.PushBack (aPacket.Clone());
                myFrames.PopFront();
            }
        }

    });

    T.detach();
}

void FrameEncoder::StopEncoding()
{
    if (myState == StateType::Disabled) {
        myFrames.Clear();
        return;
    }

    myState = StateType::ShutDown;
}

void FrameEncoder::InitScaler (int theFrameWidth, int theFrameHeigth, Base::PixelFormat theSourceFormat)
{
    if (!myScaler) {
        ScalerParameters aParams;
        aParams.SourceWidth() = theFrameWidth;
        aParams.TargetWidth() = theFrameWidth;
        aParams.SourceHeight() = theFrameHeigth;
        aParams.TargetHeight() = theFrameHeigth;
        aParams.SourcePixelFormat() = theSourceFormat;
        aParams.TargetPixelFormat() = PixelFormat::YUV420;

        myScaler = std::make_shared <Scaler> (aParams);
    }
}

}}
