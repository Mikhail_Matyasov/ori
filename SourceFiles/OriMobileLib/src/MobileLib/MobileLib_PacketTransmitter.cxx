#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_PacketTransmitter.hxx>

#include <Ori/Base_Assert.hxx>
#include <Ori/MobileEmbed_Device.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/Base_Frame.hxx>

using namespace Ori::Base;
using namespace Ori::MobileEmbed;

namespace Ori {
namespace MobileLib {

PacketTransmitter::PacketTransmitter (FrameBufferType theFrames, PacketBufferType thePackets) :
    myFrames (theFrames),
    myPackets (thePackets)
{
    Device::Instance()->RegisterObserver (this);
}

void PacketTransmitter::StartTransmitting()
{
    Assert::True (!myPackets.Container().empty());
    SendNextPacket();
}

void PacketTransmitter::OnPacketSent()
{
    if (!myPackets.Container().empty()) {
        SendNextPacket();
        return;
    }

    if (myFrames.Container().empty()) {
        myObserver->OnPacketsSent();
        return;
    }

    std::thread T ([&] () -> void {
        while (true) {
            if (!myPackets.Container().empty()) {
                SendNextPacket();
                return;
            }
            Timer::Sleep (2000000); // wait 2 sec
        }
    });
    T.detach();
}

void PacketTransmitter::SendNextPacket()
{
    auto aDevice = Device::Instance();
    const auto& aPacket = myPackets.Container().front();
    aDevice->SendVideoData (*aPacket.Data());
    myPackets.PopFront();
}

}}
