#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_StreamingManagerParameters.hxx>

namespace Ori {
namespace MobileLib {

StreamingManagerParameters::StreamingManagerParameters() :
    myWidth (0),
    myHeight (0)
{}

StreamingManagerParameters::StreamingManagerParameters (const StreamingManagerParameters& theParams) :
    myWidth (theParams.myWidth),
    myHeight (theParams.myHeight)
{}

}}
