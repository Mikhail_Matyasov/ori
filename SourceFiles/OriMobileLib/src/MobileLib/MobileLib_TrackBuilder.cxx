#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileLib_TrackBuilder.hxx>

#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/Base_Assert.hxx>

using namespace Ori::Base;
using namespace Ori::Codec;

namespace Ori {
namespace MobileLib {

void TrackBuilder::AddPacketData (const Base::ByteArrayView& thePacketData)
{
    myTrackBuffer.Push (thePacketData);
    myPacketsLength.Push (thePacketData.Lenght());

    if (myTrackLength == myTrackBuffer.Lenght()) {
        myObserver->OnTrackReady (Build());
        myPacketsLength.Reset();
        myTrackBuffer.Reset();
        myTrackLength = 0;
    }
}

void TrackBuilder::SetTrackHeader (const Base::ByteArrayView& theHeader)
{
    TypeBuffer <int> aTrackLength (theHeader.Data(), theHeader.Lenght());
    myTrackLength = aTrackLength.Value;
}

void TrackBuilder::RegisterObserver (const std::shared_ptr <Observer>& theTrack)
{
    myObserver = theTrack;
}

namespace {

typedef std::unordered_map <int, std::shared_ptr <ByteArray>> PacketType;
void DoAddPacket (const PacketType& theSource, Vector <Packet>& theTarget)
{
    auto aData = std::make_shared <ByteArray>();
    for (size_t i = 0; i < theSource.size(); i++) {
        auto anIt = theSource.find (static_cast <int> (i));
        Assert::True (anIt != theSource.end());
        aData->Push (*anIt->second);
    }

    Packet aPacket;
    aPacket.Data() = aData;
    theTarget.Push (aPacket);
}
}

std::shared_ptr <Track> TrackBuilder::Build()
{
    auto aTrack = std::make_shared <Track>();
    int aCurrentPacketStart = 0;
    std::unordered_map <int, std::shared_ptr <PacketType>> aPacketDataMap;
    for (int aPacketLength : myPacketsLength) {

        const uint8_t* aPacketStart = &myTrackBuffer [aCurrentPacketStart];
        TypeBuffer <uint16_t> aPacketNumber (aPacketStart);
        TypeBuffer <uint8_t> aSubPacketNumber (aPacketStart + 2);

        auto anIt = aPacketDataMap.find (aPacketNumber.Value);
        std::shared_ptr <PacketType> aPacket = nullptr;
        if (anIt != aPacketDataMap.end()) {
            aPacket = anIt->second;
        } else {
            aPacketDataMap.emplace (aPacketNumber.Value, std::make_shared <PacketType>());
            aPacket = aPacketDataMap [aPacketNumber.Value];
        }

        int aSubpacketLength = aPacketLength - 3;
        
        auto aSubPacketData = std::make_shared <ByteArray>();
        aSubPacketData->Push (aPacketStart + 3, aSubpacketLength);
        aPacket->emplace (aSubPacketNumber.Value, aSubPacketData);
    }

    Vector <std::shared_ptr <PacketType>> aSortedPackeContexts;
    for (size_t i = 0; i < aPacketDataMap.size(); i++) {
        aSortedPackeContexts.Push (aPacketDataMap [static_cast <int> (i)]);
    }

    for (const auto& aContext : aSortedPackeContexts) {
        DoAddPacket (*aContext, aTrack->Packets());
    }
    
    return aTrack;
}

}}
