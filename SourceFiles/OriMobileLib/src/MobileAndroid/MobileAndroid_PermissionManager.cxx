#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileAndroid_PermissionManager.hxx>

#include <QtAndroidExtras/QtAndroid>
#include <QtCore/qstring.h>
#include <QtCore/qoperatingsystemversion.h>

namespace Ori {
namespace MobileAndroid {
namespace  {
void RequestPermission (const QString& thePermission)
{
    auto  aResult = QtAndroid::checkPermission (thePermission);
    if(aResult == QtAndroid::PermissionResult::Denied){

        auto aMap = QtAndroid::requestPermissionsSync ((QStringList ({thePermission})));
        auto anIt = aMap.find (thePermission);
        if (anIt != aMap.end()) {
            if (anIt.value() == QtAndroid::PermissionResult::Denied) {
                // Handle reject
            }
        }
    }
}
}

void PermissionManager::Request()
{
    auto aDeviceVersion = QOperatingSystemVersion::current();
    if (aDeviceVersion > QOperatingSystemVersion (QOperatingSystemVersion::Android, 6)) {

        RequestPermission ("android.permission.ACCESS_WIFI_STATE");
        RequestPermission ("android.permission.CHANGE_WIFI_STATE");
        RequestPermission ("android.permission.ACCESS_NETWORK_STATE");
        RequestPermission ("android.permission.ACCESS_COARSE_LOCATION");
        RequestPermission ("android.permission.ACCESS_FINE_LOCATION");
        RequestPermission ("android.permission.CHANGE_NETWORK_STATE");
    }
}

}}
