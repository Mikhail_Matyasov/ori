#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileEmbed_Device.hxx>

#include <Ori/Base_Logger.hxx>
#include <Ori/Net_RTPEndPoint.hxx>
#include <Ori/Net_TcpObserver.hxx>
#include <Ori/MobileEmbed_EmbedCommand.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/Base_Helper.hxx>
#include <Ori/Net_NetworkInfo.hxx>
#include <Ori/Net_IPEndPoint.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/Base_Assert.hxx>
#include <Ori/Embed_Macros.hxx>

#include <QtCore/qdatastream.h>

using namespace Ori::Base;
using namespace Ori::Embed;

namespace Ori {
namespace MobileEmbed {

const char* theHotSpotPassword = "";
const char* theHotSpotIP = "192.168.0.100";
const int theHotSpotPort = 333;

Device::Device() :
    myObserver (nullptr),
    myCurCommandID (0)
{
    myTcpClient.RegisterObserver (this);
}

void Device::AddCommand (const EmbedCommand& theCommand)
{
    if (!theCommand.OnCompleted()) {
        return;
    }
    auto anIt = myCommandsCallbackMap.find (theCommand.ID());
    if (anIt != myCommandsCallbackMap.end()) {
        myCommandsCallbackMap.erase (anIt);
    }
    myCommandsCallbackMap.emplace (theCommand.ID(), theCommand.OnCompleted());
}

void Device::OpenWiFiSettings()
{
    Net::WiFiHotSpot::OpenWiFiSettings();
}

bool Device::ConnectSocket()
{
    auto anEndPoint = Net::IPEndPoint (theHotSpotIP, theHotSpotPort);
    return myTcpClient.Connect (anEndPoint);
}

bool Device::IsSocketReady()
{
    return myTcpClient.Connected();
}

void Device::OnPacket (const Base::ByteArrayView& thePacket)
{
    ORI_LOG_DEBUG((*myLogger)) << "On Packet";
    ProcessCommand (thePacket.Data(), thePacket.Lenght());
}

void Device::InvokeCallback (const Base::ByteArrayView& theData, uint8_t theID, bool theDeleteCallback)
{
    const auto& anIt = myCommandsCallbackMap.find (theID);
    if (anIt != myCommandsCallbackMap.end()) {

        if (anIt->second) {
            anIt->second (theData, theID);
        }

        if (theDeleteCallback) {
            myCommandsCallbackMap.erase (anIt);
        }
    }
}

void Device::ProcessCommand (const uint8_t* theData, int theDataLength)
{
    EmbedCommand aCmd (theData, theDataLength);
    if (!aCmd.IsValid()) {
        ORI_LOG_DEBUG((*myLogger)) << "Command not extracted";
        return; // incorrect input data
    }

    if (aCmd.Command() == CommandType::Reply) {

        ORI_LOG_DEBUG((*myLogger)) << "Reply received";
        InvokeCallback (aCmd.Data(), aCmd.ID());
    } else if (aCmd.Command() == CommandType::TcpClientFreeBufferSpaceAvaliable) {

        InvokeCallback (aCmd.Data(), aCmd.ID(), false);
    } else if (aCmd.Command() == CommandType::SX1278ListenerOnPacket) {

        ORI_LOG_DEBUG((*myLogger)) << aCmd.Data();
    } else if (aCmd.Command() == CommandType::SX1278ListenerOnAcknowledgement) {

        ORI_LOG_DEBUG((*myLogger)) << "Tcp packet Acknowledgement";
    } else if (aCmd.Command() == CommandType::SX1278ListenerOnVideoData) {

        ORI_LOG_DEBUG((*myLogger)) << "video data";
    } else {

        ORI_LOG_DEBUG((*myLogger)) << "other received";
    }
}

void Device::Send (const EmbedCommand& theCommand)
{
    while (!IsSocketReady()) {
        Base::Timer::Sleep (1000);
    }

    Base::ByteArray aMessage;
    theCommand.ToByteArray (aMessage);
    AddCommand (theCommand);

    Timer::Sleep (20000);
    myTcpClient.Send (aMessage.Data(), aMessage.Lenght());
    ORI_LOG_DEBUG((*myLogger)) << "Command sent. ID = " << theCommand.ID();
}

void Device::SendNextSubPacket (const Base::ByteArrayView& theData, uint8_t theID)
{
    ORI_LOG_DEBUG_DEFAULT() << "Sending next packet";

    const auto& anIt = myBufferMap.find (theID);
    Assert::False (anIt == myBufferMap.end());

    Assert::True (theData.Lenght() == sizeof (uint16_t));
    TypeBuffer <uint16_t> aTBuffer (theData.Data(), theData.Lenght());

    auto& aSendBuffer = *anIt->second;
    int aBufferLength = __ORI_MAX_ESP8266_DATA_SIZE + aSendBuffer.Buffer().Lenght(); // source buffer length

    aSendBuffer.DataStart() += static_cast <int> (aTBuffer.Value);
    if (aSendBuffer.DataStart() >= aBufferLength) {
        myBufferMap.erase (theID);
        myCommandsCallbackMap.erase (theID);
        if (myObserver) {
            myObserver->OnPacketSent();
        }
        return;
    }

    EmbedCommand aCommand (theID);
    aCommand.Command() = CommandType::TcpClientSendVideoData;

    const uint8_t* aDataStart = &aSendBuffer.Buffer() [aSendBuffer.DataStart() - __ORI_MAX_ESP8266_DATA_SIZE];
    int aBufferRemainder = aBufferLength - aSendBuffer.DataStart();

    int aDataLength = __ORI_MAX_ESP8266_DATA_SIZE;
    if (aBufferRemainder < __ORI_MAX_ESP8266_DATA_SIZE) {
        aDataLength = aBufferRemainder;
    }

    aCommand.Data() = ByteArrayView (aDataStart, aDataLength);
    Send (aCommand);
}

void Device::SendVideoData (const uint8_t* theData, int theLength)
{
    EmbedCommand aCommand;
    aCommand.Command() = CommandType::TcpClientSendVideoData;

    if (__ORI_MAX_ESP8266_DATA_SIZE >= theLength) {
        aCommand.Data() = ByteArrayView (theData, theLength);
    } else {
        // save data
        auto aSendBuffer = std::make_shared <SendBuffer>();
        aSendBuffer->DataStart() = 0;
        aSendBuffer->Buffer().Push (theData + __ORI_MAX_ESP8266_DATA_SIZE, theLength - __ORI_MAX_ESP8266_DATA_SIZE);
        myBufferMap.emplace (aCommand.ID(), aSendBuffer);

        // send first subpacket
        aCommand.Data() = ByteArrayView (theData, __ORI_MAX_ESP8266_DATA_SIZE);
        aCommand.OnCompleted() = [&] (const Base::ByteArrayView& theData, int theID) -> void {
            SendNextSubPacket (theData, theID);
        };
    }

    Send (aCommand);
}


void Device::SendPacket (const uint8_t* theData,
                         int theLength,
                         const CallBackType& theCallback,
                         CommandType theCommandType)
{
    Assert::True (theLength <= __ORI_EMBED_SX1278_MAX_DATA_LENGTH);

    EmbedCommand aCommand;
    aCommand.Command() = theCommandType;
    aCommand.Data() = ByteArrayView (theData, theLength);
    aCommand.OnCompleted() = theCallback;
    Send (aCommand);
}

void Device::SendUdpPacket (const uint8_t* theData, int theLength, const CallBackType& theCallback)
{
    SendPacket (theData, theLength, theCallback, CommandType::UdpClientSendPacket);
}

void Device::SendTcpPacket (const uint8_t* theData, int theLength, const CallBackType& theCallback)
{
    SendPacket (theData, theLength, theCallback, CommandType::TcpClientSendPacket);
}

void Device::SendVideoData (const ByteArrayView& theData)
{ SendVideoData (theData.Data(), theData.Lenght()); }

Device* Device::Instance()
{
    static Device* anInstance = new Device();
    return anInstance;
}

}}
