#include <Ori/MobileLibBase_Pch.hxx>
#include <Ori/MobileEmbed_EmbedCommand.hxx>

#include <Ori/Base_Helper.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/Embed_CommandDef.hxx>

using namespace Ori::Base;
using namespace Ori::Embed;

namespace Ori {
namespace MobileEmbed {

EmbedCommand::EmbedCommand() : ESP8266Packet()
{}

EmbedCommand::EmbedCommand (uint8_t theID) : ESP8266Packet (theID)
{}

EmbedCommand::EmbedCommand (const uint8_t* theData, int theDataLength)
{
    if (theDataLength < 4) { /*ID + Command + Simple data*/
		return;
	}

    myID = theData[0];
    TypeBuffer <uint16_t> aBuffer (&theData [1], sizeof (uint16_t));
    myCommand = static_cast <CommandType> (aBuffer.Value);
    myData.SetBegin (&theData [3]);
    myData.SetEnd (theData + theDataLength);
}

void EmbedCommand::ToByteArray (Base::ByteArray& theTarget) const
{
	theTarget.Reset();
    theTarget.Push (myID);
    
    TypeBuffer <uint16_t> aBuffer (static_cast <uint16_t> (myCommand));
    theTarget.Push (aBuffer.Bytes, sizeof (uint16_t));

    if (!myData.Empty()) {
        TypeBuffer <uint16_t> aDataLength (static_cast <uint16_t> (myData.Lenght()));
        theTarget.Push (aDataLength.Bytes, 2);
        theTarget.Push (myData);
    }
}

bool EmbedCommand::IsValid()
{
    return myCommand != CommandType::InvalidCommand;
}

}}
