#ifndef _Ori_MobileLib_TrackBuilder_HeaderFile
#define _Ori_MobileLib_TrackBuilder_HeaderFile

#include <Ori/Base_ByteArray.hxx>
#include <Ori/MobileLib_Track.hxx>
#include <memory>

namespace Ori {
namespace MobileLib {

class TrackBuilder
{
public:
    class Observer
    {
    public:
        virtual void OnTrackReady (const std::shared_ptr <Track>& theTrack) = 0;
    };

    void AddPacketData (const Base::ByteArrayView& thePacketData);
    void SetTrackHeader (const Base::ByteArrayView& theHeader);
    void RegisterObserver (const std::shared_ptr <Observer>& theTrack);

private:
    std::shared_ptr <Track> Build();

private:
    std::shared_ptr <Observer> myObserver;
    int myTrackLength;
    Base::ByteArray myTrackBuffer;
    Base::Vector <int> myPacketsLength;
};

}}

#endif
