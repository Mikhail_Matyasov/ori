#ifndef _Ori_MobileLib_FrameEncoder_HeaderFile
#define _Ori_MobileLib_FrameEncoder_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Base_PixelFormat.hxx>
#include <Ori/MobileLib_ThreadSafeBuffer.hxx>
#include <memory>

namespace Ori {
namespace Codec {
class Scaler;
class Encoder;
}
namespace MobileLib {

class FrameEncoder
{
public:
    enum class StateType
    {
        Disabled,
        Enabled,
        ShutDown
    };

    FrameEncoder (int theFrameWidth, int theFrameHeigth);

    void AddFrame (const Base::Frame& theFrame);

    void StartEncoding();
    void StopEncoding();

    __ORI_PROPERTY (ThreadSafeBuffer <Codec::Packet>, Packets);
    __ORI_PROPERTY (ThreadSafeBuffer <Base::Frame>, Frames);
    __ORI_PROPERTY (StateType, State);

private:
    void InitScaler (int theFrameWidth, int theFrameHeigth, Base::PixelFormat theSourceFormat);
    
private:
    std::shared_ptr <Codec::Scaler> myScaler;
    std::shared_ptr <Codec::Encoder> myEncoder;
};

}}

#endif
