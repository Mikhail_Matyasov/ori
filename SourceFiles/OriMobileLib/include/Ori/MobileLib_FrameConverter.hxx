#ifndef _Ori_MobileLib_FrameConverter_HeaderFile
#define _Ori_MobileLib_FrameConverter_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Frame.hxx>

#include <QtCore/qnamespace.h>

class QVideoFrame;

namespace Ori {
namespace MobileLib {

class FrameConverter
{
public:
    static Base::Frame Convert (const QVideoFrame& theFrame, Qt::ScreenOrientation theOrientation);
};

}}

#endif
