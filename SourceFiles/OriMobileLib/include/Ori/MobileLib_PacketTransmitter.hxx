#ifndef _Ori_MobileLib_PacketTransmitter_HeaderFile
#define _Ori_MobileLib_PacketTransmitter_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/MobileLib_ThreadSafeBuffer.hxx>
#include <Ori/MobileEmbed_Device.hxx>

namespace Ori {
namespace Base {
class Frame;
}
namespace Codec {
class Packet;
}
namespace MobileLib {

class PacketTransmitter : public MobileEmbed::Device::Observer
{
public:
    class Observer
    {
    public:
        virtual void OnPacketsSent() = 0;
    };

    typedef const ThreadSafeBuffer <Base::Frame>& FrameBufferType; 
    typedef ThreadSafeBuffer <Codec::Packet>& PacketBufferType;
    PacketTransmitter (FrameBufferType theFrames, PacketBufferType thePackets);
    void StartTransmitting();
    void RegisterObserver (Observer* theObserver)
    { myObserver = theObserver; }

    void OnPacketSent() override;

private:
    void SendNextPacket();

private:
    FrameBufferType myFrames;
    PacketBufferType myPackets;
    Observer* myObserver;
};

}}

#endif
