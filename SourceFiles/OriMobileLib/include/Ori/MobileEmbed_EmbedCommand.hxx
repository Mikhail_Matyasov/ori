#ifndef _Ori_MobileEmbed_EmbedCommand_HeaderFile
#define _Ori_MobileEmbed_EmbedCommand_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>

namespace Ori {
namespace MobileEmbed {

class EmbedCommand : public Embed::ESP8266Packet
{
public:
    typedef std::function <void (const Base::ByteArrayView&, uint8_t)> CallbackType;
    EmbedCommand();
    EmbedCommand (uint8_t theID);
    EmbedCommand (const uint8_t* theData, int theDataLength);
    void ToByteArray (Base::ByteArray& theTarget) const;
    bool IsValid();

    __ORI_PROPERTY (CallbackType, OnCompleted);
};

}}

#endif
