#ifndef _Ori_MobileLib_StreamingManager_HeaderFile
#define _Ori_MobileLib_StreamingManager_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Media_MotionDetector.hxx>
#include <Ori/MobileLib_StreamingManagerParameters.hxx>
#include <Ori/MobileLib_FrameEncoder.hxx>
#include <Ori/MobileLib_PacketTransmitter.hxx>

#include <QtCore/qnamespace.h>
#include <functional>

class QVideoFrame;
class QImage;

namespace Ori {
namespace Base {
enum class PixelFormat;
}
namespace MobileLib {

class StreamingManager
{
public:
    ORI_EXPORT void ProcessFrame (QVideoFrame* theFrame);

    ORI_EXPORT StreamingManager (const StreamingManagerParameters& theParams);

private:
    Media::MotionDetector myDetector;
    FrameEncoder myFrameEncoder;
    PacketTransmitter myPacketTransmitter;
    StreamingManagerParameters myParameters;
    Qt::ScreenOrientation myScreeenOrientation;
    uint64_t myNumberOfMotionFrames;
    uint64_t myNumberOfIdleFrames;
};

}}

#endif
