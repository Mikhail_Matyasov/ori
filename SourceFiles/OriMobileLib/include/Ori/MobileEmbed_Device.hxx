#ifndef _Ori_MobileEmbed_Device_HeaderFile
#define _Ori_MobileEmbed_Device_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Net_TcpClient.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Net_TcpObserver.hxx>
#include <Ori/MobileEmbed_EmbedCommand.hxx>
#include <Ori/Embed_CommandDef.hxx>

#include <memory>

class QTcpSocket;

namespace Ori {
namespace MobileEmbed {
class EmbedCommand;
using CallBackType = EmbedCommand::CallbackType;

class Device : public Net::TcpObserver
{
public:
    class SendBuffer
    {
    public:
        __ORI_PROPERTY (int, DataStart);
        __ORI_PROPERTY (Base::ByteArray, Buffer);
    };

    class Observer
    {
    public:
        virtual void OnPacketSent() = 0;
    };

    static void OpenWiFiSettings();
    bool ConnectSocket();
    bool IsSocketReady();
    void OnPacket (const Base::ByteArrayView& thePacket) override;
    void Send (const EmbedCommand& theCommand);
    void SendVideoData (const uint8_t* theData, int theLength);
    void SendUdpPacket (const uint8_t* theData, int theLength, const CallBackType& theCallback = nullptr);
    void SendTcpPacket (const uint8_t* theData, int theLength, const CallBackType& theCallback = nullptr);
    void SendVideoData (const Base::ByteArrayView& theData);
    void RegisterObserver (Observer* theObserver) { myObserver = theObserver; }
    static Device* Instance();
    __ORI_PROPERTY (std::shared_ptr <Base::Logger>, Logger)

private:
    Device();
    void SendNextSubPacket (const Base::ByteArrayView& theData, uint8_t theID);
    void InvokeCallback (const Base::ByteArrayView& theData, uint8_t theID, bool theDeleteCallback = true);
    void ProcessCommand (const uint8_t* theData, int theDataLength);
    void AddCommand (const EmbedCommand& theCommand);
    void SendPacket (const uint8_t* theData,
                     int theLength,
                     const CallBackType& theCallback,
                     Embed::CommandType theCommandType);

private:
    std::unordered_map <uint8_t, CallBackType> myCommandsCallbackMap;
    std::unordered_map <uint8_t, std::shared_ptr <SendBuffer>> myBufferMap;
    Net::TcpClient myTcpClient;
    Observer* myObserver;
    int myCurCommandID;
};

}}

#endif
