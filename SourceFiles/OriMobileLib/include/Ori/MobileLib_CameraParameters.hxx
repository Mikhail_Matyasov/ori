#ifndef _Ori_MobileLib_CameraParameters_HeaderFile
#define _Ori_MobileLib_CameraParameters_HeaderFile

#include <Ori/Base_Macros.hxx>

#include <QtCore/qsize.h>
#include <QtCore/qlist.h>
#include <QtMultimedia/qcamera.h>
#include <vector>

namespace Ori {
namespace MobileLib {

class CameraParameters
{
public:
    typedef QList <QSize> ResulutionVectorType;
    __ORI_PROPERTY (ResulutionVectorType, ResolutionVector);

    __ORI_PRIMITIVE_PROPERTY (QCamera::Position, Position);
};

}}

#endif
