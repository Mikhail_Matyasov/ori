#ifndef _Ori_MobileLib_Track_HeaderFile
#define _Ori_MobileLib_Track_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Base_Frame.hxx>

namespace Ori {
namespace MobileLib {
class Track
{
public:
    class Observer
    {
    public:
        virtual void OnFrame (const Base::Frame& theFrame) = 0;
    };

    void RegisterObserver (Observer* theObserver);
    void Play (int theFps);
    __ORI_PROPERTY (Base::Vector <Codec::Packet>, Packets);
    __ORI_PRIMITIVE_PROPERTY (int, Width);
    __ORI_PRIMITIVE_PROPERTY (int, Heigth);

private:
    Observer* myObserver;
};

}}

#endif
