#ifndef _Ori_MobileLib_ThreadSafeBuffer_HeaderFile
#define _Ori_MobileLib_ThreadSafeBuffer_HeaderFile

#include <Ori/Base_Macros.hxx>

#include <deque>
#include <mutex>

namespace Ori {
namespace MobileLib {

template <typename T>
class ThreadSafeBuffer
{     
public:
    void PushBack (const T& theElement)
    {
        Lock ([&]() -> void { myContainer.push_back (theElement); });
    }

    void PopFront()
    {
        Lock ([&]() -> void { myContainer.pop_front(); });
    }

    void Clear()
    {
        Lock ([&]() -> void { myContainer.clear(); });
    }

    __ORI_PROPERTY (std::deque <T>, Container);

private:
    template <typename F>
    void Lock (F theFunc)
    {
        myMutex.lock();
        theFunc();
        myMutex.unlock();
    }

private:
    std::mutex myMutex;
};

}}

#endif
