#ifndef _Ori_StreamingManagerParameters_HeaderFile
#define _Ori_StreamingManagerParameters_HeaderFile

#include <Ori/Base_Macros.hxx>

namespace Ori {
namespace MobileLib {

class StreamingManagerParameters
{
public:
    ORI_EXPORT StreamingManagerParameters();
    ORI_EXPORT StreamingManagerParameters (const StreamingManagerParameters& theParams);

    __ORI_PRIMITIVE_PROPERTY (int, Width)
    __ORI_PRIMITIVE_PROPERTY (int, Height)
};

}}


#endif