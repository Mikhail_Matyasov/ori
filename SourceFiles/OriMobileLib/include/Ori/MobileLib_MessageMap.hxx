#ifndef _Ori_MobileLib_MessageMap_HeaderFile
#define _Ori_MobileLib_MessageMap_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>

#include <unordered_map>
#include <QtCore/qstring.h>

namespace Ori {
namespace MobileLib {

class MessageMap
{

public:
    static MessageMap* Instance();
    
    template <typename T>
    T* Find (const Base::String& theKey) 
    {
        const auto& anIt = myMap.find (theKey);
        if (anIt == myMap.end()) {
            return nullptr;
        }

        auto aRes = reinterpret_cast <T*> (anIt->second.get());
        return aRes;
    }

    template <typename T>
    void Insert (const Base::String& theKey, const std::shared_ptr <T>& theValue)
    {
        myMap.emplace (theKey, theValue);
    }

    void Remove (const Base::String& theKey);

private:
    MessageMap() {}
    ~MessageMap() {}

private:

    typedef std::unordered_map <Base::String, std::shared_ptr <void>> MapType;
    __ORI_PROPERTY (MapType, Map);

};

}}

#endif
