%pragma(csharp) modulecode=%{

class IceCandidateSchema
{
    public string candidate { get; set; }
    public string sdpMid { get; set; }
    public int sdpMLineIndex { get; set; }

}

class SessionDecriptionSchema
{
    public string sdp { get; set; }
    public string type { get; set; }
}

public class JsonReader
{

public static Ori.SessionDescription ParseSdp (Ori.Base.String theSource)
{
    Ori.SessionDescription aRes = new Ori.SessionDescription();
    var aDesc = System.Text.Json.JsonSerializer.Deserialize <SessionDecriptionSchema> (theSource.Data());
    aRes.SetSdp (new Ori.Base.String (aDesc.sdp));

    if (aDesc.type == "offer") {
        aRes.SetType (Ori.SessionDescription.DescriptionType.OFFER);
    } else {
        aRes.SetType (Ori.SessionDescription.DescriptionType.ANSWER);
    }

    return aRes;
}

public static Ori.IceCandidate ParseCandidate (Ori.Base.String theSource)
{
    var aCandidate = System.Text.Json.JsonSerializer.Deserialize <IceCandidateSchema> (theSource.Data());

    Ori.IceCandidate aRes = new Ori.IceCandidate();
    aRes.SetCandidate (new Ori.Base.String (aCandidate.candidate));
    aRes.SetSdpMid (new Ori.Base.String (aCandidate.sdpMid));
    aRes.SetSdpMlineIndex (aCandidate.sdpMLineIndex);

    return aRes;
}

}

%}