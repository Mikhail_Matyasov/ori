#ifndef _Ori_WebRTCBase_SessionDescriptionObserver_HeaderFile
#define _Ori_WebRTCBase_SessionDescriptionObserver_HeaderFile

#include <api/jsep.h>
#include <rtc_base/ref_counted_object.h>

namespace Ori {
namespace WebRTCBase {

class SessionDescriptionObserver : public webrtc::SetSessionDescriptionObserver
{
public:

    void AddRef() const override;
    rtc::RefCountReleaseStatus Release() const override;
    void OnSuccess() override;
    virtual void OnFailure (webrtc::RTCError error);
    virtual void OnFailure (const std::string& error);
    static SessionDescriptionObserver* Create();
};

}}

#endif
