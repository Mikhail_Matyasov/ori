#ifndef _Ori_WebRTC_Init_HeaderFile
#define _Ori_WebRTC_Init_HeaderFile

#include <Ori/Base_Macros.hxx>

namespace rtc {
class WinsockInitializer;
}

namespace Ori {
namespace WebRTC {

class Init
{
public:
    ORI_EXPORT Init();
    ORI_EXPORT ~Init();

private:
    std::shared_ptr <rtc::WinsockInitializer> myWinSockInit;
};

}}

#endif
