#ifndef _Ori_WebRTC_SessionDescription_HeaderFile
#define _Ori_WebRTC_SessionDescription_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>

namespace Ori {
namespace WebRTC {

class SessionDescription
{
public:
    enum class DescriptionType {
        OFFER,
        ANSWER
    };

    __ORI_PRIMITIVE_PROPERTY (DescriptionType, Type);
    __ORI_PROPERTY (Base::String, Sdp);
};

}}

#endif
