#ifndef _Ori_WebRTC_DataChannelObserver_HeaderFile
#define _Ori_WebRTC_DataChannelObserver_HeaderFile

#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace WebRTC {

class DataChannelObserver
{
public:
    virtual void OnOpenned() {}
    virtual void OnMessage (const Ori::Base::ByteArray& theMessage) {}
    virtual ~DataChannelObserver() {}
};

}}

#endif
