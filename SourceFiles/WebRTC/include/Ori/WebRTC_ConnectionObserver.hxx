#ifndef _Ori_WebRTC_ConnectionObserver_HeaderFile
#define _Ori_WebRTC_ConnectionObserver_HeaderFile

#include <Ori/Base_String.hxx>

namespace Ori {
namespace WebRTC {

class ConnectionObserver
{
public:

    virtual void OnCreateAnswer (const Base::String&) {}
    virtual void OnCreateOffer (const Base::String&) {}
    virtual void OnIceCandidate (const Base::String&) {}

    virtual void OnDataChannel() {}
};

}}

#endif
