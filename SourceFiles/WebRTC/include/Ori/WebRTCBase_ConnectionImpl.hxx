#ifndef _Ori_WebRTCBase_ConnectionImpl_HeaderFile
#define _Ori_WebRTCBase_ConnectionImpl_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Logger.hxx>

#include <api/peer_connection_interface.h>
#include <api/data_channel_interface.h>
#include <rtc_base/win32_socket_server.h>
#include <tbb/concurrent_queue.h>

namespace rtc {
class Win32SocketServer;
}

namespace Ori {
namespace Base {
class String;
}

namespace WebRTC {
class IceCandidate;
class DataChannelObserver;
class SessionDescription;
class ConnectionObserver;
}

namespace WebRTCBase {
class ConnectionImpl : public webrtc::PeerConnectionObserver,
                       public webrtc::CreateSessionDescriptionObserver,
                       public webrtc::DataChannelObserver
{
private:
    void AddRef() const override {};
    rtc::RefCountReleaseStatus Release() const override { return rtc::RefCountReleaseStatus::kDroppedLastRef; };

    // implementetion in .cxx file
    void OnDataChannel (rtc::scoped_refptr <webrtc::DataChannelInterface>) override;
    void OnIceCandidate (const webrtc::IceCandidateInterface* theCandidate) override;
    void OnSuccess (webrtc::SessionDescriptionInterface* theCandidate) override;
    void OnStateChange() override;
    void OnMessage (const webrtc::DataBuffer& theBuffer) override;
    void OnBufferedAmountChange (uint64_t theSentDataSize) override;
    void OnRenegotiationNeeded() override {};
    void OnSignalingChange (webrtc::PeerConnectionInterface::SignalingState s) override;
    void OnIceGatheringChange (webrtc::PeerConnectionInterface::IceGatheringState s) override;
    void OnIceConnectionChange (webrtc::PeerConnectionInterface::IceConnectionState s) override;
    void OnStandardizedIceConnectionChange (webrtc::PeerConnectionInterface::IceConnectionState s) override;
    void OnConnectionChange (webrtc::PeerConnectionInterface::PeerConnectionState s) override;
    void OnIceCandidateError (const std::string& host_candidate,
                              const std::string& url,
                              int error_code,
                              const std::string& error_text) override;
    void OnIceCandidatesRemoved (const std::vector<cricket::Candidate>& c) override;
    void OnIceConnectionReceivingChange (bool r) override;
    void OnIceSelectedCandidatePairChanged (const cricket::CandidatePairChangeEvent& e) override;

public:

    void Send (const Base::ByteArray& theMessage);
    void SetRemoteDescription (const Ori::WebRTC::SessionDescription& theDesc);
    void CreateOffer();
    void CreateAnswer();
    void AddIceCandidate (const Ori::WebRTC::IceCandidate& theCandidate);
    bool Ready();

    void Listen();
    void StopListening() { myIsStopListening = true; }
    ConnectionImpl (WebRTC::ConnectionObserver& theConnectionObserver,
                    WebRTC::DataChannelObserver& theChannelObserver,
                    const Base::Logger& theLogger);
    ~ConnectionImpl();
    
    template <typename T>
    void AddTask (T theFunc)
    {
        myTaskQueue.push (theFunc);
    }

    template <typename T>
    void AddMessage (T theFunc)
    {
        myMessageQueue.push (theFunc);
    }

    template <typename T>
    void AddCandidateTask (T theFunc)
    {
        myCandidatesQueue.push (theFunc);
    }

private:
    std::unique_ptr <webrtc::SessionDescriptionInterface> CreateDescription (webrtc::SdpType theSdpType,
                                                                             const Base::String& theSdp);
    void CreateDataChannel();
    void CreateFactory();
    static void UpdateTiming();

private:
    rtc::Win32SocketServer                                      mySocketServer;
    rtc::Win32Thread                                            mySignalThread;
    rtc::scoped_refptr <webrtc::DataChannelInterface>           myDataChannel;
    rtc::scoped_refptr <webrtc::DataChannelInterface>           myRemoteDataChannel;
    rtc::scoped_refptr <webrtc::PeerConnectionInterface>        myPeerConnection;
    rtc::scoped_refptr <webrtc::PeerConnectionFactoryInterface> myFactory;
    Ori::WebRTC::ConnectionObserver&                            myConnectionObserver;
    Ori::WebRTC::DataChannelObserver&                           myChannelObserver;
    Ori::Base::Logger                                           myLogger;
    bool                                                        myIsStopListening;
    tbb::concurrent_queue <std::function <void()>>              myMessageQueue;
    tbb::concurrent_queue <std::function <void()>>              myTaskQueue;
    tbb::concurrent_queue <std::function <void()>>              myCandidatesQueue;
    bool myIsSetSdp;

    static unsigned int myJobTime;
    static unsigned int mySleepTime;
    static unsigned int myLoopTime;
    static unsigned int myNumberOfParticipants;
};


}}

#endif
