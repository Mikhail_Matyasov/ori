#ifndef _Ori_WebRTCBase_JsonSerializer_HeaderFile
#define _Ori_WebRTCBase_JsonSerializer_HeaderFile

#include <Ori/Base_String.hxx>

namespace webrtc {
class SessionDescriptionInterface;
class IceCandidateInterface;
}

namespace Ori {
namespace WebRTCBase {

class JsonSerializer
{
public:
    static Base::String SerializeSdp (webrtc::SessionDescriptionInterface* theSdp);
    static Base::String SerializeCandidate (const webrtc::IceCandidateInterface* theCandidate);

};

}}

#endif
