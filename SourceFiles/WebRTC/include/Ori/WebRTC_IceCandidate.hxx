#ifndef _Ori_WebRTC_IceCandidate_HeaderFile
#define _Ori_WebRTC_IceCandidate_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>

namespace Ori {
namespace WebRTC {

class IceCandidate
{
public:

    __ORI_PROPERTY (Base::String, SdpMid);
    __ORI_PROPERTY (Base::String, Candidate);
    __ORI_PRIMITIVE_PROPERTY (int, SdpMlineIndex);
};

}}

#endif
