#ifndef _Ori_WebRTC_Connection_HeaderFile
#define _Ori_WebRTC_Connection_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Logger.hxx>

namespace Ori {
namespace WebRTCBase {
class ConnectionImpl;
}

namespace WebRTC {
class SessionDescription;
class IceCandidate;
class ConnectionObserver;
class DataChannelObserver;

class Connection
{
public:
    ORI_EXPORT Connection (WebRTC::ConnectionObserver& theConnectionObserver,
                           WebRTC::DataChannelObserver& theChannelOserver,
                           const Base::Logger& theLogger = Base::Logger());

    ORI_EXPORT void Send (const Base::ByteArray& theMessage, bool theisDelay = true);
    ORI_EXPORT bool Ready();
    ORI_EXPORT void SetRemoteDescription (const WebRTC::SessionDescription& theDesc, bool theisDelay = true);
    ORI_EXPORT void CreateOffer (bool theisDelay = true);
    ORI_EXPORT void CreateAnswer (bool theisDelay = true);
    ORI_EXPORT void AddIceCandidate (const WebRTC::IceCandidate& theCandidate, bool theisDelay = true);
    ORI_EXPORT void Listen();
    ORI_EXPORT void StopListening();

private:
    template <typename T>
    void TryInvoke (T theFunc, bool theIsDelay);

private:
    std::shared_ptr <WebRTCBase::ConnectionImpl> myImpl;
};

}}

#endif
