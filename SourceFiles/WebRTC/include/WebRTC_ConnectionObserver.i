%feature("nspace") Ori::WebRTC::ConnectionObserver;

%{
#include <Ori/WebRTC_ConnectionObserver.hxx>
%}

%csmethodmodifiers "public virtual";

%feature("director") Ori::WebRTC::ConnectionObserver;
%include <Ori/WebRTC_ConnectionObserver.hxx>

%csmethodmodifiers "public";