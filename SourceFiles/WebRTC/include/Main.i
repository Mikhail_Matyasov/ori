%module(directors="1") WebRTC

%include <Base_Headers.i>
%import <Base_String.i>
%import <Base_ByteArray.i>
%import <Base_Logger.i>

%{
#define WEBRTC_WIN
#include <memory>
%}

%include <WebRTC_IceCandidate.i>
%include <WebRTC_SessionDescription.i>
%include <WebRTC_DataChannelObserver.i>
%include <WebRTC_ConnectionObserver.i>
%include <WebRTC_Connection.i>
%include <WebRTC_Init.i>