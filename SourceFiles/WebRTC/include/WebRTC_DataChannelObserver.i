%feature("nspace") Ori::WebRTC::DataChannelObserver;

%{
#include <Ori/WebRTC_DataChannelObserver.hxx>
%}

%csmethodmodifiers "public virtual";

%feature("director") Ori::WebRTC::DataChannelObserver;
%include <Ori/WebRTC_DataChannelObserver.hxx>

%csmethodmodifiers "public";