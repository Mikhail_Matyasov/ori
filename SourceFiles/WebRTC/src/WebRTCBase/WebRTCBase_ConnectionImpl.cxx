#include <Ori/WebRTCBase_Pch.hxx>
#include <Ori/WebRTCBase_ConnectionImpl.hxx>

#include <Ori/Base_Timer.hxx>
#include <Ori/WebRTCBase_JsonSerializer.hxx>
#include <Ori/WebRTCBase_SessionDescriptionObserver.hxx>
#include <Ori/WebRTC_ConnectionObserver.hxx>
#include <Ori/WebRTC_DataChannelObserver.hxx>
#include <Ori/WebRTC_IceCandidate.hxx>
#include <Ori/WebRTC_SessionDescription.hxx>

#include <api/create_peerconnection_factory.h>
#include <api/stats_types.h>
#include <api/task_queue/default_task_queue_factory.h>
#include <media/sctp/sctp_transport.h>
#include <rtc_base/logging.h>
#include <rtc_base/operations_chain.h>
#include <rtc_base/rtc_certificate_generator.h>
#include <rtc_base/task_queue.h>

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <thread>

using namespace Ori::WebRTC;

namespace Ori {
namespace WebRTCBase {

namespace {
void DispatchQueue (tbb::concurrent_queue <std::function <void()>>& theQueue)
{
    while (!theQueue.empty()) {
            
        std::function <void()> aTask;
        while (!theQueue.try_pop (aTask)) {}
        aTask();
    }
}

}

void ConnectionImpl::OnDataChannel (rtc::scoped_refptr <webrtc::DataChannelInterface> theDataChannel)
{
    myRemoteDataChannel = theDataChannel;
    myRemoteDataChannel->RegisterObserver (this);
    myConnectionObserver.OnDataChannel();
    ORI_LOG_INFO  (myLogger) << "OnDataChannel";
}

void ConnectionImpl::OnIceCandidate (const webrtc::IceCandidateInterface* theCandidate)
{
    auto aMessage = JsonSerializer::SerializeCandidate (theCandidate);
    myConnectionObserver.OnIceCandidate (aMessage);
    ORI_LOG_INFO  (myLogger) << "Received Ice Candidate: " << aMessage;
}

void ConnectionImpl::OnSuccess (webrtc::SessionDescriptionInterface* theDesc)
{
    auto aMessage = JsonSerializer::SerializeSdp (theDesc);
    myPeerConnection->SetLocalDescription (SessionDescriptionObserver::Create(), theDesc);

    if (theDesc->type() == webrtc::SessionDescriptionInterface::kOffer) {
        myConnectionObserver.OnCreateOffer (aMessage);
        ORI_LOG_INFO  (myLogger) << "Created offer: " << aMessage;
    } else {
        myConnectionObserver.OnCreateAnswer (aMessage);
        ORI_LOG_INFO  (myLogger) << "Created answer: " << aMessage;
    }
}

unsigned int ConnectionImpl::myNumberOfParticipants = 0;
unsigned int ConnectionImpl::myJobTime = 0;
unsigned int ConnectionImpl::mySleepTime = 0;

// Requests from all participants must be processed during 'LoopTime'
unsigned int ConnectionImpl::myLoopTime = 30000;

ConnectionImpl::ConnectionImpl (WebRTC::ConnectionObserver& theConnectionObserver,
                                WebRTC::DataChannelObserver& theChannelObserver,
                                const Base::Logger& theLogger) :
    myConnectionObserver (theConnectionObserver),
    myChannelObserver (theChannelObserver),
    myLogger (theLogger),
    mySignalThread (&mySocketServer),
    myIsSetSdp (false)
{
    myLogger.Level() = Ori::Base::LogLevel::Fail;
    myNumberOfParticipants++;
    UpdateTiming();

    CreateFactory();
    if (myFactory) {
        webrtc::PeerConnectionInterface::RTCConfiguration aConfig;
        aConfig.sdp_semantics = webrtc::SdpSemantics::kUnifiedPlan;
        webrtc::PeerConnectionInterface::IceServer server;
        server.uri = "stun:stun.l.google.com:19302";
        aConfig.servers.push_back (server);

        myPeerConnection = myFactory->CreatePeerConnection (aConfig, nullptr, nullptr, this);
        myPeerConnection->SignalingThread() = &mySignalThread;
        myPeerConnection->WorkerThread() = &mySignalThread;
        myPeerConnection->NetworkThread() = &mySignalThread;

        CreateDataChannel();
    } else {
        ORI_LOG_FAIL (myLogger) << "Can not create Peer connection factory";
    }
}

ConnectionImpl::~ConnectionImpl()
{
    myNumberOfParticipants--;
    if (myNumberOfParticipants > 0) {
        UpdateTiming();
    }

    myDataChannel->Close();
    myPeerConnection->Close();
}

void ConnectionImpl::UpdateTiming()
{
    myJobTime = myLoopTime / myNumberOfParticipants;
    mySleepTime = myLoopTime - myJobTime;
}

void ConnectionImpl::SetRemoteDescription (const SessionDescription& theDesc)
{
    std::unique_ptr <webrtc::SessionDescriptionInterface> aDesc;
    if (theDesc.Type() == SessionDescription::DescriptionType::OFFER) {
        aDesc = CreateDescription (webrtc::SdpType::kOffer, theDesc.Sdp());
    } else {
        aDesc = CreateDescription (webrtc::SdpType::kAnswer, theDesc.Sdp());
    }

    if (!aDesc) {
        return;
    }
    myPeerConnection->SetRemoteDescription (SessionDescriptionObserver::Create(), aDesc.release());
}

void ConnectionImpl::CreateOffer()
{
    auto anOption = webrtc::PeerConnectionInterface::RTCOfferAnswerOptions();
    myPeerConnection->CreateOffer (this, anOption);
    myIsSetSdp = true;
}

void ConnectionImpl::CreateAnswer()
{
    auto anOption = webrtc::PeerConnectionInterface::RTCOfferAnswerOptions();
    myPeerConnection->CreateAnswer (this, anOption);
    myIsSetSdp = true;
}

void ConnectionImpl::AddIceCandidate (const IceCandidate& theCandidate)
{
    webrtc::SdpParseError anError;
    auto aCandidate = webrtc::CreateIceCandidate (theCandidate.SdpMid().Data(),
                                                  theCandidate.SdpMlineIndex(),
                                                  theCandidate.Candidate().Data(),
                                                  &anError);
    if (aCandidate) {
        myPeerConnection->AddIceCandidate (aCandidate);
    } else {
        ORI_LOG_WARNING (myLogger) << "Can not create Ice Candidate: " << theCandidate.Candidate();
    }
}

void ConnectionImpl::CreateDataChannel()
{
    boost::mt19937 aRand;
    aRand.seed (static_cast <uint32_t> (time (NULL))); // one should likely seed in a better way
    boost::uuids::basic_random_generator <boost::mt19937> aGenerator (&aRand);
    boost::uuids::uuid aBoostUuid = aGenerator();
    auto aUuid = boost::lexical_cast <std::string> (aBoostUuid);

    webrtc::DataChannelInit aConfig;
    aConfig.ordered = false;
    myDataChannel = myPeerConnection->CreateDataChannel (aUuid, &aConfig);
    myDataChannel->RegisterObserver (this);
}

void ConnectionImpl::OnStateChange()
{
    if (myRemoteDataChannel && myRemoteDataChannel->state() == webrtc::DataChannelInterface::DataState::kOpen) {
        myChannelObserver.OnOpenned();
        ORI_LOG_INFO (myLogger) << "Data channel openned";
    }
}

void ConnectionImpl::OnMessage (const webrtc::DataBuffer& theBuffer)
{
    Base::ByteArray aMessage (theBuffer.data.data(), static_cast <int> (theBuffer.size()));
    myChannelObserver.OnMessage (aMessage);
}

void ConnectionImpl::OnBufferedAmountChange (uint64_t theSentDataSize)
{
    ORI_LOG_INFO (myLogger) << "OnBufferedAmountChange: " << theSentDataSize;
}

std::unique_ptr <webrtc::SessionDescriptionInterface> ConnectionImpl::CreateDescription (webrtc::SdpType theSdpType,
                                                                                         const Base::String& theSdp)
{
    if (!myPeerConnection) {
        ORI_LOG_FAIL (myLogger) << "Null peer connection";
        return nullptr;
    }

    std::unique_ptr <webrtc::SessionDescriptionInterface> aSessionDesc =
        webrtc::CreateSessionDescription (theSdpType, theSdp.Data());

    return aSessionDesc;
}

void ConnectionImpl::CreateFactory()
{
    webrtc::PeerConnectionFactoryDependencies aDependencies;
    aDependencies.network_thread = &mySignalThread;
    aDependencies.signaling_thread = &mySignalThread;
    aDependencies.worker_thread = &mySignalThread;
    aDependencies.task_queue_factory = webrtc::CreateDefaultTaskQueueFactory();
    aDependencies.call_factory = webrtc::CreateCallFactory();
    myFactory = webrtc::CreateModularPeerConnectionFactory (std::move (aDependencies));
}

void ConnectionImpl::Send (const Base::ByteArray& theMessage)
{
    if (myDataChannel) {
        rtc::CopyOnWriteBuffer aBuffer (theMessage.Data(), theMessage.Lenght());
        webrtc::DataBuffer aDataBuffer (aBuffer, true /*IsBinary*/);

        if (myDataChannel->state() == webrtc::DataChannelInterface::DataState::kOpen) {
            myDataChannel->Send (aDataBuffer);
            ORI_LOG_VERBOSE (myLogger) << "Sent packet, size = " << theMessage.Lenght();
        } else {
            ORI_LOG_WARNING (myLogger) << "Attempting to send packet in state different of 'OPEN'";
        }
    } else {
        ORI_LOG_FAIL (myLogger) << "Null Datachannel";
    }
}

bool ConnectionImpl::Ready()
{
    if (myPeerConnection && myDataChannel) {
        return true;
    }

    return false;
}

void ConnectionImpl::Listen()
{
    myIsStopListening = false;
    auto aStartListening = Base::Timer::Microseconds();
    while (!myIsStopListening) {

        if (Base::Timer::Microseconds() - aStartListening < myJobTime) {
            DispatchQueue (myMessageQueue);

            // order is important
            // Candidates should be added after remote description is set.
            DispatchQueue (myTaskQueue);
            DispatchQueue (myCandidatesQueue);

            rtc::Message aMessage;
            if (myIsSetSdp && mySignalThread.Get (&aMessage, 0)) {
                mySignalThread.Dispatch (&aMessage);
                ORI_LOG_VERBOSE (myLogger) << "Dispatching message";
            }
        } else {
            unsigned int aMinSleepTime = 100;
            unsigned int aSleepTime = mySleepTime < aMinSleepTime ? aMinSleepTime : mySleepTime;

            ORI_LOG_VERBOSE (myLogger) << "Sleeping " << aSleepTime << " microseconds";
            std::this_thread::sleep_for (std::chrono::microseconds (aSleepTime));
            aStartListening = Base::Timer::Microseconds();
        }

    }
}

void ConnectionImpl::OnSignalingChange (webrtc::PeerConnectionInterface::SignalingState theState)
{
    Base::String aMessage = "OnSignalingChange: ";
    switch (theState) {
        case webrtc::PeerConnectionInterface::SignalingState::kClosed: aMessage += "Closed"; break;
        case webrtc::PeerConnectionInterface::SignalingState::kHaveLocalOffer: aMessage += "HaveLocalOffer"; break;
        case webrtc::PeerConnectionInterface::SignalingState::kHaveLocalPrAnswer: aMessage += "HaveLocalPrAnswer"; break;
        case webrtc::PeerConnectionInterface::SignalingState::kHaveRemoteOffer: aMessage += "HaveRemoteOffer"; break;
        case webrtc::PeerConnectionInterface::SignalingState::kHaveRemotePrAnswer: aMessage += "HaveRemotePrAnswer"; break;
        case webrtc::PeerConnectionInterface::SignalingState::kStable: aMessage += "Stable"; break;
        default: break;
    }

    ORI_LOG_INFO (myLogger) << aMessage;
}

void ConnectionImpl::OnIceGatheringChange (webrtc::PeerConnectionInterface::IceGatheringState theState)
{
    Base::String aMessage = "OnIceGatheringChange: ";
    switch (theState) {
        case webrtc::PeerConnectionInterface::IceGatheringState::kIceGatheringComplete: aMessage += "IceGatheringComplete"; break;
        case webrtc::PeerConnectionInterface::IceGatheringState::kIceGatheringGathering: aMessage += "IceGatheringGathering"; break;
        case webrtc::PeerConnectionInterface::IceGatheringState::kIceGatheringNew: aMessage += "IceGatheringNew"; break;
        default: break;
    }

    ORI_LOG_INFO (myLogger) << aMessage;
}

void ConnectionImpl::OnIceConnectionChange (webrtc::PeerConnectionInterface::IceConnectionState theState)
{
    Base::String aMessage = "OnIceConnectionChange: ";
    switch (theState) {
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionChecking: aMessage += "IceConnectionChecking"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionClosed: aMessage += "IceConnectionClosed"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionCompleted: aMessage += "IceConnectionCompleted"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionConnected: aMessage += "IceConnectionConnected"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionDisconnected: aMessage += "IceConnectionDisconnected"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionFailed: aMessage += "IceConnectionFailed"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionMax: aMessage += "IceConnectionMax"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionNew: aMessage += "IceConnectionNew"; break;
        default: break;
    }

    ORI_LOG_INFO (myLogger) << aMessage;
}

void ConnectionImpl::OnStandardizedIceConnectionChange (webrtc::PeerConnectionInterface::IceConnectionState theState)
{
    Base::String aMessage = "OnStandardizedIceConnectionChange: ";
    switch (theState) {
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionChecking: aMessage += "IceConnectionChecking"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionClosed: aMessage += "IceConnectionClosed"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionCompleted: aMessage += "IceConnectionCompleted"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionConnected: aMessage += "IceConnectionConnected"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionDisconnected: aMessage += "IceConnectionDisconnected"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionFailed: aMessage += "IceConnectionFailed"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionMax: aMessage += "IceConnectionMax"; break;
        case webrtc::PeerConnectionInterface::IceConnectionState::kIceConnectionNew: aMessage += "IceConnectionNew"; break;
        default: break;
    }

    ORI_LOG_INFO (myLogger) << aMessage;
}

void ConnectionImpl::OnConnectionChange (webrtc::PeerConnectionInterface::PeerConnectionState theState)
{
    Base::String aMessage = "OnPeerConnectionChange: ";
    switch (theState) {
        case webrtc::PeerConnectionInterface::PeerConnectionState::kClosed: aMessage += "Closed"; break;
        case webrtc::PeerConnectionInterface::PeerConnectionState::kConnected: aMessage += "Connected"; break;
        case webrtc::PeerConnectionInterface::PeerConnectionState::kConnecting: aMessage += "Connecting"; break;
        case webrtc::PeerConnectionInterface::PeerConnectionState::kDisconnected: aMessage += "Disconnected"; break;
        case webrtc::PeerConnectionInterface::PeerConnectionState::kFailed: aMessage += "Failed"; break;
        case webrtc::PeerConnectionInterface::PeerConnectionState::kNew: aMessage += "New"; break;
        default: break;
    }

    ORI_LOG_INFO (myLogger) << aMessage;
}

// |host_candidate| is a stringified socket address.
void ConnectionImpl::OnIceCandidateError (const std::string& theHostCandidate,
                                          const std::string& theUrl,
                                          int theErrorCode,
                                          const std::string& theErrorText)
{
    ORI_LOG_FAIL (myLogger) << "OnPeerConnectionChange { "
                            << "Host candidate: " << theHostCandidate.c_str() << "; "
                            << "Url: " <<  theUrl.c_str() << "; "
                            << "Error code: " << std::to_string (theErrorCode).c_str() << "; "
                            << "Error text: " << theErrorText.c_str() << "; ";
}

void ConnectionImpl::OnIceCandidatesRemoved (const std::vector <cricket::Candidate>& c)
{
    ORI_LOG_INFO (myLogger) << "IceCandidatesRemoved";
}

void ConnectionImpl::OnIceConnectionReceivingChange (bool r)
{
    ORI_LOG_INFO (myLogger) << "OnIceConnectionReceivingChange" << r;
}

void ConnectionImpl::OnIceSelectedCandidatePairChanged (const cricket::CandidatePairChangeEvent& e)
{
    ORI_LOG_INFO (myLogger) << "OnIceSelectedCandidatePairChanged";
}

}}
