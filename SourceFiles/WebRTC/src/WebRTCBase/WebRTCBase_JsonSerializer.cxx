#include <Ori/WebRTCBase_Pch.hxx>
#include <Ori/WebRTCBase_JsonSerializer.hxx>

#include <rapidjson/prettywriter.h>
#include <api/jsep.h>

namespace Ori {
namespace WebRTCBase {

Base::String JsonSerializer::SerializeSdp (webrtc::SessionDescriptionInterface* theSdp)
{
    rapidjson::StringBuffer aBuffer;
    rapidjson::PrettyWriter <rapidjson::StringBuffer> aWriter (aBuffer);
    aWriter.StartObject();

        aWriter.Key ("type");
        aWriter.String (webrtc::SdpTypeToString (theSdp->GetType()));

    std::string aSdp;
    theSdp->ToString (&aSdp);

        aWriter.Key("sdp");
        aWriter.String (aSdp.c_str());

    aWriter.EndObject();
    return aBuffer.GetString();
}

Base::String JsonSerializer::SerializeCandidate (const webrtc::IceCandidateInterface* theCandidate)
{
    rapidjson::StringBuffer aBuffer;
    rapidjson::PrettyWriter <rapidjson::StringBuffer> aWriter(aBuffer);
    aWriter.StartObject();

        aWriter.Key ("sdpMid");
        aWriter.String (theCandidate->sdp_mid().c_str());

        aWriter.Key ("sdpMLineIndex");
        aWriter.Int (theCandidate->sdp_mline_index());

    std::string aCandidate;
    theCandidate->ToString (&aCandidate);

        aWriter.Key ("candidate");
        aWriter.String (aCandidate.c_str());

    aWriter.EndObject();
    return aBuffer.GetString();
}

}}