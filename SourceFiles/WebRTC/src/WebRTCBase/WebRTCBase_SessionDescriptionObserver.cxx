#include <Ori/WebRTCBase_Pch.hxx>
#include <Ori/WebRTCBase_SessionDescriptionObserver.hxx>

namespace Ori {
namespace WebRTCBase {

void SessionDescriptionObserver::AddRef() const
{}

rtc::RefCountReleaseStatus SessionDescriptionObserver::Release() const
{
    return rtc::RefCountReleaseStatus::kDroppedLastRef;
}

void SessionDescriptionObserver::OnSuccess()
{
}

void SessionDescriptionObserver::OnFailure (webrtc::RTCError error)
{
}

void SessionDescriptionObserver::OnFailure (const std::string& error)
{
}

SessionDescriptionObserver* SessionDescriptionObserver::Create()
{
    return new rtc::RefCountedObject <SessionDescriptionObserver>();
}

}}
