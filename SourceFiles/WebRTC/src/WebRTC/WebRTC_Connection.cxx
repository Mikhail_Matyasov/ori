#include <Ori/WebRTCBase_Pch.hxx>
#include <Ori/WebRTC_Connection.hxx>
#include <Ori/WebRTCBase_ConnectionImpl.hxx>

#include <Ori/Base_ByteArray.hxx>
#include <Ori/WebRTC_SessionDescription.hxx>
#include <Ori/WebRTC_IceCandidate.hxx>

#include <rtc_base/win32_socket_server.h>

namespace Ori {
namespace WebRTC {

Connection::Connection (Ori::WebRTC::ConnectionObserver& theConnectionObserver,
                        Ori::WebRTC::DataChannelObserver& theChannelOserver,
                        const Base::Logger& theLogger) :
    myImpl (std::make_shared <WebRTCBase::ConnectionImpl> (theConnectionObserver,
                                                            theChannelOserver,
                                                            theLogger))
{}

void Connection::Send (const Base::ByteArray& theMessage, bool theIsDelay)
{
    if (theIsDelay) {
        myImpl->AddMessage ([=]() -> void {
            myImpl->Send (theMessage);
        });
    } else {
        myImpl->Send (theMessage);
    }
}

bool Connection::Ready()
{
    return myImpl->Ready();
}

void Connection::SetRemoteDescription (const SessionDescription& theDesc, bool theIsDelay)
{
    TryInvoke ([=]() -> void {
        myImpl->SetRemoteDescription (theDesc);
    }, theIsDelay);
}

void Connection::CreateOffer (bool theIsDelay)
{
    TryInvoke ([=]() -> void {
        myImpl->CreateOffer();
    }, theIsDelay);
}

void Connection::CreateAnswer (bool theIsDelay)
{
    TryInvoke ([=]() -> void {
        myImpl->CreateAnswer();
    }, theIsDelay);
}

void Connection::AddIceCandidate (const IceCandidate& theCandidate, bool theIsDelay)
{
    if (theIsDelay) {
        myImpl->AddCandidateTask ([=]() -> void {
            myImpl->AddIceCandidate (theCandidate);
        });
    } else {
        myImpl->AddIceCandidate (theCandidate);
    }
    
}

void Connection::Listen()
{
    myImpl->Listen();
}

void Connection::StopListening()
{
    myImpl->StopListening();
}

template <typename T>
void Connection::TryInvoke (T theFunc, bool theIsDelay)
{
    if (theIsDelay) {
        myImpl->AddTask (theFunc);
    } else {
        theFunc();
    }
}

}}
