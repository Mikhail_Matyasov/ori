#include <Ori/WebRTCBase_Pch.hxx>
#include <Ori/WebRTC_Init.hxx>

#include <Ori/Base_Logger.hxx>

#include <rtc_base/win32_socket_init.h>
#include <rtc_base/ssl_adapter.h>
#include <rtc_base/logging.h>

using namespace Ori::Base;

namespace Ori {
namespace WebRTC {
    
Init::Init()
{   
    rtc::InitializeSSL();
    myWinSockInit = std::make_shared <rtc::WinsockInitializer>();
    rtc::LogMessage::LogToDebug (rtc::LoggingSeverity::LS_NONE);
#if 0
    switch (Logger::Level())
    {
    case LogLevel::Verbose:
        rtc::LogMessage::LogToDebug (rtc::LoggingSeverity::LS_VERBOSE);
        break;
    case LogLevel::Info:
        rtc::LogMessage::LogToDebug (rtc::LoggingSeverity::LS_INFO);
        break;
    case LogLevel::Warning:
        rtc::LogMessage::LogToDebug (rtc::LoggingSeverity::LS_WARNING);
        break;
    case LogLevel::Fail:
        rtc::LogMessage::LogToDebug (rtc::LoggingSeverity::LS_ERROR);
        break;
    default:
        rtc::LogMessage::LogToDebug (rtc::LoggingSeverity::LS_NONE);
        break;
    }
#endif
}

Init::~Init()
{
    rtc::CleanupSSL();
}


}}
