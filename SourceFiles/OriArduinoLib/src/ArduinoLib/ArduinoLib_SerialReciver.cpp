#include <ArduinoLib_SerialReciver.h>

#include <ArduinoLib_Helper.h>

namespace Ori {
namespace ArduinoLib {
namespace {
template <typename T>
union TypeBuffer
{
public:
    T Value;
    uint8_t Bytes [sizeof (T)];
};
}

SerialReciver::SerialReciver (SerialPacketSize thePacketSize) :
    myPacketSize (0),
    myAvaliableBytes (0),
    myObserver (nullptr)
{
    myBuffer = new uint8_t [static_cast <uint16_t> (thePacketSize)];
}

SerialReciver::~SerialReciver()
{
    delete[] myBuffer;
}

void SerialReciver::Setup()
{
    Reset();
    while (!Serial);
    Serial.begin (__ORI_COM_PORT_BAUD_RATE);
}

void SerialReciver::Loop()
{
    int anAvaliableBytes = Serial.available();
    if (anAvaliableBytes > 0) {

        if (myPacketSize == 0) {
            if (anAvaliableBytes < 2) {
                // Wait packet size
                return;
            }
            TypeBuffer <uint16_t> aBuffer;
            Serial.readBytes (aBuffer.Bytes, sizeof (uint16_t));
            myPacketSize = aBuffer.Value;
            return;
        }

        Serial.readBytes (myBuffer + myAvaliableBytes, anAvaliableBytes);
        myAvaliableBytes += anAvaliableBytes;

        if (myAvaliableBytes == myPacketSize) {
            if (myObserver) {
                if (myObserver->IsSetStreamSize()) {
                    mySentStreamSize += myPacketSize;
                    myObserver->OnPacket (myBuffer, myPacketSize);
                } else {
                    TypeBuffer <int32_t> aStreamSizeBuffer;
                    memcpy (aStreamSizeBuffer.Bytes, myBuffer, myPacketSize);
                    myStreamSize = aStreamSizeBuffer.Value;
                    myObserver->OnSetStreamSize (myBuffer, myPacketSize);
                }
            }

            if (myStreamSize == mySentStreamSize) {
                myStreamSize = 0;
                mySentStreamSize = 0;
                myObserver->IsSetStreamSize() = false;
            }
            Serial.write ("Packet received");
            myPacketSize = 0;
            myAvaliableBytes = 0;
        }
    }
}

void SerialReciver::RegisterObserver (Observer* theObserver)
{
    myObserver = theObserver;
}

void SerialReciver::Reset()
{
    myPacketSize = 0;
    myStreamSize = 0;
    mySentStreamSize = 0;
    myAvaliableBytes = 0;
    myObserver = nullptr;
}


}}
