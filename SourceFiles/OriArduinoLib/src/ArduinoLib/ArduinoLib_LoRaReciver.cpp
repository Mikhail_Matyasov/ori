#include <ArduinoLib_LoRaReciver.h>
#include <ArduinoLib_SerialParameters.h>

#include <LoRa.h>

#define __ORI_LORA_BUFFER_SIZE 255

namespace Ori {
namespace ArduinoLib {

namespace {

union TypeBuffer
{
public:
    int32_t Value;
    uint8_t Bytes [sizeof (int32_t)];
};

LoRaReciver* theReciver = nullptr;
void OnPacketGlobal (int thePacketSize)
{
    theReciver->OnPacket (thePacketSize);
}
}

LoRaReciver::LoRaReciver() :
    mySubpacketsCount (static_cast <int> (Ori::ArduinoLib::SerialPacketSize::LoRa) / __ORI_LORA_BUFFER_SIZE)
{}

void LoRaReciver::Setup()
{
    Reset();
    Serial.begin (__ORI_COM_PORT_BAUD_RATE);
    if (!LoRa.begin (433E6)) {
        Serial.println ("Can not initialize LoRa library");
    }

    theReciver = this;
    LoRa.setSignalBandwidth (500E3);
    LoRa.onReceive (OnPacketGlobal);
    LoRa.receive();

    int aBufferSize = static_cast <int>  (SerialPacketSize::LoRa);
    myBuffer = new uint8_t [aBufferSize];
}

void LoRaReciver::Loop()
{}

void LoRaReciver::OnPacket (int thePacketSize)
{
    uint8_t aBuffer [__ORI_LORA_BUFFER_SIZE];
    LoRa.readBytes (aBuffer, __ORI_LORA_BUFFER_SIZE);

    if (myStreamSize == 0) {
        TypeBuffer aTBuffer;
        memcpy (aTBuffer.Bytes, aBuffer, sizeof (int32_t));
        myStreamSize = aTBuffer.Value;

        String aPrefix = "Stream size = ";
        String aMessage (myStreamSize);
        Serial.write ((aPrefix + aMessage).c_str());
        return;
    }

    myReceivedStreamSize += thePacketSize;
    auto anOffset = myRecivedSubpacketsCount * __ORI_LORA_BUFFER_SIZE;
    memcpy (myBuffer + anOffset, aBuffer, thePacketSize);

    myRecivedSubpacketsCount++;

    if (myRecivedSubpacketsCount == mySubpacketsCount ||
        myReceivedStreamSize == myStreamSize) {

        int aBufferLength = anOffset + thePacketSize;
        Serial.write (myBuffer, aBufferLength);
        myRecivedSubpacketsCount = 0;
    }

    if (myReceivedStreamSize == myStreamSize) {
        myReceivedStreamSize = 0;
        myStreamSize = 0;
    }
}

void LoRaReciver::Reset()
{
    myRecivedSubpacketsCount = 0;
    myReceivedStreamSize = 0;
    myStreamSize = 0;
    if (myBuffer) {
        delete[] myBuffer;
    }
}

}}
