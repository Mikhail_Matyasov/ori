#include <ArduinoLib_NRF24L01Reciver.h>

#include <ArduinoLib_SerialParameters.h>

#include <SPI.h>
#include <nRF24L01.h>

#define __ORI_NRF_PACKET_SIZE 32

namespace Ori {
namespace ArduinoLib {
namespace {
template <typename T>
union TypeBuffer
{
public:
    T Value;
    uint8_t Bytes [sizeof (T)];
};
}

NRF24L01Reciver::NRF24L01Reciver() :
    myRFChannel (7, 8),
    myRecivedSubpacketsCount (0),
    mySubpacketsCount (static_cast <int> (Ori::ArduinoLib::SerialPacketSize::NRF24L01) / __ORI_NRF_PACKET_SIZE),
    myStreamSize (0)
{
    int aSize = static_cast <int> (SerialPacketSize::NRF24L01);
    myBuffer = new uint8_t [aSize];
}

void NRF24L01Reciver::Setup()
{
    while (!Serial);
    Serial.begin (__ORI_COM_PORT_BAUD_RATE);
    myRFChannel.begin();
    myRFChannel.setPALevel (rf24_pa_dbm_e::RF24_PA_MAX);
    myRFChannel.setDataRate (rf24_datarate_e::RF24_2MBPS);

    const uint8_t* anAdress = reinterpret_cast <const uint8_t*> ("00001");
    myRFChannel.openReadingPipe (1, anAdress);
    myRFChannel.startListening();
    myRFChannel.powerUp();
}

void NRF24L01Reciver::Loop()
{
    if (myRFChannel.available()) {

        int aDataLength = static_cast <int> (myRFChannel.getPayloadSize());
        uint8_t aBuffer [__ORI_NRF_PACKET_SIZE];
        myRFChannel.read (aBuffer, aDataLength);

        if (myStreamSize == 0) {
            TypeBuffer <int32_t> aTBuffer;
            memcpy (aTBuffer.Bytes, aBuffer, sizeof (int32_t));
            myStreamSize = aTBuffer.Value;

            String aPrefix = "Stream size = ";
            String aMessage (static_cast <unsigned long> (myStreamSize));
            Serial.write ((aPrefix + aMessage).c_str());
            return;
        }

        myRecivedStreamSize += aDataLength;

        auto anOffset = myRecivedSubpacketsCount * __ORI_NRF_PACKET_SIZE;
        memcpy (myBuffer + anOffset, aBuffer, __ORI_NRF_PACKET_SIZE);

        myRecivedSubpacketsCount++;
        if (myRecivedSubpacketsCount == mySubpacketsCount ||
            myRecivedStreamSize >= myStreamSize) {

            Serial.write (myBuffer, static_cast <int> (SerialPacketSize::NRF24L01));
            myRecivedSubpacketsCount = 0;
        }

        if (myRecivedStreamSize >= myStreamSize) {
            myRecivedStreamSize = 0;
            myStreamSize = 0;
        }
        
    }
}

void NRF24L01Reciver::Reset()
{
    myRecivedStreamSize = 0;
    myRecivedSubpacketsCount = 0;
    myStreamSize = 0;
}

}}
