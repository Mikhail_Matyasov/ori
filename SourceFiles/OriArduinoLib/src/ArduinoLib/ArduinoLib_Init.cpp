#include <ArduinoLib_Init.h>

#include <Arduino.h>

namespace Ori {
namespace ArduinoLib {

void Init::Setup()
{
    // Disable all indicators
    int aPin = 13;
    pinMode (aPin, OUTPUT);
    digitalWrite (aPin, LOW);
}

}}