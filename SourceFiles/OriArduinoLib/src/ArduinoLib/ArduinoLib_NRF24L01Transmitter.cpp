#include <ArduinoLib_NRF24L01Transmitter.h>

#include <SPI.h>
#include <nRF24L01.h>

#define __ORI_NRF_PACKET_SIZE 32

namespace Ori {
namespace ArduinoLib {

NRF24L01Observer::NRF24L01Observer() :
    SerialReciver::Observer(),
    myRFChannel (7, 8)
{}

void NRF24L01Observer::OnPacket (const uint8_t* theData, int theDataLength)
{
    for (int i = 0, j = 0; i < theDataLength; i += __ORI_NRF_PACKET_SIZE, j++) {
        while (!myRFChannel.write (theData + j * __ORI_NRF_PACKET_SIZE, __ORI_NRF_PACKET_SIZE));
    }
}

void NRF24L01Observer::OnSetStreamSize (const uint8_t* theData, int theDataLength)
{
    while (!myRFChannel.write (theData, theDataLength));
    myIsSetStreamSize = true;
}

void NRF24L01Observer::Setup()
{
    Reset();
    myRFChannel.begin();
    myRFChannel.setPALevel (rf24_pa_dbm_e::RF24_PA_MAX);
    myRFChannel.setDataRate (rf24_datarate_e::RF24_2MBPS);

    const uint8_t* aDestinationAdress = reinterpret_cast <const uint8_t*> ("00001");
    myRFChannel.openWritingPipe (aDestinationAdress);
    myRFChannel.stopListening();
    myRFChannel.powerUp();
}

void NRF24L01Observer::Reset()
{
    myIsSetStreamSize = false;
}

NRF24L01Transmitter::NRF24L01Transmitter() :
    mySerialReciver (SerialPacketSize::NRF24L01)
{}

void NRF24L01Transmitter::Setup()
{
    mySerialReciver.Setup();
    // Serial.begin() inside mySerialReciver.Setup() should be invoked before
    // myRFChannel.begin() from myObserver.Setup(), otherwise Serial port will be blocked.

    myObserver.Setup();
    mySerialReciver.RegisterObserver (&myObserver);
}

void NRF24L01Transmitter::Loop()
{
    mySerialReciver.Loop();
}

}
}
