#include <ArduinoLib_LoRaTransmitter.h>

#include <SPI.h>
#include <LoRa.h>

#define __ORI_LORA_PACKET_SIZE 255

namespace Ori {
namespace ArduinoLib {

void LoRaObserver::OnPacket (const uint8_t* theData, int theDataLength)
{
    int aNumberOfPackets = theDataLength / __ORI_LORA_PACKET_SIZE;
    int aLastPacketLength = __ORI_LORA_PACKET_SIZE;
    if (theDataLength % __ORI_LORA_PACKET_SIZE != 0) {
        aNumberOfPackets++;
        aLastPacketLength = theDataLength % __ORI_LORA_PACKET_SIZE;
    }

    int i = 0;
    for ( ;i < aNumberOfPackets - 1; i++) {
        int anOffset = i * __ORI_LORA_PACKET_SIZE;
        LoRaTransmitter::Send (theData + anOffset, __ORI_LORA_PACKET_SIZE);
    }

    int anOffset = i * __ORI_LORA_PACKET_SIZE;
    LoRaTransmitter::Send (theData + anOffset, aLastPacketLength);
}

void LoRaObserver::OnSetStreamSize (const uint8_t* theData, int theDataLength)
{
    OnPacket (theData, theDataLength);
    myIsSetStreamSize = true;
}

void LoRaObserver::Reset()
{
    myIsSetStreamSize = false;
}

LoRaTransmitter::LoRaTransmitter() :
    myReciver (SerialPacketSize::LoRa)
{}

void LoRaTransmitter::Setup()
{
    myObserver.Reset();
    myReciver.Setup();
    myReciver.RegisterObserver (&myObserver);

    // 433E6 - 433 Mhz frequency
    if (!LoRa.begin (433E6)) {
        Serial.println ("Can not initialize LoRa library");
    }
    LoRa.setSignalBandwidth (500E3);
}

void LoRaTransmitter::Loop()
{
    myReciver.Loop();
}

void LoRaTransmitter::Send (const uint8_t* theData, int theDataLength)
{
    LoRa.beginPacket();
    LoRa.write (theData, theDataLength);
    LoRa.endPacket (true);
    delay (100);
}

}}
