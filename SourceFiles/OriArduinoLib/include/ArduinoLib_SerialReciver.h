#ifndef _Ori_ArduinoLib_Transmitter_HeaderFile
#define _Ori_ArduinoLib_Transmitter_HeaderFile

#include <stdint.h>
#include <ArduinoLib_SerialParameters.h>
#include <Arduino.h>

namespace Ori {
namespace ArduinoLib {

class SerialReciver
{
public:
    class Observer
    {
    public:
        Observer() : myIsSetStreamSize (false) {}
        virtual void OnPacket (
            const uint8_t* theData,
            int theDataLength) = 0;

        virtual void OnSetStreamSize (
            const uint8_t* theData,
            int theDataLength) = 0;

        bool IsSetStreamSize() const { return myIsSetStreamSize; }
        bool& IsSetStreamSize() { return myIsSetStreamSize; }
        
    protected:
        bool myIsSetStreamSize;
    };

    SerialReciver (SerialPacketSize thePacketSize);
    ~SerialReciver();
    void Setup();
    void Loop();
    void RegisterObserver (Observer* theObserver);

private:
    void Reset();

private:
    uint16_t myPacketSize;
    uint16_t myAvaliableBytes;
    uint8_t* myBuffer;
    int32_t myStreamSize;
    int32_t mySentStreamSize;
    Observer* myObserver;
};

}}

#endif
