#ifndef _Ori_ArduinoLib_LoRaReciver_HeaderFile
#define _Ori_ArduinoLib_LoRaReciver_HeaderFile

#include <stdint.h>

namespace Ori {
namespace ArduinoLib {

class LoRaReciver
{
public:
    LoRaReciver();
    void Setup();
    void Loop();
    void OnPacket (int thePacketSize);

private:
    void Reset();

private:
    uint8_t myRecivedSubpacketsCount;
    const uint8_t mySubpacketsCount;
    int32_t myStreamSize;
    uint32_t myReceivedStreamSize;
    uint8_t* myBuffer;
};

}}


#endif