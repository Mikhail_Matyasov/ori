#ifndef _Ori_ArduinoLib_SerialParameters_HeaderFile
#define _Ori_ArduinoLib_SerialParameters_HeaderFile

#define __ORI_COM_PORT_BAUD_RATE 1000000
                                   // Packet number  |  subpacket number 
#define __ORI_PACKET_HEADER_LENGTH sizeof (uint16_t) + sizeof (uint8_t)

namespace Ori {
namespace ArduinoLib {

enum class SerialPacketSize
{
    NRF24L01 = 1024,
    LoRa = 1020
};

}}

#endif
