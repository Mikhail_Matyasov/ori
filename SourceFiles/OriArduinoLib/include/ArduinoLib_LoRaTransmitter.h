#ifndef _Ori_ArduinoLib_LoRaTransmitter_HeaderFile
#define _Ori_ArduinoLib_LoRaTransmitter_HeaderFile

#include <ArduinoLib_SerialReciver.h>

namespace Ori {
namespace ArduinoLib {

class LoRaObserver : public SerialReciver::Observer
{
public:
    void OnPacket (const uint8_t* theData, int theDataLength) override;
    void OnSetStreamSize (const uint8_t* theData, int theDataLength) override;
    void Reset();
};

class LoRaTransmitter
{
public:
    LoRaTransmitter();
    void Setup();
    void Loop();
    static void Send (const uint8_t* theData, int theDataLength);

private:
    SerialReciver myReciver;
    LoRaObserver myObserver;

};

}}

#endif