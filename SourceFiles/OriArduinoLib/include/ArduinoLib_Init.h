#ifndef _Ori_ArduinoLib_Init_HeaderFile
#define _Ori_ArduinoLib_Init_HeaderFile

namespace Ori {
namespace ArduinoLib {

class Init
{
public:
    static void Setup();
};

}}

#endif