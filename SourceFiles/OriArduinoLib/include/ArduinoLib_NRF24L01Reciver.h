#ifndef _Ori_ArduinoLib_NRF24L01Reciver_HeaderFile
#define _Ori_ArduinoLib_NRF24L01Reciver_HeaderFile

#include <RF24.h>

namespace Ori {
namespace ArduinoLib {

class NRF24L01Reciver
{
public:
    NRF24L01Reciver();
    void Setup();
    void Loop();

private:
    void Reset();

private:
    RF24 myRFChannel;
    uint8_t* myBuffer;
    uint8_t myRecivedSubpacketsCount;
    const uint8_t mySubpacketsCount;
    uint64_t myStreamSize;
    uint64_t myRecivedStreamSize;
};

}}


#endif
