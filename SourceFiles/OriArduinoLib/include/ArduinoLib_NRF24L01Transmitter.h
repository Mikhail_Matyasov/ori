#ifndef _Ori_ArduinoLib_NRF24L01Transmitter_HeaderFile
#define _Ori_ArduinoLib_NRF24L01Transmitter_HeaderFile

#include <ArduinoLib_SerialReciver.h>
#include <RF24.h>

namespace Ori {
namespace ArduinoLib {

class NRF24L01Observer : public SerialReciver::Observer
{
public:
    NRF24L01Observer();
    void OnPacket (const uint8_t* theData, int theDataLength) override;
    void OnSetStreamSize (const uint8_t* theData, int theDataLength) override;
    void Setup();

private:
    void Reset();

private:
    RF24 myRFChannel;
};

class NRF24L01Transmitter
{
public:
    NRF24L01Transmitter();
    void Setup();
    void Loop();

private:
    SerialReciver mySerialReciver;
    NRF24L01Observer myObserver;
};

}}


#endif
