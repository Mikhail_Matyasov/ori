#ifndef _Ori_CodecTestLib_Helper_HeaderFile
#define _Ori_CodecTestLib_Helper_HeaderFile

#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_CodecType.hxx>

namespace Ori {
namespace Codec {
class Packet;
}

namespace CodecTestLib {

class Helper
{
public:
	static Ori::Base::Frame CreateFrame (int theWidth, int theHeight, int theSeed);
	static Ori::Base::Frame CreateRGBAFrame (int theWidth, int theHeight, int theSeed);
	static std::shared_ptr <Base::Vector <Ori::Base::Frame>> CreateFrames (int theWidth, int theHeight, int theCount = 30);
	static std::shared_ptr <Base::Vector <Ori::Base::Frame>> CreateRGBAFrames (int theWidth, int theHeight, int theCount = 30);
	static std::shared_ptr <Base::Vector <Ori::Codec::Packet>> CreatePackets (int theWidth,
																			  int theHeight,
																			  Ori::Codec::CodecType theCodec,
																			  int theCount = 30);
	static void Helper::Show (const Ori::Base::Frame& theFame);
};

}}

#endif
