#include <Ori/CodecTestLib_Pch.hxx>
#include <Ori/CodecTestLib_Helper.hxx>

#include <Ori/Codec_Packet.hxx>
#include <Ori/Codec_CodecType.hxx>
#include <Ori/Codec_EncoderParameters.hxx>
#include <Ori/Codec_Encoder.hxx>
#include <Ori/ImageRenderer_Image.hxx>
#include <Ori/ImageRenderer_Renderer.hxx>

#include <tbb/task_arena.h>

using namespace Ori::Codec;
using namespace Ori::Base;
using namespace Ori::ImageRenderer;

namespace Ori {
namespace CodecTestLib {

static std::unordered_map <Base::String, std::shared_ptr <Base::Vector <Frame>>> myFrameMap;

namespace {
void Resize (std::shared_ptr <ByteArray>& theArr, int theSize)
{
    theArr = std::make_shared <ByteArray>();
    theArr->Resize (theSize);
}
}

Frame Helper::CreateFrame (int theWidth, int theHeight, int theSeed)
{
    int aYLenght =  theWidth * theHeight;
    int aULenght = aYLenght / 4;
    Frame aFrame (theWidth, theHeight);
    
    Vector <std::shared_ptr <ByteArray>> aData;
    aData.Resize (3);
   
    Resize (aData[0], aYLenght);
    Resize (aData[1], aULenght);
    Resize (aData[2], aULenght);

    const auto& aLinesize = aFrame.Stride();
    auto& aData0 = (*aData[0]);
    auto& aData1 = (*aData[1]);
    auto& aData2 = (*aData[2]);
    // Filling Y channel
    for (int i = 0; i < theHeight; i++) {
        for (int j = 0; j < theWidth; j++) {

            aData0[i * aLinesize[0] + j] = j + i + theSeed * 3;
        }
    }

    // Filling U and V
    for (int i = 0; i < theHeight / 2; i++) {
        for (int j = 0; j < theWidth / 2; j++) {

            int  anI1 = i * aLinesize[1] + j;
            int  anI2 = i * aLinesize[2] + j;

            aData1[anI1] = 128 + i + theSeed * 2;
            aData2[anI2] = 64 + j + theSeed * 5;
        }
    }

    auto& aVec = aFrame.Data();
    aVec.Resize (3);
    aVec[0] = aData[0];
    aVec[1] = aData[1];
    aVec[2] = aData[2];
    return aFrame;
}

Ori::Base::Frame Helper::CreateRGBAFrame (int theWidth, int theHeight, int theSeed)
{
	int aChannelSize = theWidth * theHeight * 4;
	Frame aFrame (theWidth, theHeight, Base::PixelFormat::RGBA);

	auto aDataP = std::make_shared <ByteArray>();
	aDataP->Resize (aChannelSize);
	auto& aData = *aDataP;
	for (int i = 0; i < theWidth * theHeight; i++) {
		aData[i] = theSeed % 255;
		aData[i + 1] = (theSeed * 2) % 255;
		aData[i + 2] = (theSeed * 3) % 255;
		aData[i + 3] = 255;
	}

	aFrame.Data().Resize (1);
	aFrame.Data()[0] = aDataP;
	return aFrame;
}

std::shared_ptr <Base::Vector <Frame>> Helper::CreateFrames (int theWidth, int theHeight, int theCount)
{
	String aKey = String (theWidth) + " x " + String (theHeight);
	auto& anIt = myFrameMap.find (aKey);

	if (anIt == myFrameMap.end()) {

		tbb::task_arena anArena;
		auto aFrames = std::make_shared <Base::Vector <Frame>>();
		for (int i = 0; i  < theCount; i++) {

			anArena.execute ([=]() -> void {

				auto aFrame = Helper::CreateFrame (theWidth, theHeight, i);
				aFrames->Push (aFrame);
			});

		}

		while (aFrames->Lenght() != theCount);
		myFrameMap.emplace (aKey, aFrames);
		return aFrames;
	}
	
	return anIt->second;
}

std::shared_ptr <Base::Vector <Ori::Base::Frame>> Helper::CreateRGBAFrames (int theWidth, int theHeight, int theCount)
{
	auto aFrames = std::make_shared <Base::Vector <Ori::Base::Frame>>();
	for (int i = 0; i < theCount; i++)  {
		auto aFrame = CreateRGBAFrame (theWidth, theHeight, i);
		aFrames->Push (aFrame);
	}

	return aFrames;
}

void Helper::Show (const Frame& theFame)
{
	const auto& aData = theFame.Data();
	const auto& aPitch = theFame.Stride();
	int aWidth = theFame.Width();
	int aHeight = theFame.Height();

	Image anImage (aData[0]->Data(), aPitch[0],
				   aData[1]->Data(), aPitch[1],
				   aData[2]->Data(), aPitch[2],
				   ImageRenderer::PixelFormat::YUV420,
				   aWidth, aHeight);

	/*Renderer aRenderer;
	aRenderer.Render (anImage);*/
}

std::shared_ptr <Base::Vector <Ori::Codec::Packet>> Helper::CreatePackets (int theWidth,
																		   int theHeight,
																		   CodecType theCodec,
																		   int theCount)
{
	auto aFrames = CreateFrames (theWidth, theHeight, theCount);

	EncoderParameters aParameters;
	aParameters.Width() = theWidth;
	aParameters.Height() = theHeight;
	aParameters.VideoCodecType() = theCodec;
	
	Encoder anEncoder (aParameters);
	auto aPackets = std::make_shared <Base::Vector <Packet>>();
	for (int i = 0; i < aFrames->Lenght(); i++) {

		auto& aFrame = (*aFrames)[i];
		EXPECT_TRUE (anEncoder.EncodeFrame (aFrame));

		if (auto aPacket = anEncoder.GetPacket()) {
			aPackets->Push (aPacket.Clone());
		}
	}

	return aPackets;
}

}}

