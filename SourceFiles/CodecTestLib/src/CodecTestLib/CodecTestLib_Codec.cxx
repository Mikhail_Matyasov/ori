#include <Ori/CodecTestLib_Pch.hxx>

#include <Ori/Base_Vector.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/Base_StreamWriter.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/CodecTestLib_Helper.hxx>
#include <Ori/Codec_EncoderParameters.hxx>
#include <Ori/Codec_Encoder.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_Muxer.hxx>
#include <Ori/Codec_MuxerParameters.hxx>
#include <Ori/Codec_Decoder.hxx>
#include <Ori/Codec_DecoderParameters.hxx>
#include <Ori/Codec_Scaler.hxx>
#include <Ori/Codec_ScalerParameters.hxx>
#include <Ori/Codec_AdaptiveEncoder.hxx>
#include <Ori/Codec_AdaptiveEncoderParameters.hxx>
#include <Ori/Codec_VideoSplitter.hxx>
#include <Ori/Base_TypeBuffer.hxx>

using namespace Ori::Codec;
using namespace Ori::Base;

namespace Ori {
namespace CodecTestLib {

namespace {

void EncoderTest (int theWidth,
				  int theHeight,
				  CodecType theCodecType = CodecType::VP8)
{
	EncoderParameters aParameters;
	aParameters.Width() = theWidth;
	aParameters.Height() = theHeight;
	aParameters.VideoCodecType() = theCodecType;

	Encoder anEncoder (aParameters);
	EXPECT_TRUE (anEncoder.Status() == Codec::Status::Success);
	Base::Timer aTimer;
	
	auto aFrames = Helper::CreateFrames (theWidth, theHeight);
	Base::Vector <Packet> aPackets;
	
	aTimer.Timestamp();
	for (int i = 0; i < aFrames->Lenght(); i++) {

		const auto& aFrame = (*aFrames)[i % 5];
		EXPECT_TRUE (anEncoder.EncodeFrame (aFrame));
		if (auto aPacket = anEncoder.GetPacket()) {

			EXPECT_TRUE (aPacket.Data()->Lenght() != 0);
			aPackets.Push (aPacket.Clone());
		}
	}
	ORI_LOG_WARNING_DEFAULT() << "Durartion: " << aTimer.DurationSinceLastTimestamp().Milliseconds() << "\n";
	EXPECT_TRUE (aPackets.Lenght() == 30);
}

void DecoderTest (int theWidth, int theHeight, CodecType theCodecType = CodecType::VP8)
{
	auto aPackets = Helper::CreatePackets (theWidth, theHeight, theCodecType);

	DecoderParameters aParameters;
	aParameters.Width() = theWidth;
	aParameters.Height() = theHeight;
	aParameters.VideoCodecType() = theCodecType;
	
	Decoder aDecoder (aParameters);
	Base::Timer aTimer;

	Base::Vector <Frame> aFrames;
	aTimer.Timestamp();
	for (int i = 0; i < aPackets->Lenght(); i++) {

		auto& aPacket = (*aPackets)[i];
		if (aDecoder.DecodePacket (aPacket)) {

			if (auto aFrame = aDecoder.GetFrame()) {

				// make sure, that data of the frame is touchable
				aFrames.Push (aFrame.Clone());
			}
		}
	}
	ORI_LOG_WARNING_DEFAULT() << "Durartion: " << aTimer.DurationSinceLastTimestamp().Milliseconds() << "\n";
	EXPECT_TRUE (aFrames.Lenght() == 30);
}

namespace {
Scaler CreateScaler (PixelFormat theSource, PixelFormat theTarget, int theWidth, int theHeight)
{
	ScalerParameters aParams;
	aParams.SourceWidth() = theWidth;
	aParams.SourceHeight() = theHeight;
	aParams.SourcePixelFormat() = theSource;
	aParams.TargetWidth() = theWidth;
	aParams.TargetHeight() = theHeight;
	aParams.TargetPixelFormat() = theTarget;

	Scaler aScaler (aParams);
	return aScaler;
}
}

void ScalerTest (int theWidth, int theHeight)
{
	auto aFrames = Helper::CreateFrames (theWidth, theHeight);
	auto aYuvToRgb = CreateScaler (PixelFormat::YUV420, PixelFormat::RGBA, theWidth, theHeight);
	auto anRgbToYuv = CreateScaler (PixelFormat::RGBA, PixelFormat::YUV420, theWidth, theHeight);

	Base::Timer aTimer;

	aTimer.Timestamp();
	for (const auto& aFrame : *aFrames) {

		auto anRgb = aYuvToRgb.Scale (aFrame);
		auto anRgbClone = anRgb.Clone();

		EXPECT_TRUE (anRgbClone.Data().Lenght() == 1);

		auto aYuv = anRgbToYuv.Scale (anRgb);
		auto aYuvClone = aYuv.Clone();

		EXPECT_TRUE (aYuvClone.Data().Lenght() == 3);
	}
	ORI_LOG_WARNING_DEFAULT() << "Durartion: " << aTimer.DurationSinceLastTimestamp().Milliseconds() << "\n";
}

void AdaptiveEncoderTest (int theWidth, int theHeight, int theTargetFPS = 30)
{
	AdaptiveEncoderParameters aParams;
	aParams.Width() = theWidth;
	aParams.Height() = theHeight;
	aParams.VideoCodecType() = CodecType::VP9;
	aParams.TargetFPS() = theTargetFPS;
	aParams.SourcePixelFormat() = PixelFormat::RGBA;

	AdaptiveEncoder anEncoder (aParams);
	auto aFrames = Helper::CreateRGBAFrames (aParams.Width(), aParams.Height(), 5);

	Base::Vector <Packet> aPackets;
	int i = 0;
	while (!anEncoder.IsCalibrated()) {

		const auto& aFrame = (*aFrames)[i % 5];
		auto aPacket = anEncoder.EncodeFrame (aFrame);
		EXPECT_TRUE (aPacket == true);
		aPackets.Push (aPacket.Clone());
		i++;
	}
	EXPECT_TRUE (anEncoder.CalibratedWidth() <= theWidth);
	EXPECT_TRUE (anEncoder.CalibratedHeight() <= theHeight);

	ORI_LOG_WARNING_DEFAULT() << "Calibrated width: " << anEncoder.CalibratedWidth() << "\n";
	ORI_LOG_WARNING_DEFAULT() << "Calibrated height: " << anEncoder.CalibratedHeight() << "\n";
	ORI_LOG_WARNING_DEFAULT() << "Calibrated fps: " << anEncoder.CalibratedFPS() << "\n";
}

}

// To do tests for different resolutions

TEST (Codec, Encode_vp8_320x240)
{
	EncoderTest (320, 240);
}

TEST (Codec, Encode_vp8_240x320)
{
	EncoderTest (240, 320);
}

TEST (Codec, Encode_vp8_640x480)
{
	EncoderTest (640, 480);
}

TEST (Codec, Encode_vp8_480x640)
{
	EncoderTest (480, 640);
}

TEST (Codec, Encode_vp8_1280x720)
{
	EncoderTest (1280, 720);
}

TEST (Codec, Encode_vp8_720x1280)
{
	EncoderTest (720, 1280);
}

TEST (Codec, Encode_vp8_1920x1080)
{
	EncoderTest (1920, 1080);
}

TEST (Codec, Encode_vp8_1080x1920)
{
	EncoderTest (1080, 1920);
}

TEST (Codec, Encode_vp9_320x240)
{
	EncoderTest (320, 240, CodecType::VP9);
}

TEST (Codec, Encode_vp9_240x320)
{
	EncoderTest (240, 320, CodecType::VP9);
}

TEST (Codec, Encode_vp9_640x480)
{
	EncoderTest (640, 480, CodecType::VP9);
}

TEST (Codec, Encode_vp9_480x640)
{
	EncoderTest (480, 640, CodecType::VP9);
}

TEST (Codec, Encode_vp9_1280x720)
{
	EncoderTest (1280, 720, CodecType::VP9);
}

TEST (Codec, Encode_vp9_720x1280)
{
	EncoderTest (720, 1280, CodecType::VP9);
}

TEST (Codec, Encode_vp9_1920x1080)
{
	EncoderTest (1920, 1080, CodecType::VP9);
}

TEST (Codec, Encode_vp9_1080x1920)
{
	EncoderTest (1080, 1920, CodecType::VP9);
}

TEST (Codec, EncodeAdaptively_vp9_5160x4280)
{
	AdaptiveEncoderTest (5160, 4280, 30);
}

TEST (Codec, EncodeAdaptively_vp9_320x240)
{
	AdaptiveEncoderTest (320, 240);
}

TEST (Codec, EncodeAdaptively_vp9_2560x2040)
{
	AdaptiveEncoderTest (2560, 2040, 50);
}

TEST (Codec, MaximumCompressionEncoding)
{
	EncoderParameters aParameters;
	aParameters.Width() = 640;
	aParameters.Height() = 480;
	aParameters.VideoCodecType() = CodecType::VP9;
	aParameters.KeyFrameInterval() = 100;
	aParameters.Mode() = CodecMode::MaximumCompression;

	Encoder anEncoder (aParameters);
	EXPECT_TRUE (anEncoder.Status() == Codec::Status::Success);
	
	auto aFrames = Helper::CreateFrames (640, 480, 30);
	int aSum = 0;
	Timer aTimer;
	aTimer.Timestamp();
	for (const auto& aFrame : *aFrames) {
		if (anEncoder.EncodeFrame (aFrame)) {
			if (auto aPacket = anEncoder.GetPacket()) {

				aSum += aPacket.Data()->Lenght();
			}
		}
	}

	EXPECT_TRUE (aSum == 9683);
}

TEST (Codec, MuxData)
{
#if 0
	MuxerParameters aParameters;
	aParameters.Width() = 1366;
	aParameters.Height() = 768;
	Muxer aMuxer (aParameters);
	auto aPackets = CreatePackets (aParameters.Width(), aParameters.Height());

	int aNumberFragments = 0;
	Base::Timer aTimer;

	aTimer.Start();
	for (int i = 0; i < aPackets->Lenght(); i++) {
		ASSERT_TRUE (aMuxer.MuxPacket ((*aPackets)[i]));

		Fragment aFragment;
		if (aMuxer.GetFragment (aFragment)) {
			ASSERT_TRUE (!aFragment.Data().Empty());

			aNumberFragments++;
			Fragment aNewFragment;
			ASSERT_FALSE (aMuxer.GetFragment (aNewFragment));
		}
	}
	aTimer.Stop();

	ASSERT_TRUE (aTimer.TimerDuration().Milliseconds() < 0.05);
	ASSERT_TRUE  (aNumberFragments == 7);
#endif
}

TEST (Codec, Decode_320x240)
{
	DecoderTest (320, 240);
}

TEST (Codec, Decode_240x320)
{
	DecoderTest (240, 320);
}

TEST (Codec, Decode_640x480)
{
	DecoderTest (640, 480);
}

TEST (Codec, Decode_480x640)
{
	DecoderTest (480, 640);
}

TEST (Codec, Decode_1280x720)
{
	DecoderTest (1280, 720);
}

TEST (Codec, Decode_720x1280)
{
	DecoderTest (720, 1280);
}

TEST (Codec, Decode_1920x1080)
{
	DecoderTest (1920, 1080);
}

TEST (Codec, Decode_1080x1920)
{
	DecoderTest (1080, 1920);
}

TEST (Codec, Scale_320x240)
{
	ScalerTest (320, 240);
}

TEST (Codec, Scale_240x320)
{
	ScalerTest (240, 320);
}

TEST (Codec, Scale_640x480)
{
	ScalerTest (640, 480);
}

TEST (Codec, Scale_480x640)
{
	ScalerTest (480, 640);
}

TEST (Codec, Scale_1280x720)
{
	ScalerTest (1280, 720);
}

TEST (Codec, Scale_720x1280)
{
	ScalerTest (720, 1280);
}

TEST (Codec, Scale_1920x1080)
{
	ScalerTest (1920, 1080);
}

TEST (Codec, Scale_1080x1920)
{
	ScalerTest (1080, 1920);
}

TEST (Codec, CloneFrame)
{
	auto aFrames = Helper::CreateFrames (320, 240, 1);
	auto& aSource = (*aFrames)[0];
	auto aClone = aSource.Clone();

	EXPECT_TRUE (aSource.Width() == aClone.Width());
	EXPECT_TRUE (aSource.Height() == aClone.Height());
	EXPECT_TRUE (aSource.PixelFormat() == aClone.PixelFormat());
	EXPECT_TRUE (aSource.Stride() == aClone.Stride());

	EXPECT_TRUE ((*aSource.Data()[0]) == (*aClone.Data()[0]));
	EXPECT_TRUE ((*aSource.Data()[1]) == (*aClone.Data()[1]));
	EXPECT_TRUE ((*aSource.Data()[2]) == (*aClone.Data()[2]));
}

TEST (Codec, ClonePacket)
{
	auto aPackets = Helper::CreatePackets (320, 240, CodecType::VP9, 1);
	auto& aSource = (*aPackets)[0];
	auto aClone = aSource.Clone();

	EXPECT_TRUE ((*aSource.Data()) == (*aClone.Data()));

}

TEST (Codec, SplitVideo)
{
	String aFileName = "C:/Users/Mikhail/Desktop/PeopleMotion.mpeg";
	auto aFrames = VideoSplitter::Split (aFileName);

	int aWidth = aFrames[0].Width();
	int aHeight = aFrames[0].Height();
	auto aYuvToRgb = CreateScaler (PixelFormat::YUV420, PixelFormat::RGBA, aWidth, aHeight);

	StreamWriter aWriter ("C:/Users/Mikhail/Desktop/PeopleMotion.rgba");
	for (const auto& aFrame : aFrames) {
		auto anRGBA = aYuvToRgb.Scale (aFrame);
		const auto& aData = *anRGBA.Data()[0];
		
		TypeBuffer <uint16_t> aWidth (aFrame.Width());
		TypeBuffer <uint16_t> aHeigth (aFrame.Height());
		aWriter.Write (aWidth.Bytes, sizeof (uint16_t));
		aWriter.Write (aHeigth.Bytes, sizeof (uint16_t));
		aWriter.Write (aData);
	}
}


}}
