#include <Ori/CodecTestLib_Pch.hxx>

#include <Ori/CodecTestLib_Helper.hxx>
#include <Ori/CodecJS_Decoder.hxx>
#include <Ori/CodecJS_DecoderParameters.hxx>
#include <Ori/CodecJS_ScalerParameters.hxx>
#include <Ori/CodecJS_Scaler.hxx>
#include <Ori/CodecJS_Packet.hxx>
#include <Ori/CodecJS_Frame.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Codec_CodecType.hxx>
#include <Ori/CodecJS_ObjectStorage.hxx>

namespace Ori {
namespace CodecTestLib {
using namespace Ori::Codec;
using namespace Ori::CodecJS;

namespace {
int DecodeFrame (int theWidth, int theHeight, CodecType theType)
{
    int aParams = Ori_Codec_CreateDecoderParameters();

    Ori_Codec_DecoderParameters_SetWidth (aParams, theWidth);
    Ori_Codec_DecoderParameters_SetHeight (aParams, theHeight);
    Ori_Codec_DecoderParameters_SetVideoCodecID (aParams, static_cast <int> (theType));

    int aDecoder = Ori_Codec_CreateDecoder (aParams);
    auto aPackets = Helper::CreatePackets (theWidth, theHeight, theType, 1);
    auto& aPacket = (*aPackets)[0];

    const uint8_t* aData = aPacket.Data()->Data();
    int aLenght = aPacket.Data()->Lenght();

    int aPacketIndex = Ori_Codec_Decoder_MakePacket (aDecoder, const_cast <uint8_t*> (aData), aLenght);
    int aRes = Ori_Codec_Decoder_DecodePacket (aDecoder, aPacketIndex);
    if (aRes == 1) {
        Ori_Codec_Packet_Free (aPacketIndex);
    }
                
    int aFrame = Ori_Codec_Decoder_GetFrame (aDecoder);
    EXPECT_TRUE (aFrame != -1);
    Ori_Codec_DecoderParameters_Free(aParams);
    Ori_Codec_Decoder_Free (aDecoder);

    return aFrame;
}
}

TEST (CodecJS, Decode_VP8)
{
    DecodeFrame (352, 288, CodecType::VP8);
}

TEST (CodecJS, Decode_VP9)
{
    DecodeFrame (320, 240, CodecType::VP9);
}

TEST (CodecJS, ScaleFrame)
{
    int aWidth = 352;
    int aHeight = 288;
    auto aFrame = DecodeFrame (aWidth, aHeight, CodecType::VP8);

    int aParameters = Ori_Codec_CreateScalerParameters();

    Ori_Codec_ScalerParameters_SetSourceWidth (aParameters, aWidth);
    Ori_Codec_ScalerParameters_SetSourceHeight (aParameters, aHeight);
    Ori_Codec_ScalerParameters_SetTargetWidth (aParameters, aWidth);
    Ori_Codec_ScalerParameters_SetTargetHeight (aParameters, aHeight);
    Ori_Codec_ScalerParameters_SetSourcePixelFormat (aParameters, static_cast <int> (Base::PixelFormat::YUV420));
    Ori_Codec_ScalerParameters_SetTargetPixelFormat (aParameters, static_cast <int> (Base::PixelFormat::RGBA));

    int aScaler = Ori_Codec_CreateScaler (aParameters);
    int aConvertedFrameIndex = Ori_Codec_Scaler_ConvertPixelFormat (aScaler, aFrame);

    auto aConvertedFrame = ObjectStorage::Instance()->FrameMap()[aConvertedFrameIndex];

    int anRGBALenght = aWidth * aHeight * 4;
    EXPECT_TRUE (aConvertedFrame.Data()[0]->Lenght() == anRGBALenght);
}

}}
