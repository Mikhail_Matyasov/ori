#include <Ori/CodecTestLib_Pch.hxx>
#include <Ori/Base_Exception.hxx>
#include <Ori/Base_Logger.hxx>

int main (int argc, char** argv)
{
    testing::GTEST_FLAG(filter) = "Codec.SplitVideo";
    //testing::GTEST_FLAG(filter) = "Codec.MaximumCompressionEncoding";

    Ori::Base::Logger::SetDefaultLevel (Ori::Base::LogLevel::Warning);
    testing::InitGoogleTest (&argc, argv);

    RUN_ALL_TESTS();
}