﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Ori.OriTube
{
public class OriTube_Main
{
    public static void Main (string[] args)
    {
        CreateWebHostBuilder (args).Build().Run();
    }

    public static IHostBuilder CreateWebHostBuilder (string[] theArgs) =>
                    Host.CreateDefaultBuilder (theArgs)
                    .UseContentRoot (Directory.GetCurrentDirectory())
                    .ConfigureWebHostDefaults (theBuilder => {
                        theBuilder.UseStartup <Startup>();
                    });

}
}
