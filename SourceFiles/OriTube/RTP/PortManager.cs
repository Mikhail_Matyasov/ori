﻿using System.Collections.Generic;

namespace Ori.OriTube.RTP
{
public class PortManager
{
    private static List <int> myRTPPorts =  new List <int>();
        
    static PortManager()
    {
        for (int i = 49000; i < 49100; i++) {
                
            // RTP ports - i % 3 == 0
            // RTCP ports -  i % 3 == 1
            // TCP ports -  i % 3 == 2
            if (i % 2 == 0) {
                myRTPPorts.Add (i);
                i += 3;
            }
        }
    }
    public static int Reserve()
    {
        if (myRTPPorts.Count != 0) {
            int aRes = myRTPPorts [0];
            myRTPPorts.RemoveAt (0);
            return aRes;
        }
        return -1;
    }

    public static void Release (int thePort)
    {
        myRTPPorts.Add (thePort);
    }
}

}
