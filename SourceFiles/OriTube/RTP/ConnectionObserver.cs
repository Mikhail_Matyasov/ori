﻿using Ori.OriTube.Base;
using System.Collections.Generic;

namespace Ori.OriTube.RTP
{

public class ConnectionObserver : Ori.Net.RTPListener.Observer
{
    public List <Ori.WebRTC.Connection> Peers { get; set; }

    public ConnectionObserver()
    {
        Peers = new List <Ori.WebRTC.Connection>();
    }
    public override void OnPacket (Ori.Base.ByteArray thePacket)
    {
        if (Peers.Count != 0) {

            foreach (var aPeer in Peers) {
                aPeer.Send (thePacket, false);
            }
        }
    }
}

}
