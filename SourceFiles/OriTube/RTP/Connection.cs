﻿using System;
using System.Threading;

namespace Ori.OriTube.RTP
{
    public class Connection
{

    public Ori.RTP.ConnectionDescriptor     Descriptor { get; set; }
    public int                              RTPPort { get; }

    private ConnectionObserver              myObserver;
    private Ori.Net.RTPListener             myListener;
    private Ori.Base.Logger                 myLogger;

    public Connection (Ori.RTP.ConnectionDescriptor theDescriptor)
    {
        Descriptor = theDescriptor;
        RTPPort = PortManager.Reserve();
        int aTcpPort = RTPPort + 2;
        if (RTPPort != -1) {
            myObserver = new ConnectionObserver();
            myLogger = new Ori.Base.Logger (OriTube.Base.Logger.Instance);
            myLogger.SetLevel (Ori.Base.LogLevel.Warning);

            myListener = new Ori.Net.RTPListener (RTPPort, aTcpPort, myObserver, myLogger);
            if (myListener.Status() ==  Ori.Net.RTPSessionStatus.Fail) {
                PortManager.Release (RTPPort);
                RTPPort = -2;
            }
        }
    }

    public void Start()
    {
        myListener.Start();
    }

    public void Close()
    {
        myListener.Stop();
        PortManager.Release (RTPPort);
    }

    public void AddPeer (Ori.WebRTC.Connection thePeer)
    {
        myObserver.Peers.Add (thePeer);
    }

    public void RemovePeer (Ori.WebRTC.Connection thePeer)
    {
        myObserver.Peers.Remove (thePeer);
    }

}


}
