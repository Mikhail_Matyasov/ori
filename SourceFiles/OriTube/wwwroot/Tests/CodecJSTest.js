﻿"use strict"

OriTube.TestLib.CodecJSTest = function()
{
    this.myIsRunTimeInitialized = false;
    let aContext = this;

    Module.onRuntimeInitialized = function ()
    {
        aContext.myIsRunTimeInitialized = true;
    }

}

OriTube.TestLib.CodecJSTest.prototype.InvokeAfterRuntime = function (theFunc)
{
    let aContext = this;
    setTimeout (() => {
        if (!this.myIsRunTimeInitialized) {
            aContext.InvokeAfterRuntime (theFunc);
        } else  {
            theFunc();
        }
    }, 50);
}
OriTube.TestLib.CodecJSTest.myIsRunTimeInitialized;

OriTube.TestLib.CodecJSTest.prototype.GetTestPacket = async function(theFileName)
{
    let aContext = this;
    let aHubConnection = new signalR.HubConnectionBuilder()
        .withUrl ("/testhub")
        .configureLogging (signalR.LogLevel.Information)
        .build();

    if (aHubConnection.state != 1) {
        await aHubConnection.start();
    }

    aHubConnection.on ("CodecJS_TestPacketReceived", (theFileData) => {

        var aBinString = window.atob (theFileData);
        let aBytes = new Uint8Array (aBinString.length);
        for (let i = 0; i < aBinString.length; i++) {
            aBytes[i] = aBinString.charCodeAt (i);
        }
        aContext.myPacket = aBytes;
        aContext.OnTestPacketReceived (aBytes);
    });

    aHubConnection.send ("OnCodecJS_GetTestPacket", theFileName);
}

OriTube.TestLib.CodecJSTest.prototype.CreatePixelData = function (theWidth, theHeight, theRed, theGreen, theBlue)
{
    let aDataLenght = theWidth * theHeight * 4;
    let aData = new Uint8ClampedArray (aDataLenght);

    for (let i = 0; i < aDataLenght; i += 4) {
        aData[i] = theRed;
        aData[i + 1] = theGreen;
        aData[i + 2] = theBlue;
        aData[i + 3] = 255;
    }

    return aData;
}

OriTube.TestLib.CodecJSTest.prototype.DecodePacketTest = function (thePacketWidth, thePacketHeight, theCodec, theTestByte, done)
{
    aCodecJSTestInit.InvokeAfterRuntime (() => {

            aCodecJSTestInit.OnTestPacketReceived = (theFileData) => {
                let aParams = new OriTube.CodecJS.DecoderParameters();
                aParams.SetWidth (thePacketWidth);
                aParams.SetHeight (thePacketHeight);

                if (theCodec == "vp8") {
                    aParams.SetVideoCodecID (OriTube.CodecJS.CodecType.VP8);
                } else {
                    aParams.SetVideoCodecID (OriTube.CodecJS.CodecType.VP9);
                }
                
                let aDecoder = new OriTube.CodecJS.Decoder (aParams);
                let aPacket = aDecoder.MakePacket (theFileData);
                
                let aRes = aDecoder.DecodePacket (aPacket);
                assert (aRes, "can not decode packet");
                aPacket.Free();
                
                let aFrame = aDecoder.GetFrame();
                assert (aFrame.myImpl != -1, "can not get frame");

                let aFirstData = aFrame.GetDataByIndex (0);
                assert (aFirstData.length != 0, "received empty frame");

                let aTestByte = aFirstData[1234];
                assert (aTestByte == theTestByte, "Decoded frame incorrect");

                aDecoder.Free();
                aParams.Free();
                aFrame.Free();

                done();
            }
            aCodecJSTestInit.GetTestPacket ("packet." + theCodec);
            
        });
}

OriTube.TestLib.CodecJSTest.prototype.OnTestPacketReceived = function (theFileData) {}
OriTube.TestLib.CodecJSTest.prototype.myPacket;


let aCodecJSTestInit = new OriTube.TestLib.CodecJSTest();

describe ('CodecJS', function()
{
    it('DecoderParameters_SetHeight', (done) => {

        aCodecJSTestInit.InvokeAfterRuntime (() => {
            
            let anExpectedValue = 123;
            let aParameters = new OriTube.CodecJS.DecoderParameters();
            aParameters.SetHeight (anExpectedValue);
            let aRes = aParameters.GetHeight();

            assert (anExpectedValue == aRes)
            done();
        });

    }).timeout(0);

    it('DecoderParameters_SetVideoCodecID', (done) => {
        
        aCodecJSTestInit.InvokeAfterRuntime (() => {

            let anExpectedValue = OriTube.CodecJS.CodecType.VP8;
            let aParameters = new OriTube.CodecJS.DecoderParameters();
            aParameters.SetVideoCodecID (anExpectedValue);

            let aRes = aParameters.GetVideoCodecID();

            assert (anExpectedValue == aRes);
            done();

        });

    }).timeout (0);

    it('ScalerParameters_SetSourceHeight', (done) => {
        
        aCodecJSTestInit.InvokeAfterRuntime (() => {

            let anExpectedValue = 1368;
            let aParameters = new OriTube.CodecJS.ScalerParameters();
            aParameters.SetSourceHeight (anExpectedValue);

            let aRes = aParameters.GetSourceHeight();

            assert (anExpectedValue == aRes);
            done();

        });

    }).timeout (0);

    it('ScalerParameters_SetTargetWidth', (done) => {
        
        aCodecJSTestInit.InvokeAfterRuntime (() => {

            let anExpectedValue = 768;
            let aParameters = new OriTube.CodecJS.ScalerParameters();
            aParameters.SetTargetWidth (anExpectedValue);

            let aRes = aParameters.GetTargetWidth();

            assert (anExpectedValue == aRes);
            done();

        });

    }).timeout (0);

    it('ScalerParameters_SetTargetPixelFormat', (done) => {
        
        aCodecJSTestInit.InvokeAfterRuntime (() => {

            let anExpectedValue = OriTube.CodecJS.PixelFormat.RGBA;
            let aParameters = new OriTube.CodecJS.ScalerParameters();
            aParameters.SetTargetPixelFormat (anExpectedValue);

            let aRes = aParameters.GetTargetPixelFormat();

            assert (anExpectedValue == aRes);
            done();

        });

    }).timeout (0);

    it('Decoder_Decode_VP8_Packet', (done) => {
        aCodecJSTestInit.DecodePacketTest (352, 288, "vp8", 85, done);
    }).timeout (0);

    it('Decoder_Decode_VP9_Packet', (done) => {
        aCodecJSTestInit.DecodePacketTest (320, 240, "vp9", 86, done);
    }).timeout (0);

    it('Scaler_ScaleFrame', (done) => {
        
        aCodecJSTestInit.InvokeAfterRuntime (() => {

            aCodecJSTestInit.OnTestPacketReceived = (theFileData) => {

                let aParams = new OriTube.CodecJS.DecoderParameters();
                aParams.SetWidth (352);
                aParams.SetHeight (288);
                let aDecoder = new OriTube.CodecJS.Decoder (aParams);
                let aPacket = aDecoder.MakePacket (theFileData);
                
                aDecoder.DecodePacket (aPacket);
                let aFrame = aDecoder.GetFrame();
                
                let aScalerParams = new OriTube.CodecJS.ScalerParameters();
                aScalerParams.SetSourceWidth (aParams.GetWidth());
                aScalerParams.SetSourceHeight (aParams.GetHeight());
                aScalerParams.SetTargetWidth (aParams.GetWidth());
                aScalerParams.SetTargetHeight (aParams.GetHeight());
                aScalerParams.SetSourcePixelFormat (OriTube.CodecJS.PixelFormat.YUV420);
                aScalerParams.SetTargetPixelFormat (OriTube.CodecJS.PixelFormat.RGBA);

                let aScaler = new OriTube.CodecJS.Scaler (aScalerParams);
                let aConvertedFrame = aScaler.ConvertPixelFormat (aFrame);

                aScaler.Free();
                aDecoder.Free();
                aScalerParams.Free();
                aParams.Free();

                let anRGBA = aConvertedFrame.GetDataByIndex (0);
                assert (anRGBA[3] == 255, "color format is not RGBA.");
                assert (anRGBA[7] == 255, "color format is not RGBA.");
                assert (anRGBA[11] == 255, "color format is not RGBA.");

                aPacket.Free();
                aFrame.Free();
                aConvertedFrame.Free();
                done();

            }
            aCodecJSTestInit.GetTestPacket ("packet.vp8");

        });

    }).timeout (0);

    it('PacketRenderer_RenderPacket', (done) => {
        
        aCodecJSTestInit.InvokeAfterRuntime (() => {

            aCodecJSTestInit.OnTestPacketReceived = (theFileData) => {

                let $aVideoElem = $("#video");

                let aCanvas = document.createElement ("canvas");
                aCanvas.width = $aVideoElem.width();
                aCanvas.height = $aVideoElem.height();

                let aFrameWidth = 352;
                let aFrameHeight = 288;
                let aRenderer = new OriTube.PacketRenderer (aFrameWidth, aFrameHeight, OriTube.CodecJS.CodecType.VP8, aCanvas);

                $aVideoElem[0].srcObject = aCanvas.captureStream();
                $aVideoElem[0].play();
                aRenderer.Render (theFileData);
                done();

            }
            aCodecJSTestInit.GetTestPacket ("packet.vp8");
        });

    }).timeout (0);

    it('PacketRenderer_DisplayPixelData', () => {

        aCodecJSTestInit.InvokeAfterRuntime (() => {
            let $aVideoElem = $("#video2");

            let aCanvas = document.createElement ("canvas");
            aCanvas.width = $aVideoElem.width();
            aCanvas.height = $aVideoElem.height();

            let aRenderer = new OriTube.PacketRenderer (aCanvas.width, aCanvas.height, OriTube.CodecJS.CodecType.VP8, aCanvas);
            
            //'cuptureStream' on 'canvas element' must be invoked after initialization
            // Canvas context (webgl, 2d, etc.)
            // Canvas context may be initialized by using 'getContext' method
            // In current case canvas context is initialized by PacketRenderer
            $aVideoElem[0].srcObject = aCanvas.captureStream();
            $aVideoElem[0].play();

            let aPixelData = aCodecJSTestInit.CreatePixelData (aCanvas.width, aCanvas.height, 255, 0, 0);
            aRenderer.DisplayFrame (aPixelData);
        });
    }).timeout (0);

    it('PacketRenderer_DisplayPixelDataOnDifferentSizes', () => {

        aCodecJSTestInit.InvokeAfterRuntime (() => {
            let $aVideoElem = $("#video3");

            let aCanvas = document.createElement ("canvas");
            aCanvas.width = $aVideoElem.width() / 10;
            aCanvas.height = $aVideoElem.height() / 10;

            let aRenderer = new OriTube.PacketRenderer (aCanvas.width, aCanvas.height, OriTube.CodecJS.CodecType.VP8, aCanvas);
            $aVideoElem[0].srcObject = aCanvas.captureStream();
            $aVideoElem[0].play();

            let aPixelData = aCodecJSTestInit.CreatePixelData (aCanvas.width, aCanvas.height, 127, 0, 225);
            aRenderer.DisplayFrame (aPixelData);
        });
    }).timeout (0);

});