﻿"use strict"


describe ('RTPConnection', function () {

    it('Receive stream list', async (done) => {

        let aRequest = {
            Description: "My js description",
            Password: "my password"
        }
        
        var aClient = new OriTube.HubClient.RTPHub();

        aClient.OnConnectionCreated = function ()
        {
            aClient.OnStreamListReceived = (theList, theLenght) => {

                let aSourceDesc = aRequest.Description;
                let aReceivedDesc = theList[0].description;
                if (aSourceDesc == aReceivedDesc) {
                    aClient.Close();
                    done();
                }
            }

            aClient.GetStreamList();
        }

        aClient.OnError = function (theMessage) {
            console.log (theMessage);
        }
        
        aClient.CreateConnection (JSON.stringify (aRequest));
    }).timeout (0);

    it('Bind RTP connection and peer connection', async (done) => {
        let aRequest = {
            Description: "My js description",
            Password: "my password"
        }

        var aClient = new OriTube.HubClient.RTPHub();

        aClient.OnConnectionCreated = function () {

            aClient.OnStreamListReceived = (theList, theLenght) => {

                let aTransport = new OriTube.WebRTC.ConnectionTransport();
                let aPeerConnection = new OriTube.WebRTC.Connection (aTransport);

                aTransport.OnConnected = () => {
                    let aRTPId = theList[theLenght - 1].id;
                    let aPeerConnectionId = aTransport.HubConnectionId;

                    aClient.OnPeerAdded = () => {
                        done();
                    }

                    aClient.AddPeer (aRTPId, aPeerConnectionId);
                }

                aPeerConnection.Connect();

            }

            aClient.GetStreamList();
        }

        aClient.OnError = function (theMessage) {
            console.log (theMessage);
        }

        aClient.CreateConnection (JSON.stringify (aRequest));
    }).timeout (0);

});