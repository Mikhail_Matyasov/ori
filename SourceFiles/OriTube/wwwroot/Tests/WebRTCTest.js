﻿"use strict"

function CreateConnection (done, theNumberOfConnection)
{
    let aTransport = new OriTube.WebRTC.ConnectionTransport();
    let aPeerConnection = new OriTube.WebRTC.Connection (aTransport);

    aTransport.OnConnected = () => {
        if (theNumberOfConnection == 0) {
            done();
        } else {
            console.log (theNumberOfConnection - 1);
            CreateConnection (done, theNumberOfConnection - 1);
        }
    }
    aPeerConnection.Connect();
}

describe('WebRTC', function () {

    it('Connect', function (done) {
        CreateConnection (done, 0);
    }).timeout (0);

    it('RestartConnection', function (done) {
        let aTransport = new OriTube.WebRTC.ConnectionTransport();
        let aPeerConnection = new OriTube.WebRTC.Connection (aTransport);
        aTransport.OnConnected = () => {
            aTransport.OnCandidate = () => {
                done();
            }
            aPeerConnection.SendOffer (true);
        }
        aPeerConnection.Connect();
    }).timeout (0);

    it('Receive data', async (done) => {

        let aTransport = new OriTube.WebRTC.ConnectionTransport();
        let aPeerConnection = new OriTube.WebRTC.Connection (aTransport);

        aTransport.OnConnected = async () => {

            let aTestHub = new signalR.HubConnectionBuilder()
                                      .withUrl ("/testhub")
                                      .configureLogging (signalR.LogLevel.Information)
                                      .build();

            if (aTestHub.state != 1) {
                await aTestHub.start();
            }
            let aPeerConnectionId = aTransport.HubConnectionId;
            aTestHub.send ("OnPeerConnectionReceiveData", aPeerConnectionId);
        }

        aPeerConnection.OnGetMessage = (theMessage) => {
            let aByteArray = new Uint8Array (theMessage.data);
            assert (aByteArray.length != 0);
            done();
        }

        aPeerConnection.Connect();
    }).timeout(0);

});