﻿"use strict"

OriTube.OrgeoTitlesRequester = function() 
{
    this.myCurrentPage = "";
    this.myCurrentStyles = "";
    this.myCurrentScripts = "";

    this.myHubConnection = new signalR.HubConnectionBuilder()
                                      .withUrl ("/hub_orgeotitlesrequester")
                                      .configureLogging (signalR.LogLevel.Information)
                                      .build();

    this.myHubConnection.on ("Response", (theSource) => {
        this.myActionOnResponse (theSource);
    });
}

OriTube.OrgeoTitlesRequester.prototype.SendRequest = async function (theFuncName) 
{
    if (this.myHubConnection.state == 0) {
        await this.myHubConnection.start();
    }
    this.myHubConnection.send (theFuncName);
}

OriTube.OrgeoTitlesRequester.prototype.OnResponse = function (theAction) 
{
    this.myActionOnResponse = theAction;
}

OriTube.OrgeoTitlesRequester.myCurrentPage;
OriTube.OrgeoTitlesRequester.myCurrentStyles;
OriTube.OrgeoTitlesRequester.myCurrentScripts;
OriTube.OrgeoTitlesRequester.myActionOnResponse;