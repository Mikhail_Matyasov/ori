﻿
OriTube.HubClient.RTPHub = function() { 
    this.myRTPHub = new signalR.HubConnectionBuilder()
                                .withUrl ("/rtphub")
                                .configureLogging (signalR.LogLevel.Information)
                                .build();
}

OriTube.HubClient.RTPHub.prototype.Connect = async function ()
{
    if (this.myRTPHub.state != 1) {
        await this.myRTPHub.start();
        this.RegisterEvents();
    }
}

OriTube.HubClient.RTPHub.prototype.CreateConnection = async function (theDescriptor)
{
    await this.Connect();
    this.myRTPHub.send ("OnCreateConnection", theDescriptor);
}

OriTube.HubClient.RTPHub.prototype.GetStreamList = async function ()
{
    await this.Connect();
    this.myRTPHub.send ("OnGetStreamList");
}

OriTube.HubClient.RTPHub.prototype.AddPeer = async function (theRTPClientId, thePeerConnectionClientId)
{
    await this.Connect();
    this.myRTPHub.send ("OnAddPeer", theRTPClientId, thePeerConnectionClientId);
}

OriTube.HubClient.RTPHub.prototype.RemovePeer = async function (theRTPClientId, thePeerConnectionClientId)
{
    await this.Connect();
    this.myRTPHub.send ("OnRemovePeer", theRTPClientId, thePeerConnectionClientId);
}

OriTube.HubClient.RTPHub.prototype.RegisterEvents = function()
{
    let aContext = this;

    this.myRTPHub.on ("Can not allocate port", () => {
        aContext.OnError ("RTP Hub: Can not allocate port.");
    });

    this.myRTPHub.on ("Can not create rtp session", () => {
        aContext.OnError ("RTP Hub: Can not create rtp session.");
    });

    this.myRTPHub.on ("RTP connection created", (thePort) => {
        aContext.OnConnectionCreated (thePort);
    });

    this.myRTPHub.on ("Same RTP connection already exists", () => {
        aContext.OnError ("Same RTP connection already exists");
    });

    this.myRTPHub.on ("Exception", (theMessage) => {
        aContext.OnError ("Thrown an exception: " + theMessage);
    });

    this.myRTPHub.on ("Stream list received", (theList, theLenght) => {
        aContext.OnStreamListReceived (theList, theLenght);
    });

    this.myRTPHub.on ("RTP connection does not exist", (theId) => {
        aContext.OnError ("RTP connection with id: " + theId + " does not exists.");
    });

    this.myRTPHub.on ("Peer added", () => {
        aContext.OnPeerAdded();
    });

    this.myRTPHub.on ("Peer removed", () => {
        console.log ("Peer removed");
    });

    this.myRTPHub.on ("Close", () => {
        aContext.OnClosed();
    });
}

OriTube.HubClient.RTPHub.prototype.Close = function ()
{
    if (this.myRTPHub.state != 1) {
        return;
    }
    this.myRTPHub.send ("Close");
}

OriTube.HubClient.RTPHub.prototype.OnConnectionCreated = (thePort = "") => {};
OriTube.HubClient.RTPHub.prototype.OnStreamListReceived = (theList = [], theLenght = 0) => {};
OriTube.HubClient.RTPHub.prototype.OnPeerAdded = () => {};
OriTube.HubClient.RTPHub.prototype.OnError = (theMessage = "") => {};
OriTube.HubClient.RTPHub.prototype.OnClosed = () => {};
OriTube.HubClient.RTPHub.prototype.myRTPHub;