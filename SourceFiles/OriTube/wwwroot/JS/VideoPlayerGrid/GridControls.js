﻿
OriTube.GridControls = function ($thePlayer)
{
    this.$myPlayer = $thePlayer;
    let aContext = this;

    $("#videoplayer_grid_controls #left_control").mouseenter (() => {

        if (aContext.Orientation() == "portrait") {
            let aGridWidth = aContext.GridWidth();
            $("#videoplayer_grid").css ({
                width: aGridWidth + "px",
                height: "98%",
                top: "1%",
                left: "0.5%",
                transform: "translate(0%, 0%)",
                "background-color": "var(--gray)"
            });
            aContext.IsActive = true;
            aContext.CurrentActiveControl = "left_control";
        }
    });
    $("#videoplayer_grid_controls #left_control").mouseleave (() => {
        if (aContext.Orientation() == "portrait") {
            aContext.HideGrid();
        }
    });

    $("#videoplayer_grid_controls #right_control").mouseenter (() => {

        if (aContext.Orientation() == "portrait") {
            let aGridWidth = aContext.GridWidth();
            let aScreenWidth = $(document).width();

            // 0.005 - 0.5%
            let aLeft = aScreenWidth - aGridWidth - (0.005 * aScreenWidth);
            $("#videoplayer_grid").css ({
                width: aGridWidth + "px",
                height: "98%",
                top: "1%",
                left: aLeft + "px",
                transform: "translate(0%, 0%)",
                "background-color": "var(--gray)"
            });
            aContext.IsActive = true;
            aContext.CurrentActiveControl = "right_control";
        }
    });
    $("#videoplayer_grid_controls #right_control").mouseleave (() => {

        if (aContext.Orientation() == "portrait") {
            aContext.HideGrid();
        }
    });

    $("#videoplayer_grid_controls #center_control").mouseenter (() => {

        if (aContext.Orientation() == "landscape") {
            $("#videoplayer_grid").css ({
                width: "99%",
                height: "98%",
                top: "1%",
                left: "0.5%",
                transform: "translate(0%, 0%)",
                "background-color": "var(--gray)"
            });
            aContext.IsActive = true;
            aContext.CurrentActiveControl = "center_control";
        }
    });
    $("#videoplayer_grid_controls #center_control").mouseleave (() => {

        if (aContext.Orientation() == "landscape") {
            aContext.HideGrid();
        }
    });
}

OriTube.GridControls.prototype.HideGrid = function()
{
    $("#videoplayer_grid").css ({
        width: "0px",
        height: "0px",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        "background-color": "transparent"
    });
    this.IsActive = false;
    this.CurrentActiveControl = "";
}

OriTube.GridControls.prototype.GridWidth = function()
{
    let aWidth = this.$myPlayer.width();
    let aHeight = this.$myPlayer.height();
    let aBodyHeight = $(document).height();
    let aHeightToWidth = aHeight / aWidth;

    // 0.98 -> 98%
    let aGridWidth = aBodyHeight * 0.98 / aHeightToWidth;
    return aGridWidth;
}

OriTube.GridControls.prototype.Orientation = function()
{
    let aWidth = this.$myPlayer.width();
    let aHeight = this.$myPlayer.height();
    if (aWidth > aHeight) {
        return "landscape";
    }
    return "portrait";
}

OriTube.GridControls.prototype.Show = function()
{
    $("#videoplayer_grid_controls #left_control").css ({ display: "inline" });
    $("#videoplayer_grid_controls #right_control").css ({ display: "inline" });
    $("#videoplayer_grid_controls #center_control").css ({ display: "inline" });

    this.myGridControlsZIndex = $("#videoplayer_grid_controls").css ("z-index");
    $("#videoplayer_grid_controls").css ({ "z-index": 11 });

    let aPlayerWidth = this.$myPlayer.width();
    let aPlayerHeight = this.$myPlayer.height();
    if (aPlayerWidth > aPlayerHeight) { // landscape
        $("#videoplayer_grid_controls #left_control").css ({ "opacity": 0 });
        $("#videoplayer_grid_controls #right_control").css ({ "opacity": 0 });
    } else { // portrait
        $("#videoplayer_grid_controls #center_control").css ({ "opacity": 0 });
    }
}

OriTube.GridControls.prototype.Hide = function()
{
    $("#videoplayer_grid_controls #center_control").css ({ display: "none" });
    $("#videoplayer_grid_controls #left_control").css ({ display: "none" });
    $("#videoplayer_grid_controls #right_control").css ({ display: "none" });
    $("#videoplayer_grid_controls").css ({ "z-index": this.myGridControlsZIndex });

    let aPlayerWidth = this.$myPlayer.width();
    let aPlayerHeight = this.$myPlayer.height();
    if (aPlayerWidth > aPlayerHeight) { // landscape
       $("#videoplayer_grid_controls #left_control").css ({ "opacity": 1 });
       $("#videoplayer_grid_controls #right_control").css ({ "opacity": 1 });
    } else { // portrait
       $("#videoplayer_grid_controls #center_control").css ({ "opacity": 1 });
    }
}

OriTube.GridControls.prototype.$myPlayer = null;
OriTube.GridControls.prototype.myGridControlsZIndex = 0;
OriTube.GridControls.prototype.IsActive;
OriTube.GridControls.prototype.CurrentActiveControl;