﻿$(document).ready (function () {
    "use strict"

    var myIsButtonPlayVisible = false;
    var myTimeout;
    var myIsButtonStop = true;
    var myIsCursorOnPlayer = true;

    function BeginHideButton()
    {
        myTimeout = window.setTimeout(function () { // OnHide
            $(".MainPlayerUI_StopButton").css({
                width: 0,
                height: 0
            });
            myIsButtonPlayVisible = false;

            if (myIsCursorOnPlayer) {
                $("#MainPlayerUI_Container").css({
                    cursor: "none"
                });
            }
        }, 3500);
    }

    $("#MainPlayerUI_Container").mousemove(function (theE)
    {
        if ($(theE.target).attr("class") === "MainPlayerUI_StopButton MainPlayerUI_Button" ||
            $(theE.target).attr("id") === "MainPlayerUI_Container") {

            myIsCursorOnPlayer = true;
            if (myIsButtonStop) {

                if (myTimeout !== undefined) {
                    window.clearTimeout(myTimeout);
                }
                if (!myIsButtonPlayVisible) { // OnShow
                    $(".MainPlayerUI_StopButton").css({
                        width: "calc(100vw / 15)",
                        height: "calc(100vw / 15)"
                    });
                    $("#MainPlayerUI_Container").css({
                        cursor: "pointer"
                    });
                    myIsButtonPlayVisible = true;
                }
                BeginHideButton();
            }
        } else {
            myIsCursorOnPlayer = false;
        }
    });

    $("#MainPlayerUI_Container .MainPlayerUI_Button").click(function ()
    {
        window.clearTimeout(myTimeout);
        if (myIsButtonStop) {
            $(this).attr("class", "MainPlayerUI_PlayButton MainPlayerUI_Button");
            myIsButtonStop = false;
            myHubConnection.stop();
        } else {
            $(this).attr("class", "MainPlayerUI_StopButton MainPlayerUI_Button");
            BeginHideButton();
            myIsButtonStop = true;
            myHubConnection.start();
        }
    });

    function SaveBase64AsFile(theBase64, theFileName) {
        download(theBase64, theFileName, "application/base64");
    }

    $("#CameraButton").click(function () {
        var aUrl = $("#OriPlayer").css("backgroundImage");
        var aBase64Image = aUrl.substr(5, aUrl.length - 7);
        SaveBase64AsFile (aBase64Image, "OriTube_" + GenerateDate() + ".jpg");
    });

    function GenerateDate()
    {
        var aDate = new Date();
        var aYear = aDate.getFullYear();
        var aMonth = aDate.getMonth();
        var aDay = aDate.getDate();
        var anHour = aDate.getHours();
        var aMinutes = aDate.getMinutes();
        var aSeconds = aDate.getSeconds();

        return aDay + "." + aMonth + "." + aYear + "_" + anHour + "." + aMinutes + "." + aSeconds;
    }

    $("#add_orgeo_titles").click (function() {

        let $aBlock =  $(FLOATING_BLOCK);
        $aBlock.appendTo ($("#floating_block_container"));

        let aFloatingBlock = new OriTube.FloatingBlock ($aBlock);
        aFloatingBlock.ConstructOrgeoTitles();
    });

    $("#add_video_player").click (function(){

        new OriTube.VideoPlayer();
    });

});


