﻿"use strict"

OriTube.WebRTC.Connection = function (theTransport)
{
    const anIceServers = [];
    const aStunServer = { urls: "stun:stun.l.google.com:19302" }
    anIceServers.push (aStunServer);

    const aPeerConnectionConfig = {
        iceServers: anIceServers
    };

    this.myPeerConnection = new RTCPeerConnection (aPeerConnectionConfig);

    let aUuid = uuid.v4(); // Initiator of connection must create a Data channel
    this.mySendDataChannel = this.myPeerConnection.createDataChannel (aUuid);

    this.mySendDataChannel.onopen = function (e) {
        console.log ("Data channel was opened");
    }

    this.mySendDataChannel.onerror = (e) => {
        console.log ("Data channel error: " + e.data);
    }

    this.mySendDataChannel.onclose = (e) => {
        console.log ("Data channel is closed");
    }
    this.myTransport = theTransport;
}

OriTube.WebRTC.Connection.prototype.Connect = function()
{
    this.RegisterEvents();
    this.StartConnection();
}

OriTube.WebRTC.Connection.prototype.RegisterEvents = function ()
{
    let aContext = this;
    this.myPeerConnection.ondatachannel = function (e) {
        aContext.myReceiveDataChannel = e.channel;
        aContext.myReceiveDataChannel.binaryType = "arraybuffer";

        aContext.myReceiveDataChannel.onmessage = (theMessage) => {
            aContext.OnGetMessage (theMessage);
        };

        aContext.myReceiveDataChannel.onerror = (e) => {
            console.log ("Data channel error: " + e.data);
        }

        aContext.myReceiveDataChannel.onclose = (e) => {
            console.log ("Data channel is closed");
        }
    };

    this.myPeerConnection.onicecandidate = function (e) {
        console.log ("onicecandidate");
        if (e.candidate != null) {
            let aCandidate = JSON.stringify (e.candidate);
            aContext.myTransport.SendCandidate (aCandidate);
        }
    };

    this.myPeerConnection.oniceconnectionstatechange = async function (e) {
        
        if (aContext.myPeerConnection.iceConnectionState == "disconnected" ||
            aContext.myPeerConnection.iceConnectionState == "failed") {

            // if fail ocured, before connection esteblised
            if (typeof this.myReceiveDataChannel != "undefined") {
                await aContext.SendOffer (true);
            }
        }
        console.log ("oniceconnectionstatechange | " + aContext.myPeerConnection.iceConnectionState);
    }

    this.myTransport.OnCandidate = function (theCandidate)
    {
        aContext.myPeerConnection.addIceCandidate (theCandidate);
    }

    this.myTransport.OnAnswer = async function (theAnswer) {
        try {
            // Answer may come before any candidates will be generated
            if (aContext.myPeerConnection.signalingState != "stable") {
                await aContext.myPeerConnection.setRemoteDescription (theAnswer);
            }
        } catch (e) {
            console.log (e);
        }
    }
}

OriTube.WebRTC.Connection.prototype.StartConnection = async function ()
{
    await this.myTransport.StartConnection();
    await this.SendOffer (false);
}

OriTube.WebRTC.Connection.prototype.SendOffer = async function (theIsRestart)
{
    let anOffer;
    if (theIsRestart) {
        console.log ("Restart connection");
        anOffer = await this.myPeerConnection.createOffer ({ "iceRestart": true });
    } else {
        anOffer = await this.myPeerConnection.createOffer();
    }

    try {
        await this.myPeerConnection.setLocalDescription (anOffer);
    } catch (e) {
        console.log (e);
    }

    let aJsonOffer = JSON.stringify (anOffer);

    if (theIsRestart) {
        await this.myTransport.RestartConnection (aJsonOffer);
    } else {
        await this.myTransport.SendOffer (aJsonOffer);
    }
} 

OriTube.WebRTC.Connection.prototype.OnGetMessage = (theMessage) => {}
OriTube.WebRTC.Connection.prototype.myTransport;
OriTube.WebRTC.Connection.prototype.myPeerConnection;
OriTube.WebRTC.Connection.prototype.myReceiveDataChannel;

