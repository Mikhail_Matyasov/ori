﻿"use strict"

OriTube.WebRTC.ConnectionTransport = function ()
{
    this.myWebRTCHub = new signalR.HubConnectionBuilder()
                                  .withUrl ("/webrtchub")
                                  .configureLogging (signalR.LogLevel.Information)
                                  .build();
    this.RegisterEvents();
}

OriTube.WebRTC.ConnectionTransport.prototype.StartConnection = async function() {
    if (this.myWebRTCHub.state == 0) {
        await this.myWebRTCHub.start();
    }
}

OriTube.WebRTC.ConnectionTransport.prototype.SendOffer = async function (theOffer)
{
    this.StartConnection();
    this.myWebRTCHub.send ("OnOffer", theOffer);
}

OriTube.WebRTC.ConnectionTransport.prototype.RestartConnection = async function (theOffer)
{
    this.StartConnection();
    this.myWebRTCHub.send ("OnRestartConnection", theOffer);
}

OriTube.WebRTC.ConnectionTransport.prototype.SendCandidate = function (theCandidate)
{
    this.StartConnection();
    this.myWebRTCHub.send ("OnIceCandidate", theCandidate);
}

OriTube.WebRTC.ConnectionTransport.prototype.RegisterEvents = function()
{
    let aContext = this;
    this.myWebRTCHub.on ("Answer", (theAnswer) => {
        let anAnswer = JSON.parse (theAnswer);
        aContext.OnAnswer (anAnswer);
    });

    this.myWebRTCHub.on ("IceCandidate", async (theCandidate) => {
        let aCandidate = JSON.parse (theCandidate);
        aContext.OnCandidate (aCandidate);
    });

    this.myWebRTCHub.on ("GetIdSuccess", (theId) => {
        aContext.HubConnectionId = theId;
    });

    this.myWebRTCHub.on ("Connected", () => {
        console.log ("webrtc connection was established");
        aContext.OnConnected()
    });
    
}

OriTube.WebRTC.ConnectionTransport.prototype.OnCandidate;
OriTube.WebRTC.ConnectionTransport.prototype.OnAnswer;
OriTube.WebRTC.ConnectionTransport.OnConnected = () => {}
OriTube.WebRTC.ConnectionTransport.myWebRTCHub;
OriTube.WebRTC.ConnectionTransport.HubConnectionId;