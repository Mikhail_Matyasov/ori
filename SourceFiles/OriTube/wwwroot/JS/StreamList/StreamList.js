﻿"use strict"

OriTube.StreamList = function ($theParent)
{
    this.$myParent = $theParent;
    this.$myStreamList = $(STREAM_LIST);

    $theParent.append (this.$myStreamList);
    this.Open ($theParent);
}

OriTube.StreamList.prototype.Open = function ($theParent)
{
    this.$myStreamList.css ({
        width: $theParent.width() + "px",
        height: $theParent.height() + "px",
        top: -$theParent.height() + "px"
    });
    this.$myParent.resizable ("option", "alsoResize", this.$myStreamList);
    this.ScrollDown();
    $("[data-hover=orange_control]", $theParent).attr ("data-hover", "dark_control");
}

OriTube.StreamList.prototype.Close = function()
{
    this.$myParent.resizable ("option", "alsoResize", null);
    this.ScrollUp (true);
    $("[data-hover=dark_control]", this.$myParent).attr ("data-hover", "orange_control");
}

OriTube.StreamList.prototype.CloseFast = function()
{
    this.$myParent.resizable ("option", "alsoResize", null);
    this.ScrollUp (false);
    $("[data-hover=dark_control]", this.$myParent).attr ("data-hover", "orange_control");
}

OriTube.StreamList.prototype.ScrollDown = function()
{
    let aContext = this;
    this.$myStreamList.css ({ "display":"block" });
    this.$myStreamList.animate ({
        top: 0
    }, 200, () => {
        aContext.IsOpen = true;
        aContext.Load();
    });
}

OriTube.StreamList.prototype.ScrollUp = function (theIsAnimate)
{
    let anAnimationTime = theIsAnimate ? 200 : 0;
    let aContext = this;
    this.$myStreamList.animate ({
        top: -aContext.$myParent.height() + "px",
    }, anAnimationTime, () => {
        this.IsOpen = false;
        aContext.Unload();
        aContext.$myStreamList.css ({ "display":"none" });
    });
}

OriTube.StreamList.prototype.Load = function()
{
    let aContext = this;
    let aHubClient = new OriTube.HubClient.RTPHub();
    aHubClient.OnStreamListReceived = function (theList, theLenght) {

        for (let i = 0; i < theLenght; i++) {
            let aField = new OriTube.StreamListField (theList, i, aContext.$myParent, aContext);
            aContext.myListFields.push (aField);
        }
    }
    aHubClient.GetStreamList();
}

OriTube.StreamList.prototype.RemovePeer = function()
{
    for (let i = 0; i < this.myListFields.length; i++) {

        let aField = this.myListFields[i];
        if (aField.IsActive) {
            aField.RemovePeer();
        }
    }
}

OriTube.StreamList.prototype.Unload = function()
{
    this.$myStreamList.find (".stream_list_field").remove();
}

OriTube.StreamList.prototype.$myStreamList;
OriTube.StreamList.prototype.IsOpen = false;
OriTube.StreamList.prototype.$myParent;
OriTube.StreamList.prototype.myListFields = new Array();