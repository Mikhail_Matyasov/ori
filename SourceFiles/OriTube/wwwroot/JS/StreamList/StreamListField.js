﻿"use strict"

OriTube.StreamListField = function (theList, theIndex, $theParent, theStreamListClass)
{
    let $aStreamListFieldSchema = $(STREAM_LIST_FIELD);
    let anItem = theList[theIndex];

    this.Id = anItem.id;
    this.myPacketWidth = anItem.packetWidth;
    this.myPacketHeight = anItem.packetHeight;
    this.myCodecType = anItem.codecType;
    let $aDescriptionNode = $aStreamListFieldSchema.find (".description");
    $aDescriptionNode.html (anItem.description);
    
    let aContext = this;
    $aStreamListFieldSchema.click (function (e) {
        aContext.OnClick (e);
    });

    let $aContainer = $theParent.find (".stream_list_field_container");
    $aContainer.append ($aStreamListFieldSchema);
    this.myStreamList = theStreamListClass;
    this.$myParent = $theParent;
}

OriTube.StreamListField.prototype.SetDescription = function (theDescription)
{
    this.$myDescription.html (theDescription);
}

OriTube.StreamListField.prototype.InvokeAfterRuntime = function (theFunc)
{
    let aContext = this;
    setTimeout(() => {
        if (runtimeInitialized) {
            theFunc();
        } else {
            aContext.InvokeAfterRuntime (theFunc);
        }
    }, 50);
}

OriTube.StreamListField.prototype.OnClick = function (e)
{
    let aContext = this;
    this.IsActive = true;
    this.myStreamList.ScrollUp();

    let aCanvas = document.createElement ("canvas");
    aCanvas.width = this.myPacketWidth;
    aCanvas.height = this.myPacketHeight;

    let aVideoPlayer = this.$myParent.find (".ori_player")[0];
    this.InvokeAfterRuntime(() => {

        let aCodecType = OriTube.CodecJS.CodecType.VP9;
        if (aContext.myCodecType == "VP8") {
            aCodecType = OriTube.CodecJS.CodecType.VP8;
        }

        let aPacketRenderer = new OriTube.PacketRenderer (aCanvas.width, aCanvas.height, aCodecType, aCanvas);
        aVideoPlayer.srcObject = aCanvas.captureStream();
        aVideoPlayer.play();

        let aTransport = new OriTube.WebRTC.ConnectionTransport();
        let aConnection = new OriTube.WebRTC.Connection (aTransport);
        aContext.myRTPHub = new OriTube.HubClient.RTPHub();

        aConnection.OnGetMessage = (theMessage) => {
            const aData = new Uint8Array (theMessage.data);
            aPacketRenderer.Render (aData);
        }

        aTransport.OnConnected = () => {
            aContext.myWebRTCConnectionId = aTransport.HubConnectionId;
            aContext.myRTPHub.AddPeer (aContext.Id, aTransport.HubConnectionId);
            console.log ("Peer added");
        }

        aContext.myRTPHub.OnError = function (theMessage) {
            console.log (theMessage);
        }
        aConnection.Connect();
    });
}

OriTube.StreamListField.prototype.RemovePeer = function()
{
    this.myRTPHub.RemovePeer (this.Id, this.myWebRTCConnectionId);
}

OriTube.StreamListField.prototype.myWebRTCConnectionId;
OriTube.StreamListField.prototype.$myParent;
OriTube.StreamListField.prototype.myPacketWidth;
OriTube.StreamListField.prototype.myPacketHeight;
OriTube.StreamListField.prototype.myCodecType;
OriTube.StreamListField.prototype.myStreamList;
OriTube.StreamListField.prototype.myRTPHub;
OriTube.StreamListField.prototype.IsActive = false;
OriTube.StreamListField.prototype.Id;