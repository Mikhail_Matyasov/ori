﻿"use strict"

OriTube.OrgeoTitles.Touch = function ($theBlock)
{
    this.myIsClicked = false;
    let aContext = this;
    this.$myBlock = $theBlock;

    $(".Footer .Touch", $theBlock).click (function()
    {
        let $aCover = $(".Cover", $theBlock);
        if (aContext.myIsClicked) {
            $aCover.css ({ "display": "block" });
        } else {
            $aCover.css ({ "display": "none" });
        }

        aContext.DoClick();
    });
}

OriTube.OrgeoTitles.Touch.prototype.DoClick = function()
{
    if (this.myIsClicked) {
        $(".Footer .Touch", this.$myBlock).attr ("style", "");
        this.myIsClicked = false;
    } else {
        $(".Footer .Touch", this.$myBlock).css({
            "color": "var(--grey)",
            "background-color": "white"
        });
        this.myIsClicked = true;
    }
}

OriTube.OrgeoTitles.Touch.prototype.myIsClicked;
OriTube.OrgeoTitles.Touch.$myBlock;