﻿"use strict"

OriTube.OrgeoTitles.DisableFullScreen = function (theMainContext)
{
    this.myMainContext = theMainContext;
}

OriTube.OrgeoTitles.DisableFullScreen.prototype.ShowDisableButton = function()
{
    this.Init();
    this.$myButton.css ({
        "display" : "block"
    });
}

OriTube.OrgeoTitles.DisableFullScreen.prototype.HideDisableButton = function()
{
    this.$myButton.css ({
        "display" : "none"
    });
}

OriTube.OrgeoTitles.DisableFullScreen.prototype.Init = function()
{
    if (typeof this.$myButton == "undefined") {
        this.$myButton = this.myMainContext.$myBlock.find ("iframe").contents().find ("#ori-item-close");

        let aContext = this;
        this.$myButton.click (function() {
            aContext.OnClick();
        });
    }
}

OriTube.OrgeoTitles.DisableFullScreen.prototype.OnClick = function ()
{
    let $aBlock = this.myMainContext.$myBlock;
    this.myMainContext.myIFrame.SetSize ($aBlock, "100%", "100%");

    let aLastState = this.myMainContext.myFooterItems.myFullScreen.myLastState;
    let aBlockLeft = aLastState.LastBlockLeft;
    let aBlockTop = aLastState.LastBlockTop;
    this.myMainContext.SetTopAndLeft ($aBlock, aBlockLeft, aBlockTop);

    this.myMainContext.myFooterItems.SetEnabledFooter ($aBlock, true);

    let aCoverState = aLastState.LastCoverState;
    $(".Cover", $aBlock).css ({
        "display": aCoverState
    });

    this.HideDisableButton();

    this.myMainContext.SetZIndex ($aBlock, 1); // to be higher other blocks
    this.myMainContext.myIFrame.SetZIndex ($aBlock, 0); // to be higher other elements in block
    $aBlock.removeAttr ("data-fullscreen");

    this.myMainContext.myIFrame.myFrame.find ("iframe").css ({
        "left" : aLastState.LastIFameTagLeft,
        "top" : aLastState.LastIFameTagTop
    });
}

OriTube.OrgeoTitles.DisableFullScreen.prototype.myMainContext;
OriTube.OrgeoTitles.DisableFullScreen.prototype.$myButton;