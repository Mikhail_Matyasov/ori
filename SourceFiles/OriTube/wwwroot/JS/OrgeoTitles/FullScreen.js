﻿"use strict"

OriTube.OrgeoTitles.LastFrameState = function() {}
OriTube.OrgeoTitles.LastFrameState.LastIFrameWidth;
OriTube.OrgeoTitles.LastFrameState.LastIFrameHeight;
OriTube.OrgeoTitles.LastFrameState.LastBlockLeft;
OriTube.OrgeoTitles.LastFrameState.LastBlockTop;
OriTube.OrgeoTitles.LastFrameState.LastIFameTagLeft;
OriTube.OrgeoTitles.LastFrameState.LastIFrameTagTop;
OriTube.OrgeoTitles.LastFrameState.LastCoverState;

OriTube.OrgeoTitles.FullScreen = function (theMainContext)
{
    this.myLastState = new OriTube.OrgeoTitles.LastFrameState();
    let aContext = this;

    $(".FullScreen", theMainContext.$myBlock).click (function() {
        aContext.SaveState (theMainContext);
        theMainContext.myIFrame.SetSize (theMainContext.$myBlock, "100vw", "100vh");

         // to be higher other blocks
        theMainContext.SetZIndex (theMainContext.$myBlock, 2);

         // to be higher other elements in block
        theMainContext.myIFrame.SetZIndex (theMainContext.$myBlock, 3);

        let aBorderLeft = parseFloat (theMainContext.$myBlock.css ("border-left-width"));
        let aBorderTop = parseFloat (theMainContext.$myBlock.css ("border-top-width"));
        theMainContext.SetTopAndLeft (theMainContext.$myBlock, -aBorderLeft, -aBorderTop);

        $("iframe", theMainContext.myIFrame.myFrame).css ({
            "left":0,
            "top":0
        });

        theMainContext.myFooterItems.SetEnabledFooter (theMainContext.$myBlock, false);

        $(".Cover", theMainContext.$myBlock).css ({"display":"none"});

         // create tag for detecting, what block in FullScreen mode.
        theMainContext.$myBlock.attr ("data-fullscreen", "");
        theMainContext.myDisableFullScreen.ShowDisableButton();
    });
}

OriTube.OrgeoTitles.FullScreen.prototype.SaveState = function (theMainContext)
{
    this.myLastState.LastBlockLeft = theMainContext.$myBlock.css ("left");
    this.myLastState.LastBlockTop = theMainContext.$myBlock.css ("top");
    this.myLastState.LastIFameTagLeft = $(".Frame iframe", theMainContext.$myBlock).css ("left");
    this.myLastState.LastIFameTagTop = $(".Frame iframe", theMainContext.$myBlock).css ("top");

    this.myLastState.LastCoverState = $(".Cover", theMainContext.$myBlock).css ("display");
}

OriTube.OrgeoTitles.FullScreen.myLastState;