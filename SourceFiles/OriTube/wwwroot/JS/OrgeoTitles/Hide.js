﻿"use strict"

OriTube.OrgeoTitles.LastBackground = function()
{
    this.LastFrameBackground;
    this.LastBodyBackground;
    this.LastTableBackground;
    this.LastRightBackground;
}

OriTube.OrgeoTitles.Hide = function (theContext)
{
    let $aBlock = theContext.$myBlock;
    this.myBlockContext = theContext;
    this.myIsHide = false;
    this.$myObject = $(".HideButton", $aBlock);
    this.myIsCanceled = true;

    this.myLastBackgroundState = new OriTube.OrgeoTitles.LastBackground();
    this.myLastIframeBorderWidth = $(".Frame iframe", $aBlock).css ("border-width");

    let aContext = this;
    this.$myObject.click (function() {
        aContext.OnClick ($aBlock);
    });

    this.$myObject.mouseleave (function() {
        aContext.DoHide();
    });

    this.$myObject.mouseenter (() => {
        if (aContext.myIsHide) {
            aContext.myIsCanceled = true;
            aContext.$myObject.css ( {display: "block"} );
        }
    });

    $aBlock.mouseenter (() => {
        if (aContext.myIsHide) {
            aContext.myIsCanceled = true;
            aContext.$myObject.css ( {display: "block"} );
        }
    });

    $aBlock.mouseleave (() => {
        aContext.DoHide();
    });

    this.myEyeSchema =
        "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-eye' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>" +
            "<path fill-rule='evenodd' d='M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z'/>" +
            "<path fill-rule='evenodd' d='M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z'/>" +
        "</svg>";

    this.myEyeSlashSchema =
        "<svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-eye-slash' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>" +
            "<path d='M13.359 11.238C15.06 9.72 16 8 16 8s-3-5.5-8-5.5a7.028 7.028 0 0 0-2.79.588l.77.771A5.944 5.944 0 0 1 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.134 13.134 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755-.165.165-.337.328-.517.486l.708.709z'/>" +
            "<path d='M11.297 9.176a3.5 3.5 0 0 0-4.474-4.474l.823.823a2.5 2.5 0 0 1 2.829 2.829l.822.822zm-2.943 1.299l.822.822a3.5 3.5 0 0 1-4.474-4.474l.823.823a2.5 2.5 0 0 0 2.829 2.829z'/>" +
            "<path d='M3.35 5.47c-.18.16-.353.322-.518.487A13.134 13.134 0 0 0 1.172 8l.195.288c.335.48.83 1.12 1.465 1.755C4.121 11.332 5.881 12.5 8 12.5c.716 0 1.39-.133 2.02-.36l.77.772A7.029 7.029 0 0 1 8 13.5C3 13.5 0 8 0 8s.939-1.721 2.641-3.238l.708.709z'/>" +
            "<path fill-rule='evenodd' d='M13.646 14.354l-12-12 .708-.708 12 12-.708.708z'/>" +
        "</svg>";
}

OriTube.OrgeoTitles.Hide.prototype.DoHide = function()
{
    if (this.myIsHide) {
        this.myIsCanceled = false;
        setTimeout (() => {
            if (!this.myIsCanceled) {
                this.$myObject.css ( {display: "none"} );
            }
        }, 2000);
    }
}

OriTube.OrgeoTitles.Hide.prototype.OnClick = function ($theBlock)
{
    if (!this.myIsHide) {
        this.HideElements ($theBlock);
        this.MoveHideButton();
    } else {
        this.ShowElements ($theBlock);
        this.MoveHideButtonBack();
    }

    this.myIsHide = !this.myIsHide;
    if (this.myIsHide) {
        this.DoHide();
    }
}

OriTube.OrgeoTitles.Hide.prototype.HideElements = function ($theBlock)
{
    $(".Footer", $theBlock).css ({ display: "none" });

    let aNewBlockHeight = parseFloat ($theBlock.css ("height")) - this.myBlockContext.myFooterHeight;
    $theBlock.css ({ "height": aNewBlockHeight });
    $theBlock.css ({ border:"none" });

    this.HideChromaKey ($theBlock);
}

OriTube.OrgeoTitles.Hide.prototype.ShowElements = function ($theBlock)
{
    $(".Footer", $theBlock).attr ("style", "");
    $theBlock.css ({ "border": "0.2vw solid black" });

    this.ShowChromaKey ($theBlock);
}

OriTube.OrgeoTitles.Hide.prototype.HideChromaKey = function ($theBlock)
{
    let $anIframeDoc = this.SaveBackgroundState ($theBlock);

    $(".Frame iframe", $theBlock).css ({"border-width":"0"});
    $(".Frame", $theBlock).css ({"background-color":"transparent"});
    $anIframeDoc.find ("body").css ({"background-color":"transparent"});
    $anIframeDoc.find ("#right").css ({"background-color":"transparent"});
    $anIframeDoc.find ("#table").css ({"background-color":"transparent"});
}

OriTube.OrgeoTitles.Hide.prototype.ShowChromaKey = function ($theBlock)
{
    this.RestoreBackgroundState ($theBlock);
}

OriTube.OrgeoTitles.Hide.prototype.SaveBackgroundState = function ($theBlock)
{
    let $anIframeDoc = $(".Frame iframe", $theBlock).contents();

    this.myLastBackgroundState.LastFrameBackground = $(".Frame", $theBlock).css ("background-color");
    this.myLastBackgroundState.LastBodyBackground = $anIframeDoc.find ("body").css ("background-color");
    this.myLastBackgroundState.LastRightBackground = $anIframeDoc.find ("#right").css ("background-color");
    this.myLastBackgroundState.LastTableBackground = $anIframeDoc.find ("#table").css ("background-color");

    return $anIframeDoc;
}

OriTube.OrgeoTitles.Hide.prototype.RestoreBackgroundState = function ($theBlock)
{
    let aContext = this;
    let $anIframeDoc = $(".Frame iframe", $theBlock).contents();

    $(".Frame", $theBlock).css ( {"background-color":aContext.myLastBackgroundState.LastFrameBackground} );
    $anIframeDoc.find ("body").css ( {"background-color":aContext.myLastBackgroundState.LastBodyBackground} );
    $anIframeDoc.find ("#table").css ( {"background-color":aContext.myLastBackgroundState.LastTableBackground} );
    $anIframeDoc.find ("#right").css ( {"background-color":aContext.myLastBackgroundState.LastRightBackground} );
    $(".Frame iframe", $theBlock).css ( {"border-width":aContext.myLastIframeBorderWidth} );
}

OriTube.OrgeoTitles.Hide.prototype.MoveHideButton = function()
{
    let $aBlock = this.myBlockContext.$myBlock;
    this.$myObject.appendTo ($aBlock);

    this.$myObject.html ($(this.myEyeSlashSchema));

    this.$myObject.children().css ({
        position: "relative",
        left: "50%",
        transform: "translate(-50%, 0)",
        "font-size": "5vw"
    });

    this.$myObject.css ({
        position: "relative",
        top: "50%",
        transform: "translate(0, -50%)",
    });
}

OriTube.OrgeoTitles.Hide.prototype.MoveHideButtonBack = function()
{
    let $aRemoveButton = $(".Remove", this.myBlockContext.$myBlock);
    this.$myObject.attr ("style", "");
    this.$myObject.children().attr ("style", "");
    this.$myObject.html ($(this.myEyeSchema));
    this.$myObject.insertAfter ($aRemoveButton);
}

OriTube.OrgeoTitles.Hide.myBlockContext;
OriTube.OrgeoTitles.Hide.myIsCanceled;
OriTube.OrgeoTitles.Hide.myIsHide;
OriTube.OrgeoTitles.Hide.$myObject;
OriTube.OrgeoTitles.Hide.myEyeSchema;
OriTube.OrgeoTitles.Hide.myEyeSlashSchema;