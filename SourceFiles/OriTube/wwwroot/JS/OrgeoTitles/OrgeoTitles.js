﻿"use strict"

OriTube.OrgeoTitles = function ($theFloatingBlock)
{
    this.$myBlock = $theFloatingBlock;
    this.ConstructBlock();

    this.myFooterHeight = parseFloat ($(".Footer", this.$myBlock).css ("height"));
    this.myFooterItems = new OriTube.OrgeoTitles.FooterItems (this);
    this.myIFrame = new OriTube.OrgeoTitles.IFrame ($theFloatingBlock);
    this.myDisableFullScreen = new OriTube.OrgeoTitles.DisableFullScreen (this);
    this.myLastHeight = 0;
    this.myLastWidth = 0;
    this.myHandler;
    this.myLastOffsetLeft = 0;
    this.myLastOffsetTop = 0;

    let aContext = this;

    this.myRequestManager = new OriTube.OrgeoTitlesRequester();
    this.myRequestManager.SendRequest ("OnRequest");
    this.myRequestManager.OnResponse (function (theSource) 
    {
        $(".LoadIndicator", aContext.$myBlock).css ({"display":"none"});
        $("iframe", aContext.$myBlock).attr ("src", theSource + "?var=" + Math.random());
    });

    this.$myBlock.draggable ({
        start: function() {
            OriTube.FloatingBlock.OnStartDraggable ($theFloatingBlock);
        },
        stop: function() {
            OriTube.FloatingBlock.OnStopDraggable ($theFloatingBlock);
            aContext.OnStopDraggable (aContext);
        }
    });

    this.$myBlock.mousedown (() => {
        aContext.myZIndex = aContext.$myBlock.css ("z-index");
        aContext.$myBlock.css ({
            "z-index": "10"
        });
    });

    this.$myBlock.mouseup (() => {
        aContext.$myBlock.css ({
            "z-index": aContext.myZIndex
        });
    });

    this.$myBlock.resizable ({
        handles: "n, e, w, s",
        resize: function(e, u) {
            aContext.OnResizeResizable (aContext);
        }, 
        start: function() {
            aContext.OnStartResizable (aContext);
        },
        stop: function() {
            aContext.OnStopResizable (aContext);
        }
    });

    this.$myBlock.mousedown ((e) => {
        if (e.target.className.includes ("ui-resizable-handle")) {
            aContext.myHandler = e.target.className;
        }
    });

    this.UpdateData (aContext, 0, 0);
}

OriTube.OrgeoTitles.prototype.ConstructBlock = function() 
{
    this.$myBlock.find (".Footer").append ($(ORGEO_FOOTER_ITEMS));
    this.$myBlock.append ($(ORGEO_FRAME));
}

OriTube.OrgeoTitles.prototype.OnStopDraggable = function (theMainContext) 
{
     theMainContext.UpdateOffset (theMainContext);
}

OriTube.OrgeoTitles.prototype.OnStartResizable = function (theMainContext) 
{
    if (theMainContext.myFooterItems.myTouch.myIsClicked) {
        $(".Cover", theMainContext.$myBlock).css ({ "display": "block" });
    }

    theMainContext.myLastHeight = parseFloat (theMainContext.$myBlock.height());
    theMainContext.myLastWidth = parseFloat (theMainContext.$myBlock.width());
}

OriTube.OrgeoTitles.prototype.OnStopResizable = function (theMainContext) 
{
    if (theMainContext.myFooterItems.myTouch.myIsClicked) {
        $(".Cover", theMainContext.$myBlock).css ({ "display": "none" });
    }
}

OriTube.OrgeoTitles.prototype.OnResizeResizable = function (theMainContext)
{
    let $aBlock = theMainContext.$myBlock;
    let $anIFrame = $("iframe", $aBlock);
    let aHandlerName = theMainContext.myHandler;

    let anIFrameLeft = theMainContext.myIFrame.myLastCssLeft;
    let anIFrameTop = theMainContext.myIFrame.myLastCssTop;

    if (aHandlerName.includes ("ui-resizable-s")) {

        theMainContext.CheckBottom (theMainContext);
    } else if (aHandlerName.includes ("ui-resizable-w")) {

        var anOffsetLeftDelta = theMainContext.myLastOffsetLeft - $aBlock.offset().left;
        if (anOffsetLeftDelta != 0) {

            anIFrameLeft += anOffsetLeftDelta;
            $anIFrame.css ({ left: anIFrameLeft + "px" });
        }
        theMainContext.CheckLeft ($aBlock);
    } else if (aHandlerName.includes ("ui-resizable-n")) {

        var anOffsetTopDelta = theMainContext.myLastOffsetTop - $aBlock.offset().top;
        if (anOffsetTopDelta != 0) {
            anIFrameTop += anOffsetTopDelta;
            $anIFrame.css ({ top: anIFrameTop + "px" });
        }

        theMainContext.CheckTop ($aBlock);
    } else if (aHandlerName.includes ("ui-resizable-e")) {

        theMainContext.CheckRight (theMainContext);
    }


    theMainContext.UpdateData (theMainContext,
                               parseInt ($anIFrame.css ("left")),
                               parseInt ($anIFrame.css ("top")));
}

OriTube.OrgeoTitles.prototype.CheckLeft = function ($theBlock)
{
    let $anIFrame = $("iframe", $theBlock);
    let aLeftDelta = $anIFrame.offset().left - $theBlock.offset().left
    if (aLeftDelta > 0) {
        let aFixedBlockWidth = $theBlock.width() - aLeftDelta;
        $theBlock.css({
            "left": $anIFrame.offset().left + "px",
            "width": aFixedBlockWidth
        });
        $anIFrame.css({
            "left": parseInt ($anIFrame.css("left")) - aLeftDelta + "px"
        });
    }
}

OriTube.OrgeoTitles.prototype.CheckTop = function ($theBlock)
{
    let $anIFrame = $("iframe", $theBlock);
    let aTopDelta = $anIFrame.offset().top - $theBlock.offset().top;
    if (aTopDelta > 0) {

        let aFixedBlockHeight = $theBlock.height() - aTopDelta;
        $theBlock.css({
            "top": parseInt ($theBlock.css("top")) + aTopDelta + "px",
            "height": aFixedBlockHeight,
        });
        $anIFrame.css({
            "top": parseInt ($anIFrame.css ("top")) - aTopDelta + "px"
        });
    }
}

OriTube.OrgeoTitles.prototype.CheckRight = function (theMainContext)
{
    let $aBlock = theMainContext.$myBlock;
    let $anIFrame = $("iframe", $aBlock);
    let anIFrameRightBorderPos = $anIFrame.width() + $anIFrame.offset().left;
    let aBlockRightBorderPos = $aBlock.width() + $aBlock.offset().left;
    let aRightDelta = aBlockRightBorderPos - anIFrameRightBorderPos;
    if (aRightDelta > 0) {
        $aBlock.css ({
            "width": parseInt ($aBlock.css("width")) - aRightDelta
        });
    }
}

OriTube.OrgeoTitles.prototype.CheckBottom = function (theMainContext)
{
    let $aBlock = theMainContext.$myBlock;
    let $anIFrame = $("iframe", $aBlock);
    let anIFrameBottomBorderPos = $anIFrame.offset().top + $anIFrame.height();
    let aBlockBottomBorderPos = $aBlock.offset().top + $aBlock.height();
    let aBottomDelta = aBlockBottomBorderPos - anIFrameBottomBorderPos;
    if (aBottomDelta > 0) {
        $aBlock.css({
            "height": parseInt ($aBlock.css ("height")) - aBottomDelta + "px"
        })
    }
}

OriTube.OrgeoTitles.prototype.UpdateOffset = function (theMainContext)
{
    theMainContext.myLastOffsetLeft = theMainContext.$myBlock.offset().left;
    theMainContext.myLastOffsetTop = theMainContext.$myBlock.offset().top;
}

OriTube.OrgeoTitles.prototype.UpdateData = function (theMainContext, theLeft, theTop)
{
    theMainContext.myIFrame.UpdataCssOffset (theLeft, theTop);
    theMainContext.UpdateOffset (theMainContext);
}

OriTube.OrgeoTitles.prototype.SetTopAndLeft = function ($theBlock, theNewLeft, theNewTop)
{
    $theBlock.css ({
        "left":theNewLeft,
        "top":theNewTop
    });
}

OriTube.OrgeoTitles.prototype.SetZIndex = function ($theBlock, theZIndex)
{
    $theBlock.css ({"z-index":theZIndex});
}

OriTube.OrgeoTitles.$myBlock;
OriTube.OrgeoTitles.myFooterHeight;
OriTube.OrgeoTitles.myFooterItems;
OriTube.OrgeoTitles.myIFrame;
OriTube.OrgeoTitles.myDisableFullScreen;
OriTube.OrgeoTitles.myLastHeight;
OriTube.OrgeoTitles.myLastWidth;
OriTube.OrgeoTitles.myHandler;
OriTube.OrgeoTitles.myLastOffsetLeft;
OriTube.OrgeoTitles.myLastOffsetTop;
OriTube.OrgeoTitles.myRequestManager;
OriTube.OrgeoTitles.myZIndex;