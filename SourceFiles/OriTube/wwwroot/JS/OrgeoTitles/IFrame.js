﻿"use strict"

OriTube.OrgeoTitles.IFrame = function ($theBlock)
{
    this.myFrame = $(".Frame", $theBlock);
}

OriTube.OrgeoTitles.IFrame.prototype.UpdataCssOffset = function (theLeft, theTop)
{
    this.myLastCssLeft = theLeft;
    this.myLastCssTop = theTop;
}

OriTube.OrgeoTitles.IFrame.prototype.SetSize = function ($theBlock, theWidth, theHeight)
{
    $(".Frame", $theBlock).css ({
        "width":theWidth,
        "height":theHeight
    });
}

OriTube.OrgeoTitles.IFrame.prototype.SetZIndex = function ($theBlock, theZIndex)
{
    $(".Frame", $theBlock).css ({"z-index":theZIndex});
}

OriTube.OrgeoTitles.IFrame.myLastCssLeft;
OriTube.OrgeoTitles.IFrame.myLastCssTop;