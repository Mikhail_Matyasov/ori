﻿"use strict"

OriTube.OrgeoTitles.Destructor = function ($theBlock)
{
    $(".Remove", $theBlock).click (function() {
        $theBlock.remove();
    });
}