﻿"use strict"

OriTube.OrgeoTitles.FooterItems = function (theContext)
{
    this.myHide = new OriTube.OrgeoTitles.Hide (theContext);
    this.myDestructor = new OriTube.OrgeoTitles.Destructor (theContext.$myBlock);
    this.myFullScreen = new OriTube.OrgeoTitles.FullScreen (theContext);
    this.myTouch = new OriTube.OrgeoTitles.Touch (theContext.$myBlock);
}

OriTube.OrgeoTitles.FooterItems.prototype.SetEnabledFooter = function ($theBlock, theState)
{
    let $aFooter = $(".Footer", $theBlock);

    if (theState == true) {
        $aFooter.css ({"display":"flex"});
    } else {
        $aFooter.css ({"display":"none"});
    }
}

OriTube.OrgeoTitles.FooterItems.myFullScreen;
OriTube.OrgeoTitles.FooterItems.myTouch;
OriTube.OrgeoTitles.FooterItems.myHide;
OriTube.OrgeoTitles.FooterItems.myDestructor;
