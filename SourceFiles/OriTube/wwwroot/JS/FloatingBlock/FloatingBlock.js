﻿"use strict"

OriTube.FloatingBlock = function ($theBlock) 
{
    this.$myBlock = $theBlock;
}

OriTube.FloatingBlock.OnStartDraggable = function ($theBlock)
{
    $theBlock.css ({ cursor: "move" });
}

OriTube.FloatingBlock.OnStopDraggable = function ($theBlock)
{
    $theBlock.css ({ cursor: "pointer" });
}

OriTube.FloatingBlock.prototype.ConstructOrgeoTitles = function()
{
    this.myChild = new OriTube.OrgeoTitles (this.$myBlock);
}

OriTube.FloatingBlock.myDestructor;
OriTube.FloatingBlock.$myBlock;
OriTube.FloatingBlock.myChild;
