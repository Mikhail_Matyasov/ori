﻿"use strict"

OriTube.CodecJS.Decoder = function (theParameters)
{
    let aConstructor = Module.cwrap ("Ori_Codec_CreateDecoder", "number", [ "number" ]);
    this.myImpl = aConstructor (theParameters.myImpl);
}

OriTube.CodecJS.Decoder.prototype.MakePacket = function (theData)
{
    var aLenght = theData.length;
	var aBytePerElement = theData.BYTES_PER_ELEMENT;
    var aPacketDataPtr = Module._malloc (aLenght * aBytePerElement);
    Module.HEAPU8.set (theData, aPacketDataPtr / aBytePerElement);

    let aFunc = Module.cwrap ("Ori_Codec_Decoder_MakePacket", "number", [ "number", "number", "number" ]);
    let aPacketPtr = aFunc (this.myImpl, aPacketDataPtr, aLenght);
    return new OriTube.CodecJS.Packet (aPacketPtr, aPacketDataPtr);
}

OriTube.CodecJS.Decoder.prototype.DecodePacket = function (thePacket)
{
    let aFunc = Module.cwrap ("Ori_Codec_Decoder_DecodePacket", "number", [ "number", "number" ]);
    let aRes = aFunc (this.myImpl, thePacket.myImpl);

    // Returns 0 if error occured during decoding, 1 otherwise
    return aRes;
}

OriTube.CodecJS.Decoder.prototype.GetFrame = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_Decoder_GetFrame", "number", [ "number" ]);
    let aFrameImpl = aFunc (this.myImpl); // returns index of received frame in 'Frame storage' or -1, otherwise
    return new OriTube.CodecJS.Frame (aFrameImpl);
}

OriTube.CodecJS.Decoder.prototype.Free = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_Decoder_Free", null, [ "number" ]);
    aFunc (this.myImpl);
}

OriTube.CodecJS.Decoder.myImpl;

