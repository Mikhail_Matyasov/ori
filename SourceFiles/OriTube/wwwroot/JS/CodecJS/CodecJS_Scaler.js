﻿"use strict"

OriTube.CodecJS.Scaler = function (theParameters)
{
    let aConstructor = Module.cwrap ("Ori_Codec_CreateScaler", "number", [ "number" ]);
    this.myImpl = aConstructor (theParameters.myImpl);
}

OriTube.CodecJS.Scaler.prototype.ConvertPixelFormat = function (theFrame)
{
    let aFunc = Module.cwrap ("Ori_Codec_Scaler_ConvertPixelFormat", "number", [ "number", "number" ]);
    let aFrameIndex = aFunc (this.myImpl, theFrame.myImpl);

    return new OriTube.CodecJS.Frame (aFrameIndex);
}

OriTube.CodecJS.Scaler.prototype.Free = function()
{
    Module._free (this.myMemoryPtr);
    let aFunc = Module.cwrap ("Ori_Codec_Scaler_Free", null, [ "number" ]);
    aFunc (this.myImpl);
}

OriTube.CodecJS.Scaler.myImpl;

