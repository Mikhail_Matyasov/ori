﻿"use strict"

OriTube.CodecJS.Frame = function (theImpl)
{
    this.myImpl = theImpl;
}

OriTube.CodecJS.Frame.prototype.GetHeight = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_Frame_GetWidth", "number", [ "number" ]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.Frame.prototype.GetWidth = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_Frame_GetHeight", "number", [ "number" ]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.Frame.prototype.GetDataByIndex = function (theIndex = 0)
{
    if (typeof this.mySizeArray == "undefined") {

        let aSizeOfInteger = 4;
        let aSizeOfArray = 8;
        let aHelperMemorySize = aSizeOfArray * aSizeOfInteger;
        this.mySizeArrayPtr = Module._malloc (aHelperMemorySize);

        let aPrepareDataFunc = Module.cwrap ("Ori_Codec_Frame_GetArraySize", null, [ "number", "number" ]);
        aPrepareDataFunc (this.myImpl, this.mySizeArrayPtr);

        this.mySizeArray = new Int32Array (Module.HEAP32.buffer, this.mySizeArrayPtr, aSizeOfArray);
    }

    let aDataPtr = Module._malloc (this.mySizeArray [theIndex]);
    let aGetDataByIndexFunc = Module.cwrap ("Ori_Codec_Frame_GetDataByIndex", null, [ "number", "number", "number" ]);
    aGetDataByIndexFunc (this.myImpl, theIndex, aDataPtr);

    let aData = new Uint8ClampedArray (Module.HEAP32.buffer, aDataPtr, this.mySizeArray [theIndex]);
    Module._free  (aDataPtr);
    return aData;
    
}

OriTube.CodecJS.Frame.prototype.Free = function()
{
    if (typeof this.myImpl == "undefined" || this.myImpl == -1) {
        return;
    }

    let aFunc = Module.cwrap ("Ori_Codec_Frame_Free", null, [ "number" ]);
    aFunc (this.myImpl);

    Module._free (this.mySizeArrayPtr);
}

OriTube.CodecJS.Frame.myImpl;
OriTube.CodecJS.Frame.mySizeArray;
OriTube.CodecJS.Frame.mySizeArrayPtr;

