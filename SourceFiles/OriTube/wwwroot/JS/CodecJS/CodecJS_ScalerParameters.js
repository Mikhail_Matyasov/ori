﻿"use strict"

OriTube.CodecJS.ScalerParameters = function()
{
    let aConstructor = Module.cwrap ("Ori_Codec_CreateScalerParameters", "number", [""]);
    this.myImpl = aConstructor();
}

OriTube.CodecJS.ScalerParameters.prototype.SetSourceWidth = function (theWidth)
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_SetSourceWidth", null, ["number", "number"]);
    aFunc (this.myImpl, theWidth);
}

OriTube.CodecJS.ScalerParameters.prototype.GetSourceWidth = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_GetSourceWidth", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.ScalerParameters.prototype.SetSourceHeight = function (theHeight)
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_SetSourceHeight", null, ["number", "number"]);
    aFunc (this.myImpl, theHeight);
}

OriTube.CodecJS.ScalerParameters.prototype.GetSourceHeight = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_GetSourceHeight", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.ScalerParameters.prototype.SetTargetWidth = function (theWidth)
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_SetTargetWidth", null, ["number", "number"]);
    aFunc (this.myImpl, theWidth);
}

OriTube.CodecJS.ScalerParameters.prototype.GetTargetWidth = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_GetTargetWidth", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.ScalerParameters.prototype.SetTargetHeight = function (theHeight)
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_SetTargetHeight", null, ["number", "number"]);
    aFunc (this.myImpl, theHeight);
}

OriTube.CodecJS.ScalerParameters.prototype.GetTargetHeight = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_GetTargetHeight", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.ScalerParameters.prototype.SetSourcePixelFormat = function (theFormat)
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_SetSourcePixelFormat", null, ["number", "number"]);
    aFunc (this.myImpl, theFormat);
}

OriTube.CodecJS.ScalerParameters.prototype.GetSourcePixelFormat = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_GetSourcePixelFormat", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.ScalerParameters.prototype.SetTargetPixelFormat = function (theFormat)
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_SetTargetPixelFormat", null, ["number", "number"]);
    aFunc (this.myImpl, theFormat);
}

OriTube.CodecJS.ScalerParameters.prototype.GetTargetPixelFormat = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_GetTargetPixelFormat", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.ScalerParameters.prototype.Free = function()
{
    Module._free (this.myMemoryPtr);
    let aFunc = Module.cwrap ("Ori_Codec_ScalerParameters_Free", null, [ "number" ]);
    aFunc (this.myImpl);
}

OriTube.CodecJS.ScalerParameters.myImpl;

