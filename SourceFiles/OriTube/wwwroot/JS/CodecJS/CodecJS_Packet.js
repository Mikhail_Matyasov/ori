﻿"use strict"

OriTube.CodecJS.Packet = function (theImpl, theMemoryPtr)
{
    this.myImpl = theImpl;
    this.myMemoryPtr = theMemoryPtr;
}

OriTube.CodecJS.Packet.prototype.Free = function()
{
    Module._free (this.myMemoryPtr);
    let aFunc = Module.cwrap ("Ori_Codec_Packet_Free", null, [ "number" ]);
    aFunc (this.myImpl);
}

OriTube.CodecJS.Packet.myImpl;
OriTube.CodecJS.Packet.myMemoryPtr;