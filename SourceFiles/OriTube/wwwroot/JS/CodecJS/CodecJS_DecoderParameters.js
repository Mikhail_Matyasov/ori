﻿"use strict"

OriTube.CodecJS.DecoderParameters = function()
{
    let aConstructor = Module.cwrap ("Ori_Codec_CreateDecoderParameters", "number", [""]);
    this.myImpl = aConstructor();
}

OriTube.CodecJS.DecoderParameters.prototype.SetWidth = function (theWidth)
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_SetWidth", null, ["number", "number"]);
    aFunc (this.myImpl, theWidth);
}

OriTube.CodecJS.DecoderParameters.prototype.GetWidth = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_GetWidth", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.DecoderParameters.prototype.SetHeight = function (theHeight)
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_SetHeight", null, ["number", "number"]);
    aFunc (this.myImpl, theHeight);
}

OriTube.CodecJS.DecoderParameters.prototype.GetHeight = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_GetHeight", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.DecoderParameters.prototype.SetVideoCodecID = function (theId)
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_SetVideoCodecID", null, ["number", "number"]);
    aFunc (this.myImpl, theId);
}

OriTube.CodecJS.DecoderParameters.prototype.GetVideoCodecID = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_GetVideoCodecID", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.DecoderParameters.prototype.SetAudioCodecID = function (theId)
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_SetAudioCodecID", null, ["number", "number"]);
    aFunc (this.myImpl, theId);
}

OriTube.CodecJS.DecoderParameters.prototype.GetAudioCodecID = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_GetAudioCodecID", "number", ["number"]);
    return aFunc (this.myImpl);
}

OriTube.CodecJS.DecoderParameters.prototype.Free = function()
{
    let aFunc = Module.cwrap ("Ori_Codec_DecoderParameters_Free", null, ["number"]);
    aFunc (this.myImpl);
}

OriTube.CodecJS.DecoderParameters.myImpl;

