﻿"use strict"

var myDynamicCSSBlock = document.getElementById("DinamicCSSVariable");
var myMaxPlayerHeight;
GenerateVariable();

function GenerateVariable() {
    myMaxPlayerHeight = window.innerHeight;
    myDynamicCSSBlock.innerHTML = CalcCSSVar();   
}

function CalcCSSVar() {
    return ":root {" +
        "--MaxPlayerHeight:" + myMaxPlayerHeight + "px;" +
        "}";
}