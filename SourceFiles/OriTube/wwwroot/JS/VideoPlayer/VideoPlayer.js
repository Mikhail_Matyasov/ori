﻿"use strict"

const controls = `
<div class="player_controls">
    <div class="player_control player_control_play">
        <svg data-hover='orange_control' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-play" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M10.804 8L5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z" />
        </svg>
    </div>
    <div class="player_control player_control_list">
        <svg data-hover='orange_control' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-list" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
        </svg>
    </div>
    <div class="player_control player_control_close">
        <svg data-hover='orange_control' width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z" />
            <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z" />
        </svg>
    </div>
</div>
`;

OriTube.VideoPlayer = function()
{
    this.myOpenListSchema = 
        "<svg data-hover='dark_control' width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-chevron-up' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>" +
            "<path fill-rule='evenodd' d='M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z'/>" +
        "</svg>";
    this.myCloseListSchema =
        "<svg data-hover='orange_control' width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-list' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>" +
            "<path fill-rule='evenodd' d='M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z' />" +
        "</svg>";

    this.myPlaySchema = 
        "<svg data-hover='orange_control' width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-play' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>" +
            "<path fill-rule='evenodd' d='M10.804 8L5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z' />" +
        "</svg>";

    this.myPauseSchema = 
        "<svg data-hover='orange_control' width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-pause' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>" +
            "<path fill-rule='evenodd' d='M6 3.5a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0V4a.5.5 0 0 1 .5-.5zm4 0a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0V4a.5.5 0 0 1 .5-.5z'/>" +
        "</svg>";

    this.ConstructPlayer();
    this.ConfigureEvents();
}

OriTube.VideoPlayer.prototype.ConstructPlayer = function()
{
    let $aPlayerBase = $(VIDEO_PLAYER_BASE);
    $("#floating_block_container").append ($aPlayerBase);
    this.DoConstructPlayer ($aPlayerBase);
    this.ReplaceToCenter();
}

OriTube.VideoPlayer.prototype.DoConstructPlayer = function ($thePlayerBase)
{
    this.myPlayerClass = new Plyr ($thePlayerBase[0], {
        clickToPlay: false,
        controls,
        fullscreen: { 
            enabled: false
        },
        iconUrl: "/libs/plyr/plyr.svg",
        autoplay: true
    });

    let $aLastParent = $thePlayerBase.parent();
    let $aPlyr = $thePlayerBase;
    let aParentId = String ($aLastParent.attr ("id"));
    while (!aParentId.includes ("floating_block_container")) {
        $aPlyr = $aLastParent;
        $aLastParent = $aLastParent.parent();
        aParentId = String ($aLastParent.attr ("id"));
    }

    this.$myShowStreamListButton = $(".player_control_list", $aPlyr);
    this.$myCloseButton = $(".player_control_close", $aPlyr);
    this.$myPlayer = $aPlyr;
    this.myControls = new OriTube.GridControls (this.$myPlayer);
    this.$myPlayButton = $(".player_control_play", $aPlyr);
}

OriTube.VideoPlayer.prototype.ReplaceToCenter = function()
{
    let aWidthOffset = this.$myPlayer.width() / 2;
    let aHeightOffset = this.$myPlayer.height() / 2;

    let aPageWidth = $(window).width();
    let aPageHeight = $(window).height();

    this.$myPlayer.css ({
        "top": (aPageHeight / 2 - aHeightOffset) + "px",
        "left": (aPageWidth / 2 - aWidthOffset) + "px"
    }); 
}

OriTube.VideoPlayer.prototype.ConfigureEvents = function()
{
    let aContext = this;
    this.$myCloseButton.click (() => {
        try {
            aContext.$myPlayer.remove();
            aContext.myStreamList.RemovePeer();
        } catch (e) {}
    });

    this.$myShowStreamListButton.click (() => {
        if (typeof aContext.myStreamList == "undefined") {
            aContext.myStreamList = new OriTube.StreamList (aContext.$myPlayer);
            this.$myShowStreamListButton.html ($(aContext.myOpenListSchema));
        } else {
            if (!aContext.myStreamList.IsOpen) {
                aContext.myStreamList.Open (aContext.$myPlayer);
                this.$myShowStreamListButton.html ($(aContext.myOpenListSchema));
            } else {
                aContext.myStreamList.Close();
                this.$myShowStreamListButton.html ($(aContext.myCloseListSchema));
            }
        }
    });

    let $aPlyr = this.$myPlayer;
    $aPlyr.draggable({
        containment: $("#main_container"),
        start: function(e) {
            if (e.which == 1) { // left mouse button
                aContext.myControls.Show();
            }
        },
        stop: function() {
            aContext.myControls.Hide();
            aContext.ResizePlayer();
        }
    });
    $aPlyr.resizable ({
        handles: "n, s, w, e",
        aspectRatio: true
    });

    // Explicit set width, to prevent extra resizing of player after geting video stream.
    $aPlyr.css ({
        width: "25vw"
    });


    let anIsPlayClicked = false;
    this.$myPlayButton.click (function() {
        if (!anIsPlayClicked) {
            aContext.$myPlayButton.html (aContext.myPauseSchema);
            aContext.myPlayerClass.play();
        } else {
            aContext.$myPlayButton.html (aContext.myPlaySchema);
            aContext.myPlayerClass.pause();
        }
        anIsPlayClicked = !anIsPlayClicked;
    });

    $aPlyr.on ("mousedown.OnPlayerMouseDown", { Context: this }, this.OnPlayerMouseDown);
    $aPlyr.on ("mouseup.OnPlayerMouseUp", { Context: this }, this.OnPlayerMouseUp);
}

OriTube.VideoPlayer.prototype.OnPlayerMouseUp = function (e)
{
    let aContext = e.data.Context;
    aContext.$myPlayer.css ({
        "z-index": aContext.myZIndex
    });
}

OriTube.VideoPlayer.prototype.OnPlayerMouseDown = function (e)
{
    if (e.which == 1) { // left mouse button
        let aContext = e.data.Context;
        aContext.myZIndex = aContext.$myPlayer.css ("z-index");
        aContext.$myPlayer.css ({
            "z-index": "10"
        });
    }
}

OriTube.VideoPlayer.prototype.PlayerStyle = function()
{
    let aPlayerWidth = this.$myPlayer.width();
    let aPlayerHeight = this.$myPlayer.height();
    let aBodyHeight = $(document).height();
    let aBodyWidth = $(document).width();
    let aBodyResolution = aBodyWidth / aBodyHeight;
    let aPlayerResolution = aPlayerWidth / aPlayerHeight;
    let aHeightToWidth = aPlayerHeight / aPlayerWidth;

    let aPlayerSize = { Width: 0, Height: 0 }
    let aPlayerPadding = { Top: 0, Right: 0, Bottom: 0, Left: 0 }
    let aCoef = 1.08;
    if (aPlayerResolution > aBodyResolution) {
        //  ---------
        //  =========
        // |         | <- player
        //  =========
        //  ---------
        
        aPlayerSize.Width = aBodyWidth;
        aPlayerSize.Height = aBodyHeight;
        aPlayerPadding.Top = ((aBodyHeight - (aBodyWidth * aHeightToWidth)) / 2) * aCoef;
        aPlayerPadding.Bottom = aPlayerPadding.Top;
    } else {
        // player |
        //        v
        //    ---===---
        //    ---| |---
        //    ---===---

        aPlayerSize.Width = aBodyWidth;
        aPlayerSize.Height = aBodyHeight;
        aPlayerPadding.Left = ((aBodyWidth - (aBodyHeight / aHeightToWidth)) / 2) / aCoef;
        aPlayerPadding.Right = aPlayerPadding.Left;
    }

    let aRes = { Size: aPlayerSize, Padding: aPlayerPadding }
    return aRes;
}

OriTube.VideoPlayer.prototype.ResizePlayer = function()
{
    if (this.myControls.IsActive) {

        let aContext = this;
        if (this.myControls.CurrentActiveControl == "left_control") {

            let aPlayerWidth = this.PlayerWidth();
            this.SavePlayerSize();
            this.$myPlayer.css ({
                width: aPlayerWidth + "%",
                height: "100%",
                top: 0,
                left: 0,
                "z-index": aContext.myZIndex
            });
            this.MovePlayer();
        } else if (this.myControls.CurrentActiveControl == "center_control") {

            let aPlayerStyle = this.PlayerStyle();
            this.SavePlayerSize();
            this.$myPlayer.css ({
                width: aPlayerStyle.Size.Width + "px",
                height: aPlayerStyle.Size.Height + "px",
                top: 0,
                left: 0,
                "padding-top": aPlayerStyle.Padding.Top + "px",
                "padding-right": aPlayerStyle.Padding.Right + "px",
                "padding-bottom": aPlayerStyle.Padding.Bottom + "px",
                "padding-left": aPlayerStyle.Padding.Left + "px",
                "z-index": aContext.myZIndex
            });
            this.MovePlayer();
            
        } else if (this.myControls.CurrentActiveControl == "right_control") {

            let aPlayerWidth = this.PlayerWidth();
            let aLeft = 100 - aPlayerWidth;

            this.SavePlayerSize();
            this.$myPlayer.css ({
                width: aPlayerWidth + "%",
                height: "100%",
                top: 0,
                left: aLeft + "%",
                "z-index": aContext.myZIndex
            });
            this.MovePlayer();
        }
    }
}

OriTube.VideoPlayer.prototype.MovePlayer = function()
{
    this.$myPlayer.appendTo ("#videoplayer_grid_elements");
    this.$myPlayer.off ("mousedown.OnPlayerMouseDown");
    this.$myPlayer.off ("mouseup.OnPlayerMouseUp");

    this.$myPlayer.draggable ("disable");
    this.MakePlayerDraggableWithoutJquery();
    $(".player_controls", this.$myPlayer).css ({
        "padding-right": "85%"
    });

    if (this.myStreamList) {
        this.myStreamList.CloseFast();
    }
}

OriTube.VideoPlayer.prototype.MovePlayerBack = function (e)
{
    this.$myPlayer.appendTo ("#floating_block_container");

    let aContext = this;
    let aLeft = e.pageX - (this.mySavedWidth / 2);
    let aTop = e.pageY - (this.mySavedHeight / 2);

    aContext.$myPlayer.css ({
        width: aContext.mySavedWidth + "px",
        height: aContext.mySavedHeight + "px",
        left: aLeft + "px",
        top: aTop + "px",
        padding: 0
    });
    
    $(".player_controls", this.$myPlayer).css ({
        "padding-right": "70%"
    });
}

OriTube.VideoPlayer.prototype.SavePlayerSize = function()
{
    this.mySavedWidth = this.$myPlayer.width();
    this.mySavedHeight = this.$myPlayer.height();
}

OriTube.VideoPlayer.prototype.MakePlayerDraggableWithoutJquery = function()
{
    let anIsMouseDown = false;
    let anIsMove = false;
    let aMoveDistance = 5;
    let aClickPos = {
        X: 0,
        Y: 0
    };
    let OnMouseDown = function (e) {
        anIsMouseDown = true;
        aClickPos.X = e.pageX;
        aClickPos.Y = e.pageY;
    };

    let aLastMousePos = { X: 0, Y: 0 };
    let aContext = this;
    let OnMouseMove = function (e) {
        if (!anIsMouseDown) {
            return;
        }
        if (anIsMove) {
            
            let aLeftOffset = e.pageX - aLastMousePos.X;
            let aTopOffset = e.pageY - aLastMousePos.Y;
            let aPlayerOffset = { X: aContext.$myPlayer.offset().left,
                                  Y: aContext.$myPlayer.offset().top };
            aContext.$myPlayer.css ({
                left: (aPlayerOffset.X + aLeftOffset) + "px",
                top: (aPlayerOffset.Y + aTopOffset) + "px"
            });
            aLastMousePos.X = e.pageX;
            aLastMousePos.Y = e.pageY;
        } else {
            let anOffset = { X: 0, Y: 0 };
            anOffset.X = Math.abs (e.pageX - aClickPos.X);
            anOffset.Y = Math.abs (e.pageY - aClickPos.Y);
            if (anOffset.X > aMoveDistance || anOffset.Y > aMoveDistance) {
                aLastMousePos.X = e.pageX;
                aLastMousePos.Y = e.pageY;
                aContext.MovePlayerBack (e);
                anIsMove = true;
            }
        }
    };

    let OnMouseUp = function () {
        if (anIsMove) {
            aContext.$myPlayer.off ("mousedown", OnMouseDown);
            aContext.$myPlayer.off ("mousemove", OnMouseMove);
            aContext.$myPlayer.off ("mouseup", OnMouseUp);
            aContext.$myPlayer.on ("mousedown.OnPlayerMouseDown", { Context: aContext }, aContext.OnPlayerMouseDown);
            aContext.$myPlayer.on ("mouseup.OnPlayerMouseUp", { Context: aContext }, aContext.OnPlayerMouseUp);
            aContext.$myPlayer.draggable ("enable");
        }
        anIsMouseDown = false;
        anIsMove = false;
    };

    this.$myPlayer.on ("mousedown", OnMouseDown);
    this.$myPlayer.on ("mousemove", OnMouseMove);
    this.$myPlayer.on ("mouseup", OnMouseUp);
}

OriTube.VideoPlayer.prototype.$myShowStreamListButton;
OriTube.VideoPlayer.prototype.myStreamList;
OriTube.VideoPlayer.prototype.$myCloseButton;
OriTube.VideoPlayer.prototype.$myPlayButton;
OriTube.VideoPlayer.prototype.$myPlayer;
OriTube.VideoPlayer.prototype.myPlayerClass;
OriTube.VideoPlayer.prototype.myZIndex;
OriTube.VideoPlayer.prototype.myControls;
OriTube.VideoPlayer.prototype.mySavedWidth;
OriTube.VideoPlayer.prototype.mySavedHeight;

OriTube.VideoPlayer.prototype.myOpenListSchema;
OriTube.VideoPlayer.prototype.myCloseListSchema;
OriTube.VideoPlayer.prototype.myPlaySchema;
OriTube.VideoPlayer.prototype.myPauseSchema;
