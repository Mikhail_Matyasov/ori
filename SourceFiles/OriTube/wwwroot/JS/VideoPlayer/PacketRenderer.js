﻿
OriTube.PacketRenderer = function (theWidth = 0, theHeight = 0, theCodecType = 0, theCanvas)
{
    this.InitCodecContext (theWidth, theHeight, theCodecType);
    WebGL2D.enable (theCanvas);

    this.my2DGlContext = theCanvas.getContext ("webgl-2d");
    this.my2DGlContext.fillStyle = "#343a40"; // equal var(--dark)
    this.my2DGlContext.fillRect (0, 0, theWidth, theHeight);
}

OriTube.PacketRenderer.prototype.InitCodecContext = function (theWidth, theHeight, theCodecType)
{
    let aDecoderParams = new OriTube.CodecJS.DecoderParameters();
    aDecoderParams.SetWidth (theWidth);
    aDecoderParams.SetHeight (theHeight);
    aDecoderParams.SetVideoCodecID (theCodecType);
    
    this.myFrameWidth = aDecoderParams.GetWidth();
    this.myFrameHeight = aDecoderParams.GetHeight();

    let aScalerParams = new OriTube.CodecJS.ScalerParameters();
    aScalerParams.SetSourceWidth (this.myFrameWidth);
    aScalerParams.SetSourceHeight (this.myFrameHeight);
    aScalerParams.SetTargetWidth (this.myFrameWidth);
    aScalerParams.SetTargetHeight (this.myFrameHeight);
    aScalerParams.SetSourcePixelFormat (OriTube.CodecJS.PixelFormat.YUV420);
    aScalerParams.SetTargetPixelFormat (OriTube.CodecJS.PixelFormat.RGBA);

    this.myDecoder = new OriTube.CodecJS.Decoder (aDecoderParams);
    this.myScaler = new OriTube.CodecJS.Scaler (aScalerParams);
}

OriTube.PacketRenderer.prototype.Render = function (thePacketData)
{
    let aRes = this.DecodePacket (thePacketData);
    if (aRes != 0) {
        this.ConvertFrame();
    }
}

OriTube.PacketRenderer.prototype.DecodePacket = function (thePacketData)
{
    let aPacket = this.myDecoder.MakePacket (thePacketData);        
    let aRes = this.myDecoder.DecodePacket (aPacket);
    aPacket.Free();

    return aRes;
}

OriTube.PacketRenderer.prototype.ConvertFrame = function()
{
    let aFrame = this.myDecoder.GetFrame();
    if (aFrame.myImpl != -1)  {

        let anRGBAFrame = this.myScaler.ConvertPixelFormat (aFrame);
        let aPixelData = anRGBAFrame.GetDataByIndex (0);
        this.DisplayFrame (aPixelData);

        anRGBAFrame.Free();
    }
    aFrame.Free();
}

OriTube.PacketRenderer.prototype.DisplayFrame = function (thePixelData = [])
{
    let anImageData = new ImageData (thePixelData, this.myFrameWidth, this.myFrameHeight);
    this.my2DGlContext.putImageData (anImageData, 0, 0);
}

OriTube.PacketRenderer.prototype.myFrameWidth;
OriTube.PacketRenderer.prototype.myFrameHeight;
OriTube.PacketRenderer.prototype.myDecoder;
OriTube.PacketRenderer.prototype.myScaler;
OriTube.PacketRenderer.prototype.my2DGlContext;