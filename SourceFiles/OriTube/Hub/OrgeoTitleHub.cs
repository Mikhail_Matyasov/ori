﻿using System.Threading.Tasks;
using Ori.OriTube.OrgeoTitle;
using Ori.OriTube.Base;
using Microsoft.AspNetCore.SignalR;

namespace Ori.OriTube.Hub
{
public class OrgeoTitleHub : Microsoft.AspNetCore.SignalR.Hub
{
    private void CreateContext (ref HtmlGenerator theGen, ref string thePageName)
    {
        theGen = new HtmlGenerator();

        string aDependencyDir = "OrgeoTitlesDependency";
        string aFileName = "frame.html";

        thePageName = $"\\{aDependencyDir}\\{aFileName}";
        theGen.OrgeoTitlesDir = aDependencyDir;
        theGen.FileName = aFileName;
    }
    public async void OnRequest()
    {
        var aClients = Clients;
            
        HtmlGenerator aGenerator = null;
        string aPageName = null;
        CreateContext (ref aGenerator, ref aPageName);

        if (!IO.FileExists (aPageName)) {
            await aGenerator.Generate ("https://orgeo.ru/titles/");
        }

        await SendResponse (aClients, aPageName);
    }

    public async Task SendResponse (IHubCallerClients theClients, string theIframeContent)
    {
        await theClients.Caller.SendAsync ("Response", theIframeContent);
    }
}
}
