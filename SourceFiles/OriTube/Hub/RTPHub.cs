﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.Json;

namespace Ori.OriTube.Hub
{
public class RTPHub : Microsoft.AspNetCore.SignalR.Hub
{
    static Dictionary <string, RTP.Connection> myConnectionMap = 
        new Dictionary <string, RTP.Connection>();

        #region DEFAULT_STREAM_LIST
        //static RTPHub()
        //{
        //    string aKey = "random key";
        //    Ori.RTP.ConnectionDescriptor aDescriptor = new Ori.RTP.ConnectionDescriptor();
        //    aDescriptor.Description = "my description";
        //    var aConnection = new RTP.Connection (aDescriptor);

        //    myConnectionMap.Add (aKey, aConnection);
        //    myConnectionMap.Add (aKey + "1", aConnection);
        //    myConnectionMap.Add (aKey + "2", aConnection);
        //    myConnectionMap.Add (aKey + "3", aConnection);
        //    myConnectionMap.Add (aKey + "4", aConnection);
        //    myConnectionMap.Add (aKey + "5", aConnection);
        //    myConnectionMap.Add (aKey + "6", aConnection);
        //    myConnectionMap.Add (aKey + "7", aConnection);
        //}
        #endregion
        public async void OnCreateConnection (string theDesc)
    {
        var aClient = Clients.Caller;
        try {
            var aDesc = JsonSerializer.Deserialize <Ori.RTP.ConnectionDescriptor> (theDesc);

            var aConnection = myConnectionMap.GetValueOrDefault (Context.ConnectionId);
            var aKey = Context.ConnectionId;
            if (aConnection == null) {
                var aNewConnection = new RTP.Connection (aDesc);

                if (aNewConnection.RTPPort == -1) {
                    await aClient.SendAsync ("Can not allocate port");
                    return;
                } else if (aNewConnection.RTPPort == -2) {
                    await aClient.SendAsync ("Can not create rtp session");
                    return;
                } else {
                    myConnectionMap.Add (aKey, aNewConnection);
                    aNewConnection.Start();
                    await aClient.SendAsync ("RTP connection created", aNewConnection.RTPPort.ToString());
                }   
            } else {
                await aClient.SendAsync ("Same RTP connection already exists");
            }
        } catch (Exception e)  {
            await aClient.SendAsync ("Exception", e.Message);
        }

    }

    public async void OnUpdateConnection (string theDesc)
    {
        var aClient = Clients.Caller;
        try {
            var aDesc = JsonSerializer.Deserialize <Ori.RTP.ConnectionDescriptor> (theDesc);
            var aConnection = myConnectionMap.GetValueOrDefault (Context.ConnectionId);

            if (aConnection != null) {
                aConnection.Descriptor = aDesc;
                await Clients.Caller.SendAsync ("RTP connection updated");
            } else {
                await Clients.Caller.SendAsync ("RTP connection does not exist", Context.ConnectionId);
            }
        } catch (Exception e)  {
            await aClient.SendAsync ("Exception", e.Message);
        }
    }

    // It need use different discriptor class to don't send password to browser
    class StreamDescriptor
    {
        public string Description { get; set; }
        public string Id { get; set; }
        public bool IsPasswordExist { get; set; }
        public string CodecType { get; set; }
        public int PacketWidth { get; set; }
        public int PacketHeight { get; set; }
            
    }

    public async void OnGetStreamList()
    {
        List <StreamDescriptor> aStreams = new List <StreamDescriptor>();
        foreach (var aPair in myConnectionMap) {
            StreamDescriptor aStream = new StreamDescriptor();
            aStream.Description = aPair.Value.Descriptor.Description;
            aStream.Id = aPair.Key;

            var anRtpDescriptor = aPair.Value.Descriptor;
            aStream.IsPasswordExist = anRtpDescriptor.Password?.Length != 0;
            aStream.PacketWidth = anRtpDescriptor.PacketWidth;
            aStream.PacketHeight = anRtpDescriptor.PacketHeight;
            aStream.CodecType = anRtpDescriptor.CodecType;
            aStreams.Add (aStream);
        }

        await Clients.Caller.SendAsync ("Stream list received", aStreams, aStreams.Count);
    }

    public void OnAddPeer (string theRTPConnectionId, string thePeerConnectionId)
    {
        var anRTPConnection = GetConnection (theRTPConnectionId);

        if (anRTPConnection == null) {
            Clients.Caller.SendAsync ("RTP connection does not exist", theRTPConnectionId);
            return;
        }
        var aPeerConnection = WebRTCHub.GetConnection (thePeerConnectionId);
        anRTPConnection.AddPeer (aPeerConnection);

        Clients.Caller.SendAsync ("Peer added");
    }

    public  void OnRemovePeer (string theRTPConnectionId, string thePeerConnectionId)
    {
        var anRTPConnection = GetConnection (theRTPConnectionId);

        if (anRTPConnection == null) {
            Clients.Caller.SendAsync ("RTP connection does not exist", theRTPConnectionId);
            return;
        }
        var aPeerConnection = WebRTCHub.GetConnection (thePeerConnectionId);
        anRTPConnection.RemovePeer (aPeerConnection);

        Clients.Caller.SendAsync ("Peer removed");
    }

    public void OnClose()
    {
        Close();
        Clients.Caller.SendAsync ("Close");
    }

    public override Task OnDisconnectedAsync (Exception exception)
    {
        Close();
        return Task.CompletedTask;
    }

    public void Close() 
    {
        var aConnection = myConnectionMap.GetValueOrDefault (Context.ConnectionId);
        aConnection?.Close();
        myConnectionMap.Remove (Context.ConnectionId);   
    }

    public static RTP.Connection GetConnection (string theId) 
    {
        return myConnectionMap.GetValueOrDefault (theId);
    }
}

}
