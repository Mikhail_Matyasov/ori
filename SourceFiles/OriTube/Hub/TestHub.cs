﻿using Microsoft.AspNetCore.SignalR;
using System.Text;
using Ori.OriTube.Base;

namespace Ori.OriTube.Hub
{
public class TestHub : Microsoft.AspNetCore.SignalR.Hub
{
    public void OnPeerConnectionReceiveData (string thePeerId)
    {
        var aPeerConnection = WebRTCHub.GetConnection (thePeerId);

        var aByteStream = new Ori.Base.ByteArray();
        var aBytes = Encoding.UTF8.GetBytes ("passed");
        aByteStream.Insert (aBytes, aBytes.Length);

        aPeerConnection.Send (aByteStream, false);
    }

    public void OnCodecJS_GetTestPacket (string thePacketName)
    {
        var aBytes = FileReader.ReadBinaryFile ($"Tests\\Resources\\{thePacketName}");
        Clients.Caller.SendAsync ("CodecJS_TestPacketReceived", aBytes);
    }
}
}
