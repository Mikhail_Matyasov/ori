﻿using Microsoft.AspNetCore.SignalR;
using Ori.OriTube.WebRTC;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ori.OriTube.Hub
{
public class WebRTCHub : Microsoft.AspNetCore.SignalR.Hub
{
    static Dictionary <string, ConnectionBuilder> myBuilderMap = new Dictionary <string, ConnectionBuilder>();
    static object myLocker = new object();
    public void OnIceCandidate (string theCandidate)
    {
        var aBuilder = GetBuilder();
        aBuilder.AddIceCandidate (theCandidate);
    }

    public void OnOffer (string theOffer)
    {
        var aBuilder = GetBuilder();
        aBuilder.CreateAnswer (theOffer);
    }

    public void OnRestartConnection (string theOffer)
    {
        var aBuilder = GetBuilder();
        aBuilder.RestartConnection (theOffer);
    }

    ConnectionBuilder GetBuilder()
    {
        return myBuilderMap [Context.ConnectionId];
    }

    public static Ori.WebRTC.Connection GetConnection (string theKey)
    {
        var aBuilder = myBuilderMap [theKey];
        return aBuilder?.WebRTCConnection;
    }

    public override Task OnConnectedAsync()
    {
        if (!myBuilderMap.ContainsKey (Context.ConnectionId)) {
            var aBuilder = new ConnectionBuilder (Clients.Caller);
            myBuilderMap.Add (Context.ConnectionId, aBuilder);
            Clients.Caller.SendAsync ("GetIdSuccess", Context.ConnectionId);
        }
        return Task.CompletedTask;
    }

    public override Task OnDisconnectedAsync (Exception theException)
    {
        var aBuilder = GetBuilder();
        aBuilder.WebRTCConnection?.StopListening();
        return Task.CompletedTask;
    }

}

}
