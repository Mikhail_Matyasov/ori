﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Ori.OriTube
{
    public class Startup
    {
        public void ConfigureServices (IServiceCollection theServices)
        {
            theServices.AddControllersWithViews();
            theServices.AddRazorPages();
            theServices.AddSignalR (theConfig=>{
                theConfig.EnableDetailedErrors = true;
            });
        }

        public void Configure (IApplicationBuilder theApp, IWebHostEnvironment theEnv)
        {
            theApp.UseRouting();
            theApp.UseStaticFiles();
            theApp.UseEndpoints (theEndPoint =>
            {
                theEndPoint.MapControllerRoute (name: "default",
                                                pattern: "{controller=home}/{action=Index}/{name=mikahil}");

                theEndPoint.MapControllerRoute (name: "test",
                                                pattern: "{controller=testpage}/{action=Index}/{name=mikahil}");

                theEndPoint.MapHub <Hub.OrgeoTitleHub> ("/hub_orgeotitlesrequester");
                theEndPoint.MapHub <Hub.WebRTCHub> ("/webrtchub");
                theEndPoint.MapHub <Hub.RTPHub> ("/rtphub");
                theEndPoint.MapHub <Hub.TestHub> ("/testhub");
            });

            Base.EnvironmentHelper.Initialize (theEnv);
            // It need initialize logger level before webrtc will be initialized
            Base.Logger.Init ($"{Base.EnvironmentHelper.RootDirrectory()}\\Log\\");
            WebRTC.Init.Perfome();
        }
    }
}
