﻿using Microsoft.AspNetCore.SignalR;

namespace Ori.OriTube.WebRTC
{
public class ConnectionObserver : Ori.WebRTC.ConnectionObserver
{
    public IClientProxy Client { get; set; }
    public async override void OnCreateAnswer (Ori.Base.String theAnswer)
    {
        await Client.SendAsync ("Answer", theAnswer.Data());
    }

    public async override void OnIceCandidate (Ori.Base.String theCandidate)
    {
        await Client.SendAsync ("IceCandidate", theCandidate.Data());
    }

}

}
