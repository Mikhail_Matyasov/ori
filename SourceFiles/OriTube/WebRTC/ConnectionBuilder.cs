﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ori.OriTube.WebRTC
{
public class ConnectionBuilder
{
    public Ori.WebRTC.Connection WebRTCConnection { get; set; }
    private DataChannelObserver myChannelObserver;
    private ConnectionObserver myConnectionObserver;
    private Ori.Base.Logger myLogger;
    private List <Ori.WebRTC.IceCandidate> mySavedCandidates;
    private IClientProxy myClient;
    public ConnectionBuilder (IClientProxy theClient)
    {
        myClient = theClient;
        myChannelObserver = new DataChannelObserver();
        myConnectionObserver = new ConnectionObserver();
        myLogger = new Ori.Base.Logger (OriTube.Base.Logger.Instance);
        myLogger.SetLevel (Ori.Base.LogLevel.Warning);

        myConnectionObserver.Client = myClient;
        myChannelObserver.Client = myClient;
        mySavedCandidates = new List <Ori.WebRTC.IceCandidate>();
    }

    public void CreateAnswer (string theOffer)
    {
        // Initialization of WebRTCConnection and method 'Listen' should be invoked in same thread.
        // Because 'Listen' invokes 'GetMessage' - WinAPI method, inside c++ code, which listen messages
        // from socket. Therefor, if invoke these methodes from different threads,
        // not all messages will be received.
        var aThread = new Thread (new ThreadStart (() => {
            WebRTCConnection = new Ori.WebRTC.Connection (myConnectionObserver, myChannelObserver, myLogger);
            if (!WebRTCConnection.Ready()) {
                throw new ArgumentException ("Creating of peer connection ended with error.");
            }

            RestartConnection (theOffer);
            if (mySavedCandidates.Count != 0) {
                foreach (var aCandidate in mySavedCandidates) {
                    WebRTCConnection.AddIceCandidate (aCandidate);
                }

                mySavedCandidates.Clear();
            }

            WebRTCConnection.Listen();
        }));
        aThread.Start();
    }

    public void RestartConnection (string theOffer) 
    {
        if (WebRTCConnection != null) {
            var anOffer = Ori.WebRTC.JsonReader.ParseSdp (new Ori.Base.String (theOffer));
            WebRTCConnection.SetRemoteDescription (anOffer);
            WebRTCConnection.CreateAnswer();
        }
    }

    public void AddIceCandidate (string theCandidate)
    {
        // Ice candidate may come before offer. Due to bad internet connection.
        var aCandidate = Ori.WebRTC.JsonReader.ParseCandidate (new Ori.Base.String (theCandidate));
        if (WebRTCConnection == null) {
            mySavedCandidates.Add (aCandidate);
        } else {
            WebRTCConnection.AddIceCandidate (aCandidate);   
        }
    }
}

}
