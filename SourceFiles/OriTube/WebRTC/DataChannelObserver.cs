﻿using Microsoft.AspNetCore.SignalR;

namespace Ori.OriTube.WebRTC
{
public class DataChannelObserver : Ori.WebRTC.DataChannelObserver
{

public IClientProxy Client;
public override void OnMessage (Ori.Base.ByteArray theMessage)
{}

public override void OnOpenned()
{
    Client.SendAsync ("Connected");
}

}

}
