﻿using System;
using System.Net.NetworkInformation;

namespace Ori.OriTube.WebRTC
{
public class Init
{
    private static void Print (string theText, ConsoleColor theColor)
    {
        Console.ForegroundColor = theColor;
        Console.Write (theText);
        Console.ResetColor();
    }

    private static void Load (string thePath)
    {
        bool aRes = Ori.Base.DllHelper.Load (thePath);
        if (!aRes) {

            Print ("Could not load dll: ", ConsoleColor.DarkRed);
            Print (thePath, ConsoleColor.White);
            Print (Environment.NewLine, ConsoleColor.White);
        } else {

            Print ("Loaded: ", ConsoleColor.DarkGreen);
            Print (thePath, ConsoleColor.White);
            Print (Environment.NewLine, ConsoleColor.White);
        }
    }

    public static void Perfome()
    {
        Load ("OriCore.dll");
        Load ("WebRTC.dll");
        Load ("RTP.dll");

        Ori.WebRTC.Init anInit = new Ori.WebRTC.Init();
    }
}

}
