﻿
namespace Ori.OriTube.Controller
{
public class HomeController : Microsoft.AspNetCore.Mvc.Controller
{
    string theVersion = OriTube.Base.Constants.ORITUBE_VERSION;
    string myWebRoot = "";

    [Microsoft.AspNetCore.Mvc.HttpGet]
    public Microsoft.AspNetCore.Mvc.ViewResult Index()
    {
        AddLibs();
        AddStyles();
        AddScripts();
        AddHeadLibs();
        return View();
    }

    void AddHeadLibs()
    {
        ViewBag.HeadLibs =
            $"<link href='{myWebRoot}/libs/normalize/normalize.min.css' rel='stylesheet' />" +
            $"<link href='{myWebRoot}/libs/jquery/jquery-ui.min.css' rel='stylesheet' />" +
            $"<link href='{myWebRoot}/libs/bootstrap/bootstrap.min.css' rel='stylesheet' />" +
            $"<link href='{myWebRoot}/libs/plyr/plyr.css' rel='stylesheet' />" +
            $"<script src='{myWebRoot}/libs/fontawesome/fontawesome.js' type='text/javascript'></script>" +
            $"<script src='{myWebRoot}/libs/plyr/plyr.js' type='text/javascript'></script>";
    }

    void AddStyles()
    {
        string aVersion = "?v=" + theVersion + "'";
        string aSuffix = " rel='stylesheet' />\n\t";
        ViewBag.Styles =
            $"<link href='{myWebRoot}/css/base_containers.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/css/global_variables.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/css/right_side_bar.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/css/videoplayer.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/css/videoplayer_grid.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/CSS/MainPlayer.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/CSS/LoadIndicator.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/CSS/stream_list.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/CSS/OrgeoTitles.css{aVersion}{aSuffix}" +
            $"<link href='{myWebRoot}/CSS/FloatingBlock.css{aVersion}{aSuffix}";
    }

    void AddLibs()
    {
        ViewBag.Libs =
            $"<script src='{myWebRoot}/libs/jquery/jquery.min.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/jquery/jquery-ui.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/adapterjs/adapter-latest.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/aspnet-signalr/signalr.min.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/node-uuid/uuid.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/goog/base.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/goog/structs/map.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/webgl2d/webgl-2d.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/bootstrap/bootstrap.min.js' type='text/javascript'></script>\n" +
            $"<script src='{myWebRoot}/libs/downloadjs/download.min.js' type='text/javascript'></script>\n";
    }
    void AddScripts()
    {
        string aVersion = "?v=" + theVersion + "'";
        string aSuffix = " type='text/javascript'></script>\n";

        ViewBag.Scripts = Base (aVersion, aSuffix) +
                          Schemes (aVersion, aSuffix) +
                          HubClients (aVersion, aSuffix) +
                          CrossOrigin (aVersion, aSuffix) +
                          FloatingBlock (aVersion, aSuffix) +
                          WebRTC (aVersion, aSuffix) +
                          OrgeoTitles (aVersion, aSuffix) +
                          StreamList (aVersion, aSuffix) +
                          Videoplayer (aVersion, aSuffix) +
                          VideoPlayerGrid (aVersion, aSuffix) +
                          CodecJS (aVersion, aSuffix) +
                          $"<script src='{myWebRoot}/JS/MainPlayer.js{aVersion}{aSuffix}";
    }

    string Base (string theVersion, string theSuffix)
    {
        return $"<script src = '{myWebRoot}/JS/Base/Base_Namespaces.js{theVersion}{theSuffix}";
    }
    string Schemes (string theVersion, string theSuffix)
    {
        return 
                $"<script src = '{myWebRoot}/JS/Schemes/OrgeoSchemes/HideButtonSchema.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/Schemes/OrgeoSchemes/FrameSchema.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/Schemes/OrgeoSchemes/FooterItemsSchema.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/Schemes/FloatingBlockSchema.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/Schemes/LoadIndicatorSchema.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/Schemes/video_player_schemes/video_player_base_schema.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/Schemes/stream_list_schemes/stream_list_schema.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/Schemes/stream_list_schemes/stream_list_field_schema.js{theVersion}{theSuffix}";
    }

    string CrossOrigin (string theVersion, string theSuffix)
    {
        return $"<script src = '{myWebRoot}/JS/OrgeoTitlesRequester/OrgeoTitlesRequester.js{theVersion}{theSuffix}";
    }

    string FloatingBlock (string theVersion, string theSuffix)
    {
        return $"<script src='{myWebRoot}/JS/FloatingBlock/FloatingBlock.js{theVersion}{theSuffix}";
    }

    string StreamList (string theVersion, string theSuffix)
    {
        return $"<script src = '{myWebRoot}/JS/StreamList/StreamList.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/StreamList/StreamListField.js{theVersion}{theSuffix}";
    }
        
    string Videoplayer (string theVersion, string theSuffix)
    {
        return  $"<script src = '{myWebRoot}/JS/VideoPlayer/PacketRenderer.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/VideoPlayer/VideoPlayer.js{theVersion}{theSuffix}";
    }

    string VideoPlayerGrid (string theVersion, string theSuffix)
    {
        return $"<script src = '{myWebRoot}/JS/VideoPlayerGrid/GridControls.js{theVersion}{theSuffix}";
    }

    string HubClients (string theVersion, string theSuffix)
    {
        return $"<script src = '{myWebRoot}/JS/HubClient/HubClient_RTPHub.js{theVersion}{theSuffix}";
    }

    string WebRTC (string theVersion, string theSuffix)
    {
        return $"<script src = '{myWebRoot}/JS/WebRTC/WebRTC_Connection.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/WebRTC/WebRTC_ConnectionTransport.js{theVersion}{theSuffix}";
    }

    string OrgeoTitles (string theVersion, string theSuffix)
    {
        return $"<script src = '{myWebRoot}/JS/OrgeoTitles/OrgeoTitles.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/OrgeoTitles/Destructor.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/OrgeoTitles/DisableFullScreen.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/OrgeoTitles/FooterItems.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/OrgeoTitles/FullScreen.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/OrgeoTitles/Hide.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/OrgeoTitles/IFrame.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/OrgeoTitles/Touch.js{theVersion}{theSuffix}";
    }

    string CodecJS (string theVersion, string theSuffix)
    {
        return 
                $"<script src = '{myWebRoot}/JS/CodecJS/wrapper.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_CodecType.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_PixelFormat.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_DecoderParameters.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_ScalerParameters.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Scaler.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Decoder.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Frame.js{theVersion}{theSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Packet.js{theVersion}{theSuffix}";
    }
}

}