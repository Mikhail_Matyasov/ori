﻿namespace Ori.OriTube.Controller
{
public class TestPageController : Microsoft.AspNetCore.Mvc.Controller
{
    private readonly string myVersion = "?v=" + Base.Constants.ORITUBETEST_VERSION + "'";
    private readonly string myJSSuffix = " type='text/javascript'></script>\n";
    string myWebRoot = "";

    [Microsoft.AspNetCore.Mvc.HttpGet]
    public Microsoft.AspNetCore.Mvc.ViewResult Index ()
    {
        ViewBag.Libs += Libs();
        ViewBag.Sources += Sources();
        ViewBag.Tests += Test();
        return View();
    }

    string Sources()
    {
        return $"<script src = '{myWebRoot}/JS/Base/Base_Namespaces.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/HubClient/HubClient_RTPHub.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/wrapper.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_CodecType.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_PixelFormat.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_DecoderParameters.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_ScalerParameters.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Scaler.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Decoder.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Frame.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/CodecJS/CodecJS_Packet.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/VideoPlayer/VideoPlayer.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/VideoPlayer/PacketRenderer.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/StreamList/StreamList.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/WebRTC/WebRTC_Connection.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/JS/WebRTC/WebRTC_ConnectionTransport.js{myVersion}{myJSSuffix}";
    }

    string Libs()
    {
        return $"<script src = '{myWebRoot}/Libs/aspnet-signalr/signalr.min.js'{myJSSuffix}" +
                $"<script src = '{myWebRoot}/Libs/node-uuid/uuid.js'{myJSSuffix}" +
                $"<script src = '{myWebRoot}/Libs/webgl2d/webgl-2d.js'{myJSSuffix}" +
                $"<script src = '{myWebRoot}/Libs/adapterjs/adapter-latest.js'{myJSSuffix}";
    }

    string Test()
    {
        return 
                $"<script src = '{myWebRoot}/Tests/RTPConnectionTest.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/Tests/WebRTCTest.js{myVersion}{myJSSuffix}" +
                $"<script src = '{myWebRoot}/Tests/CodecJSTest.js{myVersion}{myJSSuffix}" +
                "";
    }
}

}
