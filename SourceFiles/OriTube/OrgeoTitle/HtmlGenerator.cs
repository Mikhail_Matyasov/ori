﻿using AngleSharp;
using ExCSS;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AngleSharp.Dom;
using System;

namespace Ori.OriTube.OrgeoTitle
{
public class HtmlGenerator
{
    string myBody = "";
    StringBuilder myInlineStyles = new StringBuilder();
    StringBuilder myInlineScripts = new StringBuilder();
    StringBuilder myScripts = new StringBuilder();
    HtmlGeneratorHelper myParserHelper = new HtmlGeneratorHelper();
    string myOrgeoTitlesDir = null;
    string myFileName = null;
    public string OrgeoTitlesDir { set => myOrgeoTitlesDir = value; }
    public string FileName { set => myFileName = value; }

    public async Task Generate (string theRequest)
    {
        var aDocument = await OpenDocumentAsync (theRequest);
        var aHead = aDocument.QuerySelector ("head");

        var aScriptTags = aHead.QuerySelectorAll ("script");
        var aStyleTags = aHead.QuerySelectorAll ("link");
        var anInlineStyles = aHead.QuerySelectorAll ("style");
        var aBodyTag = aDocument.QuerySelector ("body");
        
        var aSecondContext = BrowsingContext.New (Configuration.Default.WithDefaultLoader());
        foreach (var aScript in aScriptTags) {
                
            string aScriptSrc = aScript.GetAttribute ("src");
            if (aScriptSrc == null) {
                var anInlineScript = aScript.ToHtml(); 
                myInlineScripts.AppendLine (anInlineScript);
                continue;
            }
            if (!myParserHelper.IsScriptException (aScriptSrc)) {

                TryConvertSrcToRemote (aScript, "src");
                myScripts.AppendLine (aScript.ToHtml());
            }
        }

        foreach (var aLink in aStyleTags) {
            TryConvertSrcToRemote (aLink, "href");
            string aConvertedStyleSheet = await ParseStyleSheet (aLink, aDocument.BaseUri);

            if (aConvertedStyleSheet != "") {
                myInlineStyles.AppendLine($"<style>{aConvertedStyleSheet}</style>");
            }
        }

        foreach (var aStyle in anInlineStyles) {
            myInlineStyles.AppendLine (aStyle.ToHtml());
        }
        // For preventing of setting height of this element to 0
        myInlineStyles.AppendLine ("<style> #left {" +
            "height: 100% !important;" + 
        "} </style>");
            
        ParseBody (ref aBodyTag, aDocument.BaseUri);
        myBody = aBodyTag.ToHtml();
        ParseBodyTemplates (aBodyTag, aDocument.BaseUri);
        InsertCloseButton (aBodyTag);

        string anHtmlPage = GenerateHtmlPage();

        var aPageDir = $"{Base.EnvironmentHelper.RootDirrectory()}\\{myOrgeoTitlesDir}";
        Directory.CreateDirectory (aPageDir);
        Base.Writer aWriter = new Base.Writer ($"\\{myOrgeoTitlesDir}\\{myFileName}");

        await aWriter.WriteAsync (Encoding.UTF8.GetBytes (anHtmlPage));
    }

    void TryConvertSrcToRemote (AngleSharp.Dom.IElement theSource, string theAttrName)
    {
        var aSrcValue = theSource.GetAttribute (theAttrName);

        var aRes = aSrcValue?.Contains ("http");
        if (aRes != null && !aRes.Value) {
            string aNewSrcValue = theSource.BaseUri + aSrcValue;
            theSource.SetAttribute (theAttrName, aNewSrcValue);
        }
    }

    async Task <string> ParseStyleSheet (AngleSharp.Dom.IElement theSource, string theRemoteHost)
    {
        var aStyleSource = theSource.GetAttribute ("href");

        if (!myParserHelper.IsStyleException (aStyleSource))
        {
            var aStyleDocument = await OpenDocumentAsync (aStyleSource);
            var aStylePage = aStyleDocument.ToHtml();

            StylesheetParser aParser = new StylesheetParser();
            var aStyleSheet = await aParser.ParseAsync (aStylePage);
            StringBuilder aConvertedStyleSheet = new StringBuilder();
            Base.Loader aDependencyLoader = new Base.Loader();

            foreach (var aRule in aStyleSheet.Children) {

                var aRuleValue = aRule.ToCss();
                if (aRuleValue.Contains ("body")) {
                    continue;
                }

                int aUrlIndex = aRuleValue.IndexOf ("url");
                if (aUrlIndex != -1) {
                    string aSourceFile = ConvertRuleRefToRemoteAdress (ref aRuleValue, aUrlIndex);

                    try {
                        string aDestFile = $"\\{myOrgeoTitlesDir}\\{aSourceFile}";
                        await aDependencyLoader.UpLoadFile (theRemoteHost + aSourceFile, aDestFile);
                    } catch { }
                        
                }
                aConvertedStyleSheet.AppendLine (aRuleValue);
            }

            return aConvertedStyleSheet.ToString();
        }

        return "";
    }

    async Task <IDocument> OpenDocumentAsync (string theSource)
    {
        var aBrowsingContext = BrowsingContext.New (Configuration.Default.WithDefaultLoader());
        var aDocument = await aBrowsingContext.OpenAsync (theSource);
        return aDocument;
    }

    string ConvertRuleRefToRemoteAdress (ref string theSource, int theUrlStartIndex)
    {
        int aStartPos = 0;
        int anEndPos = 0;
        for (int i = theUrlStartIndex; ; i++) {
            char aChar = theSource[i];
            if (aChar == '(') {
                aStartPos = i + 2;
            }
            if (aChar == ')') {
                anEndPos = i - 2;
                break;
            }
        }
        string aRes = theSource.Substring (aStartPos, anEndPos - aStartPos + 1);
        theSource = theSource.Insert (aStartPos, $"/{myOrgeoTitlesDir.Replace ("\\", "/")}/");
        return aRes;
    }

    void ParseBody (ref IElement theSource, string theRemoteHost)
    {
        Base.Loader aDependencyLoader = new Base.Loader();

        var anElementsWithSrc = theSource.QuerySelectorAll ("*[src]");
        foreach (var anElem in anElementsWithSrc) {
            var aSrcFile = anElem.GetAttribute ("src");
            var aNewSrc = $"/{myOrgeoTitlesDir}/{aSrcFile}";
            anElem.SetAttribute ("src", aNewSrc);

            var aTask = Task.Run (async () => {
                try {
                    await aDependencyLoader.UpLoadFile(theRemoteHost + aSrcFile,
                        $"\\{myOrgeoTitlesDir}\\{aSrcFile}");
                }
                catch { }
            });

            Task.WaitAll (aTask);
        }
    }

    void ParseBodyTemplates (IElement theSource, string theRemoteHost)
    {
        var aTemplates = theSource.QuerySelectorAll ("template");
        Base.Loader aDependencyLoader = new Base.Loader();

        foreach (var aTmp in aTemplates) {
            string anHtml = aTmp.ToHtml();
            string aNewHtml = anHtml;
            
            int anImageStart = 0;
            while (true) {
                anImageStart = anHtml.IndexOf ("<img", anImageStart);
                if (anImageStart == -1) {
                    break;
                }

                int anImageEnd = anHtml.IndexOf (">", anImageStart);
                string anImageTag = anHtml.Substring (anImageStart, anImageEnd - anImageStart + 1);

                // It need find only src='some content' tags and skip something like vs:src='some content'
                int aSrcStart = anImageTag.IndexOf (" src");

                if (aSrcStart != -1) {
                    int aSrcContentStart = anImageTag.IndexOf ("\"", aSrcStart);
                    int aSrcContentEnd = anImageTag.IndexOf ("\"", aSrcContentStart + 1);
                    string aSrcFile = anImageTag.Substring (aSrcContentStart + 1, aSrcContentEnd - aSrcContentStart - 1);

                    var aTask = Task.Run (async () => {
                        try {
                            await aDependencyLoader.UpLoadFile (theRemoteHost + aSrcFile, $"\\{myOrgeoTitlesDir}\\{aSrcFile}");
                        } catch {}
                    });
                    Task.WaitAll (aTask);

                    string aNewSrcFile = $"/{myOrgeoTitlesDir}/{aSrcFile}";
                    string aNewImageTag = anImageTag.Replace (aSrcFile, aNewSrcFile);
                    aNewHtml = aNewHtml.Replace (anImageTag, aNewImageTag);
                }

                anImageStart = anImageEnd;
            }

            if (anHtml != aNewHtml) {
                myBody.Replace (anHtml, aNewHtml);
            }
        }
    }

    void InsertCloseButton (IElement theBody)
    {
        string aCloseButton = "<li id=\"ori-item-close\" style=\"display:none;\" class=\"nav-item\" onmouseover=\"this.style.cursor='pointer'\">" +
                                    "<a class=\"nav-link\"><img src=\"\\image\\svg\\close-button.svg\"></a>" +
                              "</li>";
        
        var aListOfItems = theBody.QuerySelector ("#left-tab");
        var aTemplate = aListOfItems.QuerySelector ("template").ToHtml();
        int anInsertionPos = myBody.IndexOf (aTemplate) + aTemplate.Length;
        myBody = myBody.Insert (anInsertionPos, aCloseButton);
    }

    string GenerateHtmlPage()
    {
        string aWebRoot = "";
        string aRes = "<html>" +
                            "<head>" +
                                "<meta http-equiv='cache-control' content='no-cache' />" +
                                "<meta http-equiv='Pragma' content='no-cache' />" +
                                "<meta http-equiv='Expires' content='-1' />" +
                                "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'>" +
                                "</script>" +
                                "<script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js'></script>" +
                                myScripts.ToString() +
                                $"<script src='{aWebRoot}/Libs/popper/popper.min.js'></script>" +
                                $"<script src='{aWebRoot}/Libs/bootstrap/bootstrap.min.js'></script>" +
                                $"<script src='{aWebRoot}/Libs/jscolor/jscolor.min.js'></script>" +
                                $"<link href='{aWebRoot}/Libs/bootstrap/bootstrap.min.css' rel='stylesheet'/>" +
                                myInlineScripts.ToString() +
                                myInlineStyles.ToString() +
                            "</head>" +
                                myBody +
                        "</html>";
        return aRes;
    }

}

}
