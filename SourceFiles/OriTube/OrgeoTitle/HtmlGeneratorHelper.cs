﻿namespace Ori.OriTube.OrgeoTitle
{
public class HtmlGeneratorHelper
{
    string[] myStyleException = {
        "bootstrap"
    };

    string[] myScriptException = {
        "jquery",
        "bootstrap",
        "vue",
        "jscolor",
        "popper"
    };

    public bool IsStyleException (string theSource)
    {
        return IsException (theSource, myStyleException);
    }

    public bool IsScriptException (string theSource)
    {
        return IsException (theSource, myScriptException);
    }

    private bool IsException (string theSource, string[] theExceptions)
    {
        if (theSource == null) {
            return true;
        }

        foreach (var aSrc in theExceptions) {
            if (theSource.Contains (aSrc)) {
                return true;
            }
        }

        return false;
    }
}

}
