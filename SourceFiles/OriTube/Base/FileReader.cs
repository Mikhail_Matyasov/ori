﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Ori.OriTube.Base
{
public class FileReader
{
    public static byte[] ReadBinaryFile (string theFileName)
    {
        var aFileName = $"{EnvironmentHelper.RootDirrectory()}\\{theFileName}";
        var aFileStream = File.OpenRead (aFileName);
        
        BinaryReader aReader = new BinaryReader (aFileStream);
        return aReader.ReadBytes ((int) aReader.BaseStream.Length);
    }         
}

}
