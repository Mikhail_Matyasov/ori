﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Ori.OriTube.Base
{
public class Loader : IO
{
    WebClient myWebClient = new WebClient();

    public async Task UpLoadFile (string theSource, string theTarget) //   '\\JS\\File.js'
    {
        try {
            var aData = await myWebClient.DownloadDataTaskAsync (theSource);

            var aWriter = new Writer (theTarget);
            await aWriter.WriteAsync (aData);
        } catch (Exception e) {
            throw e;
        }
    }
}

}
