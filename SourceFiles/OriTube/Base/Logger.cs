﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;

namespace Ori.OriTube.Base
{
    public class MessageContainer
    {
        private StringBuilder myMessages;
        public int NumberOfMessages { 
            get { return myMessages.Length; }
        }
        public string Content()
        {
            return myMessages.ToString();
        }

        public void Add (string theMessage)
        {
            myMessages.Append (theMessage + "\n");
        }

        public void Clear()
        {
            myMessages.Clear();
        }

        public MessageContainer()
        {
            myMessages = new StringBuilder();
        }
    }
    public class Logger : Ori.Base.ILogger
    {
        private static string myLogFile;
        private static string myLogDir;
        private static MessageContainer myContainer = new MessageContainer();
        private static int myNumberOfMessagesBeforFlush;
        public static Logger Instance { get; }
        public static Ori.Base.LogLevel Level { get; set; }

        static Logger()
        {
            Instance = new Logger();
        }
        public static void Init (string theLogDir)
        {
            string aTs = GetTimestamp();
            myLogDir = theLogDir;
            myLogFile = $"{theLogDir}{aTs}.log";
            Level = Ori.Base.LogLevel.Verbose;

            switch (Level) {

                case Ori.Base.LogLevel.None:
                    myNumberOfMessagesBeforFlush = 0;
                    break;
                case Ori.Base.LogLevel.Fail:
                    myNumberOfMessagesBeforFlush = 1;
                    break;
                case Ori.Base.LogLevel.Warning:
                    myNumberOfMessagesBeforFlush = 1;
                    break;
                case Ori.Base.LogLevel.Info:
                    myNumberOfMessagesBeforFlush = 100;
                    break;
                case Ori.Base.LogLevel.Verbose:
                    myNumberOfMessagesBeforFlush = 200;
                    break;
            }
        }
        public static void Info (string theMessage)
        {
            if (Level >= Ori.Base.LogLevel.Info) {
                Write ($"Info: {theMessage}");
            }
        }
        
        public static void Warning (string theMessage)
        {
            if (Level >= Ori.Base.LogLevel.Warning) {
                Write ($"Warning: {theMessage}");
            }
        }

        public static void Fail (string theMessage)
        {
            if (Level >= Ori.Base.LogLevel.Fail) {
                Write ($"Fail: {theMessage}");
            }
        }
        
        public override void Print (Ori.Base.String theMessage)
        {
            Write (theMessage.Data());
        }

        private static void Write (string theMessage)
        {
            if (myContainer.NumberOfMessages >= myNumberOfMessagesBeforFlush) {
                lock (myLogFile) {
                    File.AppendAllText (myLogFile, myContainer.Content());
                    myContainer.Clear();
                    UpdateLogFile();
                }
            } else {
                myContainer.Add (theMessage);
            }
        }

        private static string GetTimestamp()
        {
            var aNow = DateTime.Now;
            string aTimestamp = $"{aNow.Day}_{aNow.Month}_{aNow.Year}__{aNow.Hour}_{aNow.Minute}_{aNow.Second}";
            return aTimestamp;
        }

        private static void UpdateLogFile()
        {
            FileInfo anInfo = new FileInfo (myLogFile);
            if (anInfo.Length > 5000000) {
                string aTs = GetTimestamp();
                myLogFile = $"{myLogDir}{aTs}.log";
            }
        }
    }
}
