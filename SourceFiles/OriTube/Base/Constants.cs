﻿namespace Ori.OriTube.Base
{
public class Constants
{
    public static readonly string ORITUBE_VERSION = "2.0.1";
    public static readonly string ORITUBETEST_VERSION = "2.0.4";
}

}
