﻿using Microsoft.AspNetCore.Hosting;

namespace Ori.OriTube.Base
{
public class EnvironmentHelper
{
    private static IWebHostEnvironment myEnvironment;
    public static void Initialize (IWebHostEnvironment theEnv)
    {
        myEnvironment = theEnv;
    }

    public static string RootDirrectory()
    {
        return myEnvironment.WebRootPath;
    }
}

}
