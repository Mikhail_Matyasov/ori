﻿using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace Ori.OriTube.Base
{
public abstract class IO
{
    protected static string myRootDirrectory;
    protected string myFileName;
    public string FileName { get => myFileName; }
    static IO ()
    {
        myRootDirrectory = EnvironmentHelper.RootDirrectory();
    }

    public static bool FileExists (string theRelativePath)
    {
        return File.Exists (myRootDirrectory + theRelativePath);
    }

    public static void Copy (string theSource, string theTarget)
    {
        File.Copy (myRootDirrectory + theSource, myRootDirrectory + theTarget);
    }

    public static int GetNumberFilesInDir (string theDir)
    {
        return Directory.GetFiles (myRootDirrectory + theDir).Length;
    }
}

}
