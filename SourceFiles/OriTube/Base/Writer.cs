﻿using Microsoft.Extensions.FileProviders;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Ori.OriTube.Base
{
public class Writer : IO
{
    public delegate Task WriteFunc (FileStream theStream);
    WriteFunc myWriteFunc;
    public Writer (string theRelativePath) //   '\\JS\\File.js'
    {
        myFileName = myRootDirrectory + theRelativePath;
    }

    public async Task WriteAsync (string theData)
    {
        myWriteFunc += (theStream) => {
            return Task.Run (()=> {
                byte[] aData = Encoding.UTF8.GetBytes (theData);
                theStream.Write (aData, 0, aData.Length);
            });
        };
        await WriteDataToFile();
    }

    public async Task WriteAsync (byte[] theData)
    {
        myWriteFunc += (theStream) => {
            return Task.Run(() => {
                theStream.Write (theData, 0, theData.Length);
            });
        };
        await WriteDataToFile();
    }

    private async Task WriteDataToFile()
    {
        using (FileStream aFileStream = new FileStream (myFileName, FileMode.Create, FileAccess.Write))
        {
            await myWriteFunc (aFileStream);
        }
    }
}

}
