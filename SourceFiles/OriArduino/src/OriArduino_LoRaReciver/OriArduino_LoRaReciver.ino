#include <ArduinoLib_Init.h>
#include <ArduinoLib_LoRaReciver.h>

Ori::ArduinoLib::LoRaReciver theReciver;

void setup()
{
    Ori::ArduinoLib::Init::Setup();
    theReciver.Setup();
}

void loop()
{
    theReciver.Loop();
}
