#include <ArduinoLib_Init.h>
#include <ArduinoLib_LoRaTransmitter.h>
#include <ESP8266WiFi.h>

Ori::ArduinoLib::LoRaTransmitter theTransmitter;

void setup()
{
    Ori::ArduinoLib::Init::Setup();
    theTransmitter.Setup();
}

void loop()
{
    theTransmitter.Loop();
}
