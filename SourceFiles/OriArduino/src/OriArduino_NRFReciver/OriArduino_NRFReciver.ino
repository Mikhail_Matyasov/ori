#include <ArduinoLib_Init.h>
#include <ArduinoLib_NRF24L01Reciver.h>

Ori::ArduinoLib::NRF24L01Reciver theReciver;

void setup()
{
    Ori::ArduinoLib::Init::Setup();
    theReciver.Setup();
}

void loop()
{
    theReciver.Loop();
}
