#include <ArduinoLib_Init.h>
#include <ArduinoLib_NRF24L01Transmitter.h>

Ori::ArduinoLib::NRF24L01Transmitter theTransmitter;

void setup()
{
    Ori::ArduinoLib::Init::Setup();
    theTransmitter.Setup();
}

void loop()
{
    theTransmitter.Loop();
}
