#include <Ori/CodecJS_ObjectStorage.hxx>

namespace Ori {
namespace CodecJS {


ObjectStorage* ObjectStorage::Instance()
{
    static ObjectStorage* aStorage = new ObjectStorage();
    return aStorage;
}

ObjectStorage::ObjectStorage()
{}

ObjectStorage::~ObjectStorage()
{}

}}
