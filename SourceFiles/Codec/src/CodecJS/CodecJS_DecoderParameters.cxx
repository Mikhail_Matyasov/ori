#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecJS_DecoderParameters.hxx>

#include <Ori/Codec_DecoderParameters.hxx>
#include <Ori/CodecJS_ObjectStorage.hxx>
#include <Ori/Codec_CodecType.hxx>
#include <Ori/CodecJS_Macros.hxx>

extern "C" {

using namespace Ori::CodecJS;

int Ori_Codec_CreateDecoderParameters()
{
    auto aParameters = std::make_shared <Ori::Codec::DecoderParameters>();
    auto& aStorage = ObjectStorage::Instance()->DecoderParametersMap();

    int aKey = static_cast <int> (aStorage.size());
    aStorage.emplace (aKey, aParameters);

    return aKey;
}

void Ori_Codec_DecoderParameters_SetWidth (int theKey, int theWidth)
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    aParams->Width() = theWidth;
}

int Ori_Codec_DecoderParameters_GetWidth (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    return aParams->Width();
}

void Ori_Codec_DecoderParameters_SetHeight (int theKey, int theHeight)
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    aParams->Height() = theHeight;
}

int Ori_Codec_DecoderParameters_GetHeight (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    return aParams->Height();
}

void Ori_Codec_DecoderParameters_SetVideoCodecID (int theKey, int theId)
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    aParams->VideoCodecType() = static_cast <Ori::Codec::CodecType> (theId);
}

int Ori_Codec_DecoderParameters_GetVideoCodecID (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    return static_cast <int> (aParams->VideoCodecType());
}

void Ori_Codec_DecoderParameters_SetAudioCodecID (int theKey, int theId)
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    aParams->AudioCodecType() = static_cast <Ori::Codec::CodecType> (theId);
}

int Ori_Codec_DecoderParameters_GetAudioCodecID (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    return static_cast <int> (aParams->AudioCodecType());
}

void Ori_Codec_DecoderParameters_Free (int theKey)
{
    auto& aParams = ObjectStorage::Instance()->DecoderParametersMap();
    aParams.erase (theKey);
}

}
