#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecJS_ScalerParameters.hxx>

#include <Ori/Codec_ScalerParameters.hxx>
#include <Ori/CodecJS_ObjectStorage.hxx>
#include <Ori/Base_PixelFormat.hxx>
#include <Ori/CodecJS_Macros.hxx>


extern "C" {

using namespace Ori::CodecJS;

int Ori_Codec_CreateScalerParameters()
{
    auto aParameters = std::make_shared <Ori::Codec::ScalerParameters>();
    auto& aStorage = ObjectStorage::Instance()->ScalerParametersMap();

    int aKey = static_cast <int> (aStorage.size());
    aStorage.emplace (aKey, aParameters);

    return aKey;
}

void Ori_Codec_ScalerParameters_SetSourceWidth (int theKey, int theWidth)
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    aParams->SourceWidth() = theWidth;
}

void Ori_Codec_ScalerParameters_SetSourceHeight (int theKey, int theHeight) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    aParams->SourceHeight() = theHeight;
}

void Ori_Codec_ScalerParameters_SetTargetWidth (int theKey, int theWidth)
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    aParams->TargetWidth() = theWidth;
}

void Ori_Codec_ScalerParameters_SetTargetHeight (int theKey, int theHeight) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    aParams->TargetHeight() = theHeight;
}

int Ori_Codec_ScalerParameters_GetSourceWidth (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    return aParams->SourceWidth();
}

int Ori_Codec_ScalerParameters_GetSourceHeight (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    return aParams->SourceHeight();
}

int Ori_Codec_ScalerParameters_GetTargetWidth (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    return aParams->TargetWidth();
}

int Ori_Codec_ScalerParameters_GetTargetHeight (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    return aParams->TargetHeight();
}

void Ori_Codec_ScalerParameters_SetSourcePixelFormat (int theKey, int theFormat) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    aParams->SourcePixelFormat() = static_cast <Ori::Base::PixelFormat> (theFormat);
}

int Ori_Codec_ScalerParameters_GetSourcePixelFormat (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    return static_cast <int> (aParams->SourcePixelFormat());
}

void Ori_Codec_ScalerParameters_SetTargetPixelFormat (int theKey, int theFormat) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    aParams->TargetPixelFormat() = static_cast <Ori::Base::PixelFormat> (theFormat);
}

int Ori_Codec_ScalerParameters_GetTargetPixelFormat (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    return static_cast <int> (aParams->TargetPixelFormat());
}

void Ori_Codec_ScalerParameters_Free (int theKey) 
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap();
    aParams.erase (theKey);
}

}