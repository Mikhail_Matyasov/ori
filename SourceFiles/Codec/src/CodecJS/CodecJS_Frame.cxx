#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecJS_Frame.hxx>

#include <Ori/Base_Frame.hxx>
#include <Ori/CodecJS_ObjectStorage.hxx>
#include <Ori/CodecJS_Macros.hxx>


extern "C" {

using namespace Ori::CodecJS;

int Ori_Codec_Frame_GetWidth (int theKey)
{
    auto& aFrame = ObjectStorage::Instance()->FrameMap()[theKey];
    return aFrame.Width();
}

int Ori_Codec_Frame_GetHeight (int theFrame)
{
    auto& aFrame = ObjectStorage::Instance()->FrameMap()[theFrame];
    return aFrame.Height();
}

void Ori_Codec_Frame_GetDataByIndex (int theKey, int theBuferIndex, uint8_t* theTargetData)
{
    auto& aFrame = ObjectStorage::Instance()->FrameMap()[theKey];

    auto aBuffer = aFrame.Data()[theBuferIndex];
    auto aData = aBuffer->Data();
    int aLenght = aBuffer->Lenght();

    std::copy (aData, aData + aLenght, theTargetData);
}

void Ori_Codec_Frame_GetArraySize (int theKey, int* theArraySize /*array size = 8*/)
{
    auto& aFrame = ObjectStorage::Instance()->FrameMap()[theKey];
    auto& aBuffer = aFrame.Data();

    for (int i = 0; i < aBuffer.Lenght(); i++) {
        theArraySize[i] = aBuffer[i]->Lenght();
    }
}

void Ori_Codec_Frame_Free (int theKey)
{
    auto& aFrameMap = ObjectStorage::Instance()->FrameMap();
    aFrameMap.erase (theKey);
}

}
