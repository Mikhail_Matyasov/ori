#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecJS_Packet.hxx>

#include <Ori/Base_Macros.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/CodecJS_ObjectStorage.hxx>
#include <Ori/CodecJS_Macros.hxx>


extern "C" {

using namespace Ori::CodecJS;

void Ori_Codec_Packet_Free (int theKey)
{
    auto& aPackets = ObjectStorage::Instance()->PacketMap();
    aPackets.erase (theKey);
}

}
