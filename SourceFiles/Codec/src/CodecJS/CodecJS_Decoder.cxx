#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecJS_Decoder.hxx>

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Codec_Decoder.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Codec_DecoderParameters.hxx>
#include <Ori/CodecJS_ObjectStorage.hxx>
#include <Ori/CodecJS_Macros.hxx>
#include <Ori/Base_Logger.hxx>

extern "C" {
using namespace Ori::CodecJS;

int Ori_Codec_CreateDecoder (int theKey)
{
    Ori::Base::Logger::SetDefaultLevel (Ori::Base::LogLevel::Warning);

    auto& aParameters = ObjectStorage::Instance()->DecoderParametersMap()[theKey];
    auto aDecoder = std::make_shared <Ori::Codec::Decoder> (*aParameters);

    auto& aDecStorage = ObjectStorage::Instance()->DecoderMap();
    int aKey = static_cast <int> (aDecStorage.size());
    aDecStorage.emplace (aKey, aDecoder);

    return aKey;
}

int Ori_Codec_Decoder_MakePacket (int theKey, uint8_t* theData, int theDataLenght)
{
    auto& aDecoder = ObjectStorage::Instance()->DecoderMap()[theKey];

    Ori::Codec::Packet aPacket;
    aPacket.Data() = std::make_shared <Ori::Base::ByteArrayView> (theData, theDataLenght);

    auto& aPackets = ObjectStorage::Instance()->PacketMap();
    int aKey = static_cast <int> (aPackets.size());
    aPackets.emplace (aKey, aPacket);
    
    return aKey;
}

int Ori_Codec_Decoder_DecodePacket (int theKey, int thePacketIndex)
{
    auto& aDecoder = ObjectStorage::Instance()->DecoderMap()[theKey];
    auto& aPacket = ObjectStorage::Instance()->PacketMap()[thePacketIndex];

    bool aRes = aDecoder->DecodePacket (aPacket);
    return aRes ? 1 : 0;
}

int Ori_Codec_Decoder_GetFrame (int theKey)
{
    auto& aDecoder = ObjectStorage::Instance()->DecoderMap()[theKey];

    if (auto aFrame = aDecoder->GetFrame()) {

        auto& aFrames = ObjectStorage::Instance()->FrameMap();
        int aKey = static_cast  <int> (aFrames.size());
        aFrames.emplace (aKey, aFrame);
        return aKey;
    }

    return -1;
}

void  Ori_Codec_Decoder_Free (int theKey) 
{
    auto& aDecoder = ObjectStorage::Instance()->DecoderMap();
    aDecoder.erase (theKey);
}

}
