#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecJS_Scaler.hxx>

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_Scaler.hxx>
#include <Ori/Codec_ScalerParameters.hxx>
#include <Ori/CodecJS_ObjectStorage.hxx>
#include <Ori/CodecJS_Macros.hxx>


extern "C" {
using namespace Ori::CodecJS;

int Ori_Codec_CreateScaler (int theKey)
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap() [theKey];
    auto aScaler = std::make_shared <Ori::Codec::Scaler> (*aParams);

    auto& aStorage = ObjectStorage::Instance()->ScalerMap();
    int aKey = static_cast <int> (aStorage.size());
    aStorage.emplace (aKey, aScaler);
    return aKey;
}

int Ori_Codec_Scaler_ConvertPixelFormat (int theScalerIndex, int theFrameIndex)
{
    auto& aScaler = ObjectStorage::Instance()->ScalerMap()[theScalerIndex];
    auto& aSourceFrame = ObjectStorage::Instance()->FrameMap()[theFrameIndex];
    auto aConvertedFrame = aScaler->Scale (aSourceFrame);
    auto& aStorage = ObjectStorage::Instance()->FrameMap();

    int aKey = static_cast <int> (aStorage.size());
    aStorage.emplace (aKey, aConvertedFrame);
    return aKey;
}

void Ori_Codec_Scaler_Free (int theScalerIndex)
{
    auto& aParams = ObjectStorage::Instance()->ScalerParametersMap();
    aParams.erase (theScalerIndex);
}

}
