#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_EncoderParameters.hxx>


namespace Ori {
namespace Codec {

EncoderParameters::EncoderParameters() :
    myKeyFrameInterval (20),
    myBitRate (0),
    myMode (CodecMode::Realtime)
{}


}}
