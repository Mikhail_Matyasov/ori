#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_Decoder.hxx>

#include <Ori/Codec_Packet.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_DecoderParameters.hxx>
#include <Ori/CodecBase_Helper.hxx>
#include <Ori/CodecBase_RefCounter.hxx>
#include <Ori/Base_Logger.hxx>

extern "C" {
#include <libavutil/error.h>
}

using namespace Ori::CodecBase;

namespace Ori  {
namespace Codec {
namespace {
Base::String ToString (int theCode)
{
    char* aBuf = new char[255];
    av_strerror (theCode, aBuf, 255);
    return Base::String (aBuf);
}
}

Decoder::Decoder() :
    myCodecContext (nullptr),
    myNumberOfDecodedPackets (0),
    myRefCounter (nullptr)
{}

Decoder::Decoder (const DecoderParameters& theParameters) :
    myNumberOfDecodedPackets (0),
    myRefCounter (std::make_shared <RefCounter> ()),
    myDecodedFrame (theParameters.Width(), theParameters.Height())
{
    myFrame = av_frame_alloc();
    myPacket = av_packet_alloc();

    Helper aHelper;
    myCodecContext = aHelper.CreateDecoder (AVPixelFormat::AV_PIX_FMT_YUV420P, 
                                            theParameters.Width(),
                                            theParameters.Height(),
                                            theParameters.VideoCodecType());

    myDecodedFrame.Data().Resize (3);
}

Decoder::~Decoder()
{
    myRefCounter->Dec();
    if (myRefCounter->Counter() == 0) {
        avcodec_free_context (&myCodecContext);
        av_frame_free (&myFrame);
        av_packet_free (&myPacket);
    }
}

Decoder::Decoder (const Decoder& theDecoder)
{
    *this = theDecoder;
}

Decoder& Decoder::operator= (const Decoder& theDecoder)
{
    myNumberOfDecodedPackets = theDecoder.myNumberOfDecodedPackets;
    myCodecContext = theDecoder.myCodecContext;
    myRefCounter = theDecoder.myRefCounter;
    myRefCounter->Inc();
    return *this;
}
bool Decoder::DecodePacket (const Packet& thePacket)
{
    myPacket->pts = myNumberOfDecodedPackets;
    myPacket->dts = myNumberOfDecodedPackets;
    myNumberOfDecodedPackets++;

    auto& aBuf = thePacket.Data();
    myPacket->data = const_cast <uint8_t*> (aBuf->Data());
    myPacket->size = aBuf->Lenght();

    int aRetCode = avcodec_send_packet (myCodecContext, myPacket);
    if (aRetCode < 0) {
        auto anError = ToString (aRetCode);
        ORI_LOG_INFO_DEFAULT() << anError;
        return false;
    }

    return true;
}

Base::Frame Decoder::GetFrame()
{
    int aRet = avcodec_receive_frame (myCodecContext, myFrame);
    if (aRet < 0)  {
        auto anError = ToString (aRet);
        ORI_LOG_WARNING_DEFAULT() << anError;
        return Base::Frame();
    }
    ORI_LOG_INFO_DEFAULT() << "Received " << (myFrame->key_frame == 1 ? "I" : "P") << " Frame";
    int aYLenght = myDecodedFrame.Width() * myDecodedFrame.Height();
    int aULenght = aYLenght / 4;
    
    myDecodedFrame.Data()[0] = std::make_shared <Base::ByteArrayView> (myFrame->data[0], aYLenght);
    myDecodedFrame.Data()[1] = std::make_shared <Base::ByteArrayView> (myFrame->data[1], aULenght);
    myDecodedFrame.Data()[2] = std::make_shared <Base::ByteArrayView> (myFrame->data[2], aULenght);

    myDecodedFrame.Stride()[0] = myFrame->linesize[0];
    myDecodedFrame.Stride()[1] = myFrame->linesize[1];
    myDecodedFrame.Stride()[2] = myFrame->linesize[2];

    return myDecodedFrame;
}

Base::Frame Decoder::Decode (const Packet& thePacket)
{
    if (DecodePacket (thePacket)) {
        return GetFrame();
    }
    return Base::Frame();
}

}}
