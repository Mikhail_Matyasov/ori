#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_Scaler.hxx>

#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_ScalerParameters.hxx>
#include <Ori/CodecBase_RefCounter.hxx>
#include <Ori/Base_Logger.hxx>

extern "C" {
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

using namespace Ori::Base;

namespace Ori {
namespace Codec {
AVPixelFormat Convert (Base::PixelFormat theFormat)
{
    switch (theFormat)
    {
    case PixelFormat::RGBA:
        return AVPixelFormat::AV_PIX_FMT_RGBA;
    case PixelFormat::BGRA:
        return AVPixelFormat::AV_PIX_FMT_BGRA;
    default:
        return AVPixelFormat::AV_PIX_FMT_YUV420P;
    }
}

Scaler::Scaler() :
    myScaleContext (nullptr),
    myRefCounter (nullptr)
{}

Scaler::Scaler (const ScalerParameters& theParams) :
    myParams (theParams),
    myRefCounter (std::make_shared <CodecBase::RefCounter>())
{
    myScaleContext = sws_getContext (theParams.SourceWidth(),
                                     theParams.SourceHeight(),
                                     Convert (theParams.SourcePixelFormat()),
                                     theParams.TargetWidth(),
                                     theParams.TargetHeight(),
                                     Convert (theParams.TargetPixelFormat()),
                                     SWS_POINT, NULL, NULL, NULL);

    if (!myScaleContext) {
        ORI_LOG_FAIL_DEFAULT() << "Can not allocate scaler context.";
        return;
    }

    myDestLinesize = new int[8];
    myDestData = new uint8_t*[8] { 0 };
    
    myBufferSize = av_image_alloc (myDestData,
                                   myDestLinesize,
                                   theParams.TargetWidth(),
                                   theParams.TargetHeight(),
                                   Convert (myParams.TargetPixelFormat()), 1);
    if (myBufferSize == 0) {
        ORI_LOG_FAIL_DEFAULT() << "Can not allocate image buffer.";
        return;
    }
}

Scaler::~Scaler()
{
    myRefCounter->Dec();
    if (myRefCounter->Counter() == 0) {
        sws_freeContext (myScaleContext);
        
        delete[] myDestLinesize;
        av_freep (&myDestData[0]);
        delete[] myDestData;
    }
}

Scaler::Scaler (const Scaler& theScaler) :
    myParams (theScaler.myParams)
{
    *this = theScaler;
}

Scaler& Scaler::operator=(const Scaler& theScaler)
{
    myBufferSize = theScaler.myBufferSize;
    myDestData = theScaler.myDestData;
    myDestLinesize = theScaler.myDestLinesize;
    myScaleContext = theScaler.myScaleContext;
    myRefCounter = theScaler.myRefCounter;
    myRefCounter->Inc();

    return *this;
}

Frame Scaler::Scale (const Frame& theFrame)
{
    int aCountOfDataArray = 8;

    const uint8_t* aData[8];
    int aNumberOfPlanes = theFrame.Data().Lenght();
    for (int i = 0; i < aNumberOfPlanes; i++) {
        aData[i] = theFrame.Data()[i]->Data();
    }

    int aRes = sws_scale (myScaleContext,
                          aData,
                          theFrame.Stride().Data(),
                          0,
                          theFrame.Height(),
                          myDestData,
                          myDestLinesize);
    if (aRes < 0) {
        ORI_LOG_FAIL_DEFAULT() << "Can not scale image";
    }

    Frame aResFrame (myParams.TargetWidth(), myParams.TargetHeight(), myParams.TargetPixelFormat());

    int aLenght = 0;
    while (myDestLinesize[aLenght]) {
        aResFrame.Stride()[aLenght] = myDestLinesize[aLenght];
        aLenght++;
    }

    aResFrame.Data().Resize (aLenght);
    if (aLenght == 1) {
        int aLenght = myDestLinesize[0] * myParams.TargetHeight();
        aResFrame.Data()[0] = std::make_shared <ByteArrayView> (myDestData[0], aLenght);
    } else if (aLenght == 3) {

        int aYLenght = myDestLinesize[0] * myParams.TargetHeight();
        int aULenght = aYLenght / 4;
        aResFrame.Data()[0] = std::make_shared <ByteArrayView> (myDestData[0], aYLenght);
        aResFrame.Data()[1] = std::make_shared <ByteArrayView> (myDestData[1], aULenght);
        aResFrame.Data()[2] = std::make_shared <ByteArrayView> (myDestData[2], aULenght);
    } else {
        throw Base::Exception ("Unsupported pixel format");
    }

    return aResFrame;
}

}}
