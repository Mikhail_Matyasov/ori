#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_AdaptiveEncoder.hxx>

#include <Ori/Codec_Encoder.hxx>
#include <Ori/Codec_Scaler.hxx>
#include <Ori/Codec_ScalerParameters.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Base_Exception.hxx>
#include <Ori/Base_Logger.hxx>

using namespace Ori::Base;

namespace Ori {
namespace Codec {

void AdaptiveEncoder::CreateContext()
{
    EncoderParameters aParams;
    aParams.AudioCodecType() = myParams.AudioCodecType();
    aParams.BitRate() = myParams.BitRate();
    aParams.KeyFrameInterval() = myParams.KeyFrameInterval();
    aParams.VideoCodecType() = myParams.VideoCodecType();
    aParams.Width() = myCalibratedWidth;
    aParams.Height() = myCalibratedHeight;

    myEncoder = std::make_shared <Encoder> (aParams);

    ScalerParameters aScaleParams;
    aScaleParams.SourcePixelFormat() = myParams.SourcePixelFormat();
    aScaleParams.TargetPixelFormat() = PixelFormat::YUV420;
    aScaleParams.SourceWidth() = myParams.Width();
    aScaleParams.SourceHeight() = myParams.Height();
    aScaleParams.TargetWidth() = myCalibratedWidth;
    aScaleParams.TargetHeight() = myCalibratedHeight;

    myScaler = std::make_shared <Scaler> (aScaleParams);
}


AdaptiveEncoder::AdaptiveEncoder (const AdaptiveEncoderParameters& theParameters) :
    myParams (theParameters),
    myCalibratedWidth (theParameters.Width()),
    myCalibratedHeight (theParameters.Height()),
    myIsCalibrated (false)
{
    CreateContext();
    myTargetDuration = 1000000 / theParameters.TargetFPS();
    ORI_LOG_INFO_DEFAULT() << "Target time per frame - "
                           << static_cast <int> (myTargetDuration) << " microseconds";
}

Packet AdaptiveEncoder::EncodeFrame (const Frame& theFrame)
{
    auto aFrame = PreprocessFrame (theFrame);

    if (myEncoder->EncodeFrame (aFrame)) {

        if (auto aPacket = myEncoder->GetPacket()) {

            if (!myIsCalibrated) {
                auto aDuration = myTimer.DurationSinceLastTimestamp();
                myTimeStamps.Push (aDuration);
            }
            return aPacket;
        } else {
            ORI_LOG_WARNING_DEFAULT() << "Can not get packet";
        }
    } else {
        ORI_LOG_WARNING_DEFAULT() << "Can not encode frame";
    }

    return Packet();
}

void AdaptiveEncoder::Reset()
{
    myEncoder->Reset();    
}

namespace {
Timer::DurationType GetMedian (Base::Vector <AdaptiveEncoder::DurationType>& theVec)
{
    std::sort (theVec.Data(), theVec.Data() + theVec.Lenght(), [] (
        const AdaptiveEncoder::DurationType& theLeft,
        const AdaptiveEncoder::DurationType& theRight) -> bool {

        if (theLeft.Microseconds() < theRight.Microseconds()) {
            return true;
        }
        return false;
    });
    auto aMid = theVec[theVec.Lenght() / 2];
    return aMid.Microseconds();
}
}

Base::Frame AdaptiveEncoder::PreprocessFrame (const Base::Frame& theFrame)
{
    if (!myIsCalibrated) {
        // Try improve encoder each 8 frames.
        if (myTimeStamps.Lenght() == 8) {

            auto aMid = GetMedian (myTimeStamps);
            ORI_LOG_INFO_DEFAULT() << "Median = " << static_cast <int> (aMid);
            int64_t aDif = static_cast <int64_t> (aMid) - static_cast <int64_t> (myTargetDuration);
            // [0, 2000] - range of target fps in microseconds.
            int aRange = 2000;
            if (aDif > aRange) {

                CalibrateEncoder (aMid);
            } else if (aDif < -aRange &&
                       myCalibratedHeight < myParams.Height() &&
                       myCalibratedWidth < myParams.Width()) {

                CalibrateEncoder (aMid);
            } else {
                myCalibratedFPS = 1000000.0 / aMid;
                myIsCalibrated = true;
            }
            myTimeStamps.Reset();
        }
    }

    if (!myIsCalibrated) {
        myTimer.Timestamp();
    }

    Frame aScaledFrame;
    aScaledFrame = myScaler->Scale (theFrame);

    return aScaledFrame;
}

void AdaptiveEncoder::CalibrateEncoder (int64_t theDuration)
{
    CalculateNewSize (theDuration);
    CreateContext();
}

void AdaptiveEncoder::CalculateNewSize (int64_t theDuration)
{
    auto aCoef = static_cast <double> (theDuration) / myTargetDuration;

    // 1) Current size of frame = 5 * 8 = 40px.
    // 2) aCoef = 10 => we need reduce size of image in 10 times.
    // 3) Target size of image => 40 / 10 = 4.
    // 4) Let's imagine 16 / 9 => current frame resolution, then
       // 16x * 9x = 4
       // 144x^2 = 4
       // x = sqrt (4 / 144) 

    int aSizeOfFrame = myCalibratedWidth * myCalibratedHeight;
    double aTargetSize = static_cast <double> (aSizeOfFrame / aCoef);
    double anSqr = aTargetSize / aSizeOfFrame;
    double x = sqrt (anSqr);

    myCalibratedWidth = static_cast <int> (myCalibratedWidth * x);
    myCalibratedHeight = static_cast <int> (myCalibratedHeight * x);
    ORI_LOG_INFO_DEFAULT() << "New width: " << myCalibratedWidth << ", New Height: " << myCalibratedHeight;
    if (myCalibratedWidth % 2 != 0) {
        myCalibratedWidth--;
    }

    if (myCalibratedHeight % 2 != 0) {
        myCalibratedHeight--;
    }
}

}}
