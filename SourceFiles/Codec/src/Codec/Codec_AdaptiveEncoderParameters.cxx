#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_AdaptiveEncoderParameters.hxx>


namespace Ori {
namespace Codec {

AdaptiveEncoderParameters::AdaptiveEncoderParameters() :
    myTargetFPS (24),
    mySourcePixelFormat (Base::PixelFormat::YUV420)
{}

}}
