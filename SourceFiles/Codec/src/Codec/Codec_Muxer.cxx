#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_Muxer.hxx>

#include <Ori/CodecBase_Helper.hxx>

extern "C" {
#include <libavformat/avformat.h>
}

using namespace Ori::CodecBase;

namespace Ori {
namespace Codec {

Muxer::Muxer (const MuxerParameters& theParameters) :
    myVidePacketPts (-1),
    myGopSize (10),
    myVideoStreamIndex (0)
{
    myFormatContext = avformat_alloc_context();
    bool aRes = InitVideoStream (theParameters);
    aRes = InitAudioStream (theParameters);

    int aRetCode = avformat_write_header (myFormatContext, nullptr);
    if (aRetCode < 0) {
        aRes = false;
    }

    if (!aRes) {
        avformat_free_context (myFormatContext);
        avcodec_free_context (&myVideoCodecContext);
    }
}

Muxer::~Muxer()
{
    avformat_free_context (myFormatContext);
    avcodec_free_context (&myVideoCodecContext);
}

bool Muxer::MuxPacket (Packet& thePacket)
{
    //thePacket.GetRawData()->stream_index = myVideoStream->index;
    //av_packet_rescale_ts (thePacket.GetRawData(),
    //                      myVideoCodecContext->time_base,
    //                      myVideoStream->time_base);

    //if (av_interleaved_write_frame (myFormatContext, thePacket.GetRawData()) < 0) {
    //    return false;
    //}
    av_interleaved_write_frame (myFormatContext, nullptr);
    return true;
}

bool Muxer::GetFragment (Fragment& theFragment)
{
    if (myFormatContext->pb->buffer == myFormatContext->pb->buf_ptr) {
        return false;
    }

    theFragment.Data().Insert (myFormatContext->pb->buffer, myFormatContext->pb->buf_ptr);
    avio_flush (myFormatContext->pb);
    return true;
}

Packet Muxer::MakePacket (const Base::ByteArray& thePacketBuffer)
{
    myVidePacketPts++;
    int aFlag = myVidePacketPts % myGopSize == 0 ? 1 : 0;
    return Packet();
    //return Packet (thePacketBuffer, myVidePacketPts, myVideoStreamIndex, aFlag);
}

bool Muxer::InitVideoStream (const MuxerParameters& theParameters)
{
    int aBufferSize = 262144; // max possible size of packet
    auto aBuffer = new uint8_t [aBufferSize];
    myFormatContext->pb = avio_alloc_context (aBuffer,
                                              aBufferSize,
                                              AVIO_FLAG_WRITE,
                                              nullptr, nullptr, nullptr, nullptr);

    myFormatContext->oformat = av_guess_format (nullptr, ".webm", nullptr);
    myVideoStream = avformat_new_stream (myFormatContext, nullptr);

    Helper aHelper;
    myVideoCodecContext = aHelper.CreateEncoder (AVPixelFormat::AV_PIX_FMT_YUV420P, 
                                                 theParameters.Width(),
                                                 theParameters.Height(),
                                                 theParameters.VideoCodecType());

    avcodec_parameters_from_context (myVideoStream->codecpar, myVideoCodecContext);
    myFormatContext->pb->min_packet_size = aBufferSize;
    return true;
}

bool Muxer::InitAudioStream (const MuxerParameters& theParameters)
{
#if 0
    auto anAudioCodec = avcodec_find_encoder (theParameters.AudioCodecID());
    myAudioStream = avformat_new_stream (myFormatContext, nullptr);
    myAudioStream->id = myFormatContext->nb_streams - 1;
    myAudioCodecContext = avcodec_alloc_context3 (anAudioCodec);

    myAudioCodecContext->sample_fmt  = anAudioCodec->sample_fmts ? 
                                       anAudioCodec->sample_fmts[0] :
                                       AVSampleFormat::AV_SAMPLE_FMT_FLTP;

    myAudioCodecContext->bit_rate    = 64000;
    myAudioCodecContext->sample_rate = 44100;

    if (anAudioCodec->supported_samplerates) {
        myAudioCodecContext->sample_rate = anAudioCodec->supported_samplerates[0];
        for (int i = 0; anAudioCodec->supported_samplerates[i]; i++) {
            if (anAudioCodec->supported_samplerates[i] == 44100)
                myAudioCodecContext->sample_rate = 44100;
        }
    }

    myAudioCodecContext->channels = av_get_channel_layout_nb_channels (myAudioCodecContext->channel_layout);
    myAudioCodecContext->channel_layout = AV_CH_LAYOUT_STEREO;
    if (anAudioCodec->channel_layouts) {
        myAudioCodecContext->channel_layout = anAudioCodec->channel_layouts[0];
        for (int i = 0; anAudioCodec->channel_layouts[i]; i++) {
            if (anAudioCodec->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
                myAudioCodecContext->channel_layout = AV_CH_LAYOUT_STEREO;
        }
    }
    myAudioCodecContext->channels = av_get_channel_layout_nb_channels (myAudioCodecContext->channel_layout);
    myAudioStream->time_base = { 1, myAudioCodecContext->sample_rate };

    if (myFormatContext->oformat->flags & AVFMT_GLOBALHEADER) {
        myAudioCodecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }

    int aRes = avcodec_open2 (myAudioCodecContext, anAudioCodec, nullptr);
    if (aRes < 0) {
        return false;
    }

    aRes = avcodec_parameters_from_context (myAudioStream->codecpar, myAudioCodecContext);
    if (aRes < 0) {
        return false;
    }
    return true;
#endif
    return false;
}

}}
