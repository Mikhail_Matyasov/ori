#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_CodecType.hxx>

namespace Ori {
namespace Codec {

Base::String Codec::ToString (const CodecType& theType)
{
	switch (theType)
	{
	case CodecType::VP8:
		return Base::String ("VP8");
	case CodecType::VP9:
		return Base::String ("VP9");
	case CodecType::MPEG:
		return Base::String ("MPEG");
	default:
		break;
	}

	return Base::String ("Unrecognized codec.");
}

}}