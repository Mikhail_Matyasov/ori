#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_Encoder.hxx>

#include <Ori/CodecBase_EncoderImpl.hxx>

namespace Ori {
namespace Codec {

Encoder::Encoder (const EncoderParameters& theParameters) :
	myImpl (std::make_shared <CodecBase::EncoderImpl> (theParameters))
{}

bool Encoder::EncodeFrame (const Base::Frame& theFrame)
{
	return myImpl->EncodeFrame (theFrame);
}

Packet Encoder::Encode (const Base::Frame& theFrame)
{
    return myImpl->Encode (theFrame);
}

Packet Encoder::GetPacket()
{
	return myImpl->GetPacket();
}

Codec::Status Encoder::Status()
{
	return myImpl->Status();
}

const EncoderParameters& Encoder::Parameters()
{
	return myImpl->Parameters();
}

void Encoder::Reset()
{
	myImpl->Reset();
}

}}
