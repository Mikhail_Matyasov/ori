#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_VideoSplitter.hxx>

#include <Ori/Base_String.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Codec_Decoder.hxx>
#include <Ori/Codec_DecoderParameters.hxx>
#include <Ori/CodecBase_Helper.hxx>

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
}

using namespace Ori::Base;

namespace Ori {
namespace Codec {

Vector <Frame> VideoSplitter::Split (const String& theFileName)
{
    Vector <Frame> aFrames;
    AVFormatContext* aFormatContext = nullptr;
    if ((avformat_open_input (&aFormatContext, theFileName.Data(), nullptr, nullptr)) < 0) {
        ORI_LOG_FAIL_DEFAULT() << "Can not open input file: " << theFileName;
        return aFrames;
    }

    if ((avformat_find_stream_info (aFormatContext, NULL)) < 0) {
        ORI_LOG_FAIL_DEFAULT() << "Cannot find stream information";
        return aFrames;
    }

    DecoderParameters aParams;

    for (unsigned int i = 0; i < aFormatContext->nb_streams; i++) {
        auto aStream = aFormatContext->streams [i];
        if (aStream->codec->codec_type == AVMediaType::AVMEDIA_TYPE_VIDEO) {
            AVCodecID anID = aStream->codec->codec_id;
            aParams.VideoCodecType() = CodecBase::Helper::ToNativeCodecType (anID);
            aParams.Width() = aStream->codec->width;
            aParams.Height() = aStream->codec->height;
        }
    }

    Decoder aDecoder (aParams);

    AVPacket* aPacket = av_packet_alloc();
    while (av_read_frame (aFormatContext, aPacket) >= 0) {

        ByteArray anArray (aPacket->data, aPacket->size);
        Packet aPack (anArray);
        if (aDecoder.DecodePacket (aPack)) {
            if (const auto& aFrame = aDecoder.GetFrame()) {

                // frames stored inside decoder, therefor we should clone them to prevent frames deallocation
                aFrames.Push (aFrame.Clone());
            }
        }
    }

    return aFrames;
}

}}
