#include <Ori/CodecBase_Pch.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/CodecBase_PacketImpl.hxx>

namespace Ori {
namespace Codec {

Packet::Packet() :
    myImpl (std::make_shared <CodecBase::PacketImpl>())
{}

Packet::Packet (const Base::ByteArray& theData) :
    myImpl (std::make_shared <CodecBase::PacketImpl> (theData.begin(), theData.end()))
{}

Packet::Packet (const std::shared_ptr <CodecBase::PacketImpl>& theImpl) :
    myImpl (theImpl)
{}

std::shared_ptr <Base::ByteArrayView>& Packet::Data()
{
    return myImpl->Data();    
}

const std::shared_ptr <Base::ByteArrayView>& Packet::Data() const
{
    return myImpl->Data();
}

bool Packet::IsKey() const
{
    return myImpl->IsKey();
}

bool& Packet::IsKey()
{
    return myImpl->IsKey();
}

Packet::operator bool()
{
    return myImpl->operator bool();
}

Packet Packet::Clone()
{
    return Packet (myImpl->Clone());
}

}}
