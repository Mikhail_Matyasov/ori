#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecBase_PacketImpl.hxx>


namespace Ori {
namespace CodecBase {

PacketImpl::PacketImpl() :
    myIsKey (false)
{}

PacketImpl::PacketImpl (const uint8_t* theFirst, const uint8_t* theLast) :
    myData (std::make_shared <Base::ByteArray> (theFirst, theLast)),
    myIsKey (false)
{}

PacketImpl::operator bool() const
{
    return myData && !myData->Empty();
}

std::shared_ptr <PacketImpl> PacketImpl::Clone()
{
    return std::make_shared <PacketImpl> (myData->begin(), myData->end());
}

}}
