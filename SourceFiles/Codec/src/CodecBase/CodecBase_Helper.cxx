#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecBase_Helper.hxx>

#include <Ori/Base_Logger.hxx>

extern "C" {
#include <libavutil/error.h>
}

namespace Ori {
namespace CodecBase {
namespace {

Base::String GetError (int theCode)
{
	typedef Base::String S;

	char* aBuf = new char[255];
	av_strerror (theCode, aBuf, 255);
	auto aRes =  S ("Code error: ") + S (theCode) + S (", ") + S (reinterpret_cast <const char*> (aBuf));

	delete[] aBuf;
	return aRes;
}

AVCodecID ToFFMPEG (Codec::CodecType theType)
{
	switch (theType)
	{
	case Codec::CodecType::VP8:
		return AVCodecID::AV_CODEC_ID_VP8;
	case Codec::CodecType::VP9:
		return AVCodecID::AV_CODEC_ID_VP9;
	case Codec::CodecType::MPEG:
		return AVCodecID::AV_CODEC_ID_MPEG1VIDEO;
	default:
		break;
	}

	return AVCodecID::AV_CODEC_ID_NONE;
}

}

AVCodecContext* Helper::CreateEncoder (AVPixelFormat thePixFormat,
									   int theWidth,
									   int theHeight,
									   Codec::CodecType theType)
{
	auto aCodecId = ToFFMPEG (theType);
    auto aCodec = avcodec_find_encoder (aCodecId);
	return CreateCodecContext (aCodec, thePixFormat, theWidth, theHeight);
}

AVCodecContext* Helper::CreateDecoder (AVPixelFormat thePixFormat,
									   int theWidth,
									   int theHeight,
									   Codec::CodecType theType)
{
	auto aCodecId = ToFFMPEG (theType);
	auto aCodec = avcodec_find_decoder (aCodecId);
	if (!aCodec) {
		ORI_LOG_FAIL_DEFAULT() << "Can not find codec: " << Codec::ToString (theType);
		return nullptr;
	}
	return CreateCodecContext (aCodec, thePixFormat, theWidth, theHeight);
	
}

Codec::CodecType Helper::ToNativeCodecType (AVCodecID theCodecID)
{
	switch (theCodecID)
	{
	case AVCodecID::AV_CODEC_ID_VP8:
		return Codec::CodecType::VP8;
	case AVCodecID::AV_CODEC_ID_VP9:
		return Codec::CodecType::VP9;
	case AVCodecID::AV_CODEC_ID_MPEG1VIDEO:
		return Codec::CodecType::MPEG;
	default:
		break;
	}

	return Codec::CodecType::Undefined;
}

AVCodecContext* Helper::CreateCodecContext (AVCodec* theCodec,
											AVPixelFormat thePixFormat,
											int theWidth,
											int theHeight)
{
	auto aCodecContext = avcodec_alloc_context3 (theCodec);
	if (!aCodecContext) {
		ORI_LOG_FAIL_DEFAULT() << "Can not allocate codec context";
		return nullptr;
	}
	// desire number bit per second. 1000 - Minimum value.
	aCodecContext->bit_rate = 256000;

	aCodecContext->width = theWidth;
	aCodecContext->height = theHeight;

	/* 1 second / number frames */
    aCodecContext->time_base = { 100000000, 1 };
    aCodecContext->framerate = { 30, 1 };

	/* Numer frames between two I frames */
	aCodecContext->gop_size = 10;
	aCodecContext->max_b_frames = 0;
    aCodecContext->pix_fmt = thePixFormat;

	int aRetCode = avcodec_open2 (aCodecContext, theCodec, nullptr);

	if (aRetCode < 0) {
		ORI_LOG_FAIL_DEFAULT() << "Can not open codec, " << GetError (aRetCode);
		avcodec_free_context (&aCodecContext);
		return nullptr;
	}

	return aCodecContext;
}

}}
