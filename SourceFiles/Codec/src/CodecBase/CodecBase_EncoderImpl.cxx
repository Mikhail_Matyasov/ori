#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecBase_EncoderImpl.hxx>

#include <Ori/Base_Frame.hxx>
#include <Ori/CodecBase_VPXTypes.hxx>
#include <Ori/Base_Logger.hxx>

namespace Ori {
namespace CodecBase {
namespace {

const VpxInterface* FindEncoder (CodecType theType)
{
	switch (theType)
	{
	case CodecType::VP8:
		return &VpxEncoders[0];
	case CodecType::VP9:
		return &VpxEncoders[1];
	default:
		break;
	}

	return nullptr;
}

int NumberOfThreads (int theWidth, int theHeight) {

	// Keep the number of encoder threads equal to the possible number of column
	// tiles, which is (1, 2, 4, 8). See comments below for VP9E_SET_TILE_COLUMNS.
	int aNumberOfCores = std::thread::hardware_concurrency();
	if (theWidth * theHeight >= 1280 * 720 && aNumberOfCores > 4) {
		return 4;
	} else if (theWidth * theHeight >= 640 * 360 && aNumberOfCores > 2) {
		return 2;
	} else {

#ifdef __ANDROID__
	// Use 2 threads for low res on ARM.
	if (theWidth * theHeight >= 320 * 180 && aNumberOfCores > 2) {
		return 2;
	}
#endif
	// 1 thread less than VGA.
		return 1;
	}
}

}

EncoderImpl::EncoderImpl (const EncoderParameters& theParameters) :
	myFrameIndex (0),
	myKeyFrameInterval (theParameters.KeyFrameInterval() != 0 ? theParameters.KeyFrameInterval() : 10),
	myCodecType (theParameters.VideoCodecType()),
	myParameters (theParameters)
{
	Init();
}

EncoderImpl::~EncoderImpl()
{
	Destroy();
}

bool EncoderImpl::EncodeFrame (const Base::Frame& theFrame)
{
    int flags = 0;
    if (myFrameIndex % myKeyFrameInterval == 0) {
        flags |= VPX_EFLAG_FORCE_KF;
    }

	FillVpxFrame (theFrame);
	vpx_codec_err_t	anError = vpx_codec_encode (&myCodecContext, &myVpxFrame, myFrameIndex++, 1, flags, VPX_DL_GOOD_QUALITY);
	if (anError != 0) {
		ORI_LOG_VERBOSE_DEFAULT() << "Can not encode frame using" << ToString (myParameters.VideoCodecType()) << "encoder";
		myStatus = Status::Fail;
		return false;
	}

	return true;
}

Packet EncoderImpl::Encode (const Base::Frame& theFrame)
{
	if (EncodeFrame (theFrame)) {
		return GetPacket();
	}

    return Packet();
}

Packet EncoderImpl::GetPacket()
{
    vpx_codec_iter_t anIt = nullptr;

	const auto aPacket = vpx_codec_get_cx_data (&myCodecContext, &anIt);
	if (!aPacket) {
		ORI_LOG_VERBOSE_DEFAULT() << "Can not get packet from " << ToString (myParameters.VideoCodecType()) << " constext";
		myStatus = Status::Fail;
		return Packet();
	}

	uint8_t* aSourceBuf = reinterpret_cast <uint8_t*> (aPacket->data.frame.buf);
	int aBufferLenght = static_cast <int> (aPacket->data.frame.sz);

	Packet aResPacket;
	aResPacket.Data() = std::make_shared <Base::ByteArrayView> (aSourceBuf, aBufferLenght);
	if ((myFrameIndex - 1) % myKeyFrameInterval == 0) {
		aResPacket.IsKey() = true;
	}
	return aResPacket;
}

void EncoderImpl::Reset()
{
	myFrameIndex = 0;
	Destroy();
	Init();
}

void EncoderImpl::Destroy()
{
	vpx_img_free (&myVpxFrame);
    vpx_codec_destroy (&myCodecContext);
}

void EncoderImpl::Init()
{
	if (!vpx_img_alloc (&myVpxFrame, VPX_IMG_FMT_I420, myParameters.Width(), myParameters.Height(), 1)) {
		ORI_LOG_FAIL_DEFAULT() << "Can not allocate vpx image";
		Destroy();
		myStatus = Status::Fail;
		return;
	}
	myVpxFrame.bit_depth = 8;

	auto anEncoder = FindEncoder (myParameters.VideoCodecType());
	vpx_codec_enc_cfg_t aConfig;

	auto aRetCode = vpx_codec_enc_config_default (anEncoder->codec_interface(), &aConfig, 0);
	if (aRetCode != 0) {
		ORI_LOG_FAIL_DEFAULT() << "Can not initialize default encoder config";
		Destroy();
		myStatus = Status::Fail;
		return;
	}

	// parameter's values takes from webrtc sources: /modules/video_coding/codecs/vp9/vp9_impl.cc
	aConfig.g_w = myParameters.Width();
	aConfig.g_h = myParameters.Height();
	aConfig.g_threads = NumberOfThreads (myParameters.Width(), myParameters.Height());
	aConfig.g_bit_depth = VPX_BITS_8;
	aConfig.g_profile = 0;
	aConfig.g_input_bit_depth = 8;
	aConfig.g_error_resilient = VPX_ERROR_RESILIENT_DEFAULT;
	aConfig.g_lag_in_frames = 0;
	aConfig.rc_undershoot_pct = 50;
    aConfig.rc_overshoot_pct = 50;
    aConfig.rc_buf_sz = 1000;
	aConfig.kf_mode = vpx_kf_mode::VPX_KF_DISABLED;
	aConfig.rc_resize_allowed = 1;
	aConfig.temporal_layering_mode = VP9E_TEMPORAL_LAYERING_MODE_NOLAYERING;
    aConfig.ts_number_layers = 1;
    aConfig.ts_rate_decimator[0] = 1;
    aConfig.ts_periodicity = 1;
    aConfig.ts_layer_id[0] = 0;
	aConfig.ss_number_layers = 1;
	aConfig.g_pass = vpx_enc_pass::VPX_RC_ONE_PASS;

	if (myParameters.BitRate() != 0) {
		aConfig.rc_target_bitrate = myParameters.BitRate();
	}
	if (myParameters.Mode() == CodecMode::MaximumCompression) {
		aConfig.rc_buf_initial_sz = 4000;
		aConfig.rc_buf_optimal_sz = 5000;
		aConfig.rc_dropframe_thresh = 70;
		aConfig.kf_min_dist = 0;
		aConfig.kf_max_dist = 0;
		aConfig.rc_min_quantizer = 63;
		aConfig.rc_max_quantizer = 63;
		aConfig.g_timebase.num = 1;
		aConfig.g_timebase.den = 100000;
	} else {
		aConfig.rc_buf_initial_sz = 500;
		aConfig.rc_buf_optimal_sz = 600;
		aConfig.rc_dropframe_thresh = 30;
		aConfig.kf_max_dist = myParameters.KeyFrameInterval();
		aConfig.kf_min_dist = aConfig.kf_max_dist;
		aConfig.rc_min_quantizer = 2;
		aConfig.rc_max_quantizer = 52;
		aConfig.g_timebase.num = 100000;
		aConfig.g_timebase.den = 1;
	}

	aRetCode = vpx_codec_enc_init (&myCodecContext, anEncoder->codec_interface(), &aConfig, 0);
	if (aRetCode != 0) {
		ORI_LOG_FAIL_DEFAULT() << "Can not initialize codec context";
		Destroy();
		myStatus = Status::Fail;
		return;
	}

	auto aVpxPrivate = reinterpret_cast <vpx_codec_alg_priv*> (myCodecContext.priv);
	aVpxPrivate->oxcf.speed = 8;
	ORI_LOG_INFO_DEFAULT() << ToString (myParameters.VideoCodecType()) << " context successfuly initialized";
	myStatus = Status::Success;
}

void EncoderImpl::FillVpxFrame (const Base::Frame& theFrame)
{
	auto& aSource = theFrame.Data();
	auto aTarget = myVpxFrame.planes;

	aTarget[0] = const_cast <uint8_t*> ((*aSource[0]).Data());
	aTarget[1] = const_cast <uint8_t*> ((*aSource[1]).Data());
	aTarget[2] = const_cast <uint8_t*> ((*aSource[2]).Data());
}

}}
