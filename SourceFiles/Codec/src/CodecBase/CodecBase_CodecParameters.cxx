#include <Ori/CodecBase_Pch.hxx>
#include <Ori/CodecBase_CodecParameters.hxx>

namespace Ori {
namespace CodecBase {

CodecParameters::CodecParameters() :
    myWidth (240),
	myHeight (320),
	myVideoCodecType (Codec::CodecType::VP8),
	myAudioCodecType (Codec::CodecType::Undefined)
{}

}}