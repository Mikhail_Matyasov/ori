
THIRD_PARTY_HOME=$1
CODEC_SRC_DIR=$2
CODEC_INCLUDE_DIR=$3
ORI_CORE_DIR=$4
OUTPUTFILE=$5

export COMPILER="em"
cd $THIRD_PARTY_HOME
source set_env.sh
source set_unix_env.sh
source set_em_env.sh

SRC="$CODEC_SRC_DIR/Codec/Codec_CodecType.cxx
     $CODEC_SRC_DIR/Codec/Codec_Frame.cxx \
     $CODEC_SRC_DIR/Codec/Codec_Packet.cxx \
     $CODEC_SRC_DIR/Codec/Codec_Scaler.cxx \
     $CODEC_SRC_DIR/Codec/Codec_Decoder.cxx \
     $CODEC_SRC_DIR/CodecBase/CodecBase_CodecParameters.cxx \
     $CODEC_SRC_DIR/CodecBase/CodecBase_FrameImpl.cxx \
     $CODEC_SRC_DIR/CodecBase/CodecBase_Helper.cxx \
     $CODEC_SRC_DIR/CodecBase/CodecBase_PacketImpl.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_Decoder.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_DecoderParameters.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_Frame.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_Macros.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_ObjectStorage.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_Packet.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_Scaler.cxx \
     $CODEC_SRC_DIR/CodecJS/CodecJS_ScalerParameters.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_String.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_UTF8String.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_UTF16String.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_LoggerImpl.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_ILogger.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_Logger.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_LoggerBuffer.cxx \
     ${ORI_CORE_DIR}/src/Base/Base_Exception.cxx"

FFMPEG_BUILD_DIR=$FFMPEG_DIR"/em/release"
BOOST_INCLUDE=$THIRD_PARTY_HOME"/boost/vc142-x86_64/debug/include"
INCLUDE_DIR="-I${FFMPEG_BUILD_DIR}/include\
             -I${ORI_CORE_DIR}/include \
             -I${CODEC_INCLUDE_DIR} \
             -I${BOOST_INCLUDE}"
             
ADDITIONAL_LIBRARIES_DIR="-L${FFMPEG_BUILD_DIR}/lib"
ADDITIONAL_LIBRARIES="-lavcodec -lavutil -lswscale"

emcc ${SRC} -o ${OUTPUTFILE} -s WASM=1 ${INCLUDE_DIR} ${ADDITIONAL_LIBRARIES_DIR} ${ADDITIONAL_LIBRARIES} -s EXIT_RUNTIME=1 -s TOTAL_MEMORY=512MB -s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap", "getValue", "setValue"]' -std=c++17