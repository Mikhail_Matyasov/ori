%feature("nspace") Ori::Codec::ScalerParameters;

%{
#include <Ori/Codec_ScalerParameters.hxx>
%}

%include <Ori/Codec_ScalerParameters.hxx>