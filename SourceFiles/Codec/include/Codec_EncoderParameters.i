%feature("nspace") Ori::Codec::EncoderParameters;

%{
#include <Ori/Codec_EncoderParameters.hxx>
%}

%include <Ori/Codec_EncoderParameters.hxx>