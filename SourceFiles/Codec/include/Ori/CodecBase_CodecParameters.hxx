#ifndef _Ori_CodecBase_CodecParameters_HeaderFile
#define _Ori_CodecBase_CodecParameters_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Codec_CodecType.hxx>

namespace Ori {
namespace CodecBase {

class CodecParameters
{
public:
	ORI_EXPORT CodecParameters();

private:

	__ORI_PRIMITIVE_PROPERTY (int, Width);
	__ORI_PRIMITIVE_PROPERTY (int, Height);
	__ORI_PRIMITIVE_PROPERTY (Codec::CodecType, VideoCodecType);
	__ORI_PRIMITIVE_PROPERTY (Codec::CodecType, AudioCodecType);
};

}}

#endif
