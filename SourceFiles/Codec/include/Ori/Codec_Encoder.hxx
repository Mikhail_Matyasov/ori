#ifndef _Ori_Codec_Encoder_HeaderFile
#define _Ori_Codec_Encoder_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Codec_Status.hxx>
#include <memory>

namespace Ori {
namespace Base {
class Frame;
class String;
}

namespace CodecBase {
class EncoderImpl;
}

namespace Codec {
class Packet;
class EncoderParameters;

class Encoder
{
public:
	ORI_EXPORT Encoder (const EncoderParameters& theParameters);
	ORI_EXPORT bool EncodeFrame (const Base::Frame& theFrame);
	ORI_EXPORT Packet Encode (const Base::Frame& theFrame);
	ORI_EXPORT Packet GetPacket();
	ORI_EXPORT Codec::Status Status();
	ORI_EXPORT const EncoderParameters& Parameters();
	ORI_EXPORT void Reset();

private:
	std::shared_ptr <CodecBase::EncoderImpl> myImpl;
};

}}

#endif
