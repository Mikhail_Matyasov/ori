#ifndef _Ori_CodecBase_VPXTypes_HeaderFile
#define _Ori_CodecBase_VPXTypes_HeaderFile

extern "C" {
#include <vpx/internal/vpx_codec_internal.h>
#include <vpx/vp8cx.h>
#include <vp9/encoder/vp9_encoder.h>
#include <vpx_util/vpx_timestamp.h>
}

typedef struct vp9_extracfg {
  int cpu_used;  // available cpu percentage in 1/16
  unsigned int enable_auto_alt_ref;
  unsigned int noise_sensitivity;
  unsigned int sharpness;
  unsigned int static_thresh;
  unsigned int tile_columns;
  unsigned int tile_rows;
  unsigned int enable_tpl_model;
  unsigned int arnr_max_frames;
  unsigned int arnr_strength;
  unsigned int min_gf_interval;
  unsigned int max_gf_interval;
  vp8e_tuning tuning;
  unsigned int cq_level;  // constrained quality level
  unsigned int rc_max_intra_bitrate_pct;
  unsigned int rc_max_inter_bitrate_pct;
  unsigned int gf_cbr_boost_pct;
  unsigned int lossless;
  unsigned int target_level;
  unsigned int frame_parallel_decoding_mode;
  AQ_MODE aq_mode;
  int alt_ref_aq;
  unsigned int frame_periodic_boost;
  vpx_bit_depth_t bit_depth;
  vp9e_tune_content content;
  vpx_color_space_t color_space;
  vpx_color_range_t color_range;
  int render_width;
  int render_height;
  unsigned int row_mt;
  unsigned int motion_vector_unit_test;
  int delta_q_uv;
} vp9_extracfg;

typedef struct vpx_codec_alg_priv {
  vpx_codec_priv_t base;
  vpx_codec_enc_cfg_t cfg;
  struct vp9_extracfg extra_cfg;
  vpx_rational64_t timestamp_ratio;
  vpx_codec_pts_t pts_offset;
  unsigned char pts_offset_initialized;
  VP9EncoderConfig oxcf;
  VP9_COMP *cpi;
  unsigned char *cx_data;
  size_t cx_data_sz;
  unsigned char *pending_cx_data;
  size_t pending_cx_data_sz;
  int pending_frame_count;
  size_t pending_frame_sizes[8];
  size_t pending_frame_magnitude;
  vpx_image_t preview_img;
  vpx_enc_frame_flags_t next_frame_flags;
  vp8_postproc_cfg_t preview_ppcfg;
  vpx_codec_pkt_list_decl(256) pkt_list;
  unsigned int fixed_kf_cntr;
  vpx_codec_priv_output_cx_pkt_cb_pair_t output_cx_pkt_cb;
  // BufferPool that holds all reference frames.
  BufferPool *buffer_pool;
} vpx_codec_alg_priv;

typedef struct VpxInterface {
  const char *const name;
  const uint32_t fourcc;
  vpx_codec_iface_t *(*const codec_interface)();
} VpxInterface;

#define VP8_FOURCC 0x30385056
#define VP9_FOURCC 0x30395056

static const VpxInterface VpxEncoders[] = {
  { "vp8", VP8_FOURCC, &vpx_codec_vp8_cx },
  { "vp9", VP9_FOURCC, &vpx_codec_vp9_cx }
};

#endif