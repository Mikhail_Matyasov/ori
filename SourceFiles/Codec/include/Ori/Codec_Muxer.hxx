#ifndef _Ori_Codec_Muxer_HeaderFile
#define _Ori_Codec_Muxer_HeaderFile

#include <Ori/Codec_MuxerParameters.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Codec_Fragment.hxx>
#include <Ori/Base_ByteArray.hxx>

struct AVFormatContext;
struct AVCodecContext;
struct AVStream;

namespace Ori {
namespace Codec {

class Muxer
{
public:
	ORI_EXPORT Muxer (const MuxerParameters& theParameters);
	ORI_EXPORT ~Muxer();
	ORI_EXPORT bool MuxPacket (Packet& thePacket);
	ORI_EXPORT bool GetFragment (Fragment& theFragment);
	ORI_EXPORT Packet MakePacket (const Base::ByteArray& thePacketBuffer);

private:
	bool InitVideoStream (const MuxerParameters& theParameters);
	bool InitAudioStream (const MuxerParameters& theParameters);

private:
	AVFormatContext* myFormatContext;
	AVCodecContext* myVideoCodecContext;
	AVStream* myVideoStream;
	AVCodecContext* myAudioCodecContext;
	AVStream* myAudioStream;
	int myVideoStreamIndex;
	int myVidePacketPts;
	int myGopSize;

};

}}

#endif
