#ifndef _Ori_CodecBase_FFMPEGTypes_HeaderFile
#define _Ori_CodecBase_FFMPEGTypes_HeaderFile

#include <Ori/CodecBase_VPXTypes.hxx>

extern "C" {
#include <libavutil/dict.h>
#include <libavutil/log.h>
#include <libavcodec/avcodec.h>
}

typedef struct VPxEncoderContext {
    AVClass *class_type;
    struct vpx_codec_ctx encoder;
    struct vpx_image rawimg;
    struct vpx_codec_ctx encoder_alpha;
    struct vpx_image rawimg_alpha;
    uint8_t is_alpha;
    struct vpx_fixed_buf twopass_stats;
    int deadline; //i.e., RT/GOOD/BEST
    uint64_t sse[4];
    int have_sse; /**< true if we have pending sse[] */
    uint64_t frame_number;
    struct FrameListData *coded_frame_list;

    int cpu_used;
    int sharpness;
    /**
     * VP8 specific flags, see VP8F_* below.
     */
    int flags;
#define VP8F_ERROR_RESILIENT 0x00000001 ///< Enable measures appropriate for streaming over lossy links
#define VP8F_AUTO_ALT_REF    0x00000002 ///< Enable automatic alternate reference frame generation

    int auto_alt_ref;

    int arnr_max_frames;
    int arnr_strength;
    int arnr_type;

    int tune;

    int lag_in_frames;
    int error_resilient;
    int crf;
    int static_thresh;
    int max_intra_rate;
    int rc_undershoot_pct;
    int rc_overshoot_pct;

    AVDictionary *vpx_ts_parameters;
    int *ts_layer_flags;
    int current_temporal_idx;

    // VP9-only
    int lossless;
    int tile_columns;
    int tile_rows;
    int frame_parallel;
    int aq_mode;
    int drop_threshold;
    int noise_sensitivity;
    int vpx_cs;
    float level;
    int row_mt;
    int tune_content;
    int corpus_complexity;
    int tpl_model;
    /**
     * If the driver does not support ROI then warn the first time we
     * encounter a frame with ROI side data.
     */
    int roi_warned;
} VPxContext;

typedef struct DecodeSimpleContext {
    AVPacket *in_pkt;
    AVFrame  *out_frame;
} DecodeSimpleContext;

typedef struct DecodeFilterContext {
    AVBSFContext **bsfs;
    int         nb_bsfs;
} DecodeFilterContext;

typedef struct AVCodecInternal {
    /**
     * Whether the parent AVCodecContext is a copy of the context which had
     * init() called on it.
     * This is used by multithreading - shared tables and picture pointers
     * should be freed from the original context only.
     */
    int is_copy;

    /**
     * An audio frame with less than required samples has been submitted and
     * padded with silence. Reject all subsequent frames.
     */
    int last_audio_frame;

    AVFrame *to_free;

    AVBufferRef *pool;

    void *thread_ctx;

    DecodeSimpleContext ds;
    DecodeFilterContext filter;

    /**
     * Properties (timestamps+side data) extracted from the last packet passed
     * for decoding.
     */
    AVPacket *last_pkt_props;

    /**
     * temporary buffer used for encoders to store their bitstream
     */
    uint8_t *byte_buffer;
    unsigned int byte_buffer_size;

    void *frame_thread_encoder;

    /**
     * Number of audio samples to skip at the start of the next decoded frame
     */
    int skip_samples;

    /**
     * hwaccel-specific private data
     */
    void *hwaccel_priv_data;

    /**
     * checks API usage: after codec draining, flush is required to resume operation
     */
    int draining;

    /**
     * buffers for using new encode/decode API through legacy API
     */
    AVPacket *buffer_pkt;
    int buffer_pkt_valid; // encoding: packet without data can be valid
    AVFrame *buffer_frame;
    int draining_done;
    /* set to 1 when the caller is using the old decoding API */
    int compat_decode;
    int compat_decode_warned;
    /* this variable is set by the decoder internals to signal to the old
     * API compat wrappers the amount of data consumed from the last packet */
    size_t compat_decode_consumed;
    /* when a partial packet has been consumed, this stores the remaining size
     * of the packet (that should be submitted in the next decode call */
    size_t compat_decode_partial_size;
    AVFrame *compat_decode_frame;

    int showed_multi_packet_warning;

    int skip_samples_multiplier;

    /* to prevent infinite loop on errors when draining */
    int nb_draining_errors;

    /* used when avctx flag AV_CODEC_FLAG_DROPCHANGED is set */
    int changed_frames_dropped;
    int initial_format;
    int initial_width, initial_height;
    int initial_sample_rate;
    int initial_channels;
    uint64_t initial_channel_layout;
} AVCodecInternal;

#endif
