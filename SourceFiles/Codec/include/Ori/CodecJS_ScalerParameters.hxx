#ifndef _Ori_CodecJS_ScalerParameters_HeaderFile
#define _Ori_CodecJS_ScalerParameters_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <stdint.h>

extern "C" {

EMSCRIPTEN_KEEPALIVE int Ori_Codec_CreateScalerParameters();
EMSCRIPTEN_KEEPALIVE void Ori_Codec_ScalerParameters_SetSourceWidth (int theKey, int theWidth);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_ScalerParameters_SetSourceHeight (int theKey, int theHeight);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_ScalerParameters_SetTargetWidth (int theKey, int theWidth);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_ScalerParameters_SetTargetHeight (int theKey, int theHeight);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_ScalerParameters_GetSourceWidth (int theKey);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_ScalerParameters_GetSourceHeight (int theKey);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_ScalerParameters_GetTargetWidth (int theKey);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_ScalerParameters_GetTargetHeight (int theKey);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_ScalerParameters_SetSourcePixelFormat (int theKey, int theFormat);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_ScalerParameters_SetTargetPixelFormat (int theKey, int theFormat);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_ScalerParameters_GetTargetPixelFormat (int theKey);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_ScalerParameters_Free (int theKey);

}

#endif
