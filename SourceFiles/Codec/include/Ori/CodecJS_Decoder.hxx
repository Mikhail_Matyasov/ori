#ifndef _Ori_CodecJS_Decoder_HeaderFile
#define _Ori_CodecJS_Decoder_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <stdint.h>

extern "C" {

EMSCRIPTEN_KEEPALIVE int Ori_Codec_CreateDecoder (int theKey);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_Decoder_MakePacket (int theKey, uint8_t* theData, int theDataLenght);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_Decoder_DecodePacket (int theKey, int thePacketIndex);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_Decoder_GetFrame (int theKey);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_Decoder_Free (int theKey); 

}

#endif
