#ifndef _Ori_Codec_ScalerParameters_HeaderFile
#define _Ori_Codec_ScalerParameters_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_PixelFormat.hxx>

namespace Ori {
namespace Codec {

class ScalerParameters
{
    __ORI_PRIMITIVE_PROPERTY (int, SourceWidth);
    __ORI_PRIMITIVE_PROPERTY (int, SourceHeight);
    __ORI_PRIMITIVE_PROPERTY (Base::PixelFormat, SourcePixelFormat);
    __ORI_PRIMITIVE_PROPERTY (int, TargetWidth);
    __ORI_PRIMITIVE_PROPERTY (int, TargetHeight);
    __ORI_PRIMITIVE_PROPERTY (Base::PixelFormat, TargetPixelFormat);
};

}}

#endif
