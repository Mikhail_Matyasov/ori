#ifndef _Ori_CodecJS_Scaler_HeaderFile
#define _Ori_CodecJS_Scaler_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <stdint.h>

extern "C" {

EMSCRIPTEN_KEEPALIVE int Ori_Codec_CreateScaler (int theKey);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_Scaler_ConvertPixelFormat (int theScalerIndex, int theFrameIndex);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_Scaler_Free (int theScalerIndex);

}

#endif
