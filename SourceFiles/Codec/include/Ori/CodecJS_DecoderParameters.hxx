#ifndef _Ori_CodecJS_DecoderParameters_HeaderFile
#define _Ori_CodecJS_DecoderParameters_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <stdint.h>

extern "C" {

EMSCRIPTEN_KEEPALIVE int Ori_Codec_CreateDecoderParameters();
EMSCRIPTEN_KEEPALIVE void Ori_Codec_DecoderParameters_SetWidth (int theKey, int theWidth);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_DecoderParameters_GetWidth (int theKey);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_DecoderParameters_SetHeight (int theKey, int theHeight);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_DecoderParameters_GetHeight (int theKey);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_DecoderParameters_SetVideoCodecID (int theKey, int theId);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_DecoderParameters_GetVideoCodecID (int theKey);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_DecoderParameters_SetAudioCodecID (int theKey, int theId);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_DecoderParameters_GetAudioCodecID (int theKey);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_DecoderParameters_Free (int theKey);

}

#endif