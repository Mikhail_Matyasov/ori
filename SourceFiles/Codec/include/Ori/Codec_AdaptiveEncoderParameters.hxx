#ifndef _Ori_Codec_AdaptiveEncoderParameters_HeaderFile
#define _Ori_Codec_AdaptiveEncoderParameters_HeaderFile

#include <Ori/Codec_EncoderParameters.hxx>
#include <Ori/Base_PixelFormat.hxx>

namespace Ori {
namespace Codec {

class AdaptiveEncoderParameters : public EncoderParameters
{
public:
    ORI_EXPORT AdaptiveEncoderParameters();
    __ORI_PRIMITIVE_PROPERTY (int, TargetFPS)
    __ORI_PRIMITIVE_PROPERTY (Base::PixelFormat, SourcePixelFormat)
};

}}

#endif
