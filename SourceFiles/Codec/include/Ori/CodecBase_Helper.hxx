#ifndef _Ori_CodecBase_Helper_HeaderFile
#define _Ori_CodecBase_Helper_HeaderFile

#include <Ori/Base_String.hxx>
#include <Ori/Codec_CodecType.hxx>

extern "C" {
#include <libavutil/pixfmt.h>
}

struct AVCodecContext;
struct AVCodec;

namespace Ori {
namespace Base {
class String;
}

namespace CodecBase {

class Helper {

public:
    enum class CodecType
    {
        Encoder,
        Decoder
    };

    AVCodecContext* CreateEncoder (AVPixelFormat thePixFormat,
                                   int theWidth,
                                   int theHeight,
                                   Codec::CodecType theType);

    AVCodecContext* CreateDecoder (AVPixelFormat thePixFormat,
                                   int theWidth,
                                   int theHeight,
                                   Codec::CodecType theType);

    static Codec::CodecType ToNativeCodecType (AVCodecID theCodecID);

private:
    AVCodecContext* CreateCodecContext (AVCodec* thecodec,
                                        AVPixelFormat thePixFormat,
                                        int theWidth,
                                        int theHeight);
};

}}

#endif