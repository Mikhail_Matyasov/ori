#ifndef _Ori_CodecJS_Frame_HeaderFile
#define _Ori_CodecJS_Frame_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <stdint.h>

extern "C" {

EMSCRIPTEN_KEEPALIVE int Ori_Codec_Frame_GetWidth (int theKey);
EMSCRIPTEN_KEEPALIVE int Ori_Codec_Frame_GetHeight (int theFrame);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_Frame_GetDataByIndex (int theKey, int theBuferIndex, uint8_t* theTargetData);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_Frame_GetArraySize (int theKey, int* theArraySize);
EMSCRIPTEN_KEEPALIVE void Ori_Codec_Frame_Free (int theKey);

}

#endif
