#ifndef _Ori_CodecJS_Packet_HeaderFile
#define _Ori_CodecJS_Packet_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <stdint.h>

extern "C" {

EMSCRIPTEN_KEEPALIVE void Ori_Codec_Packet_Free (int theKey);

}

#endif
