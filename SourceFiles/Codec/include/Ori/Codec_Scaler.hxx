#ifndef _Ori_Codec_Scaler_HeaderFile
#define _Ori_Codec_Scaler_HeaderFile

#include <Ori/Codec_ScalerParameters.hxx>
#include <Ori/Base_Frame.hxx>
#include <functional>
#include <memory>

struct SwsContext;

namespace Ori {
namespace CodecBase {
class RefCounter;
}

namespace Codec {

class Scaler
{
public:
	ORI_EXPORT Scaler();
	ORI_EXPORT Scaler (const ScalerParameters& theParams);
	ORI_EXPORT ~Scaler();

	ORI_EXPORT Scaler (const Scaler& theScaler);
	ORI_EXPORT Scaler& operator= (const Scaler& theScaler);

	ORI_EXPORT Base::Frame Scale (const Base::Frame& theFrame);
	
private:
	SwsContext* myScaleContext;
	ScalerParameters myParams;
	std::shared_ptr <CodecBase::RefCounter> myRefCounter;

	uint8_t** myDestData;
    int* myDestLinesize;
	int myBufferSize;
};

}}

#endif
