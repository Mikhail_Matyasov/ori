#ifndef _Ori_Codec_VideoSplitter_HeaderFile
#define _Ori_Codec_VideoSplitter_HeaderFile

#include <Ori/Base_Vector.hxx>
#include <Ori/Base_Frame.hxx>

namespace Ori {
namespace Base {
class String;
}
namespace Codec {

class VideoSplitter
{
public:
    ORI_EXPORT static Base::Vector <Base::Frame> Split (const Base::String& theFileName);

};

}}


#endif