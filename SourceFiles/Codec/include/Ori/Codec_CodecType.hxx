#ifndef _Ori_Codec_CodecType_HeaderFile
#define _Ori_Codec_CodecType_HeaderFile

#include <Ori/Base_String.hxx>

namespace Ori {
namespace Codec {

enum class CodecType
{
    Undefined,
    VP8,
    VP9,
    MPEG
};

ORI_EXPORT Base::String ToString (const CodecType& theType);

}}


#endif