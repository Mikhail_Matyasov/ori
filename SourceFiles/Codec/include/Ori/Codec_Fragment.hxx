#ifndef _Ori_Codec_Fragment_HeaderFile
#define _Ori_Codec_Fragment_HeaderFile

#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace Codec {

class Fragment
{
public:
	ORI_EXPORT Fragment() {}

	__ORI_PROPERTY (Base::ByteArray, Data);
	
};

}}

#endif
