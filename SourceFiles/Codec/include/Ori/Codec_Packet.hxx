#ifndef _Ori_Codec_Packet_HeaderFile
#define _Ori_Codec_Packet_HeaderFile

#include <memory>
#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace CodecBase  {
class PacketImpl;
}

namespace Codec {

class Packet
{
public:

	ORI_EXPORT Packet();
	ORI_EXPORT Packet (const Base::ByteArray& theData);

	ORI_EXPORT std::shared_ptr <Base::ByteArrayView>& Data();
	ORI_EXPORT const std::shared_ptr <Base::ByteArrayView>& Data() const;

	__ORI_PRIMITIVE_UNIMPLEMENTED_REFERENCE (bool, IsKey);

	operator bool();
	Packet Clone();

private:
	Packet (const std::shared_ptr <CodecBase::PacketImpl>& theImpl);

private:
	std::shared_ptr <CodecBase::PacketImpl> myImpl;
};

}}

#endif
