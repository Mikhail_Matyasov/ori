#ifndef _Ori_Codec_Pch_HeaderFile
#define _Ori_Codec_Pch_HeaderFile

#define __STDC_CONSTANT_MACROS

#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Macros.hxx>

extern "C" {
#include <libavcodec/avcodec.h>
}

#endif
