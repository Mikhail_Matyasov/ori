#ifndef _Ori_CodecBase_PacketImpl_HeaderFile
#define _Ori_CodecBase_PacketImpl_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace CodecBase {

class PacketImpl
{
public:

	PacketImpl();
	PacketImpl (const uint8_t* theFirst, const uint8_t* theLast);

	operator bool() const;
	std::shared_ptr <PacketImpl> Clone();

	__ORI_PROPERTY (std::shared_ptr <Base::ByteArrayView>, Data);
	__ORI_PRIMITIVE_PROPERTY (bool, IsKey);

};

}}

#endif
