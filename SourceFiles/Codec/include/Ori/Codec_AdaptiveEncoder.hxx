#ifndef _Ori_Codec_AdaptiveEncoder_HeaderFile
#define _Ori_Codec_AdaptiveEncoder_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Codec_AdaptiveEncoderParameters.hxx>

namespace Ori {
namespace Base {
class Frame;
}
namespace Codec {
class Encoder;
class Scaler;

class AdaptiveEncoder
{
public:
	typedef Base::Timer::Duration DurationType;

	ORI_EXPORT AdaptiveEncoder (const AdaptiveEncoderParameters& theParameters);
	ORI_EXPORT Packet EncodeFrame (const Base::Frame& theFrame);
	ORI_EXPORT bool IsCalibrated() { return myIsCalibrated; }
	ORI_EXPORT void Reset();
	__ORI_PRIMITIVE_PROPERTY (int, CalibratedWidth);
	__ORI_PRIMITIVE_PROPERTY (int, CalibratedHeight);
	__ORI_PRIMITIVE_PROPERTY (double, CalibratedFPS);

private:
    Base::Frame PreprocessFrame (const Base::Frame& theFrame);
	void CalibrateEncoder (int64_t theDuration);
	void CalculateNewSize (int64_t theDuration);
	void CreateContext();

private:
	Base::Timer myTimer;
	AdaptiveEncoderParameters myParams;
	std::shared_ptr <Encoder> myEncoder;
	std::shared_ptr <Scaler> myScaler;
	Base::Vector <DurationType> myTimeStamps;
	Base::Timer::DurationType myTargetDuration;
	bool myIsCalibrated;
};

}}

#endif
