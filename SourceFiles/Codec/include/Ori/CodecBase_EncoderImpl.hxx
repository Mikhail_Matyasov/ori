#ifndef _Ori_CodecBase_EncoderImpl_HeaderFile
#define _Ori_CodecBase_EncoderImpl_HeaderFile

#include <Ori/Base_String.hxx>
#include <Ori/Codec_CodecType.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Codec_EncoderParameters.hxx>
#include <Ori/Codec_Status.hxx>

using namespace Ori::Codec;

extern "C" {
#include <vpx/vpx_image.h>
#include <vpx/vpx_codec.h>
}

namespace Ori {
namespace Base {
class Frame;
}
namespace CodecBase {

class EncoderImpl
{
public:
	EncoderImpl (const EncoderParameters& theParameters);
	~EncoderImpl();
	bool EncodeFrame (const Base::Frame& theFrame);
	Packet Encode (const Base::Frame& theFrame);
	Packet GetPacket();
	void Reset();

	__ORI_PROPERTY (Ori::Codec::EncoderParameters, Parameters);
	__ORI_PRIMITIVE_PROPERTY (Codec::Status, Status)

private:
	void Destroy();
	void Init();
	void FillVpxFrame (const Base::Frame& theFrame);

private:
	vpx_codec_ctx myCodecContext;
	vpx_image myVpxFrame;
	int myFrameIndex;
	int myKeyFrameInterval;
	CodecType myCodecType;
};

}}


#endif