#ifndef _Ori_Codec_Decoder_HeaderFile
#define _Ori_Codec_Decoder_HeaderFile

#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_PixelFormat.hxx>
#include <Ori/Base_Frame.hxx>

struct AVCodecContext;
struct AVFrame;
struct AVPacket;

namespace Ori {

namespace CodecBase {
class RefCounter;
}
namespace Codec {
class DecoderParameters;
class Packet;

class Decoder
{
public:
	ORI_EXPORT Decoder();
	ORI_EXPORT Decoder (const DecoderParameters& theParameters);
	ORI_EXPORT ~Decoder();

	ORI_EXPORT Decoder (const Decoder& theDecoder);
	ORI_EXPORT Decoder& operator= (const Decoder& theDecoder);

	ORI_EXPORT bool DecodePacket (const Packet& thePacket);
	ORI_EXPORT Base::Frame GetFrame();
	ORI_EXPORT Base::Frame Decode (const Packet& thePacket);

private:
	AVCodecContext* myCodecContext;
	int myNumberOfDecodedPackets;
	std::shared_ptr <CodecBase::RefCounter> myRefCounter;
	AVFrame* myFrame;
	AVPacket* myPacket;
	Base::Frame myDecodedFrame;

};

}}

#endif
