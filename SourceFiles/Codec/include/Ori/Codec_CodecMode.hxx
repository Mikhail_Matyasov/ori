#ifndef _Ori_Codec_CodecMode_HeaderFile
#define _Ori_Codec_CodecMode_HeaderFile

namespace Ori {
namespace Codec {

enum class CodecMode
{
    Realtime,
    MaximumCompression
};

}}

#endif
