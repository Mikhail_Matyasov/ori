#ifndef _Ori_Codec_EncoderParameters_HeaderFile
#define _Ori_Codec_EncoderParameters_HeaderFile

#include <Ori/CodecBase_CodecParameters.hxx>
#include <Ori/Codec_CodecMode.hxx>

namespace Ori {
namespace Codec {

class EncoderParameters : public CodecBase::CodecParameters
{
public:
    ORI_EXPORT EncoderParameters();
    __ORI_PRIMITIVE_PROPERTY (int, KeyFrameInterval);
    __ORI_PRIMITIVE_PROPERTY (int, BitRate);
    __ORI_PRIMITIVE_PROPERTY (CodecMode, Mode);
};

}}

#endif
