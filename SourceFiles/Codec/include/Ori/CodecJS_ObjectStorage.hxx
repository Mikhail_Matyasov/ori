#ifndef _Ori_CodecJS_ObjectStorage_HeaderFile
#define _Ori_CodecJS_ObjectStorage_HeaderFile

#include <Ori/Base_Pch.hxx>
#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Frame.hxx>
#include <Ori/Codec_Packet.hxx>
#include <Ori/Codec_Decoder.hxx>
#include <Ori/Codec_Scaler.hxx>
#include <Ori/Codec_DecoderParameters.hxx>
#include <Ori/Codec_ScalerParameters.hxx>

namespace Ori {
namespace CodecJS {

class ObjectStorage
{
public:
    static ObjectStorage* Instance();
    
    typedef std::unordered_map <int, Base::Frame>                                FrameMapType;
    typedef std::unordered_map <int, Codec::Packet>                              PacketMapType;
    typedef std::unordered_map <int, std::shared_ptr <Codec::Decoder>>           DecoderMapType;
    typedef std::unordered_map <int, std::shared_ptr <Codec::Scaler>>            ScalerMapType;
    typedef std::unordered_map <int, std::shared_ptr <Codec::DecoderParameters>> DecoderParametersMapType;
    typedef std::unordered_map <int, std::shared_ptr <Codec::ScalerParameters>>  ScalerParametersMapType;

    __ORI_PROPERTY (FrameMapType,             FrameMap);
    __ORI_PROPERTY (PacketMapType,            PacketMap);
    __ORI_PROPERTY (DecoderMapType,           DecoderMap);
    __ORI_PROPERTY (ScalerMapType,            ScalerMap);
    __ORI_PROPERTY (DecoderParametersMapType, DecoderParametersMap);
    __ORI_PROPERTY (ScalerParametersMapType,  ScalerParametersMap);

private:
    ObjectStorage();
    ~ObjectStorage();
};

}}

#endif
