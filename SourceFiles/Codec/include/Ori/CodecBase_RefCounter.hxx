#ifndef _Ori_CodecBase_RefCounter_HeaderFile
#define _Ori_CodecBase_RefCounter_HeaderFile

#include <Ori/Base_Macros.hxx>

namespace Ori {
namespace CodecBase {

class RefCounter {

public:
    RefCounter() : myCounter (1) {}
    void Inc() { myCounter++; }
    void Dec() { myCounter--; }

    __ORI_PRIMITIVE_PROPERTY (int, Counter);

};

}}

#endif