%module Codec

%include <Base_Headers.i>
%import  <Base_ByteArray.i>
%import  <Base_PixelFormat.i>
%import  <Base_Frame.i>

%include <Codec_CodecType.i>
%include <Codec_CodecMode.i>
%include <Codec_EncoderParameters.i>
%include <Codec_MuxerParameters.i>
%include <Codec_Fragment.i>
%include <Codec_Packet.i>
%include <Codec_Encoder.i>
%include <Codec_Muxer.i>
%include <Codec_DecoderParameters.i>
%include <Codec_ScalerParameters.i>
%include <Codec_Scaler.i>
%include <Codec_Decoder.i>