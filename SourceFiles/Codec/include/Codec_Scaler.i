%feature("nspace") Ori::Codec::Scaler;

%{
#include <Ori/Codec_Scaler.hxx>
%}

%ignore Ori::Codec::Scaler::OnFrame;
%ignore Ori::Codec::Scaler::SetOnFrame;
%ignore Ori::Codec::Scaler::ConvertPixelFormat (const Frame& theFrame, bool theUseThread);

%include <Ori/Codec_Scaler.hxx>