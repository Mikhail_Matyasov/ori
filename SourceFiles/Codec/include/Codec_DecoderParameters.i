%feature("nspace") Ori::Codec::DecoderParameters;

%{
#include <Ori/Codec_DecoderParameters.hxx>
%}

%include <Ori/Codec_DecoderParameters.hxx>