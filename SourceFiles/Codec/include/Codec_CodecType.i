%feature("nspace") Ori::Codec::CodecType;

%{
#include <Ori/Codec_CodecType.hxx>
%}

%include <Ori/Codec_CodecType.hxx>