%feature("nspace") Ori::Codec::MuxerParameters;

%{
#include <Ori/Codec_MuxerParameters.hxx>
%}

%include <Ori/Codec_MuxerParameters.hxx>