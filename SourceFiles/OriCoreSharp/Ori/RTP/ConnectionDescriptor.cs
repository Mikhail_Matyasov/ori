﻿namespace Ori.RTP
{
    public class ConnectionDescriptor
    {
        public string Host { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }
        public string CodecType { get; set; }
        public int PacketWidth { get; set; }
        public int PacketHeight { get; set; }
    }
}
