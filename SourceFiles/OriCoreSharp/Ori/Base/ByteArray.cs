﻿
namespace Ori.Base
{
    public partial class ByteArray : ByteArrayView
    {

        public ByteArray (byte[] theData) :this()
        {
            Push (theData, theData.Length);
        }

        public byte[] Data()
        {
            int aLenght = Lenght();
            byte[] aData = new byte[aLenght];
            GetData (aData);

            return aData;
        }
    }
}
