﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Ori.Base
{
    public class DllHelper : IDisposable
    {

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr LoadLibrary (string libname);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern bool FreeLibrary (IntPtr hModule);

        private static Dictionary <string, IntPtr> myHandles = new Dictionary <string, IntPtr>();

        public static void AddDllDirrectory (string theDirName)
        {
            var aPath = Environment.GetEnvironmentVariable ("PATH");
            Environment.SetEnvironmentVariable ("PATH", $@"{aPath};{theDirName}");
        }

        public static bool Load (string thePath)
        {
            IntPtr aHandle = LoadLibrary (thePath);
            if (aHandle == IntPtr.Zero) {
                 return false;
            }

            myHandles.Add (thePath, aHandle);
            return true;
        }

        public static void Unload (string thePath)
        {
             var aHandle = myHandles.GetValueOrDefault (thePath);
             if (aHandle != null) {
                 FreeLibrary (aHandle);
             }
        }

        public static void UnloadAll()
        {
             foreach (var aPair in myHandles) {
                FreeLibrary (aPair.Value);
             }
        }

        public void Dispose()
        {
            UnloadAll();
        }
    }
}
