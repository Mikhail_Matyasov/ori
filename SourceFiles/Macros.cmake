
macro (AddGroups TARGET_PROJECT)

set (SRC_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/src)
set (INCLUDE_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/include/Ori)

foreach (GROUP ${GROUPS})
    
    file (GLOB GROUP_SRC src/${GROUP}/*.cxx)
    list(LENGTH GROUP_SRC GROUP_SRC_LENGHT)
    if (NOT ${GROUP_SRC_LENGHT} EQUAL 0)
        source_group (TREE ${SRC_ROOT} PREFIX "Source Files" FILES ${GROUP_SRC})
        list (APPEND SRC ${GROUP_SRC})
    endif()

    file (GLOB GROUP_INCLUDE include/Ori/${GROUP}_*.hxx)
    list(LENGTH GROUP_INCLUDE GROUP_INCLUDE_LENGHT)
    if (NOT ${GROUP_INCLUDE_LENGHT} EQUAL 0)
        source_group (TREE ${INCLUDE_ROOT} PREFIX "Header Files/${GROUP}" FILES ${GROUP_INCLUDE})
        list (APPEND INCLUDE ${GROUP_INCLUDE})
    endif()
    
    if (NOT USER_PCH_FILE_INCLUDE_PATH)
        include_directories (${ROOT_DIRRECTORY}/${ORI_CORE})
        if (${TARGET_PROJECT} STREQUAL ${ORI_CORE})
            list (APPEND INCLUDE ${ROOT_DIRRECTORY}/${ORI_CORE}/Ori/Base_Pch.hxx)
            source_group (TREE ${INCLUDE_ROOT} PREFIX "Header Files/${GROUP}" FILES ${ORI_CORE}/Ori/Base_Pch.hxx)
        endif()
    else()
        include_directories (${ROOT_DIRRECTORY}/${TARGET_PROJECT}/include)
        source_group (TREE ${INCLUDE_ROOT} PREFIX "Header Files/${GROUP}" FILES ${TARGET_PROJECT}/include/Base_Pch.hxx)
        list (APPEND INCLUDE ${ROOT_DIRRECTORY}/${TARGET_PROJECT}/include/Ori/Base_Pch.hxx)
    endif()
    unset (USER_PCH_FILE_INCLUDE_PATH)
    
endforeach()

endmacro()

macro (SetOutputDirs)
    set (DIR ${PROJECT_LIBRARY_DIR})
    if (${MODE} STREQUAL "debug")
        set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${DIR})
        set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${DIR})
        set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${DIR})
    else()
        set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${DIR})
        set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${DIR})
        set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${DIR})
    endif()
endmacro()

macro (AddResources TARGET_PROJECT)
    
    set (RESOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/resources)
    set (GENERATED_RESOURCE_DIR "${LIB_DIR_PREFIX}/${TARGET_PROJECT}/Resource Files")
    
    file (GLOB QRC ${RESOURCE_DIR}/*.qrc)
    file (GLOB QML ${RESOURCE_DIR}/qml/*.qml)

    foreach (RSRC ${QRC} ${QML})
        list (APPEND RESOURCES ${RSRC})
    endforeach()
    
    foreach (RSRC ${QRC})
        qt5_add_resources (QT5_RESOURCES ${RSRC})
    endforeach()
    
    source_group ("Generated Files" ${QT5_RESOURCES}) # Should be added separately
    source_group (TREE ${RESOURCE_DIR} PREFIX "Resource Files" FILES ${RESOURCES})    
    list (APPEND LIB_SRC ${RESOURCES} ${QT5_RESOURCES})
    list (APPEND SRC ${LIB_SRC})
    
endmacro()

macro (AddSwig TARGET_PROJECT)

if (${COMPILER} STREQUAL "vc142")
    include_external_msproject (${TARGET_PROJECT}Sharp ${ROOT_DIRRECTORY}/${TARGET_PROJECT}Sharp/${TARGET_PROJECT}Sharp.csproj)

    set (INCLUDE_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/include)  

    foreach (INC ${INCLUDES})
        list (APPEND SWIG_INCLUDE "-I${ROOT_DIRRECTORY}/${INC}/include")
    endforeach()

    set (SWIG_WRAPPER ${CMAKE_CURRENT_SOURCE_DIR}/src/SWIG_Wrapper.cxx)                           
        
    foreach (GROUP ${GROUPS})
        
        file (GLOB SWIG_FILES include/${GROUP}_*.i)
        list(LENGTH SWIG_FILES SWIG_FILES_LENGHT)
       
        if (NOT ${SWIG_FILES_LENGHT} EQUAL 0)
            source_group (TREE ${INCLUDE_ROOT} PREFIX "SWIG Files/${GROUP}" FILES ${SWIG_FILES})
            source_group (TREE ${INCLUDE_ROOT} PREFIX "SWIG Files" FILES include/Main.i)
            list (APPEND INCLUDE ${SWIG_FILES} ${CMAKE_CURRENT_SOURCE_DIR}/include/Main.i ${SWIG_WRAPPER})
        endif()
        
    endforeach()

    set (OUTPUT_DIR ${ROOT_DIRRECTORY}/${TARGET_PROJECT}Sharp/SWIG)
    file(MAKE_DIRECTORY ${OUTPUT_DIR})

    set (SWIG_COMMAND ${SWIG} -c++ -csharp
                      -outdir ${OUTPUT_DIR}
                      -o ${SWIG_WRAPPER}
                      ${SWIG_INCLUDE}
                      ${ROOT_DIRRECTORY}/${TARGET_PROJECT}/include/Main.i)

    add_custom_command (OUTPUT ${SWIG_WRAPPER}
                        COMMAND ${SWIG_COMMAND}
                        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/include/Main.i
                        COMMENT "SWIG is perfomed.")
endif()    
endmacro()

macro (AddSharpTestLib TARGET_PROJECT)

    if (${COMPILER} STREQUAL "vc142")
        include_external_msproject (${TARGET_PROJECT}SharpTestLib ${ROOT_DIRRECTORY}/${TARGET_PROJECT}SharpTestLib/${TARGET_PROJECT}SharpTestLib.csproj)
    endif()
    
endmacro()

macro (AddAndroidSources TARGET_PROJECT)
    
    set (ORI_ANDROID_PACKAGE_SOURCE_DIR ${ROOT_DIRRECTORY}/${TARGET_PROJECT}/android_sources)
    
    file (GLOB_RECURSE JAVA_SRC ${ORI_ANDROID_PACKAGE_SOURCE_DIR}/*.java)
    set (ORI_ANDROID_MANIFEST ${ORI_ANDROID_PACKAGE_SOURCE_DIR}/AndroidManifest.xml)
    
    list(LENGTH JAVA_SRC JAVA_SRC_LENGHT)
    if (NOT ${JAVA_SRC_LENGHT} EQUAL 0)
        source_group (TREE ${ORI_ANDROID_PACKAGE_SOURCE_DIR} PREFIX "Source Files" FILES ${JAVA_SRC} ${ORI_ANDROID_MANIFEST})
        list (APPEND SRC ${JAVA_SRC} ${ORI_ANDROID_MANIFEST})
    endif()
    
endmacro()