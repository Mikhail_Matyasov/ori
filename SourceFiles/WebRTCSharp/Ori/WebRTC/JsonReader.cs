﻿using System.Text.Json;

namespace Ori.WebRTC
{

class IceCandidateSchema
{
    public string candidate { get; set; }
    public string sdpMid { get; set; }
    public int sdpMLineIndex { get; set; }
}

class SessionDecriptionSchema
{
    public string sdp { get; set; }
    public string type { get; set; }
}
public class JsonReader
{
    public static SessionDescription ParseSdp (Base.String theSource)
    {
        SessionDescription aRes = new SessionDescription();
        var aDesc = JsonSerializer.Deserialize <SessionDecriptionSchema> (theSource.Data());
        aRes.SetSdp (new Base.String (aDesc.sdp));

        if (aDesc.type == "offer") {
            aRes.SetType (SessionDescription.DescriptionType.OFFER);
        } else {
            aRes.SetType (SessionDescription.DescriptionType.ANSWER);
        }

        return aRes;
    }

    public static IceCandidate ParseCandidate (Base.String theSource)
    {
        var aCandidate = JsonSerializer.Deserialize <IceCandidateSchema> (theSource.Data());

        IceCandidate aRes = new IceCandidate();
        aRes.SetCandidate (new Base.String (aCandidate.candidate));
        aRes.SetSdpMid (new Base.String (aCandidate.sdpMid));
        aRes.SetSdpMlineIndex (aCandidate.sdpMLineIndex);

        return aRes;
    }
}
}
