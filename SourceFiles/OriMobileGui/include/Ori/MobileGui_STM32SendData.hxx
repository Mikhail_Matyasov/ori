#ifndef _Ori_MobileGui_STM32SendData_HeaderFile
#define _Ori_MobileGui_STM32SendData_HeaderFile

#include <QtWidgets/qpushbutton.h>

namespace Ori {
namespace MobileGui {

class STM32SendData : public QPushButton
{
    Q_OBJECT

public slots:
    void onClicked();
};

}}

#endif
