#ifndef _Ori_MobileGui_Application_HeaderFile
#define _Ori_MobileGui_Application_HeaderFile

#include <vector>
#include <memory>

class QQmlApplicationEngine;
class QObject;
class QString;
class QQmlContext;

namespace Ori {
namespace Base {
class Logger;
class ILogger;
}
namespace MobileEmbed {
class Device;
}
namespace MobileGui {

class Application
{

public:
    void Load (QQmlApplicationEngine& theEngine);

private:
    template <typename T>
    std::shared_ptr <T> SetContextProperty (QQmlContext& theContext, const QString& thePropertyName);

private:
    std::shared_ptr <Base::ILogger> myILogger;
    std::vector <std::shared_ptr <QObject>> myComponents;

};

}}


#endif
