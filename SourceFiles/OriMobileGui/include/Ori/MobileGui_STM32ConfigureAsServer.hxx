#ifndef _Ori_MobileGui_STM32ConfigureAsServer_HeaderFile
#define _Ori_MobileGui_STM32ConfigureAsServer_HeaderFile

#include <memory>
#include <QtWidgets/qpushbutton.h>

namespace Ori {
namespace MobileGui {

class STM32ConfigureAsServer : public QPushButton
{
    Q_OBJECT

public slots:
    void onClicked();
};

}}

#endif
