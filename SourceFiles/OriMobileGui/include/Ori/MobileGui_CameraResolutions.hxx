#ifndef _Ori_MobileGui_CameraResolutions_HeaderFile
#define _Ori_MobileGui_CameraResolutions_HeaderFile

#include <Ori/MobileLib_CameraParameters.hxx>

#include <QtCore/qobject.h>
#include <QtCore/qsize.h>
#include <QtCore/qrect.h>

namespace Ori {
namespace MobileGui {
using namespace Ori::MobileLib;

class CameraResolutions : public QObject
{
    Q_OBJECT
signals:
    void updateResolution (int theWidth, int theHeight);

public slots:
    void activated (int theIndex);
    void appendItem (int theWidth, int theHeight);

public:
    void Load (int theCameraIndex);

    CameraParameters myCameraParameters;
};

}}


#endif