#ifndef _Ori_MobileGui_StreamingButton_HeaderFile
#define _Ori_MobileGui_StreamingButton_HeaderFile

#include <QtCore/qobject.h>
#include <QtCore/qsize.h>

namespace Ori {
namespace MobileLib {
class StreamingManager;
}

namespace MobileGui {

class StreamingButton : public QObject
{
    Q_OBJECT

signals:
    void managerReady (const QString& theKey);
    void stopStreaming();

public slots:
    void onClick (int theWidth, int theHeight);

public:
    StreamingButton();

private:
    bool myIsClicked;
    std::shared_ptr <MobileLib::StreamingManager> myManager;

};

}}


#endif
