#ifndef _Ori_MobileGui_STM32ConfigureAsClient_HeaderFile
#define _Ori_MobileGui_STM32ConfigureAsClient_HeaderFile

#include <memory>
#include <QtWidgets/qpushbutton.h>

namespace Ori {
namespace MobileGui {

class STM32ConfigureAsClient : public QPushButton
{
    Q_OBJECT

public slots:
    void onClicked();
};

}}

#endif
