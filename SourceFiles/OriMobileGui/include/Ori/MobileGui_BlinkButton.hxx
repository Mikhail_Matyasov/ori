#ifndef _Ori_MobileGui_BlinkButton_HeaderFile
#define _Ori_MobileGui_BlinkButton_HeaderFile

#include <QtWidgets/qpushbutton.h>
#include <memory>

namespace Ori {
namespace MobileGui {

class BlinkButton : public QPushButton
{
    Q_OBJECT

public slots:
    void onClicked();
};

}}

#endif
