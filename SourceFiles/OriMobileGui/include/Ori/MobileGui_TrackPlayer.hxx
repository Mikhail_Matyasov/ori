#ifndef _Ori_MobileGui_TrackPlayer_HeaderFile
#define _Ori_MobileGui_TrackPlayer_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <QtQuick/qquickimageprovider.h>

namespace Ori {
namespace MobileGui {

class TrackPlayer : public QQuickImageProvider
{
public:
    ORI_EXPORT TrackPlayer();
    ORI_EXPORT QPixmap requestPixmap (const QString& theID, QSize* theSize, const QSize& theRequestedSize) override;
    
};

}}

#endif
