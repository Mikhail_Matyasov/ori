#ifndef _Ori_MobileGui_STM32Connect_HeaderFile
#define _Ori_MobileGui_STM32Connect_HeaderFile

#include <QtWidgets/qpushbutton.h>

namespace Ori {
namespace MobileGui {

class STM32Connect : public QPushButton
{
    Q_OBJECT

public slots:
    void onClicked();
};

}}

#endif
