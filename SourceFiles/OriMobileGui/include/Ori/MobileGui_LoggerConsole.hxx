#ifndef _Ori_MobileGui_LoggerConsole_HeaderFile
#define _Ori_MobileGui_LoggerConsole_HeaderFile

#include <memory>
#include <QtCore/qobject.h>

class QString;

namespace Ori {
namespace MobileGui {

class LoggerConsole : public QObject
{
    Q_OBJECT

public:
    void Print (const QString& theMessage) const;

signals:
    void printMessage (const QString& theMessage) const;
};

}}

#endif
