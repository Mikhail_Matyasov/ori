#ifndef _Ori_MobileGui_VideoFilter_HeaderFile
#define _Ori_MobileGui_VideoFilter_HeaderFile

#include <Ori/Base_Macros.hxx>

#include <QtMultimedia/qabstractvideofilter.h>

namespace Ori {
namespace MobileLib {
class StreamingManager;
}

namespace MobileGui {

class VideoFilter : public QAbstractVideoFilter
{
    Q_OBJECT

public slots:
    void onStreamingManagerReady (const QString& theKey);

public:

    __ORI_PRIMITIVE_PROPERTY (MobileLib::StreamingManager*, Manager)
    VideoFilter (QObject* theParent = nullptr);
    QVideoFilterRunnable* createFilterRunnable() override;
};

class VideoFilterRunnable : public QVideoFilterRunnable
{
public:
    VideoFilterRunnable (VideoFilter* theFilter);
    QVideoFrame run (QVideoFrame *theInputFrame,
                     const QVideoSurfaceFormat& theSurfaceFormat,
                     RunFlags theFlags) override;

protected:
    VideoFilter* myFilter;
    int myCounter = 0;

};

}}


#endif
