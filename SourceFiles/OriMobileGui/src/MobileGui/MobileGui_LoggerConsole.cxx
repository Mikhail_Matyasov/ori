#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_LoggerConsole.hxx>


namespace Ori {
namespace MobileGui {

void LoggerConsole::Print (const QString& theMessage) const
{
    emit printMessage (theMessage);
}

}}
