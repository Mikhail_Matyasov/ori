#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_STM32SendData.hxx>

#include <Ori/MobileEmbed_EmbedCommand.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <Ori/MobileEmbed_Device.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_Timer.hxx>

using namespace Ori::MobileEmbed;

namespace Ori {
namespace MobileGui {
namespace {
int theCounter = 0;
void SendCommand()
{
    std::thread T ([&]() -> void {

        /*aCmd.OnCompleted() = [] (const Base::ByteArrayView& theReply, int theID) -> void {
            auto& aLogger = *Device::Instance()->Logger();
            ORI_LOG_DEBUG (aLogger) << "SendData completed. ID = " << theID;
            SendCommand();
        };*/

        while (true) {

            if (theCounter >= 10) {
                theCounter = 0;
            }

            Base::Timer::Sleep (2000000);
            EmbedCommand aCmd;
            aCmd.Command() = Embed::CommandType::UdpClientSendPacket;
            Base::String aMessage = theCounter++;
            aCmd.Data().SetBegin (reinterpret_cast <const uint8_t*> (aMessage.Data()));
            aCmd.Data().SetEnd (reinterpret_cast <const uint8_t*> (aMessage.Data() + aMessage.Length()));

            const auto aDevice = Device::Instance();
            aDevice->Send (aCmd);
        }
    });

    T.detach();
}
}
void STM32SendData::onClicked()
{
    const auto aDevice = Device::Instance();
    while (!aDevice->IsSocketReady()) {
        auto& aLogger = *aDevice->Logger();
        ORI_LOG_DEBUG (aLogger) << "Connecting...";
        aDevice->ConnectSocket();
    }
    
    SendCommand();

}

}}
