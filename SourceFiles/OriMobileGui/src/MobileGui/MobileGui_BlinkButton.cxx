#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_BlinkButton.hxx>

#include <Ori/MobileEmbed_EmbedCommand.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <Ori/MobileEmbed_Device.hxx>
#include <Ori/Base_Logger.hxx>

using namespace Ori::MobileEmbed;
using namespace Ori::Embed;

namespace Ori {
namespace MobileGui {

void BlinkButton::onClicked()
{
    const auto aDevice = Device::Instance();
    while (!aDevice->IsSocketReady()) {
        aDevice->ConnectSocket();
    }
    
    EmbedCommand aCmd;
    aCmd.Command() = CommandType::Blink;
    aCmd.OnCompleted() = [&] (const Base::ByteArrayView& theReply, int theID) -> void {
        auto& aLogger = *aDevice->Logger();
        ORI_LOG_DEBUG (aLogger) << "BLINK completed. ID = " << theID;
    };

    aDevice->Send (aCmd);
}

}}
