#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_STM32Connect.hxx>

#include <Ori/MobileEmbed_Device.hxx>
#include <Ori/Base_Logger.hxx>

namespace Ori {
namespace MobileGui {

void STM32Connect::onClicked()
{
    MobileEmbed::Device::OpenWiFiSettings();
}

}}
