#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_StreamingButton.hxx>

#include <Ori/Base_String.hxx>
#include <Ori/MobileLib_MessageMap.hxx>
#include <Ori/MobileLib_StreamingManager.hxx>
#include <Ori/MobileLib_StreamingManagerParameters.hxx>

#include <QtGui/qguiapplication.h>
#include <QtGui/qscreen.h>


namespace Ori {
namespace MobileGui {
using namespace Ori::MobileLib;

void StreamingButton::onClick (int theWidth, int theHeight)
{
    if (!myIsClicked) {
        
        if (!myManager) {
            StreamingManagerParameters aParams;
            aParams.Width() = theWidth;
            aParams.Height() = theHeight;

            myManager = std::make_shared <StreamingManager> (aParams);

            Base::String aKey = "Streaming Manager";
            MessageMap::Instance()->Insert (aKey, myManager);
            emit managerReady (aKey.Data());
        }

    } else {
        emit stopStreaming();
    }

    myIsClicked = !myIsClicked;
}

StreamingButton::StreamingButton() :
    myIsClicked (false)
{}

}}
