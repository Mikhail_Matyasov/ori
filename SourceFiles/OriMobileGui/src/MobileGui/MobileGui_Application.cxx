#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_Application.hxx>

#include <Ori/MobileGui_VideoFilter.hxx>
#include <Ori/MobileGui_CameraResolutions.hxx>
#include <Ori/MobileGui_StreamingButton.hxx>
#include <Ori/MobileGui_BlinkButton.hxx>
#include <Ori/MobileGui_STM32SendData.hxx>
#include <Ori/MobileGui_LoggerConsole.hxx>
#include <Ori/MobileGui_STM32ConfigureAsServer.hxx>
#include <Ori/MobileGui_STM32ConfigureAsClient.hxx>
#include <Ori/MobileGui_TrackPlayer.hxx>
#include <Ori/MobileEmbed_Device.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/MobileGui_STM32Connect.hxx>
#include <Ori/Base_ILogger.hxx>
#include <Ori/Base_Logger.hxx>

#ifdef __ANDROID__
#include <Ori/MobileAndroid_PermissionManager.hxx>
using namespace Ori::MobileAndroid;
#endif

#include <QtQml/qqmlapplicationengine.h>
#include <QtQml/qqmlcontext.h>

extern int qInitResources_MobileGui_QML();
using namespace Ori::Base;
using namespace Ori::MobileEmbed;

namespace Ori {
namespace MobileGui {
namespace {

class IConsoleLogger : public Base::ILogger
{
public:
    IConsoleLogger (const LoggerConsole& theConsoleLogger) :
        myConsoleLogger (theConsoleLogger)
    {}

    void Print (const String& theMessage) const override
    {
        QString aMessage = theMessage.Data();
        aMessage += '\n';
        myConsoleLogger.Print (aMessage);     
    };

private:
    const LoggerConsole& myConsoleLogger;
};

template <typename T>
T* FindComponent (const QObject& theRoot, const QString& theObjectName)
{
    auto anObj = theRoot.findChild <QObject*> (theObjectName);
    auto aComponent = qobject_cast <T*> (anObj);
    return aComponent;
}

void RegisterQMLTypes (QQmlApplicationEngine& theEngine)
{
    qInitResources_MobileGui_QML();

    qmlRegisterType <VideoFilter> ("OriMobileGui", 1, 0, "MobileGui_VideoFilter");

    auto aUrl = QUrl (QLatin1String ("qrc:/qml/MobileGui_MainPage.qml"));
    theEngine.load (aUrl);
}

}

void Application::Load (QQmlApplicationEngine& theEngine)
{
    auto aRootContext = theEngine.rootContext();

    auto aCamRes = SetContextProperty <CameraResolutions> (*aRootContext, "cameraResolutionsConnection");
    SetContextProperty <StreamingButton> (*aRootContext, "streamingButtonBackend");
    SetContextProperty <BlinkButton> (*aRootContext, "stm32BlinkConnection");
    SetContextProperty <STM32SendData> (*aRootContext, "stm32SendDataConnection");
    SetContextProperty <STM32Connect> (*aRootContext, "stm32ConnectConnection");
    SetContextProperty <STM32ConfigureAsServer> (*aRootContext, "stm32ConfigureAsServerConnection");
    SetContextProperty <STM32ConfigureAsClient> (*aRootContext, "stm32ConfigureAsClientConnection");
    auto aLoggerConsole = SetContextProperty <LoggerConsole> (*aRootContext, "loggerConsoleConnection");

    theEngine.addImageProvider (QLatin1String ("MobileGui_TrackPlayer"), new TrackPlayer);

    myILogger = std::make_shared <IConsoleLogger> (*aLoggerConsole);
    auto aLogger = std::make_shared <Logger> (myILogger.get());
    Device::Instance()->Logger() = aLogger;

    RegisterQMLTypes (theEngine);
    aCamRes->Load (0);

#ifdef __ANDROID__
    PermissionManager::Request();
#endif


}

template <typename T>
std::shared_ptr <T> Application::SetContextProperty (QQmlContext& theContext, const QString& thePropertyName)
{
    auto aBackend = std::make_shared <T>();
    theContext.setContextProperty (thePropertyName, aBackend.get());
    myComponents.push_back (aBackend);

    return aBackend;
}

}}
