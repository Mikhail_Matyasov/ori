#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_STM32ConfigureAsServer.hxx>

#include <Ori/MobileEmbed_Device.hxx>
#include <Ori/MobileEmbed_EmbedCommand.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <Ori/Base_ByteArray.hxx>

using namespace Ori::MobileEmbed;
using namespace Ori::Embed;
using namespace Ori::Base;

namespace Ori {
namespace MobileGui {

void STM32ConfigureAsServer::onClicked()
{
    const auto aDevice = Device::Instance();
    auto& aLogger = *aDevice->Logger();
    while (!aDevice->IsSocketReady()) {
        ORI_LOG_DEBUG (aLogger) << "Connecting...";
        aDevice->ConnectSocket();
    }

    EmbedCommand aCommand;
    aCommand.Command() = CommandType::ConfigureAsServer;
    aCommand.OnCompleted() = [&] (const ByteArrayView& theData, uint8_t theID) -> void {
        ORI_LOG_DEBUG (aLogger) << "Device configured as server";
    };

    aDevice->Send (aCommand);
}

}}
