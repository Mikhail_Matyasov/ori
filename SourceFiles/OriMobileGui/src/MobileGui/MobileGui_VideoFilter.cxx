#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_VideoFilter.hxx>

#include <Ori/MobileLib_StreamingManager.hxx>
#include <Ori/MobileLib_MessageMap.hxx>

#include <QtCore/qdebug.h>

namespace Ori {
namespace MobileGui {
using namespace Ori::MobileLib;

namespace {

}

VideoFilter::VideoFilter (QObject* theParent) :
    QAbstractVideoFilter (theParent),
    myManager (nullptr)
{}

void VideoFilter::onStreamingManagerReady (const QString& theKey)
{
    Base::String aKey (theKey.toStdString().c_str());
    myManager = MessageMap::Instance()->Find <StreamingManager> (aKey);

    if (myManager) {
        qDebug() << "Streaming manager was initialized";
    } else {
        qDebug() << "Can not find Streaming manager in MessageMap, by key: " << aKey.Data();
    }
    MessageMap::Instance()->Remove (aKey);
}

VideoFilterRunnable::VideoFilterRunnable (VideoFilter* theFilter) :
    QVideoFilterRunnable(),
    myFilter (theFilter)
{}

QVideoFrame VideoFilterRunnable::run (QVideoFrame* theInputFrame,
                                      const QVideoSurfaceFormat& theSurfaceFormat,
                                      RunFlags theFlags)
{
    if (myFilter->Manager()) {

        myFilter->Manager()->ProcessFrame (theInputFrame);
    }

    return *theInputFrame;
}

QVideoFilterRunnable* VideoFilter::createFilterRunnable()
{ return new VideoFilterRunnable (this); }

}}
