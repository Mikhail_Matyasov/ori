#include <Ori/MobileGuiBase_Pch.hxx>
#include <Ori/MobileGui_CameraResolutions.hxx>

#include <Ori/Base_String.hxx>

#include <QtMultimedia/qcameraimagecapture.h>
#include <QtMultimedia/qcamerainfo.h>

namespace Ori {
namespace MobileGui {
void CameraResolutions::Load (int theCameraIndex)
{
    auto aCameraInfo = QCameraInfo::availableCameras()[theCameraIndex];
    QCamera aCamera (aCameraInfo);
    myCameraParameters.Position() = aCameraInfo.position();
}

void CameraResolutions::appendItem (int theWidth, int theHeight)
{
    auto aSize = QSize (theWidth, theHeight);
    myCameraParameters.ResolutionVector().append (aSize);
}

void CameraResolutions::activated (int theIndex)
{
    auto& aResolutionVector = myCameraParameters.ResolutionVector();
    const auto& aNewSize = aResolutionVector[theIndex];
    emit updateResolution (aNewSize.width(), aNewSize.height());
}

}}
