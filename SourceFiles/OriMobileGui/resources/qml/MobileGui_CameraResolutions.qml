import QtQuick 2.12
import QtQuick.Controls 2.1

Rectangle {

    id: cameraResolutions

    ComboBox {
        id: comboBox
        objectName: "selectResolution"
        width: 200
        height: 200
        anchors.fill: parent
        currentIndex: listModel.count - 1

        model: ListModel {
            id: listModel
        }

        onActivated: {
            cameraResolutionsConnection.activated (currentIndex);
        }
    }

    function appendItem (theWidth, theHeight) {
        
        var anItemText = theWidth + " x " + theHeight;
        listModel.append ({ "name" : anItemText});
        cameraResolutionsConnection.appendItem (theWidth, theHeight);
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.009999999776482582}
}
##^##*/
