import QtQuick 2.12
import QtQuick.Controls 2.10
import QtMultimedia 5.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import OriMobileGui 1.0
import QtQuick.Controls.Styles 1.4

ApplicationWindow {
    id: window

    visible: true
    width: 640
    height: 480

    VideoOutput {

        width: parent.width
        height: parent.height
        autoOrientation: true
        source: camera
        filters: [ videoFilter ]
    }

    Camera {
        id: camera

        onCameraStatusChanged: {
            if (camera.cameraStatus == Camera.LoadedStatus &&
                !cameraResolutions.isItemsSet) {

                var aResolut = camera.supportedViewfinderResolutions();
                for (var i in aResolut) {
                    cameraResolutions.appendItem (aResolut[i].width, aResolut[i].height);
                }

                cameraResolutions.isItemsSet = true;
            }
        }
    }

    Button {
        id: stm32Blink
        x: 59
        y: 16
        text: "Blink"

        onClicked: function() {
            stm32BlinkConnection.onClicked();
        }

        Connections {
            target: stm32BlinkConnection
        }
    }

    Button {
        id: stm32ConfigureAsClient
        x: 59
        y: 62
        text: "Configure As Client"

        onClicked: function() {
            stm32ConfigureAsClientConnection.onClicked();
        }

        Connections {
            target: stm32ConfigureAsClient
        }
    }

    Button {
        id: stm32ConfigureAsServer
        x: 57
        y: 108
        text: "Configure As Server"

        onClicked: function() {
            stm32ConfigureAsServerConnection.onClicked();
        }

        Connections {
            target: stm32ConfigureAsServer
        }
    }

    Button {
        id: stm32SendData
        x: 57
        y: 154
        text: "SendData"

        onClicked: function() {
            stm32SendDataConnection.onClicked();
        }

        Connections {
            target: stm32SendDataConnection
        }
    }

    Button {
        id: stm32Connect
        x: 57
        y: 200
        text: "Connect"

        onClicked: function() {
            stm32ConnectConnection.onClicked();
        }

        Connections {
            target: stm32ConnectConnection
        }
    }

    Image {
        id: mobileGui_TrackPlayer
        x: 504
        y: 24
        width: 100
        height: 100
        source: "image://MobileGui_TrackPlayer/DefaultID"
    }

    ScrollView {
        id: scrollView
        x: parent.width / 4
        y: parent.height / 2.6;
        width: parent.width / 1.4
        height: parent.height / 2.1

        TextArea {
            id: loggerConsole
            color: "black"
            wrapMode: TextArea.WrapAnywhere
            background: Rectangle { color: "white" }

            Connections {
                target: loggerConsoleConnection
                onPrintMessage: {
                    loggerConsole.text = theMessage;
                    loggerConsole.cursorPosition = loggerConsole.text.length;
                }
            }
        }
    }

    MobileGui_StreamingButton {

        function getCameraResolution()
        {
            return camera.viewfinder.resolution;
        }

        function onStreamingManagerReady (theKey)
        {
            videoFilter.onStreamingManagerReady (theKey);
        }
    }

    MobileGui_VideoFilter {
        id: videoFilter
        objectName: "videoFilter"
    }

    MobileGui_CameraResolutions {
        id: cameraResolutions
        property bool isItemsSet: false

        x: (parent.width - cameraResolutions.width) / 2
        y: parent.height - cameraResolutions.height
        width: parent.width / 2
        height: parent.width / 10

        Connections {
            target: cameraResolutionsConnection

            onUpdateResolution: {
                camera.stop();
                camera.viewfinder.resolution = Qt.size (theWidth, theHeight);
                camera.start();
            }
        }
    }

}


/*##^##
Designer {
    D{i:0;formeditorZoom:0.5}
}
##^##*/
