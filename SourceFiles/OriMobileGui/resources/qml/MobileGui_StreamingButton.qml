import QtQuick 2.0
import QtQuick.Controls 2.1
import OriMobileGui 1.0

Button {
    id: streamingButton

    width: parent.width / 5
    height: parent.height / 10
    x: (parent.width - streamingButton.width) / 2
    text: "Streaming Button"

    onClicked: function() {
        var aResolution = streamingButton.getCameraResolution();
        streamingButtonBackend.onClick (aResolution.width, aResolution.height);
    }

    Connections {
        target: streamingButtonBackend

        onManagerReady: {
            streamingButton.onStreamingManagerReady (theKey);
        }
    }
 }
