#include <Ori/STM32TestLib_Pch.hxx>

#include <Ori/STM32Lib_ESP8266.hxx>
#include <Ori/STM32Lib_RingBuffer.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Allocator.hxx>
#include <Ori/STM32TestLib_Helper.hxx>
#include <Ori/STM32Lib_CommandManager.hxx>
#include <Ori/Base_Helper.hxx>
#include <Ori/STM32Lib_ESP8266Listener.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/STM32Lib_SX1278Packet.hxx>

using namespace Ori::STM32Lib;
using namespace Ori::Embed;

namespace Ori {
namespace STM32TestLib {

void Invoke (std::function <void()> theFunc)
{
    const int aCount = 2000;
    for (int i = 0; i < aCount; i++) {
        theFunc();
    }
}

TEST (STM32Lib_Allocator, AllocateDestroy)
{
    STM32Lib::Allocator::Reset();

    Invoke ([]() -> void {

        uint8_t* aBuffer = Base::Allocator::ConstructArray <uint8_t> (10);
        aBuffer[0] = 255;
        uint8_t* aBuffer2 = Base::Allocator::ConstructArray <uint8_t> (10);
        EXPECT_TRUE (aBuffer2 != aBuffer);
        Base::Allocator::DeallocateArray (aBuffer2);
        EXPECT_TRUE (aBuffer[0] == 255);
        Base::Allocator::DeallocateArray (aBuffer);

        EXPECT_FALSE (Base::Allocator::DeallocateArray (aBuffer));
    });
}

TEST (STM32Lib_Allocator, ReallocateMemoryBlockBuffers)
{
    STM32Lib::Allocator::Reset();

    Invoke ([]() -> void {

        const int aNumberOfBuffers = 9;
        uint8_t* aBuffers [aNumberOfBuffers];
        for (int i = 0; i < aNumberOfBuffers; i++) {
            aBuffers[i] = Base::Allocator::Allocate <uint8_t> (10);
        }
        EXPECT_TRUE (Helper::GetAllocatedBlocksLength() > 10);

        for (int i = 0; i < aNumberOfBuffers; i++) {
            EXPECT_TRUE (Base::Allocator::Deallocate (aBuffers[i]));
        }
        EXPECT_TRUE (Helper::GetFreeBlocksLength() > 10);
    });
}

TEST (STM32Lib_Allocator, DefragmentMemory)
{
    STM32Lib::Allocator::Reset();

    Invoke ([]() -> void {

        int aSize = STM32Lib::Allocator::HeapSize() - 1000;
        const int aNumberOfBlocks = 10;
        uint8_t* aBlocks [aNumberOfBlocks];
        for (int i = 0; i < aNumberOfBlocks; i++) {
            aBlocks[i] = nullptr;
            int aBlockSize = aSize / aNumberOfBlocks;
            aBlocks[i] = Base::Allocator::ConstructArray <uint8_t> (aBlockSize);
            EXPECT_TRUE (aBlocks[i]);
        }

        for (int i = 0; i < aNumberOfBlocks; i++) {
            bool aRes = Base::Allocator::Deallocate (aBlocks[i]);
            EXPECT_TRUE (aRes);
        }
    
        uint8_t* aData = Base::Allocator::ConstructArray <uint8_t> (aSize - 2000);
        EXPECT_TRUE (aData);
        Base::Allocator::Deallocate (aData);
    });
}

TEST (STM32Lib_RingBuffer, FillData)
{
    Invoke ([]() -> void {
        int aBufferSize = 10;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        for (int i = 0; i < aBufferSize; i++) {
            aBuffer.Push (i);
        }
        aBuffer.Push (1);

        EXPECT_TRUE (aBuffer.DataEnd() == 1);
        EXPECT_TRUE (aBuffer.Buffer()[0] = 1);
    });
}

TEST (STM32Lib_RingBuffer, FillDataThreeTime)
{
    Invoke ([]() -> void {
        int aBufferSize = 10;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        for (int i = 0; i < aBufferSize * 3; i++) {
            aBuffer.Push (i);
        }
        aBuffer.Push (1);

        EXPECT_TRUE (aBuffer.DataEnd() == 1);
        EXPECT_TRUE (aBuffer.Buffer()[0] = 1);
    });
}

TEST (STM32Lib_RingBuffer, FindSequence1)
{
    Invoke ([]() -> void {
        int aBufferSize = 6;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('O');
        aBuffer.Push ('K');
        aBuffer.Push ('A');

        EXPECT_TRUE (aBuffer.Find ("OK") == 3);
    });
}

TEST (STM32Lib_RingBuffer, FindSequence2)
{
    Invoke ([]() -> void {
        int aBufferSize = 6;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('K');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('O');

        EXPECT_TRUE (aBuffer.Find ("OK") == -1);
    });
}

TEST (STM32Lib_RingBuffer, FindSequence3)
{
    Invoke ([]() -> void {
        int aBufferSize = 6;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('O');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('L');

        EXPECT_TRUE (aBuffer.Find ("ALO") == -1);
    });
}

TEST (STM32Lib_RingBuffer, FindSequence4)
{
    Invoke ([]() -> void {
        int aBufferSize = 6;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');

        EXPECT_TRUE (aBuffer.Find ("ALO") == 4);
    });
}

TEST (STM32Lib_RingBuffer, FindSequence5)
{
    Invoke ([]() -> void {
        int aBufferSize = 6;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        EXPECT_TRUE (aBuffer.Find ("ALOHA") == 4);
    });
}

TEST (STM32Lib_RingBuffer, FindSequence6)
{
    Invoke ([]() -> void {
        int aBufferSize = 6;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        EXPECT_TRUE (aBuffer.Find ("ALOHA") == 0);
    });
}

TEST (STM32Lib_RingBuffer, FindSequence7)
{
    Invoke ([]() -> void {
        int aBufferSize = 5;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');
        aBuffer.Consume (4);
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');

        EXPECT_TRUE (aBuffer.Find ("ALO") == 0);
    });
}

TEST (STM32Lib_RingBuffer, IterateBuffer1)
{
    Invoke ([]() -> void {
        int aBufferSize = 6;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        RingBuffer::Iterator anIterator (aBuffer);
        anIterator.Next();
        anIterator.Next();
        anIterator.Next();
        uint8_t anH = *anIterator.Next();

        EXPECT_TRUE (anH == 'H');
    });
}

TEST (STM32Lib_RingBuffer, IterateBuffer2)
{
    Invoke ([]() -> void {
        int aBufferSize = 5;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        RingBuffer::Iterator anIterator (aBuffer);
        EXPECT_TRUE (*anIterator.Next() == 'A');
        EXPECT_TRUE (*anIterator.Next() == 'L');
        EXPECT_TRUE (*anIterator.Next() == 'O');
        EXPECT_TRUE (*anIterator.Next() == 'H');
        EXPECT_TRUE (*anIterator.Next() == 'A');
    });
}

TEST (STM32Lib_RingBuffer, IterateBuffer3)
{
    Invoke ([]() -> void {
        int aBufferSize = 5;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        RingBuffer::Iterator anIterator (aBuffer);
        EXPECT_TRUE (*anIterator.Next() == 'A');
        EXPECT_TRUE (*anIterator.Next() == 'L');
        EXPECT_TRUE (*anIterator.Next() == 'O');
        EXPECT_TRUE (*anIterator.Next() == 'H');
        EXPECT_TRUE (*anIterator.Next() == 'A');
        EXPECT_FALSE (anIterator.Next());
    });
}

TEST (STM32Lib_RingBuffer, IterateBuffer4)
{
    Invoke ([]() -> void {
        int aBufferSize = 5;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        RingBuffer::Iterator anIterator (aBuffer, 4);
        EXPECT_TRUE (*anIterator.Next() == 'O');
        EXPECT_TRUE (*anIterator.Next() == 'H');
        EXPECT_TRUE (*anIterator.Next() == 'A');
        EXPECT_FALSE (anIterator.Next());
    });
}

TEST (STM32Lib_RingBuffer, IterateBuffer5)
{
    Invoke ([]() -> void {
        int aBufferSize = 5;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        RingBuffer::Iterator anIterator (aBuffer, 0);
        EXPECT_TRUE (*anIterator.Next() == 'H');
        EXPECT_TRUE (*anIterator.Next() == 'A');
        EXPECT_FALSE (anIterator.Next());
    });
}

TEST (STM32Lib_RingBuffer, ConsumeData1)
{
    Invoke ([]() -> void {
        int aBufferSize = 5;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        aBuffer.Consume (3);
        RingBuffer::Iterator anIterator (aBuffer);
        EXPECT_TRUE (*anIterator.Next() == 'H');
        EXPECT_TRUE (*anIterator.Next() == 'A');
        EXPECT_FALSE (anIterator.Next());
        EXPECT_TRUE (aBuffer.DataLength() == 2);
    });
}

TEST (STM32Lib_RingBuffer, ConsumeData2)
{
    Invoke ([]() -> void {
        int aBufferSize = 5;
        RingBuffer aBuffer;
        aBuffer.Allocate (aBufferSize);
        aBuffer.Push ('A');
        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('H');
        aBuffer.Push ('A');

        aBuffer.Consume (3);

        aBuffer.Push ('L');
        aBuffer.Push ('O');
        aBuffer.Push ('P');

        RingBuffer::Iterator anIterator (aBuffer);
        EXPECT_TRUE (*anIterator.Next() == 'H');
        EXPECT_TRUE (*anIterator.Next() == 'A');
        EXPECT_TRUE (anIterator.Next());
        EXPECT_TRUE (aBuffer.DataLength() == 5);

        aBuffer.Consume (2);

        RingBuffer::Iterator anIterator1 (aBuffer);
        EXPECT_TRUE (*anIterator1.Next() == 'L');
        EXPECT_TRUE (*anIterator1.Next() == 'O');
        EXPECT_TRUE (*anIterator1.Next() == 'P');
        EXPECT_FALSE (anIterator1.Next());
        EXPECT_TRUE (aBuffer.DataLength() == 3);
    });
}

TEST (STM32Lib_RingBuffer, ConvertIndex)
{
    int aBufferSize = 5;
    RingBuffer aBuffer;
    aBuffer.Allocate (aBufferSize);

    EXPECT_TRUE (aBuffer.ToGlobalIndex (1) == -1);
    EXPECT_TRUE (aBuffer.ToLocalIndex (1) == -1);

    aBuffer.Push ('A');
    aBuffer.Push ('B');
    aBuffer.Push ('C');
    aBuffer.Push ('D');
    aBuffer.Push ('E');

    EXPECT_TRUE (aBuffer.ToGlobalIndex (1) == aBuffer.ToLocalIndex (1));

    aBuffer.Push ('V');
    aBuffer.Push ('V');

    EXPECT_TRUE (aBuffer.ToLocalIndex (2) == 0);
    EXPECT_TRUE (aBuffer.ToGlobalIndex (3) == 0);
}

class ESPTestObserver : public ESP8266Listener::Observer
{
public:
    void OnData (const uint8_t* theData, int theLength) override
    {
        myBuffer.Push (theData, theLength);
    };

    __ORI_PROPERTY (Base::ByteArray, Buffer);
};

TEST (STM32Lib_ESP8266DataObserver, TryFlushBuffer1)
{
    Invoke ([&]() -> void {
        ESPTestObserver aTestObserver;
        auto aListener = ESP8266Listener::Instance();

        aListener->RegisterObserver (&aTestObserver);
        ByteArray aBeginData = { 1, 2, 3, 4, 5, 6, 'A', '+', 'I', 4 };

        for (uint8_t aByte : aBeginData) {
            aListener->OnData (aByte);
        }

        aListener->ExtractInputCommand();
        EXPECT_TRUE (aTestObserver.Buffer().Empty());

        Base::String aData = "+IPD,0,4:1234";
        for (int i = 0; i < aData.Length(); i++) {
            aListener->OnData (aData[i]);
        }
        aListener->ExtractInputCommand();
        EXPECT_TRUE (aTestObserver.Buffer().Lenght() == 4);
    });
}

TEST (STM32Lib_ESP8266DataObserver, TryFlushBuffer2)
{
    Invoke ([]() -> void {
        ESPTestObserver aTestObserver;
        auto aListener = ESP8266Listener::Instance();
        aListener->RegisterObserver (&aTestObserver);
        std::string aData = "+IPD,0,5:1234";

        for (char aSymbol : aData) {
            aListener->OnData (aSymbol);
        }
        aListener->ExtractInputCommand();
        EXPECT_TRUE (aTestObserver.Buffer().Empty());

        aListener->OnData ('5');
        aListener->ExtractInputCommand();
        EXPECT_TRUE (aTestObserver.Buffer().Lenght() == 5);

        EXPECT_TRUE (aListener->Buffer().DataLength() == 0);
    });
}

TEST (STM32Lib_ESP8266DataObserver, TryFlushBuffer3)
{
    Invoke ([]() -> void {
        ESPTestObserver aTestObserver;
        auto aListener = ESP8266Listener::Instance();
        aListener->RegisterObserver (&aTestObserver);
        std::string aData = "+IPD,0,5:12345";

        for (char aSymbol : aData) {
            aListener->OnData (aSymbol);
        }
        aListener->ExtractInputCommand();
        EXPECT_TRUE (!aTestObserver.Buffer().Empty());

        for (char aSymbol : aData) {
            aListener->OnData (aSymbol);
        }
        aListener->ExtractInputCommand();
        EXPECT_TRUE (aTestObserver.Buffer().Lenght() == 10);
        EXPECT_TRUE (aListener->Buffer().DataLength() == 0);
    });
}

namespace {
void FillManager (CommandManager& theManager, ByteArray theCommand)
{
    theManager.OnData (theCommand.Data(), theCommand.Lenght());
}
}

TEST (STM32Lib_CommandManager, ProcessCmdCommand)
{    
    Invoke ([]() -> void {
        auto& aManager = *CommandManager::Instance();
        aManager.AllocateBuffer (500);

        ByteArray aCommandData;
        aCommandData.Push (8);

        TypeBuffer <uint16_t> aTBuffer (static_cast <uint16_t> (CommandType::SX1278ListenerOnVideoData));
        aCommandData.Push (aTBuffer.Bytes, 2);

        TypeBuffer <uint16_t> aTBuffer2 (1);
        aCommandData.Push (aTBuffer2.Bytes, 2);
        aCommandData.Push ('A');
        FillManager (aManager, aCommandData);
    
        ESP8266Packet aCmd;
        bool aRes = aManager.ExtractCommand (aCmd);
        EXPECT_TRUE (aRes);

        EXPECT_TRUE (aCmd.ID() == 8);
        EXPECT_TRUE (aCmd.Command() == CommandType::SX1278ListenerOnVideoData);
        const auto& aData = aCmd.Data();
        int aResIndex = Base::Helper::Find (aData.begin(), aData.end(), "A");
        EXPECT_TRUE (aResIndex != -1);

        const auto& aBuffer = Helper::CommandManagerBuffer (aManager);
        EXPECT_TRUE (aBuffer.DataLength() != 0);
        aManager.ConsumeLastCommand (aData.end());
        EXPECT_TRUE (aBuffer.DataLength() == 0);
    });
}

void CreatePacketTest (SX1278CommandType theType, 
                       uint8_t theDataLength,
                       uint8_t thePacketID,
                       uint8_t theSenderID,
                       uint8_t theTargetID,
                       const char* theData)
{
    SX1278Packet aPacket;
    aPacket.Header().Command() = theType;
    aPacket.Header().DataLength() = theDataLength;
    aPacket.Header().PacketID() = thePacketID;
    aPacket.Header().SenderID() = theSenderID;
    aPacket.Header().TargetID() = theTargetID;

    ByteArray aData = aPacket.Header().ToByteArray();
    aData.Push (reinterpret_cast <const uint8_t*> (theData), strlen (theData));

    SX1278Packet aCopy (aData);
    EXPECT_TRUE (aCopy.Header().Command() == aPacket.Header().Command());
    EXPECT_TRUE (aCopy.Header().DataLength() == aPacket.Header().DataLength());
    EXPECT_TRUE (aCopy.Header().PacketID() == aPacket.Header().PacketID());
    EXPECT_TRUE (aCopy.Header().SenderID() == aPacket.Header().SenderID());
    EXPECT_TRUE (aCopy.Header().TargetID() == aPacket.Header().TargetID());
    EXPECT_TRUE (Base::Helper::Find (aCopy.Data().begin(), aCopy.Data().end(), theData) != -1);
}

TEST (STM32Lib_SX1278Packet, CreatePacket) 
{
    CreatePacketTest (SX1278CommandType::TcpPacket, 120, 12, 56, 0, "ABC");
    CreatePacketTest (SX1278CommandType::Report, 56, 0, 4, 54, "ADFT");
    CreatePacketTest (SX1278CommandType::VideoData, 1, 5, 30, 10, "GHJLSJ");
    CreatePacketTest (SX1278CommandType::Acknowledgement, 1, 1, 2, 3, "GHKJJLSJ");
}

}}
