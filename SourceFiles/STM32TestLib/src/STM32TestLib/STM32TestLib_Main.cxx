#include <Ori/STM32TestLib_Pch.hxx>
#include <Ori/STM32Lib_Allocator.hxx>

int main()
{
    testing::InitGoogleTest();
    //testing::GTEST_FLAG(filter) = "Base_String.AppendStringTwice";

    Ori::STM32Lib::Allocator::InitAllocator();

    RUN_ALL_TESTS();
    getchar();
}