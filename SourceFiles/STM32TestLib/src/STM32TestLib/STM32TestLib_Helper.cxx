#include <Ori/STM32TestLib_Pch.hxx>
#include <Ori/STM32TestLib_Helper.hxx>

#include <Ori/STM32Lib_Allocator.hxx>

namespace Ori {
namespace STM32TestLib {

int Helper::GetAllocatedBlocksLength()
{
    return STM32Lib::Allocator::myMBC.myAllocatedBlocksLength;
}

int Helper::GetFreeBlocksLength()
{
    return STM32Lib::Allocator::myMBC.myFreeBlocksLength;
}

const STM32Lib::RingBuffer& Helper::CommandManagerBuffer (const STM32Lib::CommandManager& theManager)
{
    return theManager.myBuffer;
}

}}
