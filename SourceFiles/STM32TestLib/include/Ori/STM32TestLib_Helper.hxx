#ifndef _Ori_STM32TestLib_Helper_HeaderFile
#define _Ori_STM32TestLib_Helper_HeaderFile

#include <Ori/STM32Lib_MemoryBlockContainer.hxx>
#include <Ori/STM32Lib_CommandManager.hxx>

namespace Ori {
namespace STM32TestLib {

class Helper
{
public:
    static int GetAllocatedBlocksLength();
    static int GetFreeBlocksLength();
    static const STM32Lib::RingBuffer& CommandManagerBuffer (const STM32Lib::CommandManager& theManager);
};

}}


#endif
