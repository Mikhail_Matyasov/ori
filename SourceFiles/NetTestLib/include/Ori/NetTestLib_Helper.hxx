#ifndef _Ori_NetTestLib_Helper_HeaderFile
#define _Ori_NetTestLib_Helper_HeaderFile

#include <stdint.h>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_LogLevel.hxx>

namespace Ori {
namespace NetTestLib {

class Helper
{
public:
    static void SetLogLevel (Ori::Base::LogLevel theLevel);
    static void SendPacketsInHaoticOrder (const Base::Vector <Base::ByteArray>& thePackets);
    static void SendDataSequencly (const Base::ByteArray& theData, int theNumberOfPackets);
    static void SendData (const Base::ByteArray& theData, int theNumberOfPackets);
    static void SendTcpData (int theNumberOfPackets);
    static void ExchangeTcpData();
    static void RequireLostedPacket();
};

}}

#endif