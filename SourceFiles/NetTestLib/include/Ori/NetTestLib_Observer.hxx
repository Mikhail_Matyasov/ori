#ifndef _Ori_NetTestLib_Observer_HeaderFile
#define _Ori_NetTestLib_Observer_HeaderFile

#include <Ori/Net_RTPListener.hxx>
#include <vector>

using namespace Ori::Net;

namespace Ori {
namespace NetTestLib {

class Observer : public RTPListener::Observer
{
public:
    Observer() : myPacketReceived (0) {}

    void OnPacket (const Base::ByteArray& thePacket) override
    {
        myPackets.emplace_back (thePacket.begin(), thePacket.end());
        myPacketReceived++;
    }

    __ORI_PROPERTY (std::vector <std::vector <uint8_t>>, Packets);
    __ORI_PRIMITIVE_PROPERTY (int, PacketReceived);
};

}}

#endif
