#include <Ori/NetTestLib_Pch.hxx>
#include <Ori/NetTestLib_Helper.hxx>
#include <Ori/Base_Logger.hxx>

int main()
{
    //testing::GTEST_FLAG(filter) = "NetTestLib.GetConnectionStatistics";
    Ori::NetTestLib::Helper::SetLogLevel (Ori::Base::LogLevel::Warning);
    Ori::Base::Logger::SetDefaultLevel (Ori::Base::LogLevel::Warning);

    testing::InitGoogleTest();
    RUN_ALL_TESTS();
}