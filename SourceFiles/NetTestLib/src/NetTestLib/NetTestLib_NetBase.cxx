#include <Ori/NetTestLib_Pch.hxx>

#include <Ori/Base_ByteArray.hxx>
#include <Ori/NetTestLib_Helper.hxx>

namespace Ori {
namespace NetTestLib {

TEST (NetTestLib, SendTcpData)
{
    Helper::SendTcpData (10);
}

TEST (NetTestLib, ExchangeTcpData)
{
    Helper::ExchangeTcpData();
}

TEST (NetTestLib, RequireLostedPacket)
{
    Helper::RequireLostedPacket();
}

}}
