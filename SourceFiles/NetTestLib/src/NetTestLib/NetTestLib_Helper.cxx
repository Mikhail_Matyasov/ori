#include <Ori/NetTestLib_Pch.hxx>
#include <Ori/NetTestLib_Helper.hxx>

#include <Ori/NetTestLib_Observer.hxx>
#include <Ori/Net_RTPClient.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Net_TcpClient.hxx>
#include <Ori/NetBase_TcpListener.hxx>
#include <Ori/Net_TcpObserver.hxx>

using namespace Ori::Net;
using namespace Ori::NetBase;
using namespace Ori::Base;

namespace Ori {
namespace NetTestLib {
namespace {

Base::Vector <int> theSentPackets;
Base::Logger theLogger;

int GetRandomPacketIndex (int theInc)
{
    int anIndex = rand() % 9 + 1 + theInc;
    auto anIt = std::find (theSentPackets.begin(), theSentPackets.end(), anIndex);
    if (anIt == theSentPackets.end()) {
        theSentPackets.Push (anIndex);
        return anIndex;
    }
    return GetRandomPacketIndex (theInc);
}
}

void Helper::SetLogLevel (Ori::Base::LogLevel theLevel)
{
    theLogger.Level() = theLevel;
}

void Helper::SendPacketsInHaoticOrder (const Base::Vector <Base::ByteArray>& thePackets)
{
    int aListenerPortBase = 8000;
    int aClientPortBase = 8004;
    int aTcpPort = 8002;
    Observer anObserver;
    RTPListener aListener (aListenerPortBase, aTcpPort, anObserver, theLogger);

    EXPECT_TRUE (aListener.Status() == RTPSessionStatus::Success);
    aListener.Start();

    RTPEndPoint aPoint ("127.0.0.1", aListenerPortBase, aTcpPort);
    RTPClient aClient (aClientPortBase, aPoint, theLogger);
    EXPECT_TRUE (aClient.Status() == RTPSessionStatus::Success);

    srand (static_cast <uint8_t> (time (0)));
    for (int i = 0; i < thePackets.Lenght() / 10; i++) {

        aClient.SendWithTimestamp (thePackets[i * 10].Data(), thePackets[i * 10].Lenght(), i * 10, true);
        for (int j = 1; j < 10; j++) {
            int anIndex = GetRandomPacketIndex (i * 10);
            auto& aPacket = thePackets[j + i * 10];
            aClient.SendWithTimestamp (aPacket.Data(), aPacket.Lenght(), anIndex, false);
        }
    }

    while (anObserver.PacketReceived() != thePackets.Lenght()) {}
    aListener.Stop();

    for (int i = 0; i < thePackets.Lenght(); i++) {
        bool aRes = std::equal (thePackets[i].begin(), thePackets[i].end(),
                    anObserver.Packets()[i].begin(), anObserver.Packets()[i].end());

        EXPECT_TRUE (aRes);
    }

}

void Helper::SendData (const Base::ByteArray& theData, int theNumberOfPackets)
{

    int aListenerPortBase = 8000;
    int aClientPortBase = 8004;
    int aTcpPort = 8002;
    Observer anObserver;
    RTPListener aListener (aListenerPortBase, aTcpPort, anObserver, theLogger);

    EXPECT_TRUE (aListener.Status() == RTPSessionStatus::Success);
    aListener.Start();

    RTPEndPoint aPoint ("127.0.0.1", aListenerPortBase, aTcpPort);
    RTPClient aClient (aClientPortBase, { aPoint }, theLogger);
    EXPECT_TRUE (aClient.Status() == RTPSessionStatus::Success);
    
    for (int i = 0; i < theNumberOfPackets; i++) {
        EXPECT_TRUE (aClient.Send (theData));
    }

    while (anObserver.PacketReceived() != theNumberOfPackets) {}
    aListener.Stop();
}

class TcpObserverTest : public TcpObserver
{
public:
    void OnPacket (const Base::ByteArrayView& thePacket) override
    {
        ByteArray anArray (thePacket.Data(), thePacket.Lenght());
        myPackets.Push (anArray);
    }

    __ORI_PROPERTY (Base::Vector <Base::ByteArray>, Packets);   

};

void Helper::SendTcpData (int theNumberOfPackets)
{
    int aListenPort = 8888;
    TcpObserverTest anObserver;
    TcpListener aListener (aListenPort);
    aListener.RegisterObserver (&anObserver);

    IPEndPoint aPoint;
    aPoint.IPAdress() = "127.0.0.1";
    aPoint.Port() = aListenPort;
    TcpClient aClient (aPoint);
    aClient.Connect();

    while (!aClient.Connected());

    Base::Vector <Base::ByteArray> aPackets;

    for (int i = 0; i < theNumberOfPackets; i++) {
        Base::ByteArray aPacket;

        for (int j = 0; j < theNumberOfPackets + i; j++) {
            aPacket.Push (j);
        }
        aPackets.Push (aPacket);
        EXPECT_TRUE (aClient.Send (aPacket.Data(), aPacket.Lenght()));
    }

    while (theNumberOfPackets != anObserver.Packets().Lenght()) {}

    EXPECT_TRUE (anObserver.Packets() == aPackets);
}

class ExchangeObserver : public TcpObserver
{
public:
    ExchangeObserver (TcpListener& theListener) :
        myListener (theListener)
    {}

    void OnPacket (const Base::ByteArrayView& thePacket) override
    {
        myListener.Send (thePacket.Data(), thePacket.Lenght());
    }

private:
    TcpListener& myListener;
};

void Helper::ExchangeTcpData()
{
    int aListenPort = 8888;
    TcpListener aListener (aListenPort);
    ExchangeObserver anObserver (aListener);
    aListener.RegisterObserver (&anObserver);

    TcpObserverTest aClientObserver;
    IPEndPoint aPoint ("127.0.0.1", aListenPort);
    TcpClient aClient (aPoint);
    aClient.RegisterObserver (&aClientObserver);
    aClient.Connect();

    while (!aClient.Connected());

    ByteArray aPacket { 1, 2, 3, 4 };
    aClient.Send (aPacket.Data(), aPacket.Lenght());

    while (aClientObserver.Packets().Empty());

    EXPECT_TRUE (aClientObserver.Packets()[0] == aPacket);
}

void Helper::RequireLostedPacket()
{
    int aListenerPortBase = 8000;
    int aClientPortBase = 8004;
    int aTcpPort = 8002;
    Observer anObserver;
    RTPListener aListener (aListenerPortBase, aTcpPort, anObserver, theLogger);

    EXPECT_TRUE (aListener.Status() == RTPSessionStatus::Success);
    aListener.Start();

    RTPEndPoint aPoint ("127.0.0.1", aListenerPortBase, aTcpPort);
    RTPClient aClient (aClientPortBase, { aPoint }, theLogger);
    EXPECT_TRUE (aClient.Status() == RTPSessionStatus::Success);

    ByteArray aData { 1, 2, 3, 4 };
    EXPECT_TRUE (aClient.SendWithTimestamp (aData.Data(), aData.Lenght(), 0, true));
    for (int i = 1; i < 8; i++) {
        EXPECT_TRUE (aClient.SendWithTimestamp (aData.Data(), aData.Lenght(), i, false));
    }

    ByteArray aLosted { 5, 6, 7, 8 };
    aClient.CachePacket (aLosted.Data(), aLosted.Lenght(), 8, false);

    aClient.SendWithTimestamp (aData.Data(), aData.Lenght(), 9, false);
    aClient.SendWithTimestamp (aData.Data(), aData.Lenght(), 10, false);

    while (anObserver.PacketReceived() != 11) {
        aClient.FlushLostedPackets();
    }

    bool a8 = std::equal (anObserver.Packets()[8].data(),
                          anObserver.Packets()[8].data() + anObserver.Packets()[8].size(), aLosted.begin(), aLosted.end());
    EXPECT_TRUE (a8);
}

void Helper::SendDataSequencly (const Base::ByteArray& theData, int theNumberOfPackets)
{
    int aListenerPortBase = 8000;
    int aClientPortBase = 8004;
    int aTcpPort = 8002;
    Observer anObserver;
    RTPListener aListener (aListenerPortBase, aTcpPort, anObserver, theLogger);

    EXPECT_TRUE (aListener.Status() == RTPSessionStatus::Success);
    aListener.Start();

    RTPEndPoint aPoint ("127.0.0.1", aListenerPortBase, aTcpPort);
    RTPClient aClient (aClientPortBase, aPoint, theLogger);
    EXPECT_TRUE (aClient.Status() == RTPSessionStatus::Success);

    std::vector <std::vector <uint8_t>> aPackets;
    for (int i = 0; i < theNumberOfPackets; i++) {
        
        std::vector <uint8_t> aVec (theData.begin(), theData.end());
        aVec[aVec.size() - 1] = (i + 10) % 256;
        aPackets.push_back (aVec);
        EXPECT_TRUE (aClient.Send (aVec.data(), static_cast <int> (aVec.size())));
    }

    while (anObserver.PacketReceived() != theNumberOfPackets) {}
    aListener.Stop();

    for (int i = 0; i < theNumberOfPackets; i++) {
        EXPECT_TRUE (aPackets[i] == anObserver.Packets()[i]);
    }
}


}}
