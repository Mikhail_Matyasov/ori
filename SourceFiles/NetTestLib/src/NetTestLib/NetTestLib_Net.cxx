#include <Ori/NetTestLib_Pch.hxx>

#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/NetTestLib_Helper.hxx>
#include <Ori/Net_ConnectionChecker.hxx>
#include <Ori/Net_ConnectionStatistics.hxx>
#include <Ori/Net_NetworkInfo.hxx>

namespace Ori {
namespace NetTestLib {

using namespace Ori::Base;
using namespace Ori::Net;
const int theSubPacketMaxSize = 1400;

TEST (NetTestLib, SendPacketOnes)
{
    Base::ByteArray aData { 0, 1, 2, 3 };
    Helper::SendData (aData, 1);
}

TEST (NetTestLib, Send10Packet)
{ 
    Base::ByteArray aData { 0, 1, 2, 3 };
    Helper::SendData (aData, 10);
}

TEST (NetTestLib, Send10PacketSequencly)
{ 
    ByteArray aData { 0, 1, 2, 3 };
    Helper::SendDataSequencly (aData, 10);   
}

namespace {
Base::ByteArray GetData (int theDataLenght)
{
    Base::ByteArray aData;
    aData.Reserve (theDataLenght);
    for (int i = 0; i < theDataLenght; i++) {
        aData.Push (i % 256);
    }

    return aData;
}
}

TEST (NetTestLib, SendDoublePacket)
{
    auto aData = GetData (theSubPacketMaxSize * 2);
    Helper::SendData (aData, 1);
}

TEST (NetTestLib, SendDoublePacketWithHalf)
{
    auto aData = GetData (static_cast <int> (theSubPacketMaxSize * 1.5));
    Helper::SendData (aData, 1);
}

TEST (NetTestLib, SendTriplePacket)
{
    auto aData = GetData (theSubPacketMaxSize * 3);
    Helper::SendData (aData, 1);
}

TEST (NetTestLib, SendTriplePacketWithHalf)
{
    auto aData = GetData (static_cast <int> (theSubPacketMaxSize * 2.5));
    Helper::SendData (aData, 1);
}

TEST (NetTestLib, SendQueueOfDoublePacket)
{
    auto aData = GetData (theSubPacketMaxSize * 2);
    Helper::SendDataSequencly (aData, 4);
}

TEST (NetTestLib, SendQueueOfTriplePacket)
{
    auto aData = GetData (theSubPacketMaxSize * 3);
    Helper::SendDataSequencly (aData, 4);
}

TEST (NetTestLib, SendBigData)
{ 
    int aDataSize = 300000;
    Base::ByteArray aData;
    for (int i = 0; i < aDataSize; i++) {
        aData.Push (i % 256);
    }

    Helper::SendData (aData, 1);
}

TEST (NetTestLib, SendBigDataQueue)
{ 
    int aDataSize = 300000;
    Base::ByteArray aData;
    for (int i = 0; i < aDataSize; i++) {
        aData.Push (i % 256);
    }

    Helper::SendDataSequencly (aData, 10);
}

TEST (NetTestLib, SendPacketsInHaoticOrder)
{ 
    int aDataSize = 10000;
    int aNumberOfPackets = 30;
    Vector <ByteArray> aPackets;
    for (int i = 0; i < aNumberOfPackets; i++) {

        ByteArray aPacket;
        for (int i = 0; i < aDataSize; i++) {
            aPacket.Push (i % 256);
        }
        aPackets.Push (aPacket);
    }

    Helper::SendPacketsInHaoticOrder (aPackets);
}


namespace {
class StatisticsObserver : public ConnectionChecker::Observer
{
public:
    StatisticsObserver() : myNumberOfReplies (0) {}

    void OnStatistics (const ConnectionStatistics& theStat) override
    {
        myNumberOfReplies++;
    }

    __ORI_PRIMITIVE_PROPERTY (int, NumberOfReplies);
};
}

TEST (NetTestLib, GetConnectionStatistics)
{
    ConnectionChecker aChecker ("youtube.ru");
    StatisticsObserver anObserver;
    aChecker.RegisterObserver (&anObserver);

    while (anObserver.NumberOfReplies() != 4);
}

}}