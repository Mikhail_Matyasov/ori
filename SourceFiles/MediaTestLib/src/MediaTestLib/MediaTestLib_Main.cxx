#include <Ori/MediaTestLib_Pch.hxx>
#include <Ori/Base_Logger.hxx>

int main()
{
    testing::InitGoogleTest();
    RUN_ALL_TESTS();
}
