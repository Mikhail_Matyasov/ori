#include <Ori/MediaTestLib_Pch.hxx>

#include <Ori/Base_StreamReader.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Media_MotionDetector.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/Base_PixelFormat.hxx>
#include <Ori/Base_Frame.hxx>

using namespace Ori::Media;
using namespace Ori::Base;

namespace Ori {
namespace MediaTestLib {

Vector <Frame> ReadFrames()
{
    Vector <Frame> aFrames;
    String aFileName = std::getenv ("ORI_HOME");
    aFileName += "/SourceFiles/MediaTestLib/resources/PeopleMotion.rgba";
    StreamReader aReader (aFileName);
    const auto& aByteArray = aReader.Read();
    const auto& aData = *aByteArray;

    for (int i = 0; i < aData.Lenght();) {
        TypeBuffer <uint16_t> aWidth (&aData [i], sizeof (uint16_t));
        TypeBuffer <uint16_t> aHeigth (&aData [i + 2], sizeof (uint16_t));
        int aDataSize = aWidth.Value * aHeigth.Value * 4;

        Frame aFrame (aWidth.Value, aHeigth.Value, PixelFormat::RGBA);
        aFrame.Data().Resize (1);
        aFrame.Data()[0] = std::make_shared <ByteArray> (&aData [i + 4], aDataSize);
        aFrames.Push (aFrame);
        i += 4 + aDataSize;
    }

    return aFrames;
}

TEST (MediaTestLib, DetectMotion)
{
    auto aFrames = ReadFrames();

    int aFrameNumber = 0;
    MotionDetector aDetector;

    for (; aFrameNumber < aFrames.Lenght(); aFrameNumber++) {
        bool anIsMotionDetected = aDetector.Detect (aFrames [aFrameNumber]);
        if (anIsMotionDetected) {
            break;
        }
    }

    EXPECT_TRUE (aFrameNumber == 2);
}

}}