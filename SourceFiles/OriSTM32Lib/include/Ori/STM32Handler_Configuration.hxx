#ifndef _Ori_STM32Handler_Configuration_HeaderFile
#define _Ori_STM32Handler_Configuration_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Embed_CommandDef.hxx>

using namespace Ori::Embed;

namespace Ori {
namespace STM32Handler {

class Configuration
{
public:
    static void ConfigureAsServer();
    static void ConfigureAsClient();
};

}}


#endif
