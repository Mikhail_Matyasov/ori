#ifndef _Ori_STM32Lib_USART_HeaderFile
#define _Ori_STM32Lib_USART_HeaderFile

#include <stdint.h>
#include <stm32f103xb.h>

#define __ORI_USART_MAX_BAUDRATE 500000 // using of usart with baudrate above then that value may cause lost of data
#define __ORI_ESP8266_DEFAULT_BAUDRATE 115200

namespace Ori {
namespace STM32Lib {

// USART1 - TX = PA9, RX = PA10
class USART
{
public:
	enum class USARTType
	{
		USART_1,
		USART_2,
		USART_3
	};
	class Observer
	{
	public:
		virtual void OnData (uint32_t theByte) = 0;
	};
	USART (USARTType theType);
	void Send (const uint8_t* theData, int theLength) const;
	void Send (const char* theMessage) const;
	void Println (const char* theMessage) const;
	void Println (char theChar) const;
	void Println (int theNumber) const;
	void Send (int theNumber) const;
	void SendByte (uint8_t theByte) const;
	void RegisterObserver (Observer* theObserver) const;
	void StopIRQ() const;
	void StartIRQ() const;
	void ChangeBaudRate (int theNewValue) const;
	void static Reset();

private:
	void SetBaudRate (int theBaudRate) const;
	void ConfigureUSART1() const;
	void ConfigureUSART3() const;
	USART_TypeDef* CurrentUSART() const;
	
private:
	USARTType myType;
};
	
}}


#endif
