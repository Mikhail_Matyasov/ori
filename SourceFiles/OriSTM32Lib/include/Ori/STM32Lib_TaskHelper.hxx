#ifndef _Ori_STM32Lib_TaskHelper_HeaderFile
#define _Ori_STM32Lib_TaskHelper_HeaderFile

#include <Ori/STM32Lib_Macros.hxx>

#ifdef __ORI_STM32LIB
#include <FreeRTOSConfig.h>
#define DEFAULT_STACK_SIZE configMINIMAL_STACK_SIZE
#else
#define DEFAULT_STACK_SIZE 128
typedef int TaskHandle_t;
#endif

namespace Ori {
namespace STM32Lib {

class TaskHelper
{
public:
    typedef void (* TaskType)( void * );

	static TaskHandle_t Create (TaskType theTask, const char* theName, int theStackSize = DEFAULT_STACK_SIZE, int thePriority = 1);
};	
	
}}

#endif
