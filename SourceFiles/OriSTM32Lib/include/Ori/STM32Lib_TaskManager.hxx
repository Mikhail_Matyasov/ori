#ifndef _Ori_STM32Lib_TaskManager_HeaderFile
#define _Ori_STM32Lib_TaskManager_HeaderFile

#include <Ori/STM32Lib_Allocator.hxx>
#include <Ori/Base_Vector.hxx>

#ifdef __ORI_STM32LIB
#include <FreeRTOS.h>
#include <task.h>
#else
typedef int TaskHandle_t;
#endif

namespace Ori {
namespace STM32Lib {

class TaskManager
{
public:
    void AddTask (TaskHandle_t theTask);
	void ConfigurePriority();
	void RunImmediateTask();
	static TaskManager* Instance();
	
	__ORI_PRIMITIVE_PROPERTY (TaskHandle_t, ImmediateTask)
	
private:
	TaskManager();
	TaskHandle_t GetNextTask (int theCurrentTaskIndex);
	
private:
	Base::Vector <TaskHandle_t> myTasks;
	Base::Vector <bool> myInvokedTask;
	friend class STM32Lib::Allocator;
};	
	
}}

#endif
