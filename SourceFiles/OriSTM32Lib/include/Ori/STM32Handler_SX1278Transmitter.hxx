#ifndef _Ori_STM32Handler_SX1278Transmitter_HeaderFile
#define _Ori_STM32Handler_SX1278Transmitter_HeaderFile

namespace Ori {
namespace Embed {
class ESP8266Packet;
}
namespace STM32Handler {

class SX1278Transmitter
{
public:
    static void Send (const Embed::ESP8266Packet& thePacket);
};

}}


#endif
