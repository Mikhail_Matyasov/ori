#ifndef _Ori_STM32Lib_SPI_HeaderFile
#define _Ori_STM32Lib_SPI_HeaderFile

#include <stdint.h>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/STM32Lib_GPIO.hxx>

namespace Ori {
namespace STM32Lib {

class SPI
{
public:
    enum class SPIType
	{
		SPI_1,
		SPI_2
	};
	class Observer
	{
	public:
		virtual void OnData (uint8_t theByte) = 0;
	};

	SPI (SPIType theType);
	
	void Send (const uint8_t* theData, int theLength);
	void Send (const char* theMessage);
	void RegisterObserver (Observer* theObserver);
	uint8_t SendByte (uint8_t theByte);
	uint8_t ReadByte();
	void static Reset();
	
private:
	SPIType myType;
	__ORI_PRIMITIVE_PROPERTY (GPIO::Port, NSS);
};

}}

#endif
