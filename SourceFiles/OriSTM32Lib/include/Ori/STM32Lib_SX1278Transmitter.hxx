#ifndef _Ori_STM32Lib_SX1278Transmitter_HeaderFile
#define _Ori_STM32Lib_SX1278Transmitter_HeaderFile

#include <Ori/STM32Lib_SX1278.hxx>
#include <Ori/STM32Lib_SX1278Packet.hxx>
#include <Ori/STM32Lib_SX1278PacketContext.hxx>

#include <FreeRTOS.h>
#include <task.h>

#define __ORI_SX1278_PACKET_CACHE_LENGTH 16

namespace Ori {
namespace STM32Lib {
class UdpClient;

class SX1278Transmitter : public SX1278::Observer
{
public:
	void OnDIO0Interrupt() override;
	void FlushCacheBuffer();
	void AllocateTxBuffer (int theSize);
	__ORI_PRIMITIVE_PROPERTY (uint8_t, SenderID);
	
protected:
	SX1278Transmitter();
	
protected:
	Vector <SX1278PacketContext> myPacketCache;
	
private:
	void BeginPacket();
	void FlushPacket();
	void SendPacket (const SX1278PacketContext& thePacket, uint8_t thePacketID);
	void DoSend (const uint8_t* theData,
				 uint8_t theDataLength,
				 SX1278CommandType theCommand,
				 uint8_t theTargetID = 1,
				 uint8_t thePacketID = 0);
	void SendUdpPacket (const uint8_t* theData,
						uint8_t theLength,
						SX1278CommandType theCmdType);
	
	void SendAcknowledgement (uint8_t theTargetID);
	
	bool IsTransmitting();
	
private:
	bool myTxDone;
	TaskHandle_t myTask;
	friend UdpClient;
};

}}

#endif
