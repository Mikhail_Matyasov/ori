#ifndef _Ori_STM32Lib_ESP8266Listener_HeaderFile
#define _Ori_STM32Lib_ESP8266Listener_HeaderFile

#include <Ori/STM32Lib_RingBuffer.hxx>
#include <Ori/STM32Lib_USART.hxx>

#define __ORI_ESP8266_LISTENER_TIMEOUT 5000

namespace Ori {
namespace STM32Lib {

class ESP8266Listener : public USART::Observer
{
public:

	class Observer
	{
	public:
		virtual void OnData (const uint8_t* theData, int theLength) = 0;
	};
	bool Wait (const char* theSuccessReply,
		       const char* theErrorReply = "ERROR",
		       bool theisConsume = true,
		       bool theIsWaitEnd = true,
			   int theTimeout = __ORI_ESP8266_LISTENER_TIMEOUT);
	
	void ExtractData (int theStart);
    void ExtractInputCommand();
    void OnData (uint32_t theByte) override;
	void RegisterObserver (Observer* theObserver);
	int8_t ConnectionID() const;
	void RemoveConnection (uint8_t theID);
    static ESP8266Listener* Instance();
    __ORI_PROPERTY (RingBuffer, Buffer);

private:
    ESP8266Listener();
	void WaitEnd();
	void AddConnection (uint8_t theID);

private:
	Observer* myObserver;
    bool myIsWaitReply;
	Base::Vector <uint8_t> myActiveConnections;
	friend Ori::STM32Lib::Allocator;
};

}}


#endif
