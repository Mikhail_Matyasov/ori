#ifndef _Ori_STM32Lib_Assert_HeaderFile
#define _Ori_STM32Lib_Assert_HeaderFile

#include <Ori/Base_Macros.hxx>
	
namespace Ori {
namespace STM32Lib {

class Assert
{
public:
    ORI_EXPORT static void Raise (const char* theMessage = "Assert");
    ORI_EXPORT static void True (bool theExpression);
    ORI_EXPORT static void False (bool theExpression);
};

}}


#endif
