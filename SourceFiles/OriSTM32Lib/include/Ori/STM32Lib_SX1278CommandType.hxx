#ifndef _Ori_STM32Lib_SX1278CommandType_HeaderFile
#define _Ori_STM32Lib_SX1278CommandType_HeaderFile

#include <stdint.h>

namespace Ori {
namespace STM32Lib {

enum class SX1278CommandType : uint8_t
{
    Report,
    VideoData,
	TcpPacket,
	Acknowledgement,
	UdpPacket
};

}}

#endif
