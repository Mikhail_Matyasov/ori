#ifndef _Ori_STM32Lib_TcpClient_HeaderFile
#define _Ori_STM32Lib_TcpClient_HeaderFile

#include <stdint.h>
#include <Ori/STM32Lib_SX1278Transmitter.hxx>

namespace Ori {
namespace STM32Lib {

class SX1278Packet;	
	
class TcpClient : public SX1278Transmitter
{
public:	
	void OnReport (const SX1278Packet& theReport);
    void SendVideoData (const uint8_t* theData, int theDataLength);
    void SendPacket (const uint8_t* theData, int theDataLength);
    static TcpClient* Instance();

private:
    TcpClient();
	friend class Ori::STM32Lib::Allocator;

};

}}

#endif
