#ifndef _Ori_STM32Lib_RingBuffer_HeaderFile
#define _Ori_STM32Lib_RingBuffer_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Vector.hxx>
#include <stdint.h>

namespace Ori {
namespace STM32Lib {
class RingBuffer
{
public:
	class Iterator
	{
	public:
		Iterator (const RingBuffer& theBuffer, int theStartPos = -1);
		const uint8_t* Next();
		
		int ActiveBuffer();
		__ORI_PRIMITIVE_PROPERTY (int, FirstBegin);
		__ORI_PRIMITIVE_PROPERTY (int, FirstLength);
		__ORI_PRIMITIVE_PROPERTY (int, SecondBegin);
		__ORI_PRIMITIVE_PROPERTY (int, SecondLength);
		__ORI_PRIMITIVE_PROPERTY (int, FirstCurrent);
		__ORI_PRIMITIVE_PROPERTY (int, SecondCurrent);
		
	private:
		const RingBuffer& myBuffer;
		
	};
	ORI_EXPORT RingBuffer();
	ORI_EXPORT void Push (uint8_t theElement);
	ORI_EXPORT void Push (const uint8_t* theData, int theDataSize);
	ORI_EXPORT int Find (const char* theStrign) const;
	ORI_EXPORT void Consume (int theDataLength);
	ORI_EXPORT void ConsumeUpTo (const uint8_t* theLast);
	ORI_EXPORT void ConsumeUpTo (int theIndex);
	ORI_EXPORT void Clear();
	ORI_EXPORT void Allocate (int theBufferSize);
	ORI_EXPORT int ToGlobalIndex (int theLocalIndex) const;
	ORI_EXPORT int ToLocalIndex (int theGlobalIndex) const;
	__ORI_PRIMITIVE_PROPERTY (Base::ByteArray, Buffer);
	__ORI_PRIMITIVE_PROPERTY (int, DataLength);
	__ORI_PRIMITIVE_PROPERTY (int, DataEnd);
};	
	
}}

#endif