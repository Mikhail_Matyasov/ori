#ifndef _Ori_STM32Handler_Blink_HeaderFile
#define _Ori_STM32Handler_Blink_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Embed_CommandDef.hxx>

using namespace Ori::Embed;

namespace Ori {
namespace STM32Handler {

class Blink
{
public:
    static void Execute (const BlinkType& theBlinkType);
};

}}


#endif
