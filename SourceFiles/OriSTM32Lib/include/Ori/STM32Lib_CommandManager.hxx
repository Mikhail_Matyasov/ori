#ifndef _Ori_STM32Lib_CommandManager_HeaderFile
#define _Ori_STM32Lib_CommandManager_HeaderFile

#include <Ori/STM32Lib_ESP8266Listener.hxx>
#include <Ori/STM32Lib_RingBuffer.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <stdint.h>

namespace Ori {
namespace Base {
__ORI_DECLARE_BYTEARRAYVIEW	 
}
namespace Embed {
class ESP8266Packet;
}

namespace STM32Lib {

class CommandManager : public ESP8266Listener::Observer
{
public:
    void OnData (const uint8_t* theData, int theLength) override;
	void ProcessCommand (const Embed::ESP8266Packet& thePacket) const;
	bool ProcessSingleCommand (const Embed::ESP8266Packet& thePacket) const;
	bool ExtractCommand (Embed::ESP8266Packet& thePacket);
	void ConsumeLastCommand (const uint8_t* theDataEnd);
	void ConsumeLastSingleCommand();
	void AllocateBuffer (int theSize);
	static CommandManager* Instance();
	
private:
	CommandManager ();
	bool SendReply (uint8_t theID, Embed::CommandType theReply) const;
	
	bool DoProcessSingleCommand (const Embed::ESP8266Packet& thePacket,
						         Embed::CommandType theExpectedCommand,
						         const std::function <void()> theHandler = nullptr) const;
	
private:
	RingBuffer myBuffer;
	friend class Ori::STM32TestLib::Helper;
	friend class STM32Lib::Allocator;
};

}}


#endif
