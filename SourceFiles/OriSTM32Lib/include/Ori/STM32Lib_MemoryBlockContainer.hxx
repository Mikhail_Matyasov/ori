#ifndef _Ori_STM32Lib_MemoryBlockContainer_HeaderFile
#define _Ori_STM32Lib_MemoryBlockContainer_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <stdint.h>

namespace Ori {
namespace STM32TestLib {
class Helper;		 
}
namespace STM32Lib {

class MemoryBlock
{
public:
	MemoryBlock (uint8_t* theBuffer = nullptr, int theLength = 0) : 
		myBuffer (theBuffer),
		myLength (theLength)
	{}
	
	MemoryBlock (const MemoryBlock& theOther) :
		myBuffer (theOther.myBuffer),
		myLength (theOther.myLength)
	{}
	
	MemoryBlock& operator= (const MemoryBlock& theOther)
	{
		myBuffer = theOther.myBuffer;
		myLength = theOther.myLength;
		return *this;
	}
	
    bool IsNull() { return myLength == 0; }
	__ORI_PRIMITIVE_PROPERTY (uint8_t*, Buffer);
	__ORI_PRIMITIVE_PROPERTY (int, Length);
};
	
class MemoryBlockContainer
{
public:
    MemoryBlockContainer();
	
	MemoryBlock FindFreeBlock (int theSize, int& theFreeBlockIndex);
	bool AddFreeBlock (const MemoryBlock& theBlock);
	void RemoveFreeBlock (int theBlockLength, int theIndex);
	
	MemoryBlock FindAllocatedBlock (const uint8_t* theBuffer, int& theAllocatedBlockIndex);
	bool AddAllocatedBlock (const MemoryBlock& theBlock);
	void RemoveAllocatedBlock (int theIndex);
	
	int HeapSize() 
	{ return myHeapSize - (myAllocatedBlocksCapacity + myFreeBlocksCapacity); }
	
	int AvaliableSpace();
	
	void DefragmentMemory();
	void Reset();
	void AllocateContainer();
	
private:
	bool DoAddFreeBlock (const MemoryBlock& theBlock);
	bool DoAddAllocatedBlock (const MemoryBlock& theBlock);
	MemoryBlock ReallocateBlocks (int theBlockLength,
						          MemoryBlock* theExistingBlock,
						          int theExistingBlockLength,
								  int& theBlockIndex);
	
	bool ReallocateAllocatedBlocks();
	bool ReallocateFreeBlocks();
	void MoveAllocatedBlocks (const MemoryBlock& theBlock);
	void MoveFreeBlocks (const MemoryBlock& theBlock);
	
	int FindSiblingFreeBlock (const uint8_t* theTargetSiblingAdress);
	
private:
	MemoryBlock* myFreeBlocks;
	int myFreeBlocksLength;
	int myFreeBlocksCapacity;
	
	MemoryBlock* myAllocatedBlocks;
	int myAllocatedBlocksLength;
	int myAllocatedBlocksCapacity;
	
	uint8_t* myHeap;
	const int myHeapSize;
	friend class Ori::STM32TestLib::Helper;
};

}}


#endif
