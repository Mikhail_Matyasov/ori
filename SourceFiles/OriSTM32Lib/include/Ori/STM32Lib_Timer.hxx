#ifndef _Ori_STM32Lib_Timer_HeaderFile
#define _Ori_STM32Lib_Timer_HeaderFile

#ifdef __ORI_STM32LIB
#include <stm32f103xb.h>
#endif

#ifndef __ORI_STM32LIB
struct TIM_TypeDef;
#endif

namespace Ori {
namespace STM32Lib {

class Timer
{
public:
    enum class Units
    {
        Seconds,
        Milliseconds,
        Microseconds,
        Nanoseconds
    };
	static void Init();
	static void SetInterruptPeriod (TIM_TypeDef* theTimer, int thePeriodInMs);
    static void Delay (int theDelay, Units theDelayUnit = Units::Milliseconds);
	static bool DoWhile (const std::function <bool()>& theFunc, int theMilis);
	static uint64_t Millis();
};

}}

#endif
