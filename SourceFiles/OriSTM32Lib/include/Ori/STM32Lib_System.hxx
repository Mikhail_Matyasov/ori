#ifndef _Ori_STM32Lib_System_HeaderFile
#define _Ori_STM32Lib_System_HeaderFile

namespace Ori {
namespace STM32Lib {

class System
{
public:
	void Init();
	void Start();
};	
	
}}

#endif
