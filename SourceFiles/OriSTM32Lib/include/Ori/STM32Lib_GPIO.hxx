#ifndef _Ori_STM32Lib_GPIO_HeaderFile
#define _Ori_STM32Lib_GPIO_HeaderFile

#ifdef __ORI_STM32LIB

#include <stm32f103xb.h>

namespace Ori {
namespace STM32Lib {

// http://dimoon.ru/obuchalka/stm32f1/programmirovanie-stm32-chast-5-portyi-vvoda-vyivoda-gpio.html
// https://easystm32.ru/for-beginners/11-mcu-ports

// GPIO modes:
// | MODE | CNF |
//    10    00   Output 2Mhz Simple Push-Pull
//    10    01   Output 2Mhz Simple Open-Drain
//    10    10   Output 2Mhz Alternate Push-Pull
//    10    11   Output 2Mhz Alternate Open-Drain
//    01    00   Output 10Mhz Simple Push-Pull
//    01    01   Output 10Mhz Simple Open-Drain
//    01    10   Output 10Mhz Alternate Push-Pull
//    01    11   Output 10Mhz Alternate Open-Drain
//    11    00   Output 50Mhz Simple Push-Pull
//    11    01   Output 50Mhz Simple Open-Drain
//    11    10   Output 50Mhz Alternate Push-Pull
//    11    11   Output 50Mhz Alternate Open-Drain
//    00    00   Input Analog
//    00    01   Input Floating
//    00    10   Input Pull-Up-Down
//    00    11   Input Reserved

#define __ORI_GPIO_OUTPUT_2MHZ_SIMPL_PUSH_PULL(GPIOx, CR, PORT) \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR &= ~(3 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos); \
		
#define __ORI_GPIO_OUTPUT_2MHZ_SIMPL_OPEN_DRAIN(GPIOx, CR, PORT) \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_2MHZ_ALTERNATE_PUSH_PULL(GPIOx, CR, PORT) \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_2MHZ_ALTERNATE_OPEN_DRAIN(GPIOx, CR, PORT) \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_10MHZ_SIMPL_PUSH_PULL(GPIOx, CR, PORT) \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR &= ~(3 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos); \
		
#define __ORI_GPIO_OUTPUT_10MHZ_SIMPL_OPEN_DRAIN(GPIOx, CR, PORT) \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_10MHZ_ALTERNATE_PUSH_PULL(GPIOx, CR, PORT) \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_10MHZ_ALTERNATE_OPEN_DRAIN(GPIOx, CR, PORT) \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_50MHZ_SIMPL_PUSH_PULL(GPIOx, CR, PORT) \
	GPIOx->CR |= 3 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR &= ~(3 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos); \
		
#define __ORI_GPIO_OUTPUT_50MHZ_SIMPL_OPEN_DRAIN(GPIOx, CR, PORT) \
	GPIOx->CR |= 3 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_PUSH_PULL(GPIOx, CR, PORT) \
	GPIOx->CR |= 3 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_OPEN_DRAIN(GPIOx, CR, PORT) \
	GPIOx->CR |= 3 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos; \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_INPUT_ANALOG(GPIOx, CR, PORT) \
	GPIOx->CR &= ~(3 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos); \
	GPIOx->CR &= ~(3 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos); \
		
#define __ORI_GPIO_INPUT_FLOATING(GPIOx, CR, PORT) \
	GPIOx->CR &= ~(3 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos); \
	GPIOx->CR |= 1 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \
		
#define __ORI_GPIO_INPUT_PULL_UP_DOWN(GPIOx, CR, PORT) \
	GPIOx->CR &= ~(3 << GPIO_ ## CR ## _MODE ## PORT ## _ ## Pos); \
	GPIOx->CR |= 2 << GPIO_ ## CR ## _CNF ## PORT ## _ ## Pos; \

class GPIO
{
public:
    static void Reset();
	
	class Port
	{
	public:
		Port (GPIO_TypeDef& theGPIO, int thePort);
		Port (const Port& theOther);
		void High();
		void Low();
		void Toggle();
		bool Value();
		
	private:
		GPIO_TypeDef& myGPIO;
		int myPort;
		bool myIsHigh;
	};
};

}}


#endif
#endif
