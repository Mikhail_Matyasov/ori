#ifndef _Ori_STM32Lib_UdpClient_HeaderFile
#define _Ori_STM32Lib_UdpClient_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/STM32Lib_SX1278CommandType.hxx>
#include <stdint.h>

namespace Ori {
namespace STM32Lib {
class SX1278Transmitter;
	
class UdpClient
{
public:
    void Send (const uint8_t* theData, int theDataLength, SX1278CommandType theCmdType = SX1278CommandType::UdpPacket);
    void SendAcknowledgement (uint8_t theTargetID);
    static UdpClient* Instance();
    __ORI_PRIMITIVE_PROPERTY (SX1278Transmitter*, Transmitter);
	
private:
    UdpClient() {}
	friend class Ori::STM32Lib::Allocator;
};

}}

#endif
