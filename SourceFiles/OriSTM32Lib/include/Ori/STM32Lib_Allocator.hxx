#ifndef _Ori_STM32Lib_Allocator_HeaderFile
#define _Ori_STM32Lib_Allocator_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Allocator.hxx>
#include <Ori/STM32Lib_MemoryBlockContainer.hxx>
#include <Ori/STM32Lib_Assert.hxx>

using namespace Ori::Base;

namespace Ori {
namespace STM32TestLib {
class Helper;		 
}
namespace STM32Lib {
	
class Allocator
{
public:
	
	template <typename T, class... Types>
	static T* Construct (Types&&... theArgs);
	
	template <typename T>
	static T* ConstructArray (int theSize);
	
	template <typename T>
	static T* Allocate (int theSize);
	
	template <typename T>
	static bool Deallocate (T* thePointer);
	
	template <typename T>
	static bool DeallocateArray (T* thePointer);
	
	static void InitAllocator()
	{ myMBC.AllocateContainer(); }
	
	static int HeapSize()
	{ return myMBC.HeapSize(); }
	
	static void Reset()
	{ myMBC.Reset(); }
	
	static int AvaliableSpace()
	{ return myMBC.AvaliableSpace(); }
	
private:
	template <typename T>
	static uint8_t* GetFreeBuffer (int theSize);
	
private:
	static MemoryBlockContainer myMBC;
	friend class Ori::STM32TestLib::Helper;
};
	
template <typename T>	
uint8_t* Allocator::GetFreeBuffer (int theSize)
{	
	int aBlockSize = sizeof (T) * theSize;
	int aFreeBlockIndex;
	auto aBlock = myMBC.FindFreeBlock (aBlockSize, aFreeBlockIndex);
	if (aBlock.IsNull()) {
		myMBC.DefragmentMemory();
		aBlock = myMBC.FindFreeBlock (aBlockSize, aFreeBlockIndex);
		if (aBlock.IsNull()) {
			Assert::Raise ("Can not allocate required memory");
			return nullptr;
		}
	}
	
	myMBC.RemoveFreeBlock (aBlock.Length(), aFreeBlockIndex);
	if (myMBC.AddAllocatedBlock (aBlock)) {
		return aBlock.Buffer();
	}
	Assert::Raise ("Can not allocate required memory");
	return nullptr;
}
	
template <typename T, class... Types>
T* Allocator::Construct (Types&&... theArgs)
{
	uint8_t* aBuffer = GetFreeBuffer <T> (1);
	if (aBuffer) {
		return new (aBuffer) T (std::forward <Types> (theArgs)...);	
	}
	
	return nullptr;
}

template <typename T>	
T* Allocator::ConstructArray (int theSize)
{
	uint8_t* aBuffer = GetFreeBuffer <T> (theSize);
	if (aBuffer) {
		return new (aBuffer) T [theSize];
	}
	return nullptr;
}

template <typename T>	
T* Allocator::Allocate (int theSize)
{
	uint8_t* aBuffer = GetFreeBuffer <T> (theSize);
	if (aBuffer) {
		return static_cast <T*> (aBuffer);	
	}
	return nullptr;
}

template <typename T>	
bool Allocator::Deallocate (T* thePointer)
{
	int anAllocatedBlockIndex;
 	auto aBlock = myMBC.FindAllocatedBlock (reinterpret_cast <const uint8_t*> (thePointer), anAllocatedBlockIndex);
	if (aBlock.IsNull()) {
		return false;
	}
	
	myMBC.RemoveAllocatedBlock (anAllocatedBlockIndex);
	return myMBC.AddFreeBlock (aBlock);
}

template <typename T>	
bool Allocator::DeallocateArray (T* thePointer)
{
	return Deallocate (thePointer);
}

}
	
namespace Base {
	
template <typename T, class ...Types>
inline T* Allocator::Construct (Types&& ...theArgs)
{ return STM32Lib::Allocator::Construct <T> (std::forward <Types> (theArgs)...); }
	
template <typename T>
inline T* Allocator::ConstructArray (int theSize)
{ return STM32Lib::Allocator::ConstructArray <T> (theSize); }

template <typename T>
inline T* Allocator::Allocate (int theSize)
{ return STM32Lib::Allocator::Allocate <T> (theSize); }

template<typename T>
inline bool Allocator::Deallocate (T* thePointer)
{ return STM32Lib::Allocator::Deallocate <T> (thePointer); }

template<typename T>
inline bool Allocator::DeallocateArray (T* thePointer)
{ return STM32Lib::Allocator::DeallocateArray <T> (thePointer); }	
	
}
	
}


#endif
