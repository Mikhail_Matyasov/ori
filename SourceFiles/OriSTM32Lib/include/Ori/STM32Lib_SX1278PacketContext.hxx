#ifndef _Ori_STM32Lib_SX1278PacketContext_HeaderFile
#define _Ori_STM32Lib_SX1278PacketContext_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/STM32Lib_SX1278CommandType.hxx>

namespace Ori {
namespace STM32Lib {

class SX1278PacketContext
{
public:	
    SX1278PacketContext();
	void SetData (const uint8_t* theData, int theLength);
	void Nulify();
	bool IsNull() const;

    __ORI_PROPERTY (ByteArray, Buffer);
    __ORI_PRIMITIVE_PROPERTY (SX1278CommandType, Command);
    __ORI_PRIMITIVE_PROPERTY (bool, ReadyToSend);
};

}}

#endif
