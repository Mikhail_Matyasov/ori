#ifndef _Ori_STM32Lib_ESP8266Configurator_HeaderFile
#define _Ori_STM32Lib_ESP8266Configurator_HeaderFile

#include <Ori/Base_String.hxx>

namespace Ori {
namespace STM32Lib {
class USART;

class ESP8266Configurator
{
public:
	ESP8266Configurator (const USART& theUSART);
	void ConfigureOption (const char* theCommand);
	void SetBaudRate (int theBaudRate);
	String ReadMacAdress();
	
private:
	void SendCommand (const char* theCommand);
	
private:
	const USART& myUSART;
};

}}

#endif