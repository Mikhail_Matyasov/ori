#ifndef _Ori_STM32Lib_Macros_HeaderFile
#define _Ori_STM32Lib_Macros_HeaderFile

#include <stdint.h>
#include <Ori/STM32Lib_Pch.hxx>
	
//----------------------------------------	
	
#ifdef __ORI_STM32LIB
extern uint32_t __heap_start, __heap_end;	
#define __ORI_HEAP_START &__heap_start
#define __ORI_HEAP_SIZE (uint8_t*)&__heap_end - (uint8_t*)&__heap_start
#define __ORI_INIT_HEAP(POINTER) \
	POINTER = reinterpret_cast <uint8_t*> (__ORI_HEAP_START)
#else
#define __ORI_HEAP_SIZE 15360
#define __ORI_INIT_HEAP(POINTER) \
	if (!POINTER) { \
		POINTER = new uint8_t [__ORI_HEAP_SIZE]; \
	}
#endif
//----------------------------------------

#define __ORI_CURRENT_STACK_POSITION ({\
		void* p = NULL;\
		(void*) &p;\
	})
	
#ifdef __ORI_STM32LIB	
	#define __ORI_TASK(TASK_NAME, BODY) \
		extern "C" { \
			void TASK_NAME (void* theParams) \
			{ \
				BODY \
			} \
		}
#endif#ifndef __ORI_STM32LIB	#define __ORI_TASK(TASK_NAME, BODY)#endif

#endif
