#ifndef _Ori_STM32Lib_Helper_HeaderFile
#define _Ori_STM32Lib_Helper_HeaderFile

#include <stdint.h>
#include <Ori/Base_String.hxx>

namespace Ori {
namespace STM32Lib {

class Helper
{
public:

template <typename T>
static bool ReadBit (T theValue, int theBitNumber)
{
	uint16_t aMask = 1 << theBitNumber;
	return (theValue & aMask) != 0;
}		
	
};

}}

#endif
