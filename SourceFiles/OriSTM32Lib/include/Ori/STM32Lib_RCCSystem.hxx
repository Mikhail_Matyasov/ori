#ifndef _Ori_STM32Lib_RCCSystem_HeaderFile
#define _Ori_STM32Lib_RCCSystem_HeaderFile


namespace Ori {
namespace STM32Lib {

class RCCSystem
{
public:
    static void Init();
	static void Reset();
};

}}


#endif

