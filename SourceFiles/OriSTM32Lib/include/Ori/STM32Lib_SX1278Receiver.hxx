#ifndef _Ori_STM32Lib_SX1278Receiver_HeaderFile
#define _Ori_STM32Lib_SX1278Receiver_HeaderFile

#include <Ori/STM32Lib_SX1278.hxx>
#include <Ori/Base_Macros.hxx>
#include <Ori/STM32Lib_TaskHelper.hxx>
#include <Ori/STM32Lib_RingBuffer.hxx>

namespace Ori {
namespace STM32Lib {

class SX1278Receiver : public SX1278::Observer
{
public:
	void OnDIO0Interrupt() override;
	void ReceivePacket();
	void ProcessInputPackets();
	void AllocateRxBuffer (int theSize);
	
	__ORI_PROPERTY (TaskHandle_t, ReceivePacketTask);
protected:
	SX1278Receiver();
	
private:
	TaskHandle_t myProcessInputPacketsTask;
	RingBuffer myBuffer;
};

}}

#endif
