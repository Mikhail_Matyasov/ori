#ifndef _Ori_STM32Lib_Logger_HeaderFile
#define _Ori_STM32Lib_Logger_HeaderFile

#include <Ori/Base_ILogger.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/STM32Lib_USART.hxx>

namespace Ori {
namespace STM32Lib {

class UartLogger : public ILogger
{
public:
	void Print (const String& theMessage) const override;
	static UartLogger* Instance();
	
	__ORI_PROPERTY (Base::Logger, Logger);
	
private:
	UartLogger();
	
private:
	USART myUart;
	friend class Ori::STM32Lib::Allocator;
};
	
}}

#define ORI_LOG ORI_LOG_DEBUG (Ori::STM32Lib::UartLogger::Instance()->Logger())

#endif