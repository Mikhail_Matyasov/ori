#ifndef _Ori_Base_Pch_HeaderFile
#define _Ori_Base_Pch_HeaderFile


#include <string>
#include <stdint.h>
#include <functional>

#define __ORI_USE_USER_ALLOCATOR
#include <Ori/STM32Lib_Allocator.hxx>

#endif
