#ifndef _Ori_STM32Lib_SX1278Listener_HeaderFile
#define _Ori_STM32Lib_SX1278Listener_HeaderFile

#include <stdint.h>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/STM32Lib_SX1278Receiver.hxx>
#include <Ori/STM32Lib_SX1278Packet.hxx>

namespace Ori {
namespace STM32Lib {
	
class SX1278Listener : public SX1278Receiver
{
public:
	void OnPacket (const SX1278Packet& thePacket);
	void SendReport();
	void EnableReport();
	void DisableReport();
	void AllocatePacketStatistic();
	void DestroyPacketStatistic();
    static SX1278Listener* Instance();

	__ORI_PRIMITIVE_PROPERTY (TaskHandle_t, SendReportTask);
	__ORI_PRIMITIVE_PROPERTY (bool, ProcessReport);
	__ORI_PRIMITIVE_PROPERTY (bool, ProcessVideoData);
	
private:
	void SendVideoDataToDevice (const uint8_t* theData, int theLength);
	void SendPacketToDevice (const uint8_t* theData, int theLength);
	void SendAcknowledgementToDevice();
    SX1278Listener();
	
private:
	// that array contains statistic for 100 senders
	Vector <uint16_t> myPacketStatistic;
	friend class STM32Lib::Allocator;

};

}}

#endif
