#ifndef _Ori_STM32Lib_ManchesterReader_HeaderFile
#define _Ori_STM32Lib_ManchesterReader_HeaderFile

namespace Ori {
namespace STM32Lib {

class ManchesterReader
{
public:
	static void Init();
	static void OnBit (bool theBit);
};
	
}}

#endif