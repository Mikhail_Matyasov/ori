#ifndef _Ori_STM32Lib_Pch_HeaderFile
#define _Ori_STM32Lib_Pch_HeaderFile

#include <Ori/Base_Pch.hxx>
#include <Ori/STM32Lib_Allocator.hxx>

#ifdef __ORI_STM32LIB
#include <FreeRTOS.h>
#include <task.h>
#include <FreeRTOSConfig.h>
#endif

#endif
