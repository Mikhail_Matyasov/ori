#ifndef _Ori_STM32Lib_SX1278_HeaderFile
#define _Ori_STM32Lib_SX1278_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/STM32Lib_Allocator.hxx>
#include <stdint.h>

namespace Ori {
namespace STM32Lib {
class SPI;
	
class SX1278
{
public:
	enum class RoleType
	{
		None,
		Transmitter,
		Receiver
	};
	
	enum class SpreadingFactorType : uint8_t
	{
		SF6 = 6,			
		SF7,			
		SF8,			
		SF9,			
		SF10,			
		SF11,			
		SF12
	};
	
	enum class SignalBandwidthType : uint8_t
	{
		SB_7_8 = 0,
		SB_10_4,
		SB_15_6,
		SB_20_8,
		SB_31_2,
		SB_41_7,
		SB_62_5,
		SB_125,
		SB_250,
		SB_500
	};
	
	enum class TxPowerType : uint8_t
	{
		TP0 = 0,
		TP1,
		TP2,
		TP3,
		TP4,
		TP5,
		TP6,
		TP7,
		TP8,
		TP9,
		TP10,
		TP11,
		TP12,
		TP13,
		TP14,
		TP15,
		TP16,
		TP17,
		TP18,
		TP19,
		TP20
	};
	
	enum class CodingRateType : uint8_t
	{
		CR5 = 5,
		CR6,
		CR7,
		CR8
	};
	
	class Observer
	{
	public:	
		virtual void OnDIO0Interrupt() = 0;
	};
	void WriteRegister (uint8_t theRegister, uint8_t theValue);
	uint8_t ReadRegister (uint8_t theRegister);
	void RegisterObserver (Observer* theSX1278Observer);
	static SX1278* Instance();
	void Sleep();
	void StandBy();
	__ORI_PRIMITIVE_PROPERTY (RoleType, Role);
	~SX1278();
	
private:
	SX1278();
	void SetFrequency();
	void SetTxPower (TxPowerType thePower);
	void SetSignalBandwidth (SignalBandwidthType theBandwidth);
	void SetSpreadingFactor (SpreadingFactorType theFactor);
	void SetLdoFlag();
	void ConfigureForMaxDistance();
	void SetOCP (uint8_t theMA);
	void SetCodingRate (CodingRateType theRate);
	
private:
	SPI* mySPI;
	SignalBandwidthType mySignalBandwidth;
	friend class Ori::STM32Lib::Allocator;
	
};

}}

#endif