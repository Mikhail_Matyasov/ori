#ifndef _Ori_STM32Lib_ESP8266_HeaderFile
#define _Ori_STM32Lib_ESP8266_HeaderFile

#include <Ori/STM32Lib_USART.hxx>
#include <Ori/STM32Lib_RingBuffer.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <stdint.h>

namespace Ori {
namespace Embed {
class ESP8266Packet;
}
namespace STM32Lib {
// AT commands - http://room-15.github.io/blog/2015/03/26/esp8266-at-command-reference/
class ESP8266
{
public:
	void Start();
	bool Send (const Embed::ESP8266Packet& theCommand);
	static ESP8266* Instance();
	
private:
	bool SendDataLength (const Embed::ESP8266Packet& theCommand) const;
	ESP8266();
	
private:
	USART myUSART;
	friend class Ori::STM32Lib::Allocator;
};
	
}}

#endif
