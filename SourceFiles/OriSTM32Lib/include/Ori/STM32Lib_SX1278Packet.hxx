#ifndef _Ori_STM32Lib_SX1278Packet_HeaderFile
#define _Ori_STM32Lib_SX1278Packet_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/STM32Lib_SX1278CommandType.hxx>
#include <Ori/Embed_Macros.hxx>

#define __ORI_SX1278_MAX_PARTICIPANTS_COUNT (__ORI_EMBED_SX1278_MAX_DATA_LENGTH / 2)
#define __ORI_SX1278_BROADCAST_TARGET_ID __ORI_SX1278_MAX_PARTICIPANTS_COUNT // max participants + 1

namespace Ori {
namespace STM32Lib {

class SX1278Packet
{
public:
	
	class HeaderType
	{
		// Header length = 32 bit
		
	public:	
		__ORI_PRIMITIVE_PROPERTY (SX1278CommandType, Command); // 8 bit
		__ORI_PRIMITIVE_PROPERTY (uint8_t,			 DataLength); // 7 bit
		__ORI_PRIMITIVE_PROPERTY (uint8_t,			 SenderID); // 6 bit
		__ORI_PRIMITIVE_PROPERTY (uint8_t,			 TargetID); // 6 bit
		__ORI_PRIMITIVE_PROPERTY (uint8_t,			 PacketID); // 4 bit
		// 1 bit reserver
		
		void FromByteArray (const Base::ByteArrayView& theSource);
		ByteArray ToByteArray() const;
	};
	
	SX1278Packet();
	SX1278Packet (uint8_t theDataLength);
	SX1278Packet (const Base::ByteArrayView& theSource);
	
	bool IsValid();
	
	__ORI_PROPERTY (HeaderType, Header);
	__ORI_PROPERTY (Base::ByteArrayView, Data);
};

}}

#endif
