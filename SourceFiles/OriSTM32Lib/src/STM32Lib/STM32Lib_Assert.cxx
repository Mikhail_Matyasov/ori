#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_Assert.hxx>

#ifdef __ORI_STM32LIB
#include <Ori/STM32Lib_GPIO.hxx>
#include <stm32f103xb.h>
#include <Ori/STM32Lib_Timer.hxx>
#endif

namespace Ori {
namespace STM32Lib {
void Assert::Raise (const char* theMessage)
{
#ifdef __ORI_STM32LIB
	__ORI_GPIO_OUTPUT_50MHZ_SIMPL_PUSH_PULL (GPIOC, CRH, 13);
	GPIO::Port aPC13 (*GPIOC, 13);
	while (true) {
		Timer::Delay (100);
		aPC13.High();
		Timer::Delay (100);
		aPC13.Low();
	}
#endif
}	
void Assert::True (bool theExpression)
{
    if (!theExpression) {
        Raise ("Not True");
    }
}

void Assert::False (bool theExpression)
{
	if (theExpression) {
		Raise ("Not False");
	}
}

}}
