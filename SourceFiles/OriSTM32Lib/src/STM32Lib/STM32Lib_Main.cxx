#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_System.hxx>

extern "C" {
int main ()
{
	Ori::STM32Lib::System aSystem;
	aSystem.Init();
	aSystem.Start();
}

}