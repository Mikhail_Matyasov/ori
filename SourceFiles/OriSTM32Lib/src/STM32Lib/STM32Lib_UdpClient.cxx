#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_UdpClient.hxx>

#include <Ori/Base_Allocator.hxx>
#include <Ori/STM32Lib_SX1278Transmitter.hxx>

namespace Ori {
namespace STM32Lib {
	
void UdpClient::Send (const uint8_t* theData, int theDataLength, SX1278CommandType theCmdType)
{
	Assert::True (myTransmitter);
	myTransmitter->SendUdpPacket (theData, theDataLength, theCmdType);
}
	
void UdpClient::SendAcknowledgement (uint8_t theTargetID)
{
	Assert::True (myTransmitter);
	myTransmitter->SendAcknowledgement (theTargetID);
}	
	
UdpClient* UdpClient::Instance()
{
	static UdpClient* anInstance = Allocator::Construct <UdpClient>();
	return anInstance;
}
	
}}
