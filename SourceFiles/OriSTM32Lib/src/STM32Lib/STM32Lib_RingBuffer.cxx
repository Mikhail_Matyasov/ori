#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_RingBuffer.hxx>
#include <Ori/Base_Helper.hxx>

#include <Ori/STM32Lib_Assert.hxx>

#include <string.h>

namespace Ori {
namespace STM32Lib {

RingBuffer::RingBuffer() :
	myDataLength (0),
	myDataEnd (0) // next element after last element
{}


void RingBuffer::Push (uint8_t theElement)
{	
	myBuffer[myDataEnd] = theElement;
	if (myDataEnd == static_cast <int> (myBuffer.Lenght() - 1)) {
		myDataEnd = 0;
	} else {
		myDataEnd++;
	}
	if (myDataLength < static_cast <int> (myBuffer.Lenght())) {
		myDataLength++;
	}
}
	
int RingBuffer::Find (const char* theString) const
{
	const uint8_t* aBegin = reinterpret_cast <const uint8_t*> (theString);
	size_t aStringLength = strlen (theString);
	const uint8_t* anEnd = aBegin + aStringLength;
	if (myDataEnd - myDataLength >= 0) {
		int aRes = Base::Helper::Find (&myBuffer [myDataEnd - myDataLength],
			                           &myBuffer [myDataEnd],
			                           aBegin,
			                           anEnd);
		return ToGlobalIndex (aRes);
	}
		
	int aFirstArrayLength = myDataLength - myDataEnd;
	int aFirstArrayBegin = myBuffer.Lenght() - aFirstArrayLength;
	for (int i = aFirstArrayBegin; i < myBuffer.Lenght(); i++) {
		if (myBuffer[i] == aBegin[0]) {

			size_t aNumberOfEqualSymbols = 0;
			int aLengthTillEnd = myBuffer.Lenght() - i;
			for (int j = 1, k = i + 1; j < aLengthTillEnd; j++, k++) {
				if (myBuffer [k] != aBegin [j]) {
					break;
				}
				aNumberOfEqualSymbols++;
			}
		    if (aLengthTillEnd == aNumberOfEqualSymbols + 1) {
	            for (int j = 0; j < myDataEnd; j++) {
                    if (myBuffer[j] != aBegin [aNumberOfEqualSymbols + 1]) {
	                    break;
                    }
		            aNumberOfEqualSymbols++;
	            }
		    }
			if (aNumberOfEqualSymbols == aStringLength - 1) {
				return static_cast <int> (i);
			}
		}
	}
		
	// find sequence in second array
	int aRes = Base::Helper::Find (&myBuffer [0],
			                       &myBuffer [myDataEnd],
			                       aBegin,
			                       anEnd);
	return aRes;
}
	
void RingBuffer::Consume (int theDataLength)
{
	if (theDataLength > myDataLength) {
		theDataLength = myDataLength;
	}
	myDataLength -= theDataLength;
	if (myDataLength == 0) {
		myDataEnd = 0; // reset buffer
	}
}
	
void RingBuffer::ConsumeUpTo (const uint8_t* theLast)
{
	Iterator anIt (*this);
	const uint8_t* aFirstArrayEnd = &myBuffer[anIt.FirstBegin() + anIt.FirstLength() - 1];
	const uint8_t* aFirstArrayBegin = &myBuffer[anIt.FirstBegin()];
	
	int aConsumeLength = 0;
	if (aFirstArrayBegin <= theLast && aFirstArrayEnd >= theLast) {
		aConsumeLength = static_cast <int> (theLast - aFirstArrayBegin + 1);
	} else {
		int aSecondConsumedLength = theLast - &myBuffer[0] + 1;
		aConsumeLength = anIt.FirstLength() + aSecondConsumedLength;
	}
	
	Consume (aConsumeLength);
}
	
void RingBuffer::ConsumeUpTo (int theIndex)
{
	const uint8_t* aLast = &myBuffer.Data() [theIndex];
	ConsumeUpTo (aLast);
}
	
void RingBuffer::Clear()
{
	myDataEnd = 0;
	myDataLength = 0;
}	
	
RingBuffer::Iterator::Iterator (const RingBuffer& theBuffer, int theStartPos) :
	myBuffer (theBuffer),
	myFirstBegin (0),
	myFirstLength (0),
	mySecondBegin (0),
	mySecondLength (0)
{
	if (myBuffer.myDataEnd - myBuffer.myDataLength >= 0) {
		myFirstBegin = myBuffer.myDataEnd - myBuffer.myDataLength;
		myFirstLength = myBuffer.myDataLength;
	} else {
		myFirstLength = myBuffer.myDataLength - myBuffer.myDataEnd;
		myFirstBegin = static_cast <int> (myBuffer.myBuffer.Lenght() - myFirstLength);
		mySecondLength = myBuffer.myDataEnd;
	}

	if (theStartPos != -1) {
		
		if (theStartPos < myFirstBegin + myFirstLength && theStartPos >= myFirstBegin) {
			myFirstLength -= (theStartPos - myFirstBegin);
			myFirstBegin = theStartPos;
		} else {
			mySecondBegin = theStartPos;
			mySecondLength -= (theStartPos - mySecondBegin);
			myFirstLength = 0;
		}
	}
	myFirstCurrent = myFirstBegin;
	mySecondCurrent = mySecondBegin;
}
	
const uint8_t* RingBuffer::Iterator::Next()
{
	while (myFirstCurrent != myFirstBegin + myFirstLength) {
		return &myBuffer.myBuffer[myFirstCurrent++];
	}
	
	while (mySecondCurrent != mySecondBegin + mySecondLength) {
		return &myBuffer.myBuffer[mySecondCurrent++];
	}
	return nullptr;
}
	
int RingBuffer::Iterator::ActiveBuffer()
{
	if (myFirstCurrent != myFirstBegin + myFirstLength) {
		return 0;
	}
	
	if (mySecondCurrent != mySecondBegin + mySecondLength) {
		return 1;
	}
	return -1;
}
	
void RingBuffer::Allocate (int theBufferSize)
{
	myDataLength = 0;
	myDataEnd = 0;
	myBuffer.Clear();
	myBuffer.Resize (theBufferSize);
}
	
void RingBuffer::Push (const uint8_t* theData, int theDataSize)
{
	for (int i = 0; i < theDataSize; i++) {
		Push (theData[i]);
	}
}
	
int RingBuffer::ToGlobalIndex (int theLocalIndex) const
{
	if (theLocalIndex >= myDataLength || theLocalIndex < 0) {
		return -1;
	}
	
	Iterator anIt (*this);
	if (anIt.FirstLength() > theLocalIndex) {
		return anIt.FirstBegin() + theLocalIndex;
	}
	
	return theLocalIndex - anIt.FirstLength();
}


int RingBuffer::ToLocalIndex (int theGlobalIndex) const
{	
	Iterator anIt (*this);
	if (anIt.FirstBegin() <= theGlobalIndex && (anIt.FirstBegin() + anIt.FirstLength()) > theGlobalIndex) { // inside first array
		return theGlobalIndex - anIt.FirstBegin();
	} else if (anIt.SecondBegin() <= theGlobalIndex && (anIt.SecondBegin() + anIt.SecondLength()) > theGlobalIndex) {
		return theGlobalIndex + anIt.FirstLength();
	}
	
	return -1;
}	
	
}}
