#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_GPIO.hxx>

#include <stm32f103xb.h>

namespace Ori {
namespace STM32Lib {
namespace {

void DoReset (GPIO_TypeDef& theGPIO)
{
	theGPIO.BRR = 0;
	theGPIO.BSRR = 0;
	theGPIO.CRH = 0;
	theGPIO.CRL = 0;
	theGPIO.IDR = 0;
	theGPIO.LCKR = 0;
	theGPIO.ODR = 0;
}
	
}	
	
void GPIO::Reset()
{
	DoReset (*GPIOA);
	DoReset (*GPIOB);
	DoReset (*GPIOC);
	DoReset (*GPIOD);
	DoReset (*GPIOE);
}	

GPIO::Port::Port (GPIO_TypeDef& theGPIO, int thePort) :
	myGPIO (theGPIO),
	myPort (thePort)
{
	myIsHigh = false;
}

GPIO::Port::Port (const Port& theOther) :
	myGPIO (theOther.myGPIO),
	myPort (theOther.myPort),
	myIsHigh (theOther.myIsHigh)
{}	

void GPIO::Port::High()
{
	myIsHigh = true;
	myGPIO.ODR |= 1 << myPort;
}

void GPIO::Port::Low()
{
	myIsHigh = false;
	myGPIO.ODR &= ~(1 << myPort);
}
	
void GPIO::Port::Toggle()
{
	if (myIsHigh) {
		Low();
	} else {
		High();
	}
}
	
bool GPIO::Port::Value()
{
	return myGPIO.IDR & (1 << myPort);
}
	
}}
