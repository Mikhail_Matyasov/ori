#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_SX1278Listener.hxx>

#include <Ori/STM32Lib_SX1278Packet.hxx>
#include <Ori/STM32Lib_TcpClient.hxx>
#include <Ori/STM32Lib_UdpClient.hxx>
#include <Ori/STM32Lib_ESP8266.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>
#include <Ori/STM32Lib_Timer.hxx>
#include <Ori/STM32Lib_TaskHelper.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/STM32Lib_GPIO.hxx>

#include <stm32f103xb.h>

using namespace Ori::Embed;

__ORI_TASK (Ori_STM32Lib_TcpListener_SendReport, // from interrupt
	
	auto aListener = Ori::STM32Lib::SX1278Listener::Instance();
	while (true) {
		aListener->SendReport();
		vTaskSuspend (nullptr);
	}
)
	
extern "C" {
void TIM3_IRQHandler()
{
	TIM3->SR &= ~TIM_SR_UIF; // reset interrupt flag
	xTaskResumeFromISR (Ori::STM32Lib::SX1278Listener::Instance()->SendReportTask());
}
}	

namespace Ori {
namespace STM32Lib {
	
void SX1278Listener::EnableReport()
{
	Timer::SetInterruptPeriod (TIM3, 2000);
	
	TIM3->DIER |= TIM_DIER_UIE; // enable interrupt from timer
	TIM3->CR1 |= TIM_CR1_CEN; // start timer
	
	NVIC_EnableIRQ (IRQn_Type::TIM3_IRQn);
}
	
void SX1278Listener::DisableReport()
{
	TIM3->CR1 &= ~TIM_CR1_CEN; // stop timer
}
	
void SX1278Listener::AllocatePacketStatistic()
{
	myPacketStatistic.Resize (__ORI_SX1278_MAX_PARTICIPANTS_COUNT);
	for (int i = 0; i < __ORI_SX1278_MAX_PARTICIPANTS_COUNT; i++) {
		myPacketStatistic [i] = 0;
	}
}
	
void SX1278Listener::DestroyPacketStatistic()
{
	myPacketStatistic.Clear();
}
	
SX1278Listener::SX1278Listener() :
	myPacketStatistic (0)
{
	mySendReportTask = TaskHelper::Create (Ori_STM32Lib_TcpListener_SendReport,
										   "Ori_STM32Lib_TcpListener_SendReport");
	vTaskSuspend (mySendReportTask);
	__ORI_GPIO_OUTPUT_50MHZ_SIMPL_PUSH_PULL (GPIOC, CRH, 13);
}

GPIO::Port thePort (*GPIOC, 13);
	
void SX1278Listener::OnPacket (const SX1278Packet& thePacket)
{
	auto aCmd = thePacket.Header().Command();
	if (aCmd == SX1278CommandType::Report && myProcessReport) {
		TcpClient::Instance()->OnReport (thePacket);
	} else if (aCmd == SX1278CommandType::VideoData && myProcessVideoData) {
	
		uint16_t& aSenderStatistic = myPacketStatistic [thePacket.Header().SenderID()];
		uint16_t aMask = 1 << thePacket.Header().PacketID();
		aSenderStatistic |= aMask;	   
		SendVideoDataToDevice (thePacket.Data().Data(), thePacket.Data().Lenght());
	} else if (aCmd == SX1278CommandType::UdpPacket) {
		
		thePort.Toggle();
		SendPacketToDevice (thePacket.Data().Data(), thePacket.Data().Lenght());
	} else if (aCmd == SX1278CommandType::TcpPacket) {
		
		SendPacketToDevice (thePacket.Data().Data(), thePacket.Data().Lenght());
		UdpClient::Instance()->SendAcknowledgement (thePacket.Header().SenderID());
	} else if (aCmd == SX1278CommandType::Acknowledgement) {
		if (thePacket.Header().TargetID() == TcpClient::Instance()->SenderID()) {
			SendAcknowledgementToDevice();
		}
    } else {
		Assert::Raise ("Unsupported command type");
	}
}

void SX1278Listener::SendAcknowledgementToDevice()
{
	ESP8266Packet aPacket;
	aPacket.Command() = CommandType::SX1278ListenerOnAcknowledgement;
	
	auto aWifi = ESP8266::Instance();
	aWifi->Send (aPacket);
}
	
void SX1278Listener::SendVideoDataToDevice (const uint8_t* theData, int theLength)
{
	ESP8266Packet aPacket;
	aPacket.Command() = CommandType::SX1278ListenerOnVideoData;
	aPacket.Data() = ByteArrayView (theData, theLength);
	
	auto aWifi = ESP8266::Instance();
	aWifi->Send (aPacket);
}	

void SX1278Listener::SendPacketToDevice (const uint8_t* theData, int theLength)
{
	ESP8266Packet aPacket;
	aPacket.Command() = CommandType::SX1278ListenerOnPacket;
	aPacket.Data() = ByteArrayView (theData, theLength);
	
	auto aWifi = ESP8266::Instance();
	aWifi->Send (aPacket);
}
	
void SX1278Listener::SendReport()
{
	const uint8_t* aReport = reinterpret_cast <const uint8_t*> (myPacketStatistic.Data());
	int aLength = __ORI_SX1278_MAX_PARTICIPANTS_COUNT * sizeof (uint16_t);
	UdpClient::Instance()->Send (aReport, aLength, SX1278CommandType::Report);
}
	
SX1278Listener* SX1278Listener::Instance()
{
	static SX1278Listener* anInstance = Allocator::Construct <SX1278Listener>();
	return anInstance;
}	
	
}}
