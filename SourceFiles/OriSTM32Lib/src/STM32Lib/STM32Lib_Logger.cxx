#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_Logger.hxx>

#include <Ori/Base_Allocator.hxx>

namespace Ori {
namespace STM32Lib {

UartLogger::UartLogger() :
	myLogger (this),
	myUart (USART::USARTType::USART_3)
{}	
	
void UartLogger::Print (const String& theMessage) const
{
	myUart.Println (theMessage.Data());	
}
	
UartLogger* UartLogger::Instance()
{
	static UartLogger* aLogger = Base::Allocator::Construct <UartLogger>();
	return aLogger;
}	
	
}}
