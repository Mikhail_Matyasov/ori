#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_Timer.hxx>

#include <math.h>

extern "C" {
uint64_t theTimer2Counter = 0;	
void TIM2_IRQHandler()
{
	TIM2->SR &= ~TIM_SR_UIF; // reset interrupt flag
	theTimer2Counter++;
}
}

namespace Ori {
namespace STM32Lib {
namespace {
	
void InitTimer2()
{	
	Timer::SetInterruptPeriod (TIM2, 1); // timer will raise each 1 nanoseconds
	   
	TIM2->DIER |= TIM_DIER_UIE; // enable interrupt from timer
	TIM2->CR1 |= TIM_CR1_CEN; // start timer;
	
	NVIC_EnableIRQ (IRQn_Type::TIM2_IRQn);
}

	
const int theClock = 36000000;
const uint16_t thePrescaler = static_cast <uint16_t> ((1 << 16) - 1); // max value due PSK register is 16 bit

}
	
// Count of timer updates in seconds = TIM_CLK / ((PSC + 1) * (ARR + 1))
	
void Timer::SetInterruptPeriod (TIM_TypeDef* theTimer, int thePeriodInMilliSeconds)
{	
	uint16_t anUpdateFrequency = theClock / thePrescaler; // 1098 time per second
	theTimer->PSC = thePrescaler; // 1000 Hz
	
	uint32_t anAutoReload = static_cast <uint32_t> ((theClock / (1000.0 * thePrescaler)) * thePeriodInMilliSeconds);
	Assert::True (anAutoReload <= thePrescaler);
	theTimer->ARR = anAutoReload == 0 ? 1 : anAutoReload;
}	
	
void Timer::Init()
{
	InitTimer2();
}	
	
void Timer::Delay (int theDelay, Units theDelayUnit)
{
	if (theDelayUnit == Units::Milliseconds) {
		uint64_t aTargetTime = theTimer2Counter + theDelay;
		if (aTargetTime < theTimer2Counter) { // occured overflow
			theTimer2Counter = 0;
			aTargetTime = theDelay;
		}
		while (theTimer2Counter < aTargetTime);
	}
}
	

uint64_t Timer::Millis()
{
	return theTimer2Counter;
}
	
bool Timer::DoWhile (const std::function <bool()>& theFunc, int theMilis)
{
	uint64_t aStopTime = theTimer2Counter + theMilis;
	while (theTimer2Counter < aStopTime) {
		bool aRes = theFunc();
		if (aRes) {
			return true;
		}
	}
	
	return false;
}	
	
}}
