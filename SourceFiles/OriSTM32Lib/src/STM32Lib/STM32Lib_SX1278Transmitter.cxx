#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_SX1278Transmitter.hxx>
#include <Ori/STM32Lib_TcpClient.hxx>

#include <Ori/STM32Lib_Macros.hxx>
#include <Ori/STM32Lib_SX2178Registers.hxx>
#include <Ori/STM32Lib_Assert.hxx>
#include <Ori/STM32Lib_TaskHelper.hxx>
#include <Ori/STM32Lib_SX1278Listener.hxx>
#include <Ori/Embed_Macros.hxx>

#include <FreeRTOS.h>
#include <task.h>

__ORI_TASK (Ori_STM32Lib_SX1278TransmitterTask, 
	
	auto aTransmitter = Ori::STM32Lib::TcpClient::Instance();
	while (true) {
		aTransmitter->FlushCacheBuffer();
		vTaskSuspend (nullptr);
	}
)

namespace Ori {
namespace STM32Lib {
	
SX1278Transmitter::SX1278Transmitter() :
	mySenderID (0),
	myPacketCache (0)
{
	myTask = TaskHelper::Create (Ori_STM32Lib_SX1278TransmitterTask,
								 "Ori_STM32Lib_SX1278TransmitterTask",
								 128, 3);
	vTaskSuspend (myTask);
	SX1278::Instance(); // Init SX1278
}
	
void SX1278Transmitter::AllocateTxBuffer (int theSize)
{
	myPacketCache.Clear();
	myPacketCache.Resize (theSize);
}

void SX1278Transmitter::SendPacket (const SX1278PacketContext& thePacket, uint8_t thePacketID)
{
	if (thePacket.Buffer().Lenght() == 0) {
		return;	
	}
	
	DoSend (thePacket.Buffer().Data(),
			thePacket.Buffer().Lenght(),
			thePacket.Command(),
			0,
			thePacketID);
}
	
void SX1278Transmitter::FlushCacheBuffer()
{
	for (int i = 0; i < myPacketCache.Lenght(); i++) {
		
		auto& aCache = myPacketCache [i];
		if (aCache.ReadyToSend()) {
			SendPacket (aCache, i);
			aCache.ReadyToSend() = false;
		}
	}
}
	

void SX1278Transmitter::BeginPacket()
{	
	auto aLoRa = SX1278::Instance();
	aLoRa->StandBy();
	
	// set header mode
	uint8_t aConfig1 = aLoRa->ReadRegister (REG_MODEM_CONFIG_1);
	aLoRa->WriteRegister (REG_MODEM_CONFIG_1, aConfig1 & REG_MODEM_CONFIG_1_EXPLICIT_HEADER_MODE);
	
	// reset fifo
	aLoRa->WriteRegister (REG_FIFO_ADDR_PTR, 0);
	aLoRa->WriteRegister (REG_PAYLOAD_LENGTH, 0);
}
	
void SX1278Transmitter::OnDIO0Interrupt()
{
	myTxDone = true;
}	

void SX1278Transmitter::FlushPacket()
{
	myTxDone = false;
	auto aLoRa = SX1278::Instance();
	
	// put into tx mode
    aLoRa->WriteRegister (REG_OP_MODE,
						  REG_OP_MODE_LONG_RANGE_MODE | REG_OP_MODE_TX);
	// wait till transmitting finished
	while (IsTransmitting());
}

void SX1278Transmitter::DoSend (const uint8_t* theData,
							    uint8_t theDataLength,
							    SX1278CommandType theCommand,
							    uint8_t theTargetID,
							    uint8_t thePacketID)
{
	taskENTER_CRITICAL();
	{
		BeginPacket();
	
		auto aLoRa = SX1278::Instance();
		// write header
		SX1278Packet::HeaderType aHeader;
		aHeader.Command() = theCommand;
		aHeader.DataLength() = theDataLength;
		aHeader.PacketID() = thePacketID;
		aHeader.SenderID() = mySenderID;
		aHeader.TargetID() = theTargetID;
		ByteArray aHeaderBytes = aHeader.ToByteArray();
		for (uint8_t aByte : aHeaderBytes) {
			aLoRa->WriteRegister (REG_FIFO, aByte);
		}
	
		for (int i = 0; i < theDataLength; i++) {
			aLoRa->WriteRegister (REG_FIFO, theData[i]);
		}
		aLoRa->WriteRegister (REG_PAYLOAD_LENGTH, theDataLength + aHeaderBytes.Lenght()); // set data length
	
		FlushPacket();
	}
	taskEXIT_CRITICAL();
}
	
void SX1278Transmitter::SendUdpPacket (const uint8_t* theData,
									   uint8_t theDataLength,
									   SX1278CommandType theCmdType)
{
	Assert::True (theDataLength <= __ORI_EMBED_SX1278_MAX_DATA_LENGTH);
	DoSend (theData, theDataLength, theCmdType);
}
	
void SX1278Transmitter::SendAcknowledgement (uint8_t theTargetID)
{
	uint8_t aDefaultData;
	DoSend (&aDefaultData, 1, SX1278CommandType::Acknowledgement, theTargetID);
}	
	
bool SX1278Transmitter::IsTransmitting()
{
	auto aLoRa = SX1278::Instance();
	uint8_t aRegOpMode = aLoRa->ReadRegister (REG_OP_MODE);
	return aRegOpMode != (REG_OP_MODE_LONG_RANGE_MODE | REG_OP_MODE_STDBY);
}

}}
