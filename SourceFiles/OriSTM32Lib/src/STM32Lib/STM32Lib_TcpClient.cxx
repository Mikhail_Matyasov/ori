#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_TcpClient.hxx>

#include <Ori/STM32Lib_UdpClient.hxx>
#include <Ori/STM32Lib_ESP8266.hxx>
#include <Ori/Base_Allocator.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/Embed_Macros.hxx>
#include <Ori/STM32Lib_Helper.hxx>

using namespace Ori::Base;
using namespace Ori::Embed;

namespace Ori {
namespace STM32Lib {

TcpClient::TcpClient() : SX1278Transmitter()
{
	UdpClient::Instance()->Transmitter() = this;
}

namespace {	
	
}
	
void TcpClient::OnReport (const SX1278Packet& theReport)
{
	const uint16_t* aReport = reinterpret_cast <const uint16_t*> (theReport.Data().Data());
	uint16_t aMyReport = aReport [this->mySenderID];
	
	uint16_t anAvaliableBufferSpace = 0;
	for (int i = 0; i < __ORI_SX1278_PACKET_CACHE_LENGTH; i++) {
		bool anIsPacketReceived = Helper::ReadBit (aMyReport, i);
		auto& aCache = this->myPacketCache [i];
		if (anIsPacketReceived) {
			anAvaliableBufferSpace += aCache.Buffer().Lenght();
			aCache.Nulify();
		} else {
			if (!aCache.IsNull()) {
				aCache.ReadyToSend() = true;
			}
		}
	}
	
	if (anAvaliableBufferSpace != 0) {
		
		ESP8266Packet aPacket;
		aPacket.Command() = CommandType::TcpClientFreeBufferSpaceAvaliable;
		
		TypeBuffer <uint16_t> aBuffer (anAvaliableBufferSpace);
		aPacket.Data() = ByteArrayView (aBuffer.Bytes, aBuffer.Bytes + sizeof (uint16_t));
		
		auto aWiFi = ESP8266::Instance();
		aWiFi->Send (aPacket);
	}
}

void TcpClient::SendVideoData (const uint8_t* theData, int theDataLength)
{
	taskENTER_CRITICAL();
	{
		int aPacketCount = theDataLength / __ORI_EMBED_SX1278_MAX_DATA_LENGTH;
		int anAddedPacketCount = 0;
		
		int i = 0;
		for (; i < this->myPacketCache.Lenght(); i++) {
			auto& aPacketContext = this->myPacketCache [i];
			if (aPacketContext.IsNull()) {
				aPacketContext.Command() = SX1278CommandType::VideoData;
				aPacketContext.SetData (theData + anAddedPacketCount * __ORI_EMBED_SX1278_MAX_DATA_LENGTH,
										__ORI_EMBED_SX1278_MAX_DATA_LENGTH);
				
				aPacketContext.ReadyToSend() = true;
				aPacketCount--;
				anAddedPacketCount++;
				if (aPacketCount == 0) {
					i++;
					break;		
				}
			}
		}
		
		int aRemainder = theDataLength % __ORI_EMBED_SX1278_MAX_DATA_LENGTH;
		if (aRemainder != 0) {
			for (; i < this->myPacketCache.Lenght(); i++) {
				auto& aPacketContext = this->myPacketCache [i];
				if (aPacketContext.IsNull()) {
					aPacketContext.Command() = SX1278CommandType::VideoData;
					aPacketContext.SetData (theData + anAddedPacketCount * __ORI_EMBED_SX1278_MAX_DATA_LENGTH, aRemainder);
					aPacketContext.ReadyToSend() = true;
					break;
				}
			}
		}
	}
	taskEXIT_CRITICAL();
}
	
void TcpClient::SendPacket (const uint8_t* theData, int theDataLength)
{
	UdpClient::Instance()->Send (theData, theDataLength, SX1278CommandType::TcpPacket);
}	
	
TcpClient* TcpClient::Instance()
{
	static TcpClient* anInstance = Allocator::Construct <TcpClient>();
	return anInstance;
}
	
}}
