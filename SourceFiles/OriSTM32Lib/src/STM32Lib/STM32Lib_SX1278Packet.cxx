#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_SX1278Packet.hxx>

#include <Ori/STM32Lib_Helper.hxx>
#include <Ori/Embed_Macros.hxx>

#ifdef __ORI_STM32LIB
#include <Ori/STM32Lib_TcpClient.hxx>				  
#endif

namespace Ori {
namespace STM32Lib {

SX1278Packet::SX1278Packet() {}
	
SX1278Packet::SX1278Packet (uint8_t theDataLength)
{
	myHeader.Command() = SX1278CommandType::TcpPacket;
	myHeader.DataLength() = theDataLength;
	myHeader.SenderID() = 0;
	myHeader.TargetID() = 0;
	myHeader.PacketID() = 0;
}	
	
void SX1278Packet::HeaderType::FromByteArray (const Base::ByteArrayView& theSource)
{
	uint8_t aFirstByte = theSource [0];
	uint8_t aSecondByte = theSource [1];
	uint8_t aThirdByte = theSource [2];
	uint8_t aFourthByte = theSource [3];
	
	myCommand = static_cast <SX1278CommandType> (aFirstByte);
	myDataLength = static_cast <uint8_t> (aSecondByte >> 1);
	
	// sender id
	bool aBit = Helper::ReadBit (aSecondByte, 0);
	mySenderID = static_cast <uint8_t> (aBit) << 5;
	uint8_t aSenderIDSecondPart = aThirdByte >> 3;
	mySenderID |= aSenderIDSecondPart;
	
	// target id
	uint8_t aFirstPartTargetID = (aThirdByte & 0b00000111) << 3;
	uint8_t aSecondPartTargetID = (aFourthByte & 0b11100000) >> 5;
	myTargetID = aFirstPartTargetID | aSecondPartTargetID;
	
	myPacketID = (aFourthByte & 0b00011110) >> 1;
}
	
SX1278Packet::SX1278Packet (const Base::ByteArrayView& theSource)
{
	if (theSource.Lenght() < 5) {
		return;
	}
	
	myHeader.FromByteArray (theSource);
	
	myData.SetBegin (&theSource[4]);
	myData.SetEnd (theSource.end());
}
	
bool SX1278Packet::IsValid()
{
	bool aRes = myHeader.DataLength() == myData.Lenght();
#ifdef __ORI_STM32LIB
	aRes &= (TcpClient::Instance()->SenderID() == myHeader.TargetID());
#endif
	return aRes;
}
	
ByteArray SX1278Packet::HeaderType::ToByteArray() const
{
	ByteArray aHeader (4);
	aHeader.Push (static_cast <uint8_t> (myCommand));
	
	uint8_t aSecondByte = myDataLength << 1;
	aSecondByte |= static_cast <uint8_t> (Helper::ReadBit (mySenderID, 5));
	aHeader.Push (aSecondByte);
	
	uint8_t aThirdByte = (mySenderID & 0b00011111) << 3;
	aThirdByte |= (myTargetID & 0b00111000) >> 3;
	aHeader.Push (aThirdByte);
	
	uint8_t aFourthByte = (myTargetID & 0b00000111) << 5;
	aFourthByte |= (myPacketID & 0b00001111) << 1;
	aHeader.Push (aFourthByte);
	
	return aHeader;
}
	
}}
