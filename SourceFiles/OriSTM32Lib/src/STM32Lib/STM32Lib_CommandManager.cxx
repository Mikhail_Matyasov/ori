#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_CommandManager.hxx>

#include <Ori/Base_String.hxx>
#include <Ori/Base_Helper.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <Ori/STM32Lib_Macros.hxx>
#include <Ori/STM32Lib_Helper.hxx>
#include <Ori/STM32Lib_ESP8266.hxx>
#include <Ori/STM32Lib_TaskHelper.hxx>
#include <Ori/STM32Lib_Assert.hxx>
#include <Ori/STM32Lib_TaskManager.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>
#include <Ori/STM32Lib_SX1278Listener.hxx>
#include <Ori/STM32Handler_Blink.hxx>
#include <Ori/STM32Handler_SX1278Transmitter.hxx>
#include <Ori/STM32Handler_Configuration.hxx>
#include <Ori/STM32Lib_UdpClient.hxx>

#ifdef __ORI_STM32LIB
#include <FreeRTOS.h>
#include <task.h>
#endif

using namespace Ori::Embed;
using namespace Ori::Base;

#ifdef __ORI_STM32LIB
Ori::STM32Lib::CommandManager* theManager = nullptr;
__ORI_TASK (Ori_STM32Lib_CommandManager_ProcessCommand,
	
	auto aTaskManager = Ori::STM32Lib::TaskManager::Instance();
	while (true) {
		if (theManager) {
			ESP8266Packet aPacket;
			bool aRes = theManager->ExtractCommand (aPacket);
			
			if (aRes) {
				if (!theManager->ProcessSingleCommand (aPacket)) {
					theManager->ProcessCommand (aPacket);
				}
				
				// remove all data releted to processed command
				if (aPacket.Data().Empty()) {
					theManager->ConsumeLastSingleCommand();
				} else {
					theManager->ConsumeLastCommand (aPacket.Data().end());
				}
			}
		}
		aTaskManager->ConfigurePriority();
		vTaskDelay (100);
	}
)
#endif

namespace Ori {
namespace STM32Lib {
	
CommandManager::CommandManager()
{
#ifdef __ORI_STM32LIB
	theManager = this;
	auto aTask = TaskHelper::Create (Ori_STM32Lib_CommandManager_ProcessCommand,
									 "Ori_STM32Lib_CommandManager_ProcessCommand", 600, 2);
	TaskManager::Instance()->AddTask (aTask);
#endif
}
	
void CommandManager::AllocateBuffer (int theSize)
{
	myBuffer.Allocate (theSize);
}	
	
void CommandManager::OnData (const uint8_t* theData, int theLength)
{
	myBuffer.Push (theData, theLength);
}
	
bool CommandManager::DoProcessSingleCommand (const ESP8266Packet& thePacket,
											 CommandType theExpectedCommand,
											 const std::function <void()> theHandler) const
{
	if (thePacket.Command() == theExpectedCommand) {
		if (theHandler) {
			theHandler();
			bool aRes = SendReply (thePacket.ID(), CommandType::Success);
			return true;
		}
	}
	return false;
}	
	
bool CommandManager::ProcessSingleCommand (const ESP8266Packet& thePacket) const
{
#ifdef __ORI_STM32LIB	
	if (thePacket.Data().Empty()) { // simple command
		
		if (DoProcessSingleCommand (thePacket, CommandType::Blink, [&]() -> void {
			STM32Handler::Blink::Execute (BlinkType::Simple);
		})) {} else if (DoProcessSingleCommand (thePacket, CommandType::ConfigureAsClient, [&]() -> void {
			STM32Handler::Configuration::ConfigureAsClient();
		})) {} else if (DoProcessSingleCommand (thePacket, CommandType::ConfigureAsServer, [&]() -> void {
			STM32Handler::Configuration::ConfigureAsServer();
		})) {} else { return false; }
	}
#endif
	return false;
}	
	
void CommandManager::ProcessCommand (const ESP8266Packet& thePacket) const
{	
#ifdef __ORI_STM32LIB
	if (thePacket.Command() == CommandType::UdpClientSendPacket ||
        thePacket.Command() == CommandType::TcpClientSendPacket ||
	    thePacket.Command() == CommandType::TcpClientSendVideoData) {
		    
		STM32Handler::SX1278Transmitter::Send (thePacket);
		if (thePacket.Command() == CommandType::UdpClientSendPacket) {
			SendReply (thePacket.ID(), CommandType::Success);
		}
   }
	
#endif
}	
	
bool CommandManager::ExtractCommand (ESP8266Packet& thePacket)
{
//	uint8_t aData = 1;
//	UdpClient::Instance()->Send (&aData, 1);
//	vTaskDelay (5000);
	
	if (myBuffer.DataLength() < 3) { // id + command
		return false;
	}
	
	RingBuffer::Iterator anIt (myBuffer);
	thePacket.ID() = *anIt.Next();

	TypeBuffer <uint16_t> aBuffer;
	aBuffer.Bytes[0] = *anIt.Next();
	aBuffer.Bytes[1] = *anIt.Next();
	thePacket.Command() = static_cast <CommandType> (aBuffer.Value);

	if (myBuffer.DataLength() < 6) { // id + command + data length + simple data
		return true;
	}
	TypeBuffer <uint16_t> aDataLengthBuffer;
	aDataLengthBuffer.Bytes [0] = *anIt.Next();
	aDataLengthBuffer.Bytes [1] = *anIt.Next();
	uint16_t aDataLength = aDataLengthBuffer.Value;
	
	const uint8_t* aDataStart = anIt.Next();
	thePacket.Data().SetBegin (aDataStart);
	thePacket.Data().SetEnd (aDataStart + aDataLength);
	return true;
}
	
void CommandManager::ConsumeLastCommand (const uint8_t* theDataEnd)
{
	myBuffer.ConsumeUpTo (theDataEnd);
}
	
void CommandManager::ConsumeLastSingleCommand()
{
	myBuffer.Consume (3); // id + command
}
	
bool CommandManager::SendReply (uint8_t theID, CommandType theReply) const
{
	ESP8266Packet aCommand (theID);
	aCommand.Command() = CommandType::Reply;

	TypeBuffer <uint16_t> aDataBuffer (static_cast <uint16_t> (theReply));
	aCommand.Data().SetBegin (aDataBuffer.Bytes);
	aCommand.Data().SetEnd (aDataBuffer.Bytes + 2);
	
#ifdef __ORI_STM32LIB
	auto aWiFi = ESP8266::Instance();
	return aWiFi->Send (aCommand);
#else
	return false;
#endif
}
	
CommandManager* CommandManager::Instance()
{
	static CommandManager* anInstance = Allocator::Construct <CommandManager>();
	return anInstance;
}	
	
}}
