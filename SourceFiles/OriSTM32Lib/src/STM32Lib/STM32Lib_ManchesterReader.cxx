#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_ManchesterReader.hxx>

#include <Ori/STM32Lib_GPIO.hxx>
#include <Ori/STM32Lib_Timer.hxx>
#include <Ori/STM32Lib_RingBuffer.hxx>

#include <stm32f103xb.h>

extern "C" {
uint64_t CurrentPulseDuration = 0;
int64_t LastPulseDuration = -1;
	
// That pulse need to encode few same sequence bit (111 or 000)
// Duration was established empirically
uint16_t ShortPulseDuration = 70;
uint16_t NumberOfNoise = 0;
uint16_t NumberOfShortPulseDuration = 0;
	
Ori::STM32Lib::GPIO::Port DataPin (*GPIOB, 1);	
bool LastPulse = false;
bool IsSynchronized = false;
	
	
void Sync()
{
	if (CurrentPulseDuration > ShortPulseDuration) {
		IsSynchronized = true;
	}
}	
	
using Reader = Ori::STM32Lib::ManchesterReader;	
Ori::STM32Lib::RingBuffer* Buffer;
	
void EXTI1_IRQHandler()
{
	EXTI->PR = EXTI_PR_PR1;
	
	bool aCurrentPulse = DataPin.Value();
	if (aCurrentPulse == LastPulse) {
		// noise
		NumberOfNoise++;
		return;
	}
	
	if (!IsSynchronized) {
		Sync();
	} else {
		
		if (LastPulseDuration == -1) {
			LastPulseDuration = CurrentPulseDuration;
		} else {
			
			if (Buffer->DataLength() == Buffer->Buffer().Capacity()) {
				Buffer->Clear();
			}
			
			if (CurrentPulseDuration > ShortPulseDuration) {
				Buffer->Push (1); // long
			} else {
				Buffer->Push (0); // short
			}
			
			if (LastPulseDuration > ShortPulseDuration ||
				CurrentPulseDuration > ShortPulseDuration) {
				Reader::OnBit (LastPulse);
				LastPulseDuration = CurrentPulseDuration;
			} else { // two short pulse
				NumberOfShortPulseDuration++;
				LastPulseDuration = -1; // skip next pulse
				Reader::OnBit (aCurrentPulse);
			}
		}
	}
		
	LastPulse = aCurrentPulse;
	CurrentPulseDuration = 0;
}
	
uint64_t theEllapsed = 0;	
void TIM4_IRQHandler()
{
	CurrentPulseDuration++;
	TIM4->SR &= ~TIM_SR_UIF; // reset interrupt flag
}	

}

namespace Ori {
namespace STM32Lib {		
namespace {
void ConfigureEXTI()
{
	__ORI_GPIO_INPUT_FLOATING (GPIOB, CRL, 1); // raw data pin
	
	AFIO->EXTICR[0] |= AFIO_EXTICR1_EXTI1_PB;
	
	EXTI->RTSR |= EXTI_RTSR_TR1; // Interrupt on high impuls
	EXTI->FTSR |= EXTI_FTSR_TR1; // Interrupt on low impuls
	
	EXTI->PR = EXTI_PR_PR1; // reset interrupt 0
	EXTI->IMR |= EXTI_IMR_MR1; // enable interrupt 0
	NVIC_EnableIRQ (IRQn_Type::EXTI1_IRQn);
}
	
// Count of timer updates in seconds = TIM_CLK / ((PSC + 1) * (ARR + 1))	
void ConfigureTimer()
{
	TIM4->PSC = 0; // 1 MHz
	TIM4->ARR = 1;
	TIM4->RCR = 0;
	
	TIM4->DIER |= TIM_DIER_UIE; // enable interrupt from timer
	TIM4->CR1 |= TIM_CR1_CEN; // start timer
	
	NVIC_EnableIRQ (IRQn_Type::TIM4_IRQn);
}	
}	
	
void ManchesterReader::Init()
{
	Buffer = Allocator::Construct <RingBuffer>();
	Buffer->Allocate (2000);
	ConfigureEXTI();
	ConfigureTimer();
}

void ManchesterReader::OnBit (bool theBit)
{	
	if (theBit) {
		
	} else {
		
	}
}
	
}}
