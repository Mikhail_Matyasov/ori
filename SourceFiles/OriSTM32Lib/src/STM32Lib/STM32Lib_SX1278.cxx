#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_SX1278.hxx>

#include <Ori/STM32Lib_Macros.hxx>
#include <Ori/STM32Lib_SX2178Registers.hxx>
#include <Ori/STM32Lib_SX1278Transmitter.hxx>
#include <Ori/STM32Lib_Assert.hxx>
#include <Ori/STM32Lib_SPI.hxx>

#include <math.h>
#include <FreeRTOS.h>
#include <task.h>

#define SX1278_CORRECT_VERSION 0x12

extern "C" {
Ori::STM32Lib::SX1278::Observer* theSX1278Observer = nullptr;
void EXTI0_IRQHandler()
{
	EXTI->PR = EXTI_PR_PR0;
	
	// writing a 1 clears interrupt
	auto anInstance = Ori::STM32Lib::SX1278::Instance();
	uint8_t anIrqMask = anInstance->ReadRegister (REG_IRQ_FLAGS);
	anInstance->WriteRegister (REG_IRQ_FLAGS, anIrqMask);
	
	if (theSX1278Observer) {
		theSX1278Observer->OnDIO0Interrupt();
	}
}
}

namespace Ori {
namespace STM32Lib {

uint8_t SX1278::ReadRegister (uint8_t theRegister)
{
	mySPI->NSS().Low();
	mySPI->SendByte (theRegister);
	uint8_t aByte = mySPI->SendByte (0);
	mySPI->NSS().High();
	return aByte;
}

void SX1278::WriteRegister (uint8_t theRegister, uint8_t theValue)
{
	mySPI->NSS().Low();
	mySPI->SendByte (theRegister | 0x80);
	mySPI->SendByte (theValue);
	mySPI->NSS().High();
}
	
void SX1278::RegisterObserver (SX1278::Observer* theObserver)
{
	theSX1278Observer = theObserver;
	if (dynamic_cast <SX1278Transmitter*> (theSX1278Observer)) {
		myRole = RoleType::Transmitter;
	} else {
		myRole = RoleType::Receiver;
	}
}	

void SX1278::SetFrequency()
{
	int aFrequency = 433000000.0; // 433Mhz
	double anFxosc = 32000000.0; // 32Mhz, SX1278 crystal oscillator frequency
	uint64_t aFrf = (static_cast <uint64_t> (aFrequency) << 19) / anFxosc;
	
	uint8_t anMsb = static_cast <uint8_t> (aFrf >> 16);
	uint8_t aMid = static_cast <uint8_t> (aFrf >> 8);
	uint8_t anLsb = static_cast <uint8_t> (aFrf);
	
	WriteRegister (REG_FRF_MSB, anMsb);
	WriteRegister (REG_FRF_MID, aMid);
	WriteRegister (REG_FRF_LSB, anLsb);
}
	
void SX1278::SetTxPower (TxPowerType thePower)
{
	uint8_t aPower = static_cast <uint8_t> (thePower);
	if (thePower > TxPowerType::TP17) {
		
		// subtract 3 from level, so 18 - 20 maps to 15 - 17
		aPower -= 3;
		
		// High Power +20 dBm Operation (Semtech SX1276/77/78/79 5.4.3.)
        WriteRegister (REG_PA_DAC, REG_PA_DAC_PA_BOOST_ON);
		SetOCP (140);
	} else {
		
		//Default value PA_HF/LF or +17dBm
        WriteRegister (REG_PA_DAC, 0x84);
		SetOCP (100);
	}
	
	WriteRegister (REG_PA_CONFIG, REG_PA_CONFIG_PA_BOOST | (aPower - 2));
}
	
namespace {

int ToNumericBandwidth (SX1278::SignalBandwidthType theBandwidth)
{
	switch (theBandwidth) {
	case SX1278::SignalBandwidthType::SB_7_8:
		return 7800;
	case SX1278::SignalBandwidthType::SB_10_4:
		return 10400;
	case SX1278::SignalBandwidthType::SB_15_6:
		return 15600;
	case SX1278::SignalBandwidthType::SB_20_8:
		return 20800;
	case SX1278::SignalBandwidthType::SB_31_2:
		return 31200;
	case SX1278::SignalBandwidthType::SB_41_7:
		return 41700;
	case SX1278::SignalBandwidthType::SB_62_5:
		return 62500;
	case SX1278::SignalBandwidthType::SB_125:
		return 125000;	
	case SX1278::SignalBandwidthType::SB_250:
		return 250000;
	default:
		return 500000;
	}	
}	
	
}	
	
void SX1278::SetLdoFlag()
{
	  // Section 4.1.1.5
	int aSignalBandwidth = ToNumericBandwidth (mySignalBandwidth);
	uint8_t aSpreadingFactor = ReadRegister (REG_MODEM_CONFIG_2) >> 4;
    int aSymbolDuration = 1000 / (aSignalBandwidth / (1 << aSpreadingFactor)) ;
    
    // Section 4.1.1.6
    uint8_t aConfig3 = ReadRegister (REG_MODEM_CONFIG_3);
	if (aSymbolDuration > 16) {
		aConfig3 |= 0x08; // set 3-rd bit
	} else {
		aConfig3 &= ~0x08; // reset 3-rd bit
	}
    WriteRegister (REG_MODEM_CONFIG_3, aConfig3);
}
	
void SX1278::SetSignalBandwidth (SignalBandwidthType theBandwidth)
{
	mySignalBandwidth = theBandwidth;
	uint8_t aRegValue = ReadRegister (REG_MODEM_CONFIG_1);
	uint8_t aBw = static_cast <uint8_t> (theBandwidth);
	WriteRegister (REG_MODEM_CONFIG_1, (aRegValue & 0x0f) | (aBw << 4));
	SetLdoFlag();
}
	
void SX1278::SetSpreadingFactor (SpreadingFactorType theFactor)
{ 
    if (theFactor == SpreadingFactorType::SF6) {
      WriteRegister (REG_DETECTION_OPTIMIZE, 0xc5);
      WriteRegister (REG_DETECTION_THRESHOLD, 0x0c);
    } else {
      WriteRegister (REG_DETECTION_OPTIMIZE, 0xc3);
      WriteRegister (REG_DETECTION_THRESHOLD, 0x0a);
    }
  
	uint8_t aRegModemConfig2 = ReadRegister (REG_MODEM_CONFIG_2);
	uint8_t aSF = static_cast <uint8_t> (theFactor);
    WriteRegister (REG_MODEM_CONFIG_2, (aRegModemConfig2 & 0x0f) | ((aSF << 4) & 0xf0));
    SetLdoFlag();
}		
	
namespace {
void EnableDIO0Interrupt()
{
	__ORI_GPIO_INPUT_FLOATING (GPIOA, CRL, 0);
	
	GPIOA->ODR |= 1; // set PA0 to HIGH
	AFIO->EXTICR[0] &= ~(AFIO_EXTICR1_EXTI0);
	
	EXTI->RTSR |= EXTI_RTSR_TR0; // Interrupt on high impuls
	
	EXTI->PR = EXTI_PR_PR0; // reset interrupt 0
	EXTI->IMR |= EXTI_IMR_MR0; // enable interrupt 0
	NVIC_EnableIRQ (IRQn_Type::EXTI0_IRQn);
	
	// From that interrupt will be invoked ISR function, so freertos requires
	// set priority above then configMAX_SYSCALL_INTERRUPT_PRIORITY
	NVIC_SetPriority (IRQn_Type::EXTI0_IRQn, configMAX_SYSCALL_INTERRUPT_PRIORITY + 1);
}
}

SX1278::~SX1278()
{
	Allocator::Deallocate (mySPI);
}
	
void SX1278::ConfigureForMaxDistance()
{
	//SetSignalBandwidth (SignalBandwidthType::SB_125);
	SetSpreadingFactor (SpreadingFactorType::SF12);
	SetTxPower (TxPowerType::TP20);
	SetCodingRate (CodingRateType::CR8);
}	
	
SX1278::SX1278()
{
	mySPI = Allocator::Construct <SPI> (SPI::SPIType::SPI_1);
	EnableDIO0Interrupt();
	
	while (ReadRegister (REG_VERSION) != SX1278_CORRECT_VERSION);
	
	Sleep();
	SetFrequency();

	// enable full duplex mode
	WriteRegister (REG_FIFO_TX_BASE_ADDR, 0);
	WriteRegister (REG_FIFO_RX_BASE_ADDR, 128);
	
	uint8_t aLna = ReadRegister (REG_LNA);
	WriteRegister (REG_LNA, aLna | REG_LNA_BOOST_HF_ON);
	WriteRegister (REG_MODEM_CONFIG_3, REG_MODEM_CONFIG_3_AGC_AUTO_ON);
	ConfigureForMaxDistance();
	
	StandBy();
	myRole = RoleType::None;
}

void SX1278::Sleep()
{
	WriteRegister (REG_OP_MODE, REG_OP_MODE_LONG_RANGE_MODE | REG_OP_MODE_SLEEP);
}


void SX1278::StandBy()
{
	WriteRegister (REG_OP_MODE, REG_OP_MODE_LONG_RANGE_MODE | REG_OP_MODE_STDBY);
}
	
SX1278* SX1278::Instance()
{
	static SX1278* anInstance = Allocator::Construct <SX1278>();
	return anInstance;
}
	
void SX1278::SetOCP (uint8_t theMA)
{
    uint8_t anOCPTrim = 27;
    
    if (theMA <= 120) {
      anOCPTrim = (theMA - 45) / 5;
    } else if (theMA <=240) {
      anOCPTrim = (theMA + 30) / 10;
    }
    
    WriteRegister (REG_OCP, 0x20 | (0x1F & anOCPTrim));
}	
	
}}


void Ori::STM32Lib::SX1278::SetCodingRate (CodingRateType theRate)
{	
    uint8_t aRate = static_cast <uint8_t> (theRate) - 4;
	uint8_t aConfig1 = ReadRegister (REG_MODEM_CONFIG_1);
    WriteRegister (REG_MODEM_CONFIG_1, (aConfig1 & 0xf1) | (aRate << 1));
}
