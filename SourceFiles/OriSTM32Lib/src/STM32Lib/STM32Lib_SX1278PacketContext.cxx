#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_SX1278PacketContext.hxx>

#include <Ori/Embed_Macros.hxx>

namespace Ori {
namespace STM32Lib {
		
SX1278PacketContext::SX1278PacketContext() :
	myBuffer (__ORI_EMBED_SX1278_MAX_DATA_LENGTH),
	myCommand (SX1278CommandType::VideoData),
	myReadyToSend (false)
{}
	
void SX1278PacketContext::SetData (const uint8_t* theData, int theLength)
{
	Assert::True (theLength <= __ORI_EMBED_SX1278_MAX_DATA_LENGTH);
	myBuffer.Push (theData, theLength);
}
	
void SX1278PacketContext::Nulify()
{
	myBuffer.Reset();
}

bool SX1278PacketContext::IsNull() const
{
	return myBuffer.Empty();
}
	
}}
