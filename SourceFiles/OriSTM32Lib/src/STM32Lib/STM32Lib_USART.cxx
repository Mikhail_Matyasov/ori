#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_USART.hxx>

#include <Ori/STM32Lib_Assert.hxx>
#include <Ori/Base_Helper.hxx>
#include <Ori/STM32Lib_Timer.hxx>
#include <Ori/STM32Lib_GPIO.hxx>

#include <string.h>


Ori::STM32Lib::USART::Observer* theUSART1Observer = nullptr;
extern "C" {
void USART1_IRQHandler()
{
	if (USART1->SR & USART_SR_RXNE) { // if byte received
		USART1->SR &= ~USART_SR_RXNE; // nullify status register, for next using
		if (theUSART1Observer) {
			theUSART1Observer->OnData (USART1->DR);	
		}
	}
}
}

namespace Ori {
namespace STM32Lib {	
	
void USART::ConfigureUSART1() const
{
	// Configure TX
	__ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_PUSH_PULL (GPIOA, CRH, 9);
	USART1->CR1 |= USART_CR1_TE; // enable TX

	// Configure RX
	__ORI_GPIO_INPUT_FLOATING (GPIOA, CRH, 10);
	USART1->CR1 |= USART_CR1_RE; // enable RX

	SetBaudRate (__ORI_ESP8266_DEFAULT_BAUDRATE);
	
	USART1->CR1 &= ~USART_CR1_M; // 8 bit word
	USART1->CR2 &= ~USART_CR2_STOP_Msk; // 1 stop bit
	USART1->CR1 &= ~USART_CR1_PCE; // no parity

	USART1->CR1 |= USART_CR1_UE; // enable USART
	USART1->CR1 |= USART_CR1_RXNEIE; // enable interrupt on data receiving
	NVIC_EnableIRQ (IRQn_Type::USART1_IRQn);
}
	
void USART::ConfigureUSART3() const
{
	// Configure TX
	__ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_PUSH_PULL (GPIOB, CRH, 10);
	USART3->CR1 |= USART_CR1_TE; // enable TX

	// Configure RX
	__ORI_GPIO_INPUT_FLOATING (GPIOB, CRH, 11);
	USART3->CR1 |= USART_CR1_RE; // enable RX

	SetBaudRate (115200);
	
	USART3->CR1 &= ~USART_CR1_M; // 8 bit word
	USART3->CR2 &= ~USART_CR2_STOP_Msk; // 1 stop bit
	USART3->CR1 &= ~USART_CR1_PCE; // no parity

	USART3->CR1 |= USART_CR1_UE; // enable USART
}	
	
USART::USART (USARTType theType) :
	myType (theType)
{
	if (theType == USART::USARTType::USART_1) {
		ConfigureUSART1();
	} else if (theType == USART::USARTType::USART_3) {
		ConfigureUSART3();
	} else {
		Assert::Raise ("Unsupported usart");
	}
}

void USART::Send (const uint8_t* theData, int theLength) const
{
	for (int i = 0; i < theLength; i++) {
		SendByte (theData[i]);
	}
}

void USART::Send (const char* theMessage) const
{
	int aDataLength = static_cast <int> (strlen (theMessage));
	const uint8_t* aData = reinterpret_cast <const uint8_t*> (theMessage);
	Send (aData, aDataLength);
}
	
void USART::Println (const char* theMessage) const
{
	Send (theMessage);
	SendByte ('\r');
	SendByte ('\n');
}
	
void USART::Println (char theChar) const
{
	SendByte (theChar);
	SendByte ('\r');
	SendByte ('\n');
}
	
void USART::Println (int theNumber) const
{
	auto aString = Helper::ToString (theNumber);
	Println (aString.Data());
}
	
void USART::Send (int theNumber) const
{
	auto aString = Helper::ToString (theNumber);
	Send (aString.Data());
}

void USART::RegisterObserver (Observer* theObserver) const
{
	if (myType == USART::USARTType::USART_1) {
		theUSART1Observer = theObserver;
	} else {
		Assert::Raise();
	}
}	
	
namespace {
void DoResetUSART (USART_TypeDef& theUSART)
{
	theUSART.BRR = 0;
	theUSART.CR1 = 0;
	theUSART.CR2 = 0;
	theUSART.CR3 = 0;
	theUSART.GTPR = 0;
	theUSART.SR = 0;
}		 
}
	
USART_TypeDef* USART::CurrentUSART() const
{
	if (myType == USART::USARTType::USART_1) {
		return USART1;
	} else if (myType == USART::USARTType::USART_2) {
		return USART2;
	} else {
		return USART3;
	}
}	
	
void USART::SendByte (uint8_t theByte) const
{
	// wait till previouse data will be sent
	uint64_t aCurrentTime = Timer::Millis();
	const int aTimeout = 100;
	uint64_t aStopTime = aCurrentTime + aTimeout;
	auto aUart = CurrentUSART();
	while (Timer::Millis() < aStopTime) {
		if (aUart->SR & USART_SR_TC) {
			aUart->DR = theByte; // send data
			return;
		}
	}
	
	DoResetUSART (*aUart);
	if (myType == USART::USARTType::USART_1) {
		ConfigureUSART1();
	} else if (myType == USART::USARTType::USART_3) {
		ConfigureUSART3();
	}
	SendByte (theByte);
}

void USART::Reset()
{
	DoResetUSART (*USART1);
	DoResetUSART (*USART2);
	DoResetUSART (*USART3);
}
	

void USART::StopIRQ() const
{
	CurrentUSART()->CR1 &= ~USART_CR1_RXNEIE;
}

void USART::StartIRQ() const
{
	CurrentUSART()->CR1 |= USART_CR1_RXNEIE;
}
	
void USART::SetBaudRate (int theBaudRate) const
{
	// USART speed formula: ((APB + Boudrate / 2) / Boudrate) -> convert to HEX
	// APB: Bus from which USART is clocked
	// Boudrate: desirable boudrate (9600, 19200, ...)
	
	int anApbClock = 72000000; // apb2 clock
	if (myType != USART::USARTType::USART_1) {
		anApbClock = 36000000; // apb1 clock
	}
	int aBrr = (anApbClock + theBaudRate / 2) / theBaudRate;
	CurrentUSART()->BRR = aBrr;
}
	
void USART::ChangeBaudRate (int theNewValue) const
{
	CurrentUSART()->CR1 &= ~USART_CR1_UE; // disable USART
	SetBaudRate (theNewValue);
	CurrentUSART()->CR1 |= USART_CR1_UE; // enable USART
}	
	
}}
