#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_TaskManager.hxx>

#include <Ori/Base_Helper.hxx>
#include <Ori/STM32Lib_SX1278Listener.hxx>
#include <Ori/STM32Lib_GPIO.hxx>

struct tskTaskControlBlock;
extern tskTaskControlBlock * pxCurrentTCB;

extern "C" {
void Ori_STM32Lib_TaskManager_SwitchContextIn()
{
	Ori::STM32Lib::TaskManager::Instance()->RunImmediateTask();
}
	
}

namespace Ori {
namespace STM32Lib {

TaskManager::TaskManager() :
	myImmediateTask (nullptr),
	myTasks (5),
	myInvokedTask (5)
{}
	
void TaskManager::AddTask (TaskHandle_t theTask)
{
	myTasks.Push (theTask);
	myInvokedTask.Push (false);
}
	
void TaskManager::RunImmediateTask()
{
	if (myImmediateTask) {
		// change to null in external code
		pxCurrentTCB = myImmediateTask;
	}
}	
	
TaskHandle_t TaskManager::GetNextTask (int theCurrentTaskIndex)
{	
	TaskHandle_t aTask = nullptr;
	for (int i = 0; i < myTasks.Lenght(); i++) {
		if (i == theCurrentTaskIndex) {
			myInvokedTask [i] = true;
		} else if (!aTask && i != theCurrentTaskIndex && !myInvokedTask[i]) {
			myInvokedTask [i] = true;
			aTask = myTasks [i];
		}
	}
	
	if (aTask) {
		return aTask;	
	}
	
	for (int i = 0; i < myTasks.Lenght(); i++) {
		if (i != theCurrentTaskIndex) {
			myInvokedTask[i] = false;
		}
	}
	
	return GetNextTask (theCurrentTaskIndex);
}
	
void TaskManager::ConfigurePriority()
{
	TaskHandle_t aNextTask = nullptr;
	
	if (myTasks.Empty()) {
		return;
	} else if (myTasks.Lenght() == 1) {
		aNextTask = myTasks[0];
	} else {
		auto aCurrentTask = pxCurrentTCB;
		int anIndex = myTasks.Find (aCurrentTask);
		aNextTask = GetNextTask (anIndex);
	}

	vTaskSuspendAll(); // use that insed of enter critical section
	vTaskPrioritySet (pxCurrentTCB, 0);
	vTaskPrioritySet (aNextTask, configMAX_PRIORITIES - 1);
	xTaskResumeAll();
}
	
TaskManager* TaskManager::Instance()
{
	static TaskManager* anInstance = Allocator::Construct <TaskManager>();
	return anInstance;
}
	
}}
