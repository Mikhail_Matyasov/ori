#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_SPI.hxx>
#include <Ori/STM32Lib_GPIO.hxx>

#include <stm32f103xb.h>
#include <string.h>
#include <FreeRTOS.h>

extern "C" {
Ori::STM32Lib::SPI::Observer* theSPIObserver = nullptr;
void SPI1_IRQHandler()
{
	if (SPI1->SR & SPI_SR_RXNE) { // if byte received
		SPI1->SR &= ~SPI_SR_RXNE; // nullify status register, for next using
		if (theSPIObserver) {
			theSPIObserver->OnData (static_cast <uint8_t> (SPI1->DR & 0x00FF));
		}
	}
}
}

namespace Ori {
namespace STM32Lib {
namespace {
void DoConfigureSPI (SPI_TypeDef& theSPI)
{
	theSPI.CR1 &= ~SPI_CR1_DFF; // set packet size to 8 bit
	theSPI.CR1 &= ~SPI_CR1_LSBFIRST; // set big-endian bit order
	
	theSPI.CR2 |= SPI_CR2_SSOE;
	
	// Configure NSS program control
	theSPI.CR1 |= SPI_CR1_SSM;
	theSPI.CR1 |= SPI_CR1_SSI;
	
	theSPI.CR1 |= 0x04 << SPI_CR1_BR_Pos; // set speed to (SPI clock) / 32
	theSPI.CR1 |= SPI_CR1_MSTR; // configure spi as master
	
	// Configure SPI Polarity and Clock Phase
	// https://www.corelis.com/education/tutorials/spi-tutorial/
	theSPI.CR1 &= ~SPI_CR1_CPOL; // Clock is LOW when inactive
	theSPI.CR1 |= SPI_CR1_CPHA;
}
	
void ConfigureSPI1()
{
	// Configure NSS
	__ORI_GPIO_OUTPUT_50MHZ_SIMPL_PUSH_PULL (GPIOA, CRL, 4);
	GPIOA->ODR |= 1 << 4;
	
	// Configure SCK
	__ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_PUSH_PULL (GPIOA, CRL, 5);
	
	// Configure MISO
	__ORI_GPIO_INPUT_FLOATING (GPIOA, CRL, 6);
	
	// Configure MOSI
	__ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_PUSH_PULL (GPIOA, CRL, 7);
	
	DoConfigureSPI (*SPI1);
	SPI1->CR1 |= SPI_CR1_SPE; // enable SPI
}
	
void ConfigureSPI2()
{
	// Configure NSS
	__ORI_GPIO_OUTPUT_50MHZ_SIMPL_PUSH_PULL (GPIOB, CRH, 12);
	GPIOA->ODR |= 1 << 4;
	
	// Configure SCK
	__ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_PUSH_PULL (GPIOB, CRH, 13);
	
	// Configure MISO
	__ORI_GPIO_INPUT_FLOATING (GPIOB, CRH, 14);
	
	// Configure MOSI
	__ORI_GPIO_OUTPUT_50MHZ_ALTERNATE_PUSH_PULL (GPIOA, CRH, 15);
	
	DoConfigureSPI (*SPI2);
	SPI2->CR1 |= SPI_CR1_SPE; // enable SPI
}
}
SPI::SPI (SPI::SPIType theType) :
	myType (theType),
	myNSS (*GPIOA, 4)
{
	if (theType == SPI::SPIType::SPI_1) {
		ConfigureSPI1();
	} else {
		ConfigureSPI2();
	}
}
	
void SPI::Send (const uint8_t* theData, int theLength)
{
	for (int i = 0; i < theLength; i++) {
		SendByte (theData[i]);
	}	
}
	
void SPI::Send (const char* theMessage)
{
	int aDataLength = static_cast <int> (strlen (theMessage));
	const uint8_t* aData = reinterpret_cast <const uint8_t*> (theMessage);
	Send (aData, aDataLength);
}
	
void SPI::RegisterObserver (SPI::Observer* theLocalObserver)
{
	theSPIObserver = theLocalObserver;
}
	
namespace {
void DoSendByte (SPI_TypeDef& theSPI, uint8_t theByte)
{
	while (!(theSPI.SR & SPI_SR_TXE)) {} // wait till previous byte will be sent
	theSPI.DR = theByte;
}	 
}
uint8_t SPI::SendByte (uint8_t theByte)
{
	if (myType == SPI::SPIType::SPI_1) {
		DoSendByte (*SPI1, theByte);
	} else {
		DoSendByte (*SPI2, theByte);
	}
	
	return ReadByte();
}

	
namespace {
uint8_t DoReadByte  (SPI_TypeDef& theSPI)
{
    while (!(theSPI.SR & SPI_SR_RXNE));
	return theSPI.DR;
}	 
}	
uint8_t SPI::ReadByte()
{
	if (myType == SPI::SPIType::SPI_1) {
		return DoReadByte (*SPI1);
	}
	return DoReadByte (*SPI2);
}
	
namespace {
void DoResetSPI (SPI_TypeDef& theSPI)
{
	theSPI.CR1 = 0;
	theSPI.CR2 = 0;
	theSPI.CRCPR = 0;
	theSPI.I2SCFGR = 0;
	theSPI.RXCRCR = 0;
	theSPI.SR = 0;
	theSPI.TXCRCR = 0;
}		 
}	
void SPI::Reset()
{
	DoResetSPI (*SPI1);
	DoResetSPI (*SPI2);
}
	
}}
