#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_SX1278Receiver.hxx>
#include <Ori/STM32Lib_SX1278Listener.hxx>

#include <Ori/STM32Lib_Macros.hxx>
#include <Ori/STM32Lib_SX2178Registers.hxx>
#include <Ori/STM32Lib_SX1278Packet.hxx>
#include <Ori/STM32Lib_Assert.hxx>
#include <Ori/STM32Lib_TaskManager.hxx>
#include <Ori/STM32Lib_GPIO.hxx>
#include <Ori/Embed_CommandDef.hxx>
#include <Ori/Embed_Macros.hxx>

#include <stm32f103xb.h>

using namespace Ori::Base;
using namespace Ori::Embed;

__ORI_TASK (Ori_STM32Lib_SX1278Receiver_SendPacket,
	
	auto aReceiver = Ori::STM32Lib::SX1278Listener::Instance();
	auto aTaskManager = Ori::STM32Lib::TaskManager::Instance();
	while (true) {
		aReceiver->ProcessInputPackets();
		aTaskManager->ConfigurePriority();
		vTaskDelay (100);
	}
)

extern "C" {
void Ori_STM32Lib_SX1278Receiver_RececivePacket (void* theParams)
{
	auto aReceiver = Ori::STM32Lib::SX1278Listener::Instance();
	while (true) {
		aReceiver->ReceivePacket();
		vTaskSuspend (nullptr);
	}
}
}

namespace Ori {
namespace STM32Lib {
	
SX1278Receiver::SX1278Receiver() :
	myReceivePacketTask (nullptr)
{	
	myReceivePacketTask = TaskHelper::Create (Ori_STM32Lib_SX1278Receiver_RececivePacket,
										      "Ori_STM32Lib_SX1278Receiver_RececivePacket", 256, 4);
	
	myProcessInputPacketsTask = TaskHelper::Create (Ori_STM32Lib_SX1278Receiver_SendPacket,
													"Ori_STM32Lib_SX1278Receiver_SendPacket", 256);
	
	vTaskSuspend (myReceivePacketTask);
	vTaskSuspend (myProcessInputPacketsTask);
	
	auto aLoRa = SX1278::Instance(); // Init SX1278
	aLoRa->WriteRegister (REG_DIO_MAPPING_1, REG_DIO_MAPPING_1_RX_DONE); // Map DIO0 to RX Done
	aLoRa->WriteRegister (REG_OP_MODE, REG_OP_MODE_LONG_RANGE_MODE | REG_OP_MODE_RX_CONTINUOUS);
	aLoRa->RegisterObserver (this);
}
	
void SX1278Receiver::AllocateRxBuffer (int theSize)
{
	myBuffer.Allocate (theSize);
}
	
void SX1278Receiver::OnDIO0Interrupt()
{
	TaskManager::Instance()->ImmediateTask() = myReceivePacketTask; // first
}
	
void SX1278Receiver::ReceivePacket()
{	
	auto aLoRa = SX1278::Instance();
	uint8_t aCurrentAdress = aLoRa->ReadRegister (REG_FIFO_RX_CURRENT_ADDR);
	aLoRa->WriteRegister (REG_FIFO_ADDR_PTR, aCurrentAdress);
	
	int aPacketLength = static_cast <int> (aLoRa->ReadRegister (REG_RX_NB_BYTES));
	ByteArray anArray (aPacketLength);
	for (int i = 0; i < aPacketLength; i++) {
		uint8_t aByte = aLoRa->ReadRegister (REG_FIFO);
		anArray.Push (aByte);
	}
	
	SX1278Packet aPacket (anArray);
	if (!aPacket.IsValid()) {
		TaskManager::Instance()->ImmediateTask() = nullptr;
		return;
	}
	
	if (myBuffer.DataLength() + aPacketLength > myBuffer.Buffer().Capacity()) {
		SX1278Listener::Instance()->OnPacket (aPacket);
	} else {
		myBuffer.Push (anArray.Data(), anArray.Lenght());
	}
	TaskManager::Instance()->ImmediateTask() = myProcessInputPacketsTask; // second
}	
	
void SX1278Receiver::ProcessInputPackets()
{
	if (myBuffer.DataLength() == 0) {
		TaskManager::Instance()->ImmediateTask() = nullptr;
		return;
	}
	
	RingBuffer::Iterator anIt (myBuffer);
	SX1278Packet aPacket;
	ByteArray aDataBuffer (__ORI_EMBED_SX1278_RX_BUFFER_LENGTH);
	while (const uint8_t* aNext = anIt.Next()) {
		
		aDataBuffer.Push (*aNext);
		aDataBuffer.Push (*anIt.Next());
		aDataBuffer.Push (*anIt.Next());
		aDataBuffer.Push (*anIt.Next());
		SX1278Packet::HeaderType aHeader;
		aHeader.FromByteArray (aDataBuffer);
		
		// make consecutive array
		for (int i = 0; i < aHeader.DataLength(); i++) {
			aDataBuffer.Push (*anIt.Next());
		}
		aPacket.Header() = aHeader;
		aPacket.Data() = aDataBuffer;
		SX1278Listener::Instance()->OnPacket (aPacket);
		
		// free space in tx buffer
		myBuffer.ConsumeUpTo (aPacket.Data().end());
		
		// free tmp buffer
		aDataBuffer.Reset();
		TaskManager::Instance()->ImmediateTask() = nullptr; // third
	}
}
	
}}
