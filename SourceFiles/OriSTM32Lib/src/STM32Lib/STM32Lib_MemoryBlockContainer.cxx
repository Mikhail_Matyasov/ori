#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_MemoryBlockContainer.hxx>

#include <Ori/STM32Lib_Allocator.hxx>
#include <Ori/STM32Lib_Macros.hxx>

namespace Ori {
namespace STM32Lib {
namespace {
	
void RemoveFreeBlockFromArray (int theBlockLength, int theIndex, MemoryBlock* theArray)
{
	if (theIndex != -1) {
		auto& aFreeBlock = theArray [theIndex];
		if (aFreeBlock.Length() > theBlockLength) { // shift start of free block
			aFreeBlock.Length() -= theBlockLength;
			aFreeBlock.Buffer() += theBlockLength;
		} else {
			aFreeBlock.Length() = 0;
			aFreeBlock.Buffer() = nullptr;
		}	
	}
}	
	
}	
	
MemoryBlockContainer::MemoryBlockContainer() :
    myFreeBlocks (nullptr),
    myFreeBlocksLength (0),
    myFreeBlocksCapacity (0),
	myAllocatedBlocks (nullptr),
	myAllocatedBlocksLength (0),
	myAllocatedBlocksCapacity (0),
	myHeapSize (static_cast <int> (__ORI_HEAP_SIZE)),
	myHeap (nullptr)
{}

MemoryBlock MemoryBlockContainer::FindFreeBlock (int theSize, int& theFreeBlockIndex)
{
    int aMinBufferSize = myHeapSize;
	theFreeBlockIndex = -1;
    for (int i = 0; i < myFreeBlocksLength; i++) {
	    auto aFreeBlock = myFreeBlocks[i];
        if (aFreeBlock.Length() >= theSize && aFreeBlock.Length() < aMinBufferSize) {
	        theFreeBlockIndex = i;
	        aMinBufferSize = aFreeBlock.Length();
        }
    }
    if (theFreeBlockIndex == -1) {
	    // no free memory
	    return MemoryBlock();
    }
	
	auto aFreeBlock = myFreeBlocks [theFreeBlockIndex];
	MemoryBlock aRes (aFreeBlock.Buffer(), theSize);
	return aRes;
}

bool MemoryBlockContainer::AddFreeBlock (const MemoryBlock& theBlock)
{
	bool aRes = DoAddFreeBlock (theBlock);
	return aRes;
}

MemoryBlock MemoryBlockContainer::FindAllocatedBlock (const uint8_t* theBuffer, int& theAllocatedBlockIndex)
{
	theAllocatedBlockIndex = -1;
	for (int i = 0; i < myAllocatedBlocksLength; i++) {
		if (myAllocatedBlocks[i].Buffer() == theBuffer) {
			theAllocatedBlockIndex = i;
			return myAllocatedBlocks [i];
		}
	}
	return MemoryBlock();
}
	
bool MemoryBlockContainer::AddAllocatedBlock (const MemoryBlock& theBlock)
{
	bool aRes = DoAddAllocatedBlock (theBlock);	
	return aRes;
}
	
void MemoryBlockContainer::AllocateContainer()
{
	__ORI_INIT_HEAP (myHeap);
	
	myAllocatedBlocksCapacity = 10;
	myFreeBlocksCapacity = myAllocatedBlocksCapacity;
		
	int aLength = sizeof (MemoryBlock) * myAllocatedBlocksCapacity;
		
	MemoryBlock anAlocatedBlocksMemory (myHeap, aLength);
	MemoryBlock aFreeBlocksMemory (myHeap + aLength, aLength);
	myAllocatedBlocks = reinterpret_cast <MemoryBlock*> (anAlocatedBlocksMemory.Buffer());
	myFreeBlocks =  reinterpret_cast <MemoryBlock*> (aFreeBlocksMemory.Buffer());
	DoAddAllocatedBlock (anAlocatedBlocksMemory);
	DoAddAllocatedBlock (aFreeBlocksMemory);
	DoAddFreeBlock (MemoryBlock (myHeap + aLength * 2, myHeapSize - aLength * 2));
}
	
void MemoryBlockContainer::RemoveFreeBlock (int theBlockLength, int theIndex)
{
	RemoveFreeBlockFromArray (theBlockLength, theIndex, myFreeBlocks);
}


void MemoryBlockContainer::RemoveAllocatedBlock (int theIndex)
{
	if (theIndex != -1) {
		auto& aBlock = myAllocatedBlocks [theIndex];
		aBlock.Buffer() = nullptr;
		aBlock.Length() = 0;
	}
}
	
void MemoryBlockContainer::MoveFreeBlocks (const MemoryBlock& theBlock)
{
	// change FreeBlocks location
	MemoryBlock aCurBlock = MemoryBlock (reinterpret_cast <uint8_t*> (myFreeBlocks),
										 myFreeBlocksCapacity * sizeof (MemoryBlock)); // we should create block with old values
	
	myFreeBlocks = reinterpret_cast <MemoryBlock*> (theBlock.Buffer());
	myFreeBlocksCapacity = theBlock.Length() / sizeof (MemoryBlock);
	
	for (int i = 0; i < myAllocatedBlocksLength; i++) {
		auto anAlBlock = myAllocatedBlocks [i];
		if (anAlBlock.Buffer() == aCurBlock.Buffer())	{
			RemoveAllocatedBlock (i);
			break;
		}
	}
	DoAddFreeBlock (aCurBlock);
	DoAddAllocatedBlock (theBlock);
}


void MemoryBlockContainer::MoveAllocatedBlocks (const MemoryBlock& theBlock)
{
	// change AllocatedBlocks location
	MemoryBlock aCurBlock = MemoryBlock (reinterpret_cast <uint8_t*> (myAllocatedBlocks),
										 myAllocatedBlocksCapacity * sizeof (MemoryBlock)); // we should create block with old values
	
	myAllocatedBlocks = reinterpret_cast <MemoryBlock*> (theBlock.Buffer());
	myAllocatedBlocksCapacity = theBlock.Length() / sizeof (MemoryBlock);
	
	// remove old allocated block
	for (int i = 0; i < myAllocatedBlocksLength; i++) {
		if (myAllocatedBlocks [i].Buffer() == aCurBlock.Buffer())	{
			RemoveAllocatedBlock (i);
			break;
		}
	}
	DoAddFreeBlock (aCurBlock);
	DoAddAllocatedBlock (theBlock);
}	

bool MemoryBlockContainer::DoAddFreeBlock (const MemoryBlock& theBlock)
{	
	if (myFreeBlocksLength == myFreeBlocksCapacity) {
		for (int i = 0; i < myFreeBlocksLength; i++) {
			auto& aBlock = myFreeBlocks[i];
			if (aBlock.IsNull()) {
				aBlock.Buffer() = theBlock.Buffer();
				aBlock.Length() = theBlock.Length();
				return true;
			}
		}
		if (!ReallocateFreeBlocks()) {
			return false;
		}
	}
	
	myFreeBlocks [myFreeBlocksLength++] = theBlock;
	return true;
}

bool MemoryBlockContainer::DoAddAllocatedBlock (const MemoryBlock& theBlock)
{			
	for (int i = 0; i < myAllocatedBlocksLength; i++) {
		auto& aBlock = myAllocatedBlocks [i];
		if (aBlock.IsNull()) {
			aBlock.Buffer() = theBlock.Buffer();
			aBlock.Length() = theBlock.Length();
			return true;
		}
	}
	
	if (myAllocatedBlocksLength == myAllocatedBlocksCapacity) {
		if (!ReallocateAllocatedBlocks()) {
			return false;
		}	
	}
	
	myAllocatedBlocks [myAllocatedBlocksLength] = theBlock;
	myAllocatedBlocks [myAllocatedBlocksLength].Buffer()[0] = '\0';
	myAllocatedBlocksLength++;
	return true;
}
	
MemoryBlock MemoryBlockContainer::ReallocateBlocks (int theBlockLength,
										            MemoryBlock* theExistingBlock,
											        int theExistingBlockLength,
													int& theBlockIndex)
{
	int aNewSize = static_cast <int> (theBlockLength * sizeof (MemoryBlock) * 1.5) + 1;
	auto aFreeBlock = FindFreeBlock (aNewSize, theBlockIndex);
	if (aFreeBlock.IsNull()) {
		// We take 2 MemoryBlock to store new location of 'BlocksArray' and "Block", which caused reallocation
		int aMinNewSize = static_cast <int> (theBlockLength + sizeof (MemoryBlock) * 2);
		aFreeBlock = FindFreeBlock (aMinNewSize, theBlockIndex);
		if (aFreeBlock.IsNull()) {
			return MemoryBlock();
		}
	}
	
	MemoryBlock* aBuffer = reinterpret_cast <MemoryBlock*> (aFreeBlock.Buffer());
	std::copy (theExistingBlock, theExistingBlock + theExistingBlockLength, aBuffer);
	return aFreeBlock;
}
	
bool MemoryBlockContainer::ReallocateAllocatedBlocks()
{
	int anIndex;
	auto aBlock = ReallocateBlocks (myAllocatedBlocksLength, myAllocatedBlocks, myAllocatedBlocksLength, anIndex);
	if (aBlock.IsNull()) {
		return false;
	}
	
	 // first remove free block
	RemoveFreeBlock (aBlock.Length(), anIndex);
	MoveAllocatedBlocks (aBlock);
	return true;
}

bool MemoryBlockContainer::ReallocateFreeBlocks()
{
	int anIndex;
	auto aBlock = ReallocateBlocks (myFreeBlocksLength, myFreeBlocks, myFreeBlocksLength, anIndex);
	if (aBlock.IsNull()) {
		return false;
	}
	
	// first remove free block
	RemoveFreeBlockFromArray (aBlock.Length(), anIndex, reinterpret_cast <MemoryBlock*> (aBlock.Buffer()));
	MoveFreeBlocks (aBlock);
	return true;
}	
	
int MemoryBlockContainer::FindSiblingFreeBlock (const uint8_t* theTargetSiblingAdress)
{
	for (int i = 0 ; i < myFreeBlocksLength; i++) {
		uint8_t* anAdress = myFreeBlocks[i].Buffer();
		if (anAdress == theTargetSiblingAdress) {
			return i;
		}
	}
	return -1;
}	
	
void MemoryBlockContainer::DefragmentMemory()
{
	for (int i = 0 ; i < myFreeBlocksLength; i++) {
		auto& aBlock = myFreeBlocks [i];
		if (aBlock.IsNull()) {
			continue;
		}
		while (true) {
			uint8_t* aTargetSiblingAdress = aBlock.Buffer() + aBlock.Length();
			int anIndex = FindSiblingFreeBlock (aTargetSiblingAdress);
			if (anIndex == -1) {
				break;
			}
			
			auto& aSiblingBlock = myFreeBlocks [anIndex];
			aBlock.Length() += aSiblingBlock.Length();
			aSiblingBlock.Buffer() = nullptr;
			aSiblingBlock.Length() = 0;	
		}
	}
}
	
// method for using in tests
void MemoryBlockContainer::Reset()
{
	myAllocatedBlocks = nullptr;
	myAllocatedBlocksCapacity = 0;
	myAllocatedBlocksLength = 0;
	myFreeBlocks = nullptr;
	myFreeBlocksCapacity = 0;
	myFreeBlocksLength = 0;
	
	AllocateContainer();
}

int MemoryBlockContainer::AvaliableSpace()
{
	int aResult = 0;
	for (int i = 0; i < myFreeBlocksLength; i++) {
		auto aBlock = myFreeBlocks [i];
		aResult += aBlock.Length();
	}
	return aResult;
}	
	
}}
