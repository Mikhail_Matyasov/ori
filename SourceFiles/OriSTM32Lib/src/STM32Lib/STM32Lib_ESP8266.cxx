#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_ESP8266.hxx>

#include <Ori/Base_Allocator.hxx>
#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Helper.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/STM32Lib_Macros.hxx>
#include <Ori/STM32Lib_Timer.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>
#include <Ori/STM32Lib_ESP8266Configurator.hxx>
#include <Ori/STM32Lib_ESP8266Listener.hxx>
#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/STM32Lib_GPIO.hxx>

#include <FreeRTOS.h>
#include <task.h>
#include <string.h>
#include <memory>

using namespace Ori::Base;
using namespace Ori::Embed;

namespace Ori {
namespace STM32Lib {
	
void ConfigureESP32()
{
	__ORI_GPIO_OUTPUT_2MHZ_SIMPL_PUSH_PULL (GPIOA, CRH, 12); // pull down P26
	__ORI_GPIO_OUTPUT_2MHZ_SIMPL_PUSH_PULL (GPIOA, CRH, 11);
	
	GPIO::Port aP (*GPIOA, 11);
	aP.High(); // pull up P32
	
	__ORI_GPIO_OUTPUT_2MHZ_SIMPL_PUSH_PULL (GPIOA, CRH, 8);
	GPIO::Port aEN (*GPIOA, 8); // reset pin
	aEN.Low();
	Timer::Delay (100);
	aEN.High();
}	
	
ESP8266::ESP8266() :
	myUSART (USART::USARTType::USART_1)
{
	myUSART.StopIRQ();
	ConfigureESP32();
	
	auto aListener = ESP8266Listener::Instance();
	myUSART.RegisterObserver (aListener);
	
	ESP8266Configurator aConfigurator (myUSART);
	
	myUSART.StartIRQ();
	aConfigurator.ConfigureOption ("AT+RST");
	Timer::Delay (100);
	
	aConfigurator.ConfigureOption ("AT");
	
	// DO NOT USE AT+CWMODE=3 - dual mode, android devices bad connects to esp8266 in this mode
	// https://stackoverflow.com/questions/38934906/how-to-use-android-to-connect-to-an-esp8266-access-point-without-internet-in-a-s
	aConfigurator.ConfigureOption ("AT+CWMODE=2");
	
	// set ssid, password
	String anOption = "AT+CWSAP=\"OriAP";
	auto aMac = aConfigurator.ReadMacAdress();
	anOption << aMac << "\",\"\",5,0";
	aConfigurator.ConfigureOption (anOption.Data());
	
	// set static ip adress
	aConfigurator.ConfigureOption ("AT+CIPAP=\"192.168.0.100\"");
	
	// allow multiple connection, need to configure AP as server (next command)
	aConfigurator.ConfigureOption ("AT+CIPMUX=1");
	
	// configure esp8266 as server, with port = 333
	// do not use port 80, because android will send additional packets (GET / HTTP/1.1" 200 2117 "-" "Dalvik/2.1.0 ). 
	aConfigurator.ConfigureOption ("AT+CIPSERVER=1");
	
	myUSART.StopIRQ();
}
	
void ESP8266::Start()
{
	myUSART.StartIRQ();
}
	
ESP8266* ESP8266::Instance()
{
	static ESP8266* anInstance = Allocator::Construct <ESP8266>();
	return anInstance;
}
	
bool ESP8266::Send (const Embed::ESP8266Packet& theCommand)
{
	
	if (!SendDataLength (theCommand)) {
		return false;
	}
	
	myUSART.SendByte (theCommand.ID());

	TypeBuffer <uint16_t> aBuffer (static_cast <uint16_t> (theCommand.Command()));
	myUSART.Send (aBuffer.Bytes, sizeof (uint16_t));
	
	const auto& aDataBuffer = theCommand.Data();
	myUSART.Send (aDataBuffer.Data(), aDataBuffer.Lenght());
	
	auto aListener = ESP8266Listener::Instance();
	bool aRes = aListener->Wait ("SEND OK", "SEND FAIL");
	return aRes;
}
	
	
bool ESP8266::SendDataLength (const ESP8266Packet& theCommand) const
{
	auto aListener = ESP8266Listener::Instance();
	auto& aBuffer = aListener->Buffer();
	aBuffer.Consume (aBuffer.DataLength()); // clear buffer before sending of data
	
	int8_t aConnectionID = aListener->ConnectionID();
	if (aConnectionID == -1) {
		return false;
	}
	String aPrefix;
	aPrefix << "AT+CIPSEND=" << aConnectionID << ',';
	myUSART.Send (aPrefix.Data());

	uint16_t aDataLength = 1 + 2 + theCommand.Data().Lenght();
	myUSART.Println (aDataLength);
	
	bool aRes = aListener->Wait (">", "ERROR", true, false);
	if (!aRes)  {
		aListener->RemoveConnection (aConnectionID);
		return SendDataLength (theCommand);
	}
	
	return aRes;
}	
	
}}
