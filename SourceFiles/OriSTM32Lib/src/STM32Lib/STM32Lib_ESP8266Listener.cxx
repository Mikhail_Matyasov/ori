#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_ESP8266Listener.hxx>

#include <Ori/STM32Lib_TaskHelper.hxx>
#include <Ori/STM32Lib_Assert.hxx>
#include <Ori/STM32Lib_Timer.hxx>
#include <Ori/STM32Lib_TaskManager.hxx>
#include <Ori/STM32Lib_CommandManager.hxx>

#ifdef __ORI_STM32LIB
#include <FreeRTOS.h>
#include <task.h>

Ori::STM32Lib::ESP8266Listener* theListener = nullptr;

__ORI_TASK (Ori_STM32Lib_ESP8266Listener_ListenInputData,
	
	auto aTaskManager = Ori::STM32Lib::TaskManager::Instance();
	while (true) {
		theListener->ExtractInputCommand();
		aTaskManager->ConfigurePriority();
		vTaskDelay (100);
	}
)
#endif

namespace Ori {
namespace STM32Lib {

ESP8266Listener::ESP8266Listener() :
	myIsWaitReply (false),
	myActiveConnections (5)
{
	myBuffer.Allocate (500);
#ifdef __ORI_STM32LIB
	theListener = this;
	auto aTask = TaskHelper::Create (Ori_STM32Lib_ESP8266Listener_ListenInputData,
								    "Ori_STM32Lib_ESP8266Listener_ListenInputData", 256);		 
	TaskManager::Instance()->AddTask (aTask);
#endif
}	
	
void ESP8266Listener::OnData (uint32_t theByte)
{
	uint8_t aByte = static_cast <uint8_t> (theByte & 0x00FF);
	myBuffer.Push (aByte);
}
	
void ESP8266Listener::ExtractData (int theStart)
{
	RingBuffer::Iterator anIterator (myBuffer, theStart);
	
	Base::String aField;
	auto aManager = CommandManager::Instance();
	while (const uint8_t* aNext = anIterator.Next()) {
        if (*aNext == ',') {
		    aField = "";
	        continue;
        }
        if (*aNext == ':') {
	        
	        int aDataStart = anIterator.SecondCurrent();
			if (anIterator.ActiveBuffer() == 0) {
				aDataStart = anIterator.FirstCurrent();
			}	
	        RingBuffer::Iterator aDataIterator (myBuffer, aDataStart);
	        int aReceivedDataLength = aDataIterator.FirstLength() + aDataIterator.SecondLength();
	        int aDataLength = std::atoi (aField.Data());
			if (aReceivedDataLength < aDataLength) {
				return;
			}
	        const uint8_t* aData = &myBuffer.Buffer() [aDataIterator.FirstBegin()];
	        int aLength = aDataIterator.FirstLength() < aDataLength ? aDataIterator.FirstLength() : aDataLength;
	        myObserver->OnData (aData, aLength);
			if (aDataIterator.SecondLength() != 0) {
				aData = &myBuffer.Buffer()[aDataIterator.SecondBegin()];
				aLength = aDataLength - aLength;
				myObserver->OnData (aData, aLength);
			}
	        
	        myBuffer.ConsumeUpTo (aNext + aDataLength);
	        break;
        }
		aField += *aNext;
	}
}	
	
void ESP8266Listener::AddConnection (uint8_t theID)
{
	for (int i = 0; i < myActiveConnections.Lenght(); i++) {
		if (myActiveConnections[i] == theID) {
			return;
		}
	}
	
	myActiveConnections.Push (theID);
}

void ESP8266Listener::RemoveConnection (uint8_t theID)
{
	for (int i = 0; i < myActiveConnections.Lenght(); i++) {
		if (myActiveConnections[i] == theID) {
			myActiveConnections.Remove (i);
			return;
		}
	}
}	
	
void ESP8266Listener::ExtractInputCommand()
{
	if (myIsWaitReply) {
		return;
	}
	
	int aStart = myBuffer.Find (",CONNECT");
	if (aStart != -1) {
		uint8_t anID = myBuffer.Buffer() [aStart - 1];
		AddConnection (anID);
		myBuffer.ConsumeUpTo (aStart);
	}
	
	aStart = myBuffer.Find (",CLOSED");
	if (aStart != -1) {
		uint8_t anID = myBuffer.Buffer() [aStart - 1];
		RemoveConnection (anID);
		myBuffer.ConsumeUpTo (aStart);
	}
	
	aStart = myBuffer.Find ("+IPD");
	if (aStart != -1) {
		const uint8_t* aData = &myBuffer.Buffer() [aStart];
		ExtractData (aStart);
	}
}			
	
void ESP8266Listener::WaitEnd()
{
#ifdef __ORI_STM32LIB
	Timer::DoWhile ([&]() -> bool {
		if (myBuffer.DataLength() == 0) {
			return false;
		}
		if (myBuffer.Buffer() [myBuffer.DataEnd() - 1] == '\n') {
			return true;
		}
		return false;
	}, __ORI_ESP8266_LISTENER_TIMEOUT);
#endif	
}	
	
bool ESP8266Listener::Wait (const char* theSuccessReply,
							const char* theErrorReply,
							bool theisConsume,
							bool theIsWaitEnd,
							int theTimeout)
{
	bool aRes = false;	
#ifdef __ORI_STM32LIB
	myIsWaitReply = true;
	
	aRes = Timer::DoWhile ([&]() -> bool {
		int anIndex = myBuffer.Find (theSuccessReply);
		if (anIndex != -1) {
			if (theisConsume) {
				myBuffer.ConsumeUpTo (anIndex);
				int aConsumeCount = static_cast <int> (strlen (theSuccessReply) - 1);
				myBuffer.Consume (aConsumeCount);
			}
			return true;
		}
		
		anIndex = myBuffer.Find (theErrorReply);
		if (anIndex != -1) {
			myBuffer.ConsumeUpTo (anIndex);
			int aConsumeCount = static_cast <int> (strlen (theErrorReply) - 1);
			myBuffer.Consume (aConsumeCount);
			return false;
		}
		return false;
	}, theTimeout);
	
	if (theIsWaitEnd) {
		WaitEnd();	
	}
	
	myIsWaitReply = false;
#endif
	return aRes;
}	
	
ESP8266Listener* ESP8266Listener::Instance()
{
	static ESP8266Listener* anInstance = Allocator::Construct <ESP8266Listener>();
	return anInstance;
}
	
void ESP8266Listener::RegisterObserver (Observer* theObserver)
{
	myObserver = theObserver;
}
	
int8_t ESP8266Listener::ConnectionID() const
{
	if (myActiveConnections.Empty()) {
		return -1;
	}
	return myActiveConnections [myActiveConnections.Lenght() - 1];
}
	
}}
