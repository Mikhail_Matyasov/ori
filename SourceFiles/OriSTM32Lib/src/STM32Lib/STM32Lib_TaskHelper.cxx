#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_TaskHelper.hxx>

#include <Ori/STM32Lib_Assert.hxx>

namespace Ori {
namespace STM32Lib {

TaskHandle_t TaskHelper::Create (TaskType theTask, const char* theName, int theStackSize, int thePriority)
{	
	TaskHandle_t aCreatedTask = nullptr;
	auto aRes = xTaskCreate (theTask,
				             theName,
				             theStackSize,
				             nullptr,
				             thePriority,
				             &aCreatedTask);
	
	Assert::True (aCreatedTask);
	return aCreatedTask;
}
	
	
}}
