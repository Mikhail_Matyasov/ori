#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_RCCSystem.hxx>

#include <stm32f103xb.h>

namespace Ori {
namespace STM32Lib {
namespace {
void ConfigureGeneral()
{
	// For more explanation see: http://dimoon.ru/obuchalka/stm32f1/uroki-stm32f103-chast-4-nastroyka-rcc.html
	
	RCC->CR |= RCC_CR_HSEON;  // Enable HSE
	while(!(RCC->CR & RCC_CR_HSERDY)); 	// Wait till HSE is ready
	
	FLASH->ACR |= FLASH_ACR_LATENCY_1;  // Two latency cicles for FLASH
	
	RCC->CFGR |= RCC_CFGR_PLLSRC;  // enable HSE source for PLL Source Mux
	RCC->CFGR |= RCC_CFGR_PLLMULL9;  // Set PLLMul to x9										
	
	RCC->CR |= RCC_CR_PLLON;  // enable PLLCLK (for System Clock Mux switch)
	while((RCC->CR & RCC_CR_PLLRDY) == 0);  // wait till PLL is ready
	
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;  // AHB Prescaler /1
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;  // APB1 Prescale /2
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;  // APB2 Prescale /1
	// AHB = 72 Mhz
	// APB1 = 36 Mhz
	// APB2 = 72 Mhz
	
	RCC->CFGR |= RCC_CFGR_SW_PLL;  // select source SYSCLK = PLL
	while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_1);  // wait till PLL is used
	
	RCC->CR &= ~RCC_CR_HSION;  // Disable HSI
}
	
void ConfigureUSART()
{
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;  // enable clock for USART1
	RCC->APB1ENR |= RCC_APB1ENR_USART3EN;  // enable clock for USART3
}
void ConfigureGPIO()
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;  // enable clock for GPIO A
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;  // enable clock for GPIO B
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;  // enable clock for GPIO C
}
	
void ConfigureAFIO()
{
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;  // enable clock for Alternate function
}	
	
void ConfigureSPI()
{
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN; // enable clock for SPI1
	//RCC->APB1ENR |= RCC_APB1ENR_SPI2EN; // enable clock for SPI2
}
	
void ConfigureTimers()
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
}	
}
void RCCSystem::Init()
{
    ConfigureGeneral();
	ConfigureGPIO();
	ConfigureAFIO();
	ConfigureUSART();
	ConfigureSPI();
	ConfigureTimers();
}
	
void RCCSystem::Reset()
{
	RCC->AHBENR = 0;
	RCC->APB1ENR = 0;
	RCC->APB1RSTR = 0;
	RCC->APB2ENR = 0;
	RCC->APB2RSTR = 0;
	RCC->BDCR = 0;
	RCC->CFGR = 0;
	RCC->CIR = 0;
	RCC->CR = 0;
	RCC->CSR = 0;
}	

}}
