#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_ESP8266Configurator.hxx>

#include <Ori/STM32Lib_ESP8266Listener.hxx>
#include <Ori/STM32Lib_USART.hxx>
#include <Ori/Base_Helper.hxx>

using namespace Ori::Base;

namespace Ori {
namespace STM32Lib {

ESP8266Configurator::ESP8266Configurator (const USART& theUSART) :
	myUSART (theUSART)
{}	

String ESP8266Configurator::ReadMacAdress()
{
	while (true) {
		myUSART.Println ("AT+CIPSTAMAC?");
		bool aRes = ESP8266Listener::Instance()->Wait ("+CIPSTAMAC:", "ERROR", false);
		if (aRes) {
			aRes = ESP8266Listener::Instance()->Wait ("OK", "ERROR", false);
			if (aRes) {
				break;
			}
		}
	}
	
	const char* aCipstaMac = "+CIPSTAMAC:";
	auto& aBuffer = ESP8266Listener::Instance()->Buffer();
	int anIndex = aBuffer.Find (aCipstaMac);
	int aLength = static_cast <int> (strlen (aCipstaMac));
	int aMacIndex = anIndex + aLength;
	
	String aMac;
	RingBuffer::Iterator anIt (aBuffer, aMacIndex + 1);
	while (auto aNext = anIt.Next()) {
		
		if (*aNext == '\"') {
			break;
		}
		if (*aNext == ':') {
			continue;
		}
		aMac += *aNext;
	}
	return aMac;
}

void ESP8266Configurator::ConfigureOption (const char* theCommand)
{
	SendCommand (theCommand);
	auto& aBuffer = ESP8266Listener::Instance()->Buffer();
	aBuffer.Clear();
}

void ESP8266Configurator::SendCommand (const char* theCommand)
{
	myUSART.Println (theCommand);
	ESP8266Listener::Instance()->Wait ("OK");
}
	
void ESP8266Configurator::SetBaudRate (int theBaudRate)
{
	String aBaudRateOption;
	aBaudRateOption << "AT+UART_CUR=" << theBaudRate << ",8,1,0,0";
	myUSART.Println (aBaudRateOption.Data());
	
	auto aListener = ESP8266Listener::Instance();
	bool aRes = aListener->Wait ("OK");
	aListener->Buffer().Clear();
	myUSART.ChangeBaudRate (theBaudRate);
}
	
}}
