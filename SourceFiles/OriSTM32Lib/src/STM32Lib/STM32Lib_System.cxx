#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Lib_System.hxx>

#include <Ori/STM32Lib_RCCSystem.hxx>
#include <Ori/STM32Lib_GPIO.hxx>
#include <Ori/STM32Lib_SPI.hxx>
#include <Ori/STM32Lib_ESP8266.hxx>
#include <Ori/STM32Lib_Allocator.hxx>
#include <Ori/STM32Lib_CommandManager.hxx>
#include <Ori/STM32Handler_Blink.hxx>
#include <Ori/STM32Handler_Configuration.hxx>
#include <Ori/STM32Lib_Timer.hxx>
#include <Ori/STM32Lib_ESP8266Listener.hxx>
#include <Ori/STM32Lib_TaskManager.hxx>
#include <Ori/STM32Lib_TcpClient.hxx>
#include <Ori/STM32Lib_SX1278Listener.hxx>
#include <Ori/STM32Lib_UdpClient.hxx>
#include <Ori/STM32Lib_Logger.hxx>
#include <Ori/STM32Lib_ManchesterReader.hxx>

#include <FreeRTOS.h>
#include <task.h>

extern "C" {
void HardFault_Handler() 
{
	__ORI_GPIO_OUTPUT_2MHZ_SIMPL_PUSH_PULL (GPIOC, CRH, 13);
	Ori::STM32Lib::GPIO::Port aPort (*GPIOC, 13);
	aPort.Low();
	while (1);
}
	
void vApplicationStackOverflowHook (TaskHandle_t xTask, char* pcTaskName)
{
	Ori::STM32Lib::Assert::Raise ("Stack overflow");
}
	
void Ori_STM32Lib_System_HandleFreeRTOSAssert (const char* theFile, int theLine)
{
	Ori::STM32Lib::Assert::Raise ("FrerRTOS assert");
}

}

namespace Ori {
namespace STM32Lib {
namespace {
void Reset()
{
	GPIO::Reset();
	SPI::Reset();
	USART::Reset();
}			 
}
	
void System::Init()
{
	RCCSystem::Reset();
	RCCSystem::Init();
	Reset(); // Reset should be invoked only after RCCSystem::Init()
	Timer::Init();
	STM32Lib::Allocator::InitAllocator();
	//ManchesterReader::Init();
}	
	
void System::Start()
{	
	//auto aWiFi = ESP8266::Instance();
//	auto aManager = CommandManager::Instance();
//	ESP8266Listener::Instance()->RegisterObserver (aManager);
//	aWiFi->Start();
//	
	STM32Handler::Configuration::ConfigureAsClient();
	
	uint8_t aByte = 0;
	while (true) {
		UdpClient::Instance()->Send (&aByte, 1);
	}
	
	STM32Handler::Blink::Execute (BlinkType::Simple);
	vTaskStartScheduler();
}
	
}}
