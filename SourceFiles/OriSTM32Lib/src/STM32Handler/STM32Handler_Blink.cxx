#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Handler_Blink.hxx>

#include <Ori/STM32Lib_Macros.hxx>
#include <Ori/STM32Lib_GPIO.hxx>
#include <Ori/STM32Lib_TaskHelper.hxx>
#include <Ori/STM32Lib_Assert.hxx>
#include <Ori/STM32Lib_UdpClient.hxx>

#ifdef __ORI_STM32LIB
Ori::STM32Lib::GPIO::Port GetBlinkPort()
{
	__ORI_GPIO_OUTPUT_2MHZ_SIMPL_PUSH_PULL (GPIOC, CRH, 13);
	Ori::STM32Lib::GPIO::Port aPort (*GPIOC, 13);
	return aPort;
}
#endif

using namespace Ori::STM32Lib;
	

extern "C" {
#ifdef __ORI_STM32LIB
	
void Ori_STM32Handler_SimpleBlinkTask (void*)
{
	auto aPort = GetBlinkPort();
	for (int i = 0; i < 4; i++) {
		aPort.High();
		vTaskDelay (500);
		aPort.Low();
		vTaskDelay (500);
	}
	
	aPort.High();
	vTaskDelete (nullptr);
}
	
#endif	
}

namespace Ori {
namespace STM32Handler {	
	
void Blink::Execute (const BlinkType& theBlinkType)
{
#ifdef __ORI_STM32LIB
	bool aRes = false;
	switch (theBlinkType) {
	case BlinkType::Simple:
		TaskHelper::Create (Ori_STM32Handler_SimpleBlinkTask, "Ori_STM32Handler_SimpleBlinkTask");
		break;
	default:
		break;
	}
#endif
}	
	
}}
