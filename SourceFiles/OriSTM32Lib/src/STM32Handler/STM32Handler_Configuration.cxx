#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Handler_Configuration.hxx>

#include <Ori/STM32Lib_CommandManager.hxx>
#include <Ori/STM32Lib_ESP8266Listener.hxx>
#include <Ori/STM32Lib_SX1278Listener.hxx>
#include <Ori/STM32Lib_TcpClient.hxx>
#include <Ori/Embed_Macros.hxx>
#include <Ori/STM32Lib_Allocator.hxx>

using namespace Ori::STM32Lib;

namespace Ori {
namespace STM32Handler {

void Configuration::ConfigureAsServer()
{	
	int aBufferSize = 500;
	CommandManager::Instance()->AllocateBuffer (aBufferSize);
	ESP8266Listener::Instance()->Buffer().Allocate (aBufferSize);
	
	auto aListener = SX1278Listener::Instance();
	aListener->AllocatePacketStatistic();
	aListener->EnableReport();
	aListener->ProcessReport() = false;
	aListener->ProcessVideoData() = true;
	aListener->AllocateRxBuffer (__ORI_EMBED_SX1278_RX_BUFFER_LENGTH * 16);
	
	int aNumberOfPacketCache = 3;
	auto aClient = TcpClient::Instance();
	aClient->AllocateTxBuffer (aNumberOfPacketCache);
	aClient->SenderID() = 0;
}
	
void Configuration::ConfigureAsClient()
{
	int aBufferSize = 2100;
	CommandManager::Instance()->AllocateBuffer (aBufferSize);
	ESP8266Listener::Instance()->Buffer().Allocate (aBufferSize);
	
	auto aListener = SX1278Listener::Instance();
	aListener->DestroyPacketStatistic();
	aListener->DisableReport();
	aListener->ProcessReport() = true;
	aListener->ProcessVideoData() = false;
	aListener->AllocateRxBuffer (__ORI_EMBED_SX1278_RX_BUFFER_LENGTH * 3);
	
	int aNumberOfPacketCache = 16;
	auto aClient = TcpClient::Instance();
	aClient->AllocateTxBuffer (aNumberOfPacketCache);
	aClient->SenderID() = 1;
	
	STM32Lib::Allocator::AvaliableSpace();
}
	
}}
