#include <Ori/STM32Lib_Pch.hxx>
#include <Ori/STM32Handler_SX1278Transmitter.hxx>

#include <Ori/STM32Lib_TcpClient.hxx>
#include <Ori/STM32Lib_UdpClient.hxx>
#include <Ori/Embed_ESP8266Packet.hxx>

using namespace Ori::STM32Lib;
using namespace Ori::Embed;

namespace Ori {
namespace STM32Handler {
	
void SX1278Transmitter::Send (const ESP8266Packet& thePacket)
{
	if (thePacket.Command() == CommandType::TcpClientSendPacket ||
		thePacket.Command() == CommandType::TcpClientSendVideoData) {
		auto aTcpClient = TcpClient::Instance();	
		const auto& aBuffer = thePacket.Data();
		if (thePacket.Command() == CommandType::TcpClientSendPacket) {
			aTcpClient->SendPacket (aBuffer.Data(), aBuffer.Lenght());
		} else {
			aTcpClient->SendVideoData (aBuffer.Data(), aBuffer.Lenght());	
		}
	} else if (thePacket.Command() == CommandType::UdpClientSendPacket) {
		auto aUdpClient = UdpClient::Instance();
		const auto& aBuffer = thePacket.Data();
		aUdpClient->Send (aBuffer.Data(), aBuffer.Lenght(), SX1278CommandType::UdpPacket);
	}
}		
	
}}
