#include <Ori/NetBase_Pch.hxx>
#include <Ori/Net_TcpClient.hxx>

#ifdef WIN32
#include <Ori/NetWin_TcpClientImpl.hxx>
#else
#include <Ori/NetUnix_TcpClientImpl.hxx>
#endif


namespace Ori {
namespace Net {

TcpClient::TcpClient (const IPEndPoint& theEndPoint) :
    myImpl (std::make_shared <TcpClientImpl> (theEndPoint))
{}

bool TcpClient::Connect (const IPEndPoint& theEndPoint)
{
    return myImpl->Connect (theEndPoint);
}

bool TcpClient::Connected() const
{
    return myImpl->Connected();
}

bool TcpClient::Send (const uint8_t* theData, int theLength)
{
    return myImpl->Send (theData, theLength);
}

const IPEndPoint& TcpClient::EndPoint() const
{
    return myImpl->IPEndPoint();
}

IPEndPoint& TcpClient::EndPoint()
{
    return myImpl->IPEndPoint();
}

void TcpClient::RegisterObserver (TcpObserver* theObserver)
{
    myImpl->RegisterObserver (theObserver);
}

}}
