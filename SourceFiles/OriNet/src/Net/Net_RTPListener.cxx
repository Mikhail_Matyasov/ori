#include <Ori/NetBase_Pch.hxx>
#include <Ori/Net_RTPListener.hxx>

#include <Ori/NetBase_RTPSession.hxx>

#include <rtppacket.h>
#include <rtpudpv4transmitter.h>
#include <rtpipv4address.h>
#include <rtpsessionparams.h>
#include <rtpsession.h>
#include <chrono>
#include <thread>
#include <boost/thread.hpp>

namespace Ori {
namespace Net {
using namespace Ori::NetBase;

RTPListener::RTPListener (int theUdpPort,
                          int theTcpPort,
                          RTPListener::Observer& theObserver,
                          const Base::Logger& theLogger) :
    myRTPSession (std::make_shared <NetBase::RTPSession> (theObserver, theLogger, theTcpPort)),
    myIsSessionActive (false),
    myLogger (theLogger)
{

#ifdef RTP_SOCKETTYPE_WINSOCK
	WSADATA aData;
	WSAStartup (MAKEWORD (2,2),&aData);
#endif

    jrtplib::RTPUDPv4TransmissionParams aTransmissionParams;
    jrtplib::RTPSessionParams aSessionParams;

    // IMPORTANT: The local timestamp unit MUST be set, otherwise
    //            RTCP Sender Report info will be calculated wrong
    // In this case, we'll be just use 8000 samples per second.
    aSessionParams.SetOwnTimestampUnit (1.0 / 8000.0);
    aTransmissionParams.SetPortbase (theUdpPort);
    int aStatus = myRTPSession->Create (aSessionParams, &aTransmissionParams);
    if (aStatus < 0) {
        ORI_LOG_FAIL (myLogger) << "Can not create RTP session - " << jrtplib::RTPGetErrorString (aStatus).c_str();
        myStatus = RTPSessionStatus::Fail;
    } else {
        myStatus = RTPSessionStatus::Success;
        ORI_LOG_INFO (myLogger) << "RTP session successfuly initilized";
    }
}

RTPListener::~RTPListener()
{
    Stop();
    myRTPSession->BYEDestroy (jrtplib::RTPTime (1, 0), 0, 0);

#ifdef RTP_SOCKETTYPE_WINSOCK
	WSACleanup();
#endif
}

void RTPListener::Start()
{
    myIsSessionActive = true;
    myListenThread = std::make_shared <boost::thread> ([&]() -> void {

        int aSleepTime = 1000;
        while (myIsSessionActive) {
            std::this_thread::sleep_for (std::chrono::microseconds (aSleepTime));
            myRTPSession->Poll();
        }
    });
}

void RTPListener::Stop()
{
    myIsSessionActive = false;
    if (myListenThread) {
        myListenThread->join();
    }
}

}}
