#include <Ori/NetBase_Pch.hxx>
#include <Ori/Net_WiFiHotSpot.hxx>

#ifdef WIN32
#include <Ori/NetWin_WiFiHotSpotImpl.hxx>
#else

#ifdef __ANDROID__
#include <Ori/NetAndroid_WiFiHotSpotImpl.hxx>
#endif

#endif

namespace Ori {
namespace Net {

WiFiHotSpot::WiFiHotSpot (const std::shared_ptr <WiFiHotSpotImpl>& theImpl) :
    myImpl (theImpl)
{}

const Base::String WiFiHotSpot::SSID() const
{
    return myImpl->SSID();
}

void WiFiHotSpot::OpenWiFiSettings()
{
    return WiFiHotSpotImpl::OpenWiFiSettings();
}

}}
