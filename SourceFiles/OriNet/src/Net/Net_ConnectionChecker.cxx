#include <Ori/NetBase_Pch.hxx>
#include <Ori/Net_ConnectionChecker.hxx>

#include <Ori/NetBase_ConnectionCheckerImpl.hxx>
#include <Ori/Base_String.hxx>

using namespace Ori::NetBase;
using namespace Ori::Base;

namespace Ori {
namespace Net {

ConnectionChecker::ConnectionChecker (const String& theHost) :
    myImpl (std::make_shared <ConnectionCheckerImpl> (theHost))
{}

void ConnectionChecker::RegisterObserver (ConnectionChecker::Observer* theObserver)
{
    myImpl->RegisterObserver (theObserver);
}

void ConnectionChecker::SetHost (Base::String& theHost)
{
    myImpl->SetHost (theHost);
}

const String& ConnectionChecker::Host() const
{
    return myImpl->Host();
}

}}
