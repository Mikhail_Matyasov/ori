#include <Ori/NetBase_Pch.hxx>
#include <Ori/Net_NetworkInfo.hxx>

using namespace Ori::Base;

#include <Ori/NetBase_Macros.hxx>

#ifdef WIN32
#include <Ori/NetWin_NetworkInfoImpl.hxx>
#else

#ifdef __ANDROID__
#include <Ori/NetAndroid_NetworkInfoImpl.hxx>
#endif

#endif

__ORI_NET_DEFINE_WIN_ANDROID_IMPL (NetworkInfo);

namespace Ori {
namespace Net {

Vector <WiFiHotSpot> NetworkInfo::AvaliableHotSpots()
{
    return NetworkInfoImpl::AvaliableHotSpots();
}

}}
