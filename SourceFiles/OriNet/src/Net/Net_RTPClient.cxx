#include <Ori/NetBase_Pch.hxx>
#include <Ori/Net_RTPClient.hxx>

#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/Net_TcpClient.hxx>
#include <Ori/Net_TcpObserver.hxx>

#include <rtppacket.h>
#include <rtpudpv4transmitter.h>
#include <rtpipv4address.h>
#include <rtpsessionparams.h>
#include <rtpsession.h>
#include <rtpdefines.h>
#include <chrono>
#include <thread>

using namespace Ori::Base;

namespace Ori {
namespace Net {
namespace {

Base::ByteArray ParseIP (const Base::String& theIp)
{
    Base::ByteArray aRes;
    Base::String aChunk;
    for (int i = 0; i < theIp.Length(); i++) {

        if (theIp[i] == '.') {
            int anAdressChunk = std::stoi (aChunk.Data());
            aRes.Push (anAdressChunk);
            aChunk = "";
        } else {
            aChunk += theIp[i];
        }
    }
    int anAdressChunk = std::stoi (aChunk.Data());
    aRes.Push (anAdressChunk);

    return aRes;
}

}

class RTPClient::ClientTcpObserver : public TcpObserver
{
public:
    ClientTcpObserver (RTPClient& theClient) :
        myClient (theClient)
    {}

    void OnPacket (const Base::ByteArrayView& thePacket) override
    {
        uint64_t aPacketTs = *reinterpret_cast <const uint64_t*> (thePacket.Data());
        if (thePacket.Lenght() == sizeof (uint64_t)) { 
            myClient.RecordLostedPacket (aPacketTs);
            return;
        }

        const uint16_t* aSubIndices = reinterpret_cast <const uint16_t*> (thePacket.Data() + sizeof (uint64_t));
        int aSubIndicesLength = (thePacket.Lenght() - sizeof (uint64_t)) / sizeof (uint16_t);
        myClient.RecordLostedSubPackets (aPacketTs, aSubIndices, aSubIndicesLength);
    }

private:
    RTPClient& myClient;
};

RTPClient::RTPClient (int thePortBase,
                      const RTPEndPoint& thePoint,
                      const Base::Logger& theLogger) :
    myRTPSession (std::make_shared <jrtplib::RTPSession>()),
    myTcpClient (std::make_shared <TcpClient> (IPEndPoint (thePoint.IPAdress(), thePoint.TcpPort()))),
    myTcpObserver (std::make_shared <RTPClient::ClientTcpObserver> (*this)),
    myLogger (theLogger),
    myTimestamp (0),
    myIsSessionActive (false)
{
    unsigned long long anExtendedDataLenght = sizeof (uint8_t) + // Is last packet
                                              sizeof (uint16_t) + // Number of subpackets
                                              sizeof (size_t) + // Packet lenght
                                              sizeof (uint16_t) + // Sub index
                                              sizeof (uint8_t) + // Is key
                                              sizeof (uint64_t); // timestamp
    myExtendedDataBuffer.Resize (static_cast <int> (anExtendedDataLenght));

#ifdef RTP_SOCKETTYPE_WINSOCK
	WSADATA aData;
	WSAStartup (MAKEWORD (2,2),&aData);
#endif

    jrtplib::RTPUDPv4TransmissionParams aTransmissionParams;
    jrtplib::RTPSessionParams aSessionParams;

    // IMPORTANT: The local timestamp unit MUST be set, otherwise
    //            RTCP Sender Report info will be calculated wrong
    // In this case, we'll be just use 8000 samples per second.
    aSessionParams.SetOwnTimestampUnit (1.0 / 8000.0);
    int aMaxPacketSize = 65500;
    aSessionParams.SetMaximumPacketSize (aMaxPacketSize);

    aTransmissionParams.SetPortbase (thePortBase);

    int aStatus = myRTPSession->Create (aSessionParams, &aTransmissionParams);
    if (aStatus < 0) {
        auto anError = jrtplib::RTPGetErrorString (aStatus).c_str();
        ORI_LOG_FAIL (myLogger) << "Can not create RTP session - " << anError;
        myStatus = RTPSessionStatus::Fail;
        return;
    } else {
        ORI_LOG_INFO (myLogger) << "RTP session successfuly initilized";
        myStatus = RTPSessionStatus::Success;
    }

    auto anIp = ParseIP (thePoint.IPAdress());
    jrtplib::RTPIPv4Address aDestinationAdress (anIp.Data(), thePoint.UdpPort());
    myRTPSession->AddDestination (aDestinationAdress);
    
    myRTPSession->SetDefaultPayloadType (96);
    myRTPSession->SetDefaultMark (false);
    myRTPSession->SetDefaultTimestampIncrement (160);

    myTcpClient->RegisterObserver (myTcpObserver.get());
    myTcpClient->Connect();
}

RTPClient::~RTPClient()
{
#ifdef RTP_SOCKETTYPE_WINSOCK
	WSACleanup();
#endif
}

bool RTPClient::Send (const Base::ByteArray& thePacket, bool theIsKey)
{
    return Send (thePacket.Data(), thePacket.Lenght(), theIsKey);
}

bool RTPClient::Send (const uint8_t* thePacket, int thePacketLenght, bool theIsKey)
{
    bool aRes = SendWithTimestamp (thePacket, thePacketLenght, myTimestamp, theIsKey);
    FlushLostedPackets();
    // Cache packet after sending current packet and losted packets
    CachePacket (thePacket, thePacketLenght, myTimestamp, theIsKey);
    myTimestamp++;
    return aRes;
}

bool RTPClient::SendWithTimestamp (const uint8_t* thePacket, int thePacketLenght, uint64_t theTs, bool theIsKey)
{
    bool aRes = true;
    ORI_LOG_INFO (myLogger) << "Sent packet with timestamp = " << theTs;

    if (thePacketLenght > RTP_DEFAULTPACKETSIZE) {

        int aNumberOfSubPackets = thePacketLenght / RTP_DEFAULTPACKETSIZE;
        int aLastPacketSize = RTP_DEFAULTPACKETSIZE;
        if (thePacketLenght % RTP_DEFAULTPACKETSIZE != 0) {
            aNumberOfSubPackets++;
            aLastPacketSize = thePacketLenght % RTP_DEFAULTPACKETSIZE;
        }
        PrepareExtendedBuffer (aNumberOfSubPackets, thePacketLenght, theIsKey, theTs);
        InsertIsLast (0);

        // Process first packet
        InsertSubIndex (0);
        aRes &= DoSend (thePacket, RTP_DEFAULTPACKETSIZE);

        // Process middle packets
        int i = 1;
        if (aNumberOfSubPackets > 2) {

            for (; i < aNumberOfSubPackets - 1; i++) {
                InsertSubIndex (i); // set specific index
                aRes &= DoSend (thePacket + i * RTP_DEFAULTPACKETSIZE, RTP_DEFAULTPACKETSIZE);
                std::this_thread::sleep_for (std::chrono::microseconds (10)); // sleep to give socket relax
            }
        }

        // Process last packet
        InsertIsLast (1);
        InsertSubIndex (aNumberOfSubPackets - 1);
        aRes &= DoSend (thePacket + i * RTP_DEFAULTPACKETSIZE, aLastPacketSize);
        ORI_LOG_INFO (myLogger) << "Sent multiple packet, number of subpackets = " << aNumberOfSubPackets
            << ", packet size = " << thePacketLenght;
    } else {

        InsertIsLast (1);
        InsertSubIndex (0);
        PrepareExtendedBuffer (1, thePacketLenght, theIsKey, theTs);
        aRes = DoSend (thePacket, thePacketLenght);
        ORI_LOG_INFO (myLogger) << "Sent single packet, size = " << thePacketLenght;
    }
    return aRes;
}

bool RTPClient::DoSend (const uint8_t* thePacket, size_t thePacketLenght)
{

    int aRes = myRTPSession->SendPacketEx (thePacket,
                                           thePacketLenght,
                                           -1,
                                           myExtendedDataBuffer.Data(),
                                           myExtendedDataBuffer.Lenght());
    return aRes < 0 ? false : true;
}

void RTPClient::PrepareExtendedBuffer (uint16_t theNumOfSubPackets,
                                       size_t thePacketLenght,
                                       bool theIsKey,
                                       uint64_t theTs)
{
    TypeBuffer <uint16_t> aShort (theNumOfSubPackets);
    TypeBuffer <size_t> aLong (thePacketLenght);

    std::copy (aShort.Bytes, aShort.Bytes + sizeof (uint16_t), &myExtendedDataBuffer[1]);
    std::copy (aLong.Bytes, aLong.Bytes + sizeof (size_t), &myExtendedDataBuffer[1 + sizeof (uint16_t)]);

    int anIsKeyIndex = sizeof (uint8_t) + sizeof (uint16_t) + sizeof (size_t) + sizeof (uint16_t);
    myExtendedDataBuffer[anIsKeyIndex] = theIsKey ? 1 : 0;

    TypeBuffer <uint64_t> aTs (theTs);
    std::copy (aTs.Bytes, aTs.Bytes + sizeof (uint64_t), &myExtendedDataBuffer[anIsKeyIndex + sizeof (uint8_t)]);
}

void RTPClient::InsertSubIndex (uint16_t theSubIndex)
{
    ORI_LOG_VERBOSE (myLogger) << "Sent subpacket with index = " << theSubIndex;
    TypeBuffer <uint16_t> aSub (theSubIndex);

    std::copy (aSub.Bytes,
               aSub.Bytes + sizeof (uint16_t),
               &myExtendedDataBuffer[1 + sizeof (uint16_t) + sizeof (size_t)]);
}

void RTPClient::InsertIsLast (uint8_t theIsLast)
{
    myExtendedDataBuffer[0] = theIsLast;
}

void RTPClient::CachePacket (const uint8_t* thePacket, size_t thePacketLenght, uint64_t theTs, bool theIsKey)
{
    if (theIsKey) {
        for (const auto& aPair : myCachedPacketMap) {
            aPair.second->Reset();
            myFreePacketBuffer.push_back (aPair.second);
        }
        myCachedPacketMap.clear();
    }

    std::shared_ptr <ByteArray> aPacket = nullptr;
    if (myFreePacketBuffer.empty()) {
        aPacket = std::make_shared <ByteArray>();
    } else {
        aPacket = myFreePacketBuffer[0];
        myFreePacketBuffer.erase (myFreePacketBuffer.begin());
    }

    aPacket->Reserve (static_cast <int> (thePacketLenght));
    aPacket->Insert (thePacket, static_cast <int> (thePacketLenght));
    myCachedPacketMap.emplace (theTs, aPacket);
}

void RTPClient::RecordLostedPacket (uint64_t theTs)
{
    const auto& anIt = myCachedPacketMap.find (theTs);
    if (anIt != myCachedPacketMap.end()) {

        LostedPacket aPacket;
        aPacket.Timestamp() = theTs;
        myLostedPackets.push_back (aPacket);
    }
}

void RTPClient::RecordLostedSubPackets (uint64_t theTs, const uint16_t* theData, int theLenght)
{
    const auto& anIt = myCachedPacketMap.find (theTs);
    if (anIt != myCachedPacketMap.end()) {

        const auto& aPacketBuffer = *anIt->second;
        LostedPacket aPacket;
        aPacket.Timestamp() = theTs;
        aPacket.Indices().Push (theData, theLenght);
        myLostedPackets.push_back (aPacket);
    }
}

void RTPClient::FlushLostedPackets()
{
    if (myLostedPackets.empty()) {
        return;
    }

    for (const auto& aPacket : myLostedPackets) {

        if (aPacket.Indices().Empty()) {
            const auto& aPacketBuffer = myCachedPacketMap[aPacket.Timestamp()];
            SendWithTimestamp (aPacketBuffer->Data(), aPacketBuffer->Lenght(), aPacket.Timestamp(), false);
            continue;
        }

        const auto& aPacketBuffer = *myCachedPacketMap[aPacket.Timestamp()];

        int aNumberOfSubPackets = aPacketBuffer.Lenght() / RTP_DEFAULTPACKETSIZE;
        int aLastPacketSize = RTP_DEFAULTPACKETSIZE;
        if (aPacketBuffer.Lenght() % RTP_DEFAULTPACKETSIZE != 0) {
            aNumberOfSubPackets++;
            aLastPacketSize = aPacketBuffer.Lenght() % RTP_DEFAULTPACKETSIZE;
        }
        PrepareExtendedBuffer (aNumberOfSubPackets, aPacketBuffer.Lenght(), false, aPacket.Timestamp());
        InsertIsLast (0);
        
        for (uint16_t anIndex : aPacket.Indices()) {
            InsertSubIndex (anIndex); // set specific index
            if (anIndex == aNumberOfSubPackets - 1) {
                InsertIsLast (1);
                DoSend (aPacketBuffer.Data() + anIndex * RTP_DEFAULTPACKETSIZE, RTP_DEFAULTPACKETSIZE);
                std::this_thread::sleep_for (std::chrono::microseconds (10));
                InsertIsLast (0);
                continue;
            }
            DoSend (aPacketBuffer.Data() + anIndex * RTP_DEFAULTPACKETSIZE, RTP_DEFAULTPACKETSIZE);
            std::this_thread::sleep_for (std::chrono::microseconds (10));
        }
    }
    myLostedPackets.clear();
}


}}
