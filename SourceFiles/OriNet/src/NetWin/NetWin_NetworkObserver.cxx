#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetWin_NetworkObserver.hxx>


namespace Ori {
namespace NetWin {

namespace {
bool theIsScaned = false;
void Callback (PWLAN_NOTIFICATION_DATA theData, PVOID)
{
    if (theData->NotificationSource == WLAN_NOTIFICATION_SOURCE_ACM) {
        theIsScaned = true;
    }
}
}

void NetworkObserver::ObserveAvaliableNetworks (
    std::function <bool (const WLAN_AVAILABLE_NETWORK_V2&, HANDLE, GUID)> theFunc,
    bool theIsScan)
{
    DWORD aClientVersion = 2; // Client version for Windows Vista and Windows Server 2008
    DWORD aVersion = 0;
    HANDLE aClient = nullptr;
    DWORD aRes = WlanOpenHandle (aClientVersion, nullptr, &aVersion, &aClient);
    if (aRes != 0) {
        return;
    }
    
    PWLAN_INTERFACE_INFO_LIST anInterfaceList = nullptr;
    aRes = WlanEnumInterfaces (aClient, nullptr, &anInterfaceList);
    auto Deallocate = [&]() -> void {
        if (anInterfaceList) {
            WlanFreeMemory (anInterfaceList);
        }
    };
    if (aRes != 0) {
        Deallocate();
        return;
    }
    
    for (DWORD i = 0; i < anInterfaceList->dwNumberOfItems; i++) {
        const auto& anInfo = anInterfaceList->InterfaceInfo[i];
        GUID anInterfaceGuid = anInfo.InterfaceGuid;

        if (anInfo.isState == wlan_interface_state_disconnected) {
            continue;
        }

        if (theIsScan) {
            theIsScaned = false;
            WlanRegisterNotification  (aClient, WLAN_NOTIFICATION_SOURCE_ALL, true, Callback, nullptr, nullptr, nullptr);
            WlanScan (aClient, &anInterfaceGuid, nullptr, nullptr, nullptr);
            while (!theIsScaned);
        }

        PWLAN_AVAILABLE_NETWORK_LIST_V2 anAvaliableNetworks = nullptr;
        aRes = WlanGetAvailableNetworkList2 (aClient,
                                            &anInterfaceGuid,
                                            WLAN_AVAILABLE_NETWORK_INCLUDE_ALL_ADHOC_PROFILES,
                                            nullptr, 
                                            &anAvaliableNetworks);
    
        if (aRes != 0) {
            continue;
        }

        for (DWORD j = 0; j < anAvaliableNetworks->dwNumberOfItems; j++) {
            const auto& aNetwork = anAvaliableNetworks->Network [j];
            if (!theFunc (aNetwork, aClient, anInterfaceGuid)) {
                break;
            }
        }
        WlanFreeMemory (anAvaliableNetworks);
    }
    
    Deallocate();
}

}}
