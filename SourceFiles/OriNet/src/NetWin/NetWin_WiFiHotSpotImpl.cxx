#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetWin_WiFiHotSpotImpl.hxx>

#include <Ori/NetWin_NetworkObserver.hxx>
#include <Ori/Base_Logger.hxx>

using namespace Ori::Base;

namespace Ori {
namespace NetWin {
namespace {

wchar_t* ToWchar (const char* theString)
{
    size_t aLength = strlen (theString);
    uint8_t* aRes = new uint8_t [aLength * 2 + 2];
    int j = 0;
    for (int i = 0; i < aLength; i++, j += 2) {
        aRes[j] = theString[i];
        aRes[j + 1] = '\0';
    }
    
    aRes[j] = '\0';
    aRes[j + 1] = '\0';
    return reinterpret_cast <wchar_t*> (aRes);
}

std::string GetConnectedSSID (HANDLE theClient, GUID theGuid)
{
    DWORD aDataSize = sizeof (WLAN_CONNECTION_ATTRIBUTES);
    PWLAN_CONNECTION_ATTRIBUTES anInfo = nullptr;
    WLAN_OPCODE_VALUE_TYPE aCode = WLAN_OPCODE_VALUE_TYPE::wlan_opcode_value_type_invalid;

    DWORD aRes = WlanQueryInterface (theClient,
                                     &theGuid,
                                     wlan_intf_opcode_current_connection,
                                     nullptr,
                                     &aDataSize,
                                     reinterpret_cast <PVOID*> (&anInfo), 
                                     &aCode);
    if (aRes != 0 || !anInfo) {
        return "";
    }
    return reinterpret_cast <const char*> (anInfo->wlanAssociationAttributes.dot11Ssid.ucSSID);
}

bool theIsConnected = false;;
void WiFiCallback (PWLAN_NOTIFICATION_DATA theData, PVOID)
{
    if ((theData->NotificationSource == WLAN_NOTIFICATION_SOURCE_ACM) &&
        (theData->NotificationCode == wlan_notification_acm_connection_complete)) {

        theIsConnected = true;
    }
};

}

bool WiFiHotSpotImpl::DoConnect (const WLAN_AVAILABLE_NETWORK_V2& theNetwork,
                             HANDLE theClient,
                             GUID theGuid,
                             const Base::String& thePassword) const
{
    auto aConnectedSSID = GetConnectedSSID (theClient, theGuid);
    if (aConnectedSSID == mySSID.Data()) {
        return true;
    }

    ORI_LOG_DEBUG_DEFAULT() << "Connecting to WiFi hotspot";
    WLAN_CONNECTION_PARAMETERS aParams;
    aParams.dot11BssType = theNetwork.dot11BssType;
    aParams.dwFlags = 0;
    aParams.pDesiredBssidList = 0;
    aParams.pDot11Ssid = nullptr;

    std::wstring aProfile;

    if (thePassword.Empty()) {
        aParams.strProfile = ToWchar (reinterpret_cast <const char*> (theNetwork.dot11Ssid.ucSSID));
        aParams.wlanConnectionMode = WLAN_CONNECTION_MODE::wlan_connection_mode_profile;
    } else {
        const char* aUTF8 = reinterpret_cast <const char*> (theNetwork.dot11Ssid.ucSSID);
        wchar_t* aSSID = ToWchar (aUTF8);
        wchar_t* aPassword = ToWchar (thePassword.Data());

        aProfile += L"<?xml version=\"1.0\"?><WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\">";
        aProfile += L"<name>";
        aProfile += aSSID;
        aProfile += L"</name>";
        aProfile += L"<SSIDConfig><SSID><name>";
        aProfile += aSSID;
        aProfile += L"</name></SSID></SSIDConfig><connectionType>ESS</connectionType><connectionMode>auto</connectionMode><autoSwitch>false</autoSwitch><MSM><security><authEncryption><authentication>WPA2PSK</authentication><encryption>AES</encryption><useOneX>false</useOneX></authEncryption><sharedKey><keyType>passPhrase</keyType><protected>false</protected>";
        aProfile += L"<keyMaterial>";
        aProfile += aPassword;
        aProfile += L"</keyMaterial></sharedKey></security></MSM></WLANProfile>";

        delete[] aSSID;
        delete[] aPassword;
        aParams.strProfile = aProfile.c_str();
        aParams.wlanConnectionMode = WLAN_CONNECTION_MODE::wlan_connection_mode_temporary_profile;
    }

    WlanRegisterNotification (theClient,
                              WLAN_NOTIFICATION_SOURCE_ALL,
                              true,
                              WiFiCallback,
                              nullptr,
                              nullptr,
                              nullptr);

    theIsConnected = false;
    DWORD aRes = WlanConnect (theClient, &theGuid, &aParams, nullptr);
    if (aRes != 0) {
        return false;
    }

    while (!theIsConnected);
    ORI_LOG_DEBUG_DEFAULT() << "Connected to hotspot";
    return true;
}

WiFiHotSpotImpl::WiFiHotSpotImpl (const Base::String& theSSID) :
    mySSID (theSSID)
{}

void WiFiHotSpotImpl::OpenWiFiSettings()
{
    system ("explorer ms-availablenetworks:");
}

bool WiFiHotSpotImpl::IsConnected() const
{
    bool aRes = false;
    NetworkObserver::ObserveAvaliableNetworks ([&] (const WLAN_AVAILABLE_NETWORK_V2& theNetwork,
                                                    HANDLE theClient,
                                                    GUID theGuid) -> bool {

        const char* anSSID = reinterpret_cast <const char*> (theNetwork.dot11Ssid.ucSSID);
        if (mySSID == anSSID) {
            auto aConnectedSSID = GetConnectedSSID (theClient, theGuid);
            if (aConnectedSSID == mySSID.Data()) {
                aRes = true;
            }
            return false;
        }

        return true;
    }, false);

    return aRes;
}

}}
