#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetWin_TcpClientImpl.hxx>

#include <Ori/Base_Logger.hxx>
#include <Ori/Net_TcpObserver.hxx>

#include <ws2tcpip.h>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

using namespace Ori::Net;
using namespace Ori::Base;

namespace Ori {
namespace NetWin {

TcpClientImpl::TcpClientImpl (const Net::IPEndPoint& theEndPoint) :
    myIPEndPoint (theEndPoint),
    myObserver (nullptr),
    mySocket (INVALID_SOCKET)
{
    WSADATA aData;
    myError = WSAStartup (MAKEWORD(2,2), &aData);
    if (myError != 0) {
        return;
    }

    if (!myIPEndPoint.IsNull()) {
        DoConnect();
    }

    std::thread T ([&]() -> void {

        while (true) {
            const int aReceiveBufferLength = 2000;
            char aBuffer [aReceiveBufferLength];
            int aReceivedLength = recv (mySocket, aBuffer, aReceiveBufferLength, 0);
            if (aReceivedLength > 0) {
                if (myObserver) {
                    ByteArrayView aPacket (reinterpret_cast <const uint8_t*> (aBuffer), aReceivedLength);
                    myObserver->OnPacket (aPacket);
                }
            } else if (aReceivedLength == 0) {
                Destroy();
                ORI_LOG_DEBUG_DEFAULT() << "Connection closed";
            } else {
                // recv failed with error
                myError = WSAGetLastError();
            }
        }

    });
    T.detach();
}

TcpClientImpl::~TcpClientImpl()
{
    Destroy();
}

bool TcpClientImpl::DoConnect()
{
    addrinfo anAdressInfo;
    ZeroMemory( &anAdressInfo, sizeof (anAdressInfo) );
    anAdressInfo.ai_family = AF_UNSPEC;
    anAdressInfo.ai_socktype = SOCK_STREAM;
    anAdressInfo.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    addrinfo* aResultAdress = nullptr;
    myError = getaddrinfo (myIPEndPoint.IPAdress().Data(),
                           std::to_string (myIPEndPoint.Port()).c_str(),
                           &anAdressInfo,
                           &aResultAdress);
    if (myError != 0) {
        return false;
    }

    // Attempt to connect to an address until one succeeds
    for(addrinfo* anIt = aResultAdress; anIt != nullptr; anIt = anIt->ai_next) {

        // Create a SOCKET for connecting to server
        mySocket = socket (anIt->ai_family, anIt->ai_socktype, anIt->ai_protocol);
        if (mySocket == INVALID_SOCKET) {
            myError = WSAGetLastError();
            return false;
        }

        // Connect to server.
        int aRes = connect (mySocket, anIt->ai_addr, static_cast <int> (anIt->ai_addrlen));
        if (aRes == SOCKET_ERROR) {
            myError = WSAGetLastError();
            ORI_LOG_DEBUG_DEFAULT() << "Attempt to connect failed";
            //closesocket (mySocket);
            mySocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    freeaddrinfo (aResultAdress);
    if (mySocket == INVALID_SOCKET) {
        // Unable to connect to server
        return false;
    }
    ORI_LOG_DEBUG_DEFAULT() << "Connected to " << myIPEndPoint.IPAdress() << " on port " << myIPEndPoint.Port();
    return true;
}

void TcpClientImpl::Destroy()
{
    if (mySocket != INVALID_SOCKET) {
        closesocket (mySocket);
    }
    WSACleanup();
}

bool TcpClientImpl::Connect (const Net::IPEndPoint& theEndPoint)
{
    if (!theEndPoint.IsNull()) {
        myIPEndPoint = theEndPoint;
    }

    if (!myIPEndPoint.IsNull()) {
        return DoConnect();
    }
    return false;
}

bool TcpClientImpl::Connected() const
{
    return mySocket != INVALID_SOCKET;
}

bool TcpClientImpl::Send (const uint8_t* theData, int theLength)
{
    // Send an initial buffer
    const char* aData = reinterpret_cast <const char*> (theData);
    int aRes = send (mySocket, aData, theLength, 0);
    if (aRes == SOCKET_ERROR) {
        myError = WSAGetLastError();
        return false;
    }
    return true;
}

void TcpClientImpl::RegisterObserver (Net::TcpObserver* theObserver)
{
    myObserver = theObserver;
}

}}
