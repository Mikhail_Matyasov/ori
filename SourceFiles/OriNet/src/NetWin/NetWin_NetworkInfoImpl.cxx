#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetWin_NetworkInfoImpl.hxx>
#include <Ori/NetWin_WiFiHotSpotImpl.hxx>

#include <Ori/NetWin_NetworkObserver.hxx>

#include <wlanapi.h>

namespace Ori {
namespace NetWin {

Base::Vector <Net::WiFiHotSpot> NetworkInfoImpl::AvaliableHotSpots()
{
    Base::Vector <Net::WiFiHotSpot> aHotSpots;

    auto IsSSIDExists = [&] (const char* theSSID) -> bool {
        for (auto it = aHotSpots.begin(); it != aHotSpots.end(); it++) {
            if (it->SSID() == theSSID) {
                return true;
            }
        }
        return false;
    };

    NetworkObserver::ObserveAvaliableNetworks ([&] (const WLAN_AVAILABLE_NETWORK_V2& theNetwork, HANDLE, GUID) -> bool {

        const char* anSSIDData = reinterpret_cast <const char*> (theNetwork.dot11Ssid.ucSSID);
        if (IsSSIDExists (anSSIDData)) {
            return true;
        }

        Base::String anSSID (anSSIDData);

        const auto& aHotSpotImpl = std::make_shared <Ori::NetWin::WiFiHotSpotImpl> (anSSID);
        Net::WiFiHotSpot aHotSpot (aHotSpotImpl);
        aHotSpots.Push (aHotSpot);
        return true;
    });

    
   return aHotSpots;
}

}}
