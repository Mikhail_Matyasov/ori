#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetBase_PacketArena.hxx>

#include <Ori/Base_TypeBuffer.hxx>
#include <Ori/NetBase_RTPSession.hxx>

#include <rtppacket.h>
#include <rtpdefines.h>

using namespace Ori::Base;

namespace Ori {
namespace NetBase {

PacketArena::PacketArena (
    Net::RTPListener::Observer& theObserver,
    NetBase::RTPSession& theSession,
    const Base::Logger& theLogger,
    int theTcpPort) :
    myObserver (theObserver),
    mySession (theSession),
    myListener (theTcpPort),
    myLogger (theLogger),
    myExpectedTimestamp (0),
    myNumberOfUnexpectedPackets (0)
{}

namespace {

uint8_t IsLast (uint8_t* theExtendedData) // theExtendedData[0]
{ return theExtendedData[0]; }

uint16_t NumberOfSubPackets (uint8_t* theExtendedData) // theExtendedData[1, 2]
{
    TypeBuffer <uint16_t> aBuffer (theExtendedData, 2);
    return aBuffer.Value;
}

size_t PacketLenght (uint8_t* theExtendedData) // theExtendedData[3, 4, 5, 6, 7, 8, 9, 10]
{
    TypeBuffer <size_t> aBuffer;
    unsigned long long anOffset = 1 + sizeof (uint16_t);
    for (int i = 0; i < sizeof (size_t); i++) {
        aBuffer.Bytes[i] = theExtendedData[i + anOffset];
    }

    return aBuffer.Value;
}

uint16_t SubIndex (uint8_t* theExtendedData) // theExtendedData[11, 12]
{
    TypeBuffer <uint16_t> aBuffer;
    unsigned long long anOffset = 1 + sizeof (size_t) + sizeof (uint16_t);
    for (int i = 0; i < sizeof (uint16_t); i++) {
        aBuffer.Bytes[i] = theExtendedData[i + anOffset];
    }

    return aBuffer.Value;
}

bool IsKey (uint8_t* theExtendedData) // theExtendedData[13]
{
    TypeBuffer <uint8_t> aBuffer;
    unsigned long long anOffset = 1 + sizeof (size_t) + sizeof (uint16_t) + sizeof (uint16_t);
    for (int i = 0; i < sizeof (uint8_t); i++) {
        aBuffer.Bytes[i] = theExtendedData[i + anOffset];
    }

    return aBuffer.Value == 0 ? false : true;
}

uint64_t Timestamp (uint8_t* theExtendedData)
{
    TypeBuffer <uint64_t> aBuffer;
    unsigned long long anOffset = 1 + sizeof (size_t) + sizeof (uint16_t) + sizeof (uint16_t) + sizeof (uint8_t);
    for (int i = 0; i < sizeof (uint64_t); i++) {
        aBuffer.Bytes[i] = theExtendedData[i + anOffset];
    }

    return aBuffer.Value;
}

}

void PacketArena::ProcessRTPPacket (jrtplib::RTPPacket* thePacket)
{
    uint8_t* anExtendedData = thePacket->GetExtensionData();
    uint8_t* aPlayloadData = thePacket->GetPayloadData();
    size_t aDataLenght = thePacket->GetPayloadLength();
    uint64_t aTimeStamp = Timestamp (anExtendedData);

    ORI_LOG_VERBOSE (myLogger) << "Received subpacket with number - " << aTimeStamp << "." << SubIndex (anExtendedData);

    if (IsLast (anExtendedData) == 1) {

        ProcessLastSubPacket (anExtendedData, aPlayloadData, aDataLenght, aTimeStamp);
    } else {

        ProcessSubPacket (anExtendedData, aPlayloadData, aDataLenght, aTimeStamp);
    }
}

void PacketArena::ProcessLastSubPacket (uint8_t* theExtendedData,
                                        uint8_t* thePlayloadData,
                                        size_t   theDataLenght,
                                        uint64_t theTimeStamp)
{
    uint16_t aNumOfSubPackets = NumberOfSubPackets (theExtendedData);
    if (aNumOfSubPackets == 1) {

        InsertFirstSubPacket (theExtendedData, thePlayloadData,
                              theDataLenght, theTimeStamp, 0, 1);

        ProcessFilledPacket (theTimeStamp);
        ORI_LOG_INFO (myLogger) << "Received single packet, size = " << theDataLenght << ", timestamp = "
                                << theTimeStamp;
    } else {

        ProcessSubPacket  (theExtendedData, thePlayloadData, theDataLenght, theTimeStamp);
    }
}

void PacketArena::ProcessSubPacket (uint8_t* theExtendedData,
                                    uint8_t* thePlayloadData,
                                    size_t   theDataLenght,
                                    uint64_t theTimeStamp)
{
    uint16_t aNumOfSubPackets = NumberOfSubPackets (theExtendedData);
    uint16_t aSubIndex = SubIndex (theExtendedData);
    auto anIt = myPacketMap.find (theTimeStamp);

    if (anIt == myPacketMap.end()) {

        InsertFirstSubPacket (theExtendedData, thePlayloadData, theDataLenght,
                              theTimeStamp, aSubIndex, aNumOfSubPackets);
    } else {

        InsertSubPacket (thePlayloadData, theDataLenght, theTimeStamp, aSubIndex);
    }
}

void PacketArena::InsertFirstSubPacket (uint8_t* theExtendedData,
                                        uint8_t* thePlayloadData,
                                        size_t   theDataLenght,
                                        uint64_t theTimeStamp,
                                        uint16_t theSubIndex,
                                        uint16_t theNumOfSubPackets)
{
    bool anIsKey = IsKey (theExtendedData);
    if (myFreePackets.empty()) {
        auto aPacket = std::make_shared <RTPPacket>();
        myPacketMap.emplace (theTimeStamp, aPacket);
    } else {

        bool anIsFound = false;
        for (const auto& aPair : myFreePackets) {

            const auto& aPacket = aPair.second;
            if (aPacket->IsKey() == anIsKey) {

                myPacketMap.emplace (theTimeStamp, aPacket);
                myFreePackets.erase (aPair.first);
                anIsFound = true;
                break;
            }
        }

        if (!anIsFound) {
            for (const auto& aPair : myFreePackets) {
                const auto& aPacket = aPair.second;
                myPacketMap.emplace (theTimeStamp, aPacket);
                myFreePackets.erase (aPair.first);
                break;
            }
        }
    }

    auto& aPacket = myPacketMap[theTimeStamp];

    auto& aPacketData = aPacket->Buffer();
    size_t aPacketLenght = PacketLenght (theExtendedData);
    aPacketData.Reserve (static_cast <int> (aPacketLenght));

    int aStartPos = theSubIndex * RTP_DEFAULTPACKETSIZE;
    aPacketData.Insert (thePlayloadData, static_cast <int> (theDataLenght), aStartPos);

    aPacket->NumberOfSubpackets() = theNumOfSubPackets;
    aPacket->ReceivedSubPacketIndices().Push (theSubIndex);
    aPacket->IsKey() = anIsKey;
}

void PacketArena::InsertSubPacket (uint8_t* thePlayloadData,
                                   size_t   theDataLenght,
                                   uint64_t theTimeStamp,
                                   uint16_t theSubIndex)
{
    const auto& anIt = myPacketMap.find (theTimeStamp);
    if (anIt == myPacketMap.end()) {
        throw Exception ("Can not find Packet by subindex");
    }

    auto& aPacket = *anIt->second;
    auto& aBuf = aPacket.Buffer();

    int aStartPos = theSubIndex * RTP_DEFAULTPACKETSIZE;
    aBuf.Insert (thePlayloadData, static_cast <int> (theDataLenght), aStartPos);
    aPacket.ReceivedSubPacketIndices().Push (theSubIndex);

    if (aPacket.ReceivedSubPacketIndices().Lenght() == aPacket.NumberOfSubpackets()) {

        ORI_LOG_INFO (myLogger) << "Received multiple packet, size = " << aPacket.Buffer().Lenght()
                                << ", number of subpackets = " << aPacket.NumberOfSubpackets()
                                << ", timestamp = " << theTimeStamp;

        ProcessFilledPacket (theTimeStamp);
    }
}

void PacketArena::ProcessFilledPacket (uint64_t theTimeStamp)
{
    const auto& anIt = myPacketMap.find (theTimeStamp);

    if (theTimeStamp == myExpectedTimestamp) {

        ORI_LOG_INFO(myLogger) << "Filled packet with expected timestamp, timestamp = " << myExpectedTimestamp; 
        GiveSignal (theTimeStamp, anIt->second);
        ProcessNextPacket();
    } else if (anIt->second->IsKey()) {

        ORI_LOG_INFO(myLogger) << "Filled key packet, expected timestamp = " << myExpectedTimestamp
                               << ", packet timestamp = " << theTimeStamp;
        GiveSignal (theTimeStamp, anIt->second);
        DropOutdatedPackets (theTimeStamp);
    } else {

            ORI_LOG_INFO(myLogger) << "Filled packet with unexpected timestamp, expected = " << myExpectedTimestamp
                                   << ", packet timestamp = " << theTimeStamp;

            myNumberOfUnexpectedPackets++;
            if (myNumberOfUnexpectedPackets >= 2) {

                if (!myRecuiredPackets.Contains (myExpectedTimestamp)) {
                    RequireLostedSubpackets();

                    if (myRecuiredPackets.Lenght() >= 10) {
                        myRecuiredPackets.Reset();
                    }
                    myRecuiredPackets.Push (myExpectedTimestamp);
                }
                myNumberOfUnexpectedPackets = 0;
            }
    }
}

void PacketArena::DropOutdatedPackets (uint64_t theKeyPacketTimeStamp)
{ 
    Vector <uint64_t> anOutdatedIndices;
    for (const auto& anIt : myPacketMap) {
        if (anIt.first < theKeyPacketTimeStamp) {
            anOutdatedIndices.Push (anIt.first);
        }
    }

    if (!anOutdatedIndices.Empty()) {
        ORI_LOG_INFO(myLogger) << "Dropped packets.";
    }

    for (const auto& i : anOutdatedIndices) {
        const auto& aValue = myPacketMap[i];
        ResetPacket (i, aValue);
    }
}

void PacketArena::GiveSignal (uint64_t theTimeStamp, const std::shared_ptr <RTPPacket>& thePacket)
{
    if (thePacket->IsKey()) {
        ORI_LOG_VERBOSE (myLogger) << "Received 'I' packet";
    } else {
        ORI_LOG_VERBOSE (myLogger) << "Received 'P' packet";
    }
    
    myObserver.OnPacket (thePacket->Buffer());
    myExpectedTimestamp = theTimeStamp + 1;

    ResetPacket (theTimeStamp, thePacket);
}

void PacketArena::ProcessNextPacket()
{
    const auto& anIt = myPacketMap.find (myExpectedTimestamp);
    if (anIt != myPacketMap.end()) {
        myNumberOfUnexpectedPackets = myNumberOfUnexpectedPackets == 0 ? 0 : (myNumberOfUnexpectedPackets - 1);
        GiveSignal (anIt->first, anIt->second);
        ProcessNextPacket();
    }
}

void PacketArena::RequireLostedSubpackets()
{
    const auto& anIt = myPacketMap.find (myExpectedTimestamp);
    if (anIt == myPacketMap.end()) {
        RequireLostedPacket (myExpectedTimestamp);
    } else {

        const auto& aPacket = *anIt->second;
        const auto& aBuf = aPacket.ReceivedSubPacketIndices();

        Vector <uint16_t> aSubIndices;
        for (int i = 0; i < aPacket.NumberOfSubpackets(); i++) {
            const auto anIt = std::find (aBuf.begin(), aBuf.end(), i);
            if (anIt == aBuf.end()) {
                aSubIndices.Push (*anIt);
            }
        }
    
        DoRequireLostedSubpackets (myExpectedTimestamp, aSubIndices);
    }
}

void PacketArena::ResetPacket (uint64_t theTimeStamp, const std::shared_ptr <RTPPacket>& thePacket)
{
    // We reuse that packet for other received packet, therefore don't free allocated buffer,
    // just reset it.
    thePacket->Buffer().Reset();
    thePacket->ReceivedSubPacketIndices().Reset();
    thePacket->NumberOfSubpackets() = -1;

    myFreePackets.emplace (theTimeStamp, thePacket);
    myPacketMap.erase (theTimeStamp);
}

void PacketArena::DoRequireLostedSubpackets (uint64_t theTimestamp, const Base::Vector <uint16_t>& theIndices)
{
    Base::Vector <uint8_t> aVec;
    auto aVecSize = sizeof (uint64_t) + theIndices.Lenght() * sizeof (uint16_t);
    aVec.Reserve (static_cast <int> (aVecSize));
    
    TypeBuffer <uint64_t> aBuffer (theTimestamp);
    aVec.Push (aBuffer.Bytes, aBuffer.Bytes + sizeof (uint64_t));

    const uint8_t* anIndexData = reinterpret_cast <const uint8_t*> (theIndices.Data());
    aVec.Push (anIndexData, anIndexData + theIndices.Lenght() * sizeof (uint16_t));

    myListener.Send (aVec.Data(), aVec.Lenght());
}

void PacketArena::RequireLostedPacket (uint64_t theTimestamp)
{
    TypeBuffer <uint64_t> aBuffer (theTimestamp);
    myListener.Send (aBuffer.Bytes, sizeof (uint64_t));
}

}}
