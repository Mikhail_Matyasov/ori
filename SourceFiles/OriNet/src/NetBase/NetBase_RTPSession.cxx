#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetBase_RTPSession.hxx>

#include <Ori/Base_TypeBuffer.hxx>

#include <rtpsourcedata.h>
#include <rtpipv4address.h>

namespace Ori {
namespace NetBase {

RTPSession::RTPSession (Net::RTPListener::Observer& theObserver, const Base::Logger& theLogger, int theTcpPort) : 
    myPacketArena (theObserver, *this, theLogger, theTcpPort)
{}

void RTPSession::OnValidatedRTPPacket (jrtplib::RTPSourceData*, jrtplib::RTPPacket* thePacket, bool, bool*)
{
    myPacketArena.ProcessRTPPacket (thePacket);
}

}}
