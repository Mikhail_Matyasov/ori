#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetBase_TcpHelper.hxx>

#include <Ori/Base_Timer.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Net_TcpObserver.hxx>

#include <boost/bind.hpp>

using namespace Ori::Base;

namespace Ori {
namespace NetBase {

TcpHelper::TcpHelper (boost::asio::io_context& theContext, boost::asio::ip::tcp::socket& theSocket) :
    myContext (theContext),
    mySocket (theSocket),
    myIsReading (false)
{}

void TcpHelper::RegisterObserver (Net::TcpObserver* theObserver)
{
    myObserver = theObserver;
}

void TcpHelper::Listen()
{
    try {
    myListenThread = std::make_shared <boost::thread> ([&]() -> void {
        myIsActive = true;
        while (myIsActive)
        {
            if (!myIsReading) {
                myIsReading = true;

                boost::system::error_code anError;
                auto aBuffer = boost::asio::buffer (&myBuffer.Lenght(), sizeof (myBuffer.Lenght()));

                boost::asio::read (mySocket,
                                   aBuffer,
                                   anError);
                if (!anError) {
                    OnLenghtAvaliable();
                } else {
                    myIsReading = false;
                }

                Timer::Sleep (500);
            } else {
                ORI_LOG_VERBOSE_DEFAULT() << "Sleep until data reading";
                Timer::Sleep (500);
            }
        }
    });
    } catch (...) {}
}

void TcpHelper::OnLenghtAvaliable()
{
    ORI_LOG_INFO_DEFAULT() << "Reading buffer length";

    myBuffer.PacketBuffer().Resize (myBuffer.Lenght());
    uint8_t* aData = myBuffer.PacketBuffer().Data();
    int aLength = myBuffer.PacketBuffer().Lenght();

    boost::system::error_code anError;
    boost::asio::read (mySocket,
                       boost::asio::buffer (aData,
                                            aLength),
                       anError);
    if (!anError) {
        OnBufferAvaliable();
    }
    myIsReading = false;
}

void TcpHelper::OnBufferAvaliable()
{
    ORI_LOG_INFO_DEFAULT() << "Reading buffer";

    if (myObserver) {
        myObserver->OnPacket (myBuffer.PacketBuffer());
    }
    myBuffer.PacketBuffer().Reset();
}

void TcpHelper::RunContext()
{
    myContextThread = std::make_shared <boost::thread> ([&]() -> void {
        try  {
            myContext.run();
        } catch (...) {}
    });
}

void TcpHelper::Stop()
{
    ORI_LOG_VERBOSE_DEFAULT() << "Stoping tcp helper";

    mySocket.close();
    myContext.stop();
    if (myContextThread) {
        myContextThread->join();
    }
    if (myListenThread) {
        myIsActive = false;
        myListenThread->join();
    }
}

bool TcpHelper::Send (const uint8_t* theData, int theLenght)
{
    boost::system::error_code anError;

    auto aLenght = boost::asio::buffer (&theLenght, sizeof (int));
    boost::asio::write (mySocket, aLenght, anError);
    bool aRes = anError.failed();

    auto aBuffer = boost::asio::buffer (theData, theLenght);
    boost::asio::write (mySocket, aBuffer, anError);
    aRes &= anError.failed();

    return !aRes;
}

}}
