#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetBase_ConnectionCheckerImpl.hxx>

#include <Ori/NetBase_IcmpHeader.hxx>
#include <Ori/NetBase_IPv4Header.hxx>
#include <Ori/Net_ConnectionStatistics.hxx>

#include <boost/bind.hpp>

namespace Ori {
namespace NetBase {
ConnectionCheckerImpl::ConnectionCheckerImpl (const Base::String& theHost) :
    mySocket (myContext, icmp::v4()),
    myTimer (myContext),
    mySequenceNumber (0),
    myNumberOfReplies (0)
{
    icmp::resolver aResolver (myContext);
    myEndPoint = *aResolver.resolve (icmp::v4(), theHost.Data(), "").begin();
    StartSend();
    StartReceive();
    
    myContextThread = std::make_shared <boost::thread> ([&]() -> void {
        try {
            myContext.run();
        } catch (...) {}
    });
}

ConnectionCheckerImpl::~ConnectionCheckerImpl()
{
    mySocket.close();
    myContext.stop();
    if (myContextThread) {
        myContextThread->join();
    }
}

void ConnectionCheckerImpl::RegisterObserver (Net::ConnectionChecker::Observer* theObserver)
{
    myObserver = theObserver;
}

void ConnectionCheckerImpl::SetHost (const Base::String& theHost)
{
    myHost = theHost;
    // Update
}

const Base::String& ConnectionCheckerImpl::Host() const
{
    return myHost;
}

void ConnectionCheckerImpl::StartSend()
{
    auto aBuffer = IcmpHeader::Buffer (IcmpHeader::HeaderType::EchoRequest, ++mySequenceNumber);

    // Send the request.
    mySentTime = steady_timer::clock_type::now();
    mySocket.send_to (aBuffer->data(), myEndPoint);

    // Wait up to five seconds for a reply.
    myNumberOfReplies = 0;
    myTimer.expires_at (mySentTime + chrono::seconds(5));
    myTimer.async_wait (boost::bind (&ConnectionCheckerImpl::OnTimeout,
                        this));
}

void ConnectionCheckerImpl::StartReceive()
{
    // Clear the buffer
    myBuffer.consume (myBuffer.size());

    // Wait for a reply. We prepare the buffer to receive up to 64KB.
    mySocket.async_receive (myBuffer.prepare (65536),
                            boost::bind (&ConnectionCheckerImpl::OnReply,
                            this,
                            boost::placeholders::_2));
}

void ConnectionCheckerImpl::OnTimeout()
{
    if (myNumberOfReplies == 0) {
        if (myObserver) {
            myObserver->OnTimeout();
        }
    }

    // Requests must be sent no less than one second apart.
    myTimer.expires_at (mySentTime + chrono::seconds(1));
    myTimer.async_wait (boost::bind (&ConnectionCheckerImpl::StartSend, this));
}

void ConnectionCheckerImpl::OnReply (std::size_t theLength)
{
    // Resize buffer
    myBuffer.commit (theLength);

    IPv4Header anIpv4 (myBuffer);
    IcmpHeader anIcmp (myBuffer);

    // We can receive all ICMP packets received by the host, so we need to
    // filter out only the echo replies that match the our identifier and
    // expected sequence number.
    auto aType = anIcmp.Type();
    uint16_t anId =  anIcmp.Identifire();
    uint16_t aSeqNumber = anIcmp.SequenceNumber();
    if (aType == IcmpHeader::HeaderType::EchoReply &&
        anId == IcmpHeader::ProcessId() && 
        aSeqNumber == mySequenceNumber) {

        if (myNumberOfReplies++ == 0) {
            myTimer.cancel();
        }
  
        Net::ConnectionStatistics aStatistics;
        aStatistics.TimeToLive() = anIpv4.TimeToLive();
        aStatistics.BytesReceived() = static_cast <int> (theLength - anIpv4.HeaderLenght());

        auto anElapsedTime = chrono::steady_clock::now() - mySentTime;
        aStatistics.ReplyTime() = chrono::duration_cast <chrono::milliseconds> (anElapsedTime).count();

        if (myObserver) {
            myObserver->OnStatistics (aStatistics);
        }
    }

    StartReceive();
}

}}
