#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetBase_IPv4Header.hxx>


namespace Ori {
namespace NetBase {

IPv4Header::IPv4Header (boost::asio::streambuf& theBuf)
{
    std::istream aStream (&theBuf);
    aStream.read (reinterpret_cast<char*> (myBuffer), 20);
    if (Version() != 4) {
        aStream.setstate (std::ios::failbit);
    }

    std::streamsize anOptionsLenght = static_cast <uint8_t> (HeaderLenght() - 20);
    if (anOptionsLenght < 0 || anOptionsLenght > 40) {
        aStream.setstate (std::ios::failbit);
    } else {
        aStream.read (reinterpret_cast<char*> (myBuffer) + 20, anOptionsLenght);
    }
}

int IPv4Header::Version() const
{
    return (myBuffer[0] >> 4) & 0xF;
}

int IPv4Header::HeaderLenght() const
{
    return static_cast <int> ((myBuffer[0] & 0xF) * 4);
}

uint8_t IPv4Header::TypeOfService() const
{
    return myBuffer[1];
}

namespace {
uint16_t Decode (int i1, int i2, const uint8_t* theBuffer)
{
    return (theBuffer[i1] << 8) + theBuffer[i2];
}
}

uint16_t IPv4Header::TotalLength() const
{
    return Decode (2, 3, myBuffer);
}

uint16_t IPv4Header::Identification() const
{
    return Decode (4, 5, myBuffer);
}

bool IPv4Header::DontFragment() const
{
    return (myBuffer[6] & 0x40) != 0;
}

bool IPv4Header::MoreFragments() const
{
    return (myBuffer[6] & 0x20) != 0;
}

uint16_t IPv4Header::FragmentOffset() const
{
    return Decode (6, 7, myBuffer) & 0x1FFF;
}

uint32_t IPv4Header::TimeToLive() const
{
    return myBuffer[8];
}

uint8_t IPv4Header::Protocol() const
{
    return myBuffer[9];
}

uint16_t IPv4Header::HeaderChecksum() const
{
    return Decode (10, 11, myBuffer);
}

boost::asio::ip::address_v4 IPv4Header::SourceAdress() const
{
    boost::asio::ip::address_v4::bytes_type aBytes = {
        { myBuffer[12],
          myBuffer[13],
          myBuffer[14],
          myBuffer[15] }
    };

    return boost::asio::ip::address_v4 (aBytes);
}

boost::asio::ip::address_v4 IPv4Header::DestinationAdress() const
{
    boost::asio::ip::address_v4::bytes_type aBytes = {
        { myBuffer[16],
          myBuffer[17],
          myBuffer[18],
          myBuffer[19] }
    };
    return boost::asio::ip::address_v4 (aBytes);
}

}}
