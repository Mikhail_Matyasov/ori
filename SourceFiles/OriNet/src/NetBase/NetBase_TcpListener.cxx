#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetBase_TcpListener.hxx>

#include <Ori/Base_Timer.hxx>
#include <Ori/Base_Logger.hxx>

#include <boost/bind.hpp>
#include <boost/thread.hpp>

using namespace boost::asio::ip;
using namespace Ori::Base;

namespace Ori {
namespace NetBase {

TcpListener::TcpListener (int thePort) :
    mySocket (myContext),
    myEndPoint (tcp::v4(), thePort),
    myAcceptor (myContext, myEndPoint),
    myHelper (myContext, mySocket)
{
    Accept();
    myHelper.RunContext();
}

void TcpListener::RegisterObserver (Net::TcpObserver* theObserver)
{
    myHelper.RegisterObserver (theObserver);
}

bool TcpListener::Send (const uint8_t* theData, int theLenght)
{
    return myHelper.Send (theData, theLenght);
}

TcpListener::~TcpListener()
{
    ORI_LOG_VERBOSE_DEFAULT() << "Stoping tcp listener";
    myHelper.Stop();
}

void TcpListener::OnAccepted (const boost::system::error_code& theError)
{
    if (!theError) {
        myHelper.Listen();
    }
}

void TcpListener::Accept()
{
    myAcceptor.async_accept (mySocket, boost::bind (&TcpListener::OnAccepted,
                                                    this,
                                                    boost::asio::placeholders::error));
}

}}
