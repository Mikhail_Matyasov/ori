#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetBase_IcmpHeader.hxx>

#include <boost/asio.hpp>
#include <istream>
#include <iostream>
#include <ostream>

namespace Ori {
namespace NetBase {

IcmpHeader::IcmpHeader (boost::asio::streambuf& theBuf)
{
    std::istream aStream (&theBuf);
    aStream.read (reinterpret_cast <char*> (myBuffer), 8);
}

namespace {
uint16_t Decode (int i1, int i2, const uint8_t* theBuffer)
{
    return (theBuffer[i1] << 8) + theBuffer[i2];
}
}

IcmpHeader::HeaderType IcmpHeader::Type() const
{
    HeaderType aType = static_cast <HeaderType> (myBuffer[0]);
    return aType;
}

uint8_t IcmpHeader::Code() const
{
    return myBuffer[1];
}

uint16_t IcmpHeader::Checksum() const
{
    return Decode (2, 3, myBuffer);
}

uint16_t IcmpHeader::Identifire() const
{
    return Decode (4, 5, myBuffer);
}

uint16_t IcmpHeader::SequenceNumber() const
{
    return Decode (6, 7, myBuffer);
}

namespace {

void Encode (int i1, int i2, uint16_t theVal, uint8_t* theTarget)
{
    theTarget[i1] = static_cast <uint8_t> (theVal >> 8);
    theTarget[i2] = static_cast <uint8_t> (theVal & 0xFF);
}

uint16_t CheckSum (uint8_t theType, uint8_t theCode, uint16_t theId, uint16_t theSeqNumber, const std::string& theBody)
{
    uint32_t aSum = (theType << 8) + theCode + theId + theSeqNumber;

    for (auto anIt = theBody.begin(); anIt != theBody.end(); ) {

        aSum += (static_cast <unsigned char> (*anIt++) << 8);
        if (anIt != theBody.end()) {
            aSum += static_cast <unsigned char> (*anIt++);
        }
    }

    aSum = (aSum >> 16) + (aSum & 0xFFFF);
    aSum += (aSum >> 16);
    return ~aSum;
}
}

std::shared_ptr <boost::asio::streambuf> IcmpHeader::Buffer (HeaderType theType, uint16_t theSeqNumber)
{
    uint8_t aHeader[8];

    uint8_t aType = static_cast <uint8_t> (theType);
    aHeader[0] = aType;
    aHeader[1] = 0; // Code

    uint16_t aProcessId = ProcessId();
    Encode (4, 5, aProcessId, aHeader);
    Encode (6, 7, theSeqNumber, aHeader);

    std::string aBody = "Hello from Ori";
    uint16_t aCheckSum = CheckSum (aType, 0, aProcessId, theSeqNumber, aBody);
    Encode (2, 3, aCheckSum, aHeader);

    auto aStreamBuf = std::make_shared <boost::asio::streambuf>();
    std::ostream aStream (aStreamBuf.get());
    aStream.write (reinterpret_cast <const char*> (aHeader), 8);
    aStream << aBody.c_str();

    return aStreamBuf;
}

uint16_t IcmpHeader::ProcessId()
{
#if defined(BOOST_ASIO_WINDOWS)
    return static_cast <uint16_t> (::GetCurrentProcessId());
#else
    return static_cast <uint16_t> (::getpid());
#endif
}

}}
