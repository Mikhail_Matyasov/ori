#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetAndroid_NetworkInfoImpl.hxx>

#include <Ori/NetAndroid_WiFiManager.hxx>
#include <Ori/NetAndroid_WiFiHotSpotImpl.hxx>

using namespace Ori::Base;

namespace Ori {
namespace NetAndroid {

Vector <Net::WiFiHotSpot> NetworkInfoImpl::AvaliableHotSpots()
{
    Vector <Net::WiFiHotSpot> aHotSpots;

    auto aWiFi = WiFiManager::Instance();
    auto aPoints = aWiFi->AvaliableAccessPoints();
    String aHotSpotSSID;
    for (int i = 0; i < aPoints.Length(); i++) {
        if (aPoints[i] == '|') {
            auto anImpl = std::make_shared <WiFiHotSpotImpl> (aHotSpotSSID);
            Net::WiFiHotSpot aHS (anImpl);
            aHotSpots.Push (aHS);
            aHotSpotSSID = "";
            continue;
        }
        aHotSpotSSID += aPoints[i];
    }

    return aHotSpots;
}

}}
