#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetAndroid_WiFiHotSpotImpl.hxx>

#include <Ori/NetAndroid_WiFiManager.hxx>

namespace Ori {
namespace NetAndroid {

WiFiHotSpotImpl::WiFiHotSpotImpl (const Base::String& theSSID) :
    mySSID (theSSID)
{}

void WiFiHotSpotImpl::OpenWiFiSettings()
{
    auto aWiFi = WiFiManager::Instance();
    return aWiFi->OpenWiFiSettings();
}

}}
