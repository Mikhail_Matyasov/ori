#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetAndroid_WiFiManager.hxx>

#include <QtAndroidExtras/qandroidjniobject.h>
#include <QtAndroidExtras/QtAndroid>

namespace Ori {
namespace NetAndroid {

WiFiManager::WiFiManager()
{
    myImpl = std::make_shared <QAndroidJniObject> (
        "org/ori/orimobilejava/OriMobileJava_WiFiManager",
        "(Landroid/content/Context;Landroid/app/Activity;)V",
        QtAndroid::androidContext().object(),
        QtAndroid::androidActivity().object());
}

WiFiManager* WiFiManager::Instance()
{
    static WiFiManager* anInstance = new WiFiManager();
    return anInstance;
}

Base::String WiFiManager::AvaliableAccessPoints()
{
    QString anAccessPoints;
    while (true) {
        auto aPoints = myImpl->callObjectMethod <jstring> ("AvaliableAccessPoints");
        anAccessPoints = aPoints.toString();
        if (!anAccessPoints.isEmpty()) {
            break;
        }
    }

    return anAccessPoints.toStdString().c_str();
}

void WiFiManager::OpenWiFiSettings()
{
    myImpl->callMethod <void> ("OpenWiFiSettings");
}

}}
