#include <Ori/NetBase_Pch.hxx>
#include <Ori/NetUnix_TcpClientImpl.hxx>

#include <Ori/Base_Logger.hxx>
#include <Ori/Base_Timer.hxx>
#include <Ori/Net_TcpObserver.hxx>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

using namespace Ori::Net;
using namespace Ori::Base;

namespace Ori {
namespace NetUnix {

TcpClientImpl::TcpClientImpl (const Net::IPEndPoint& theEndPoint) :
    myIPEndPoint (theEndPoint),
    myObserver (nullptr),
    mySocket (-1),
    myIsConnected (false)
{
    mySocket = socket (AF_INET, SOCK_STREAM, 0);
    if (mySocket < 0) {
        myError = errno;
    }

    if (!myIPEndPoint.IsNull()) {
        DoConnect();
    }

    std::thread T ([&]() -> void {

        while (true) {
            const int aReceiveBufferLength = 2000;
            char aBuffer [aReceiveBufferLength];
            int aReceivedLength = recv (mySocket, aBuffer, aReceiveBufferLength, 0);
            if (aReceivedLength > 0) {
                if (myObserver) {
                    ByteArrayView aPacket (reinterpret_cast <const uint8_t*> (aBuffer),
                                           aReceivedLength);
                    myObserver->OnPacket (aPacket);
                }
            } else if (aReceivedLength == 0) {
                Destroy();
                ORI_LOG_DEBUG_DEFAULT() << "Connection closed";
            } else {
                // recv failed with error
                myError = errno;
            }
        }

    });
    T.detach();
}

TcpClientImpl::~TcpClientImpl()
{
    Destroy();
}

bool TcpClientImpl::DoConnect()
{
    sockaddr_in anAdress;
    if (inet_pton (AF_INET, myIPEndPoint.IPAdress().Data(), &anAdress.sin_addr) <= 0) {
        return false;
    }
    anAdress.sin_family = AF_INET;
    anAdress.sin_port = htons (myIPEndPoint.Port());

    if (connect (mySocket, reinterpret_cast <sockaddr*> (&anAdress), sizeof (anAdress)) < 0) {
        myError = errno;
        mySocket = -1;
        return false;
    }
    myIsConnected = true;
    return true;
}

void TcpClientImpl::Destroy()
{
    close (mySocket);
}

bool TcpClientImpl::Connect (const Net::IPEndPoint& theEndPoint)
{
    if (!theEndPoint.IsNull()) {
        myIPEndPoint = theEndPoint;
    }

    if (!myIPEndPoint.IsNull()) {
        return DoConnect();
    }
    return false;
}

bool TcpClientImpl::Connected() const
{
    return myIsConnected;
}

bool TcpClientImpl::Send (const uint8_t* theData, int theLength)
{
    Timer::Sleep (1000);
    int aRes = send (mySocket, theData, theLength, 0);
    if (aRes == -1) {
        return false;
    }
    return true;
}

void TcpClientImpl::RegisterObserver (Net::TcpObserver* theObserver)
{
    myObserver = theObserver;
}

}}
