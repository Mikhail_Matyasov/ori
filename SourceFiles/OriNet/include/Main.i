%module(directors="1") OriNet

%include <Base_Headers.i>
%import <Base_String.i>
%import <Base_ByteArray.i>
%import <Base_Logger.i>

%include <Net_RTPEndPoint.i>
%include <Net_RTPSessionStatus.i>
%include <Net_RTPClient.i>
%include <Net_RTPListener.i>