%feature("nspace") Ori::Net::RTPListener;
%feature("nspace") Ori::Net::RTPListener::Observer;

%{
#include <Ori/Net_RTPListener.hxx>
%}

%csmethodmodifiers Ori::Net::RTPListener::Observer::OnPacket "public virtual";
%feature("director") Ori::Net::RTPListener::Observer;

%include <Ori/Net_RTPListener.hxx>
