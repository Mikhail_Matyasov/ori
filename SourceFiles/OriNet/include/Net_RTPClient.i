%feature("nspace") Ori::Net::RTPClient;
%feature("nspace") Ori::Base::Vector <Ori::Net::RTPEndPoint>;

%{
#include <Ori/Net_RTPClient.hxx>
%}

typedef unsigned char uint8_t;
%csmethodmodifiers Ori::Net::RTPClient::Send "public unsafe";
%apply unsigned char FIXED[] { unsigned char* thePacket }

%include <Ori/Net_RTPClient.hxx>
