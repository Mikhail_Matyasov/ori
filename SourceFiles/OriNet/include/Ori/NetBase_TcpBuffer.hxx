#ifndef _Ori_NetBase_TcpBuffer_HeaderFile
#define _Ori_NetBase_TcpBuffer_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace NetBase {

class TcpBuffer
{
public:
    __ORI_PRIMITIVE_PROPERTY (int, Lenght);
    __ORI_PROPERTY (Base::ByteArray, PacketBuffer);
};

}}

#endif
