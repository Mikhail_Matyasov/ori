#ifndef _Ori_NetBase_RTPPacket_HeaderFile
#define _Ori_NetBase_RTPPacket_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace NetBase {

class RTPPacket
{
    __ORI_PROPERTY (Base::ByteArray, Buffer);
    __ORI_PRIMITIVE_PROPERTY (int, NumberOfSubpackets);
    __ORI_PROPERTY (Base::Vector <uint16_t>, ReceivedSubPacketIndices);
    __ORI_PRIMITIVE_PROPERTY (bool, IsKey);

};

}}


#endif