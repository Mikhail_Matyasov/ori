#ifndef _Ori_Net_ConnectionStatistics_HeaderFile
#define _Ori_Net_ConnectionStatistics_HeaderFile

#include <Ori/Base_Macros.hxx>

namespace Ori {
namespace Net {

class ConnectionStatistics
{
    __ORI_PRIMITIVE_PROPERTY (int, TimeToLive);
    __ORI_PRIMITIVE_PROPERTY (int, BytesReceived);
    __ORI_PRIMITIVE_PROPERTY (int64_t, ReplyTime);
};

}}

#endif
