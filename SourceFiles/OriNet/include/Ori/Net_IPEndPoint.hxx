#ifndef _Ori_Net_IPEndPoint_HeaderFile
#define _Ori_Net_IPEndPoint_HeaderFile

#include <Ori/Base_String.hxx>

namespace Ori {
namespace Net {

class IPEndPoint
{
public:
    IPEndPoint() :
        myIPAdress (""),
        myPort (-1)
    {}

    IPEndPoint (const Base::String& theIPAdress, int thePort) :
        myIPAdress (theIPAdress),
        myPort (thePort)
    {}

    bool IsNull() const
    { return myIPAdress.Empty() || myPort < 0; }

    __ORI_PROPERTY (Base::String, IPAdress);
    __ORI_PROPERTY (int, Port);
    
};

}}


#endif
