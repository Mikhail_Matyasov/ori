#ifndef _Ori_NetWin_NetworkObserver_HeaderFile
#define _Ori_NetWin_NetworkObserver_HeaderFile

#include <functional>
#include <wtypes.h>
#include <wlanapi.h>

namespace Ori {
namespace NetWin {

class NetworkObserver
{
public:
    static void ObserveAvaliableNetworks (
        std::function <bool (const WLAN_AVAILABLE_NETWORK_V2&, HANDLE, GUID)> theFunc,
        bool theIsScan = true);
};

}}


#endif
