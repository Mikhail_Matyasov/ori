#ifndef _Ori_RTP_RTPListener_HeaderFile
#define _Ori_RTP_RTPListener_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Net_IPEndPoint.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Net_RTPSessionStatus.hxx>

#include <memory>

namespace boost {
class thread;
}

namespace Ori {
namespace NetBase {
class RTPSession;
}
namespace Net {

class RTPListener
{
public:

    class Observer
    {
    public:
        virtual void OnPacket (const Base::ByteArray& thePacket) {}
        virtual ~Observer() {}
    };

    ORI_EXPORT RTPListener (int theUdpPort,
                            int theTcpPort,
                            RTPListener::Observer& theObserver,
                            const Base::Logger& theLogger = Base::Logger());
    ORI_EXPORT ~RTPListener();
    ORI_EXPORT void Start();
    ORI_EXPORT void Stop();
    __ORI_READONLY_PRIMITIVE_PROPERTY (bool, IsSessionActive);
    __ORI_READONLY_PRIMITIVE_PROPERTY (RTPSessionStatus, Status);

private:
    std::shared_ptr <NetBase::RTPSession> myRTPSession;
    std::shared_ptr <boost::thread> myListenThread;
    Base::Logger myLogger;
};

}}


#endif