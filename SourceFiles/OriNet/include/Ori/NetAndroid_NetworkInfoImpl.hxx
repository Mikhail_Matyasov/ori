#ifndef _Ori_NetAndroid_NetworkInfoImpl_HeaderFile
#define _Ori_NetAndroid_NetworkInfoImpl_HeaderFile

#include <Ori/Base_Vector.hxx>
#include <Ori/Net_WiFiHotSpot.hxx>

namespace Ori {
namespace NetAndroid {

class NetworkInfoImpl
{
public:
    static Base::Vector <Net::WiFiHotSpot> AvaliableHotSpots();

};

}}


#endif
