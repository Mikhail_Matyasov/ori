#ifndef _Ori_Net_TcpObserver_HeaderFile
#define _Ori_Net_TcpObserver_HeaderFile

#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace Net {

class TcpObserver
{
public:
    virtual void OnPacket (const Base::ByteArrayView& thePacket) = 0;
};

}}


#endif