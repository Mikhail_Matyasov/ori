#ifndef _Ori_NetBase_Macros_HeaderFile
#define _Ori_NetBase_Macros_HeaderFile


#ifdef WIN32

#define __ORI_NET_DEFINE_WIN_ANDROID_IMPL(TYPE) \
    namespace Ori { \
    namespace NetWin { \
    class TYPE ## Impl; \
    \
    }} \
    using TYPE ## Impl = Ori::NetWin:: ## TYPE ## Impl; \

#define __ORI_NET_DEFINE_WIN_UNIX_IMPL(TYPE) \
    __ORI_NET_DEFINE_WIN_ANDROID_IMPL(TYPE)

#else

    #define __ORI_NET_DEFINE_WIN_UNIX_IMPL(TYPE) \
    namespace Ori { \
    namespace NetUnix { \
    class TYPE ## Impl; \
    \
    }} \
    using TYPE ## Impl = Ori::NetUnix::TYPE ## Impl;

#ifdef __ANDROID__
    #define __ORI_NET_DEFINE_WIN_ANDROID_IMPL(TYPE) \
    namespace Ori { \
    namespace NetAndroid { \
    class TYPE ## Impl; \
    \
    }} \
    using TYPE ## Impl = Ori::NetAndroid::TYPE ## Impl;
#else
    
#endif

#endif

#endif
