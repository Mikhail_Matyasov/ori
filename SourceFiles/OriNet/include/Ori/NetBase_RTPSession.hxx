#ifndef _Ori_NetBase_RTPSession_HeaderFile
#define _Ori_NetBase_RTPSession_HeaderFile

#include <Ori/Net_RTPListener.hxx>
#include <Ori/NetBase_PacketArena.hxx>

#include <rtpsession.h>

namespace jrtplib {
class RTPSourceData;
class RTPPacket;
}

namespace Ori {
namespace NetBase {

class RTPSession : public jrtplib::RTPSession
{

public:
    RTPSession (Net::RTPListener::Observer& theObserver, const Base::Logger& theLogger, int theTcpPort);

protected:
    virtual void OnValidatedRTPPacket (jrtplib::RTPSourceData*, jrtplib::RTPPacket* thePacket, bool, bool*);

private:
    PacketArena myPacketArena;
};

}}

#endif
