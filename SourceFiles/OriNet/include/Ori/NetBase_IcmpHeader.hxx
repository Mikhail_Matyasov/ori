#ifndef _Ori_NetBase_IcmpHeader_HeaderFile
#define _Ori_NetBase_IcmpHeader_HeaderFile

#include <istream>
#include <ostream>
#include <algorithm>
#include <boost/asio/streambuf.hpp>

namespace Ori {
namespace NetBase {

// ICMP header for both IPv4 and IPv6.
//
// The wire format of an ICMP header is:
// 
// 0               8               16                             31
// +---------------+---------------+------------------------------+      ---
// |               |               |                              |       ^
// |     type      |     code      |          checksum            |       |
// |               |               |                              |       |
// +---------------+---------------+------------------------------+    8 bytes
// |                               |                              |       |
// |          identifier           |       sequence number        |       |
// |                               |                              |       v
// +-------------------------------+------------------------------+      ---

class IcmpHeader
{
public:
    enum class HeaderType 
    { 
        EchoReply = 0,
        DestinationUnreachable = 3,
        SourceQuench = 4,
        Redirect = 5,
        EchoRequest = 8,
        TimeExceeded = 11,
        ParameterProblem = 12,
        TimestampRequest = 13,
        TimestampReply = 14,
        InfoRequest = 15,
        InfoReply = 16,
        AddressRequest = 17,
        AddressReply = 18
    };

    IcmpHeader (boost::asio::streambuf& theBuf);
    HeaderType Type() const;
    uint8_t Code() const;
    uint16_t Checksum() const;
    uint16_t Identifire() const;
    uint16_t SequenceNumber() const;
    static std::shared_ptr <boost::asio::streambuf> Buffer (HeaderType theType, uint16_t theSeqNumber);
    static uint16_t ProcessId();

private:
    uint8_t myBuffer[8];
};

}}


#endif