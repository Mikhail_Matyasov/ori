#ifndef _Ori_NetAndroid_WiFiManager_HeaderFile
#define _Ori_NetAndroid_WiFiManager_HeaderFile

#include <Ori/Base_String.hxx>

class QAndroidJniObject;

namespace Ori {
namespace NetAndroid {

class WiFiManager
{
public:
    static WiFiManager* Instance();
    void OpenWiFiSettings();
    Base::String AvaliableAccessPoints();

private:
    WiFiManager();

private:
    std::shared_ptr <QAndroidJniObject> myImpl;
};

}}


#endif
