#ifndef _Ori_Net_RTPClient_HeaderFile
#define _Ori_Net_RTPClient_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <Ori/Net_RTPEndPoint.hxx>
#include <Ori/Base_Logger.hxx>
#include <Ori/Net_RTPSessionStatus.hxx>

#include <memory>
#include <unordered_map>

namespace jrtplib {
class RTPSession;
}

namespace Ori {
namespace NetTestLib {
class Helper;
}

namespace Base {
class String;
}

namespace Net {
class TcpClient;

class RTPClient
{
public:

    ORI_EXPORT RTPClient (int thePortBase,
                          const RTPEndPoint& thePoint,
                          const Ori::Base::Logger& theLogger = Ori::Base::Logger());

    ORI_EXPORT ~RTPClient();
    ORI_EXPORT bool Send (const Base::ByteArray& thePacket, bool theIsKey = true);
    ORI_EXPORT bool Send (const uint8_t* thePacket, int thePacketLenght, bool theIsKey = true);
    __ORI_READONLY_PRIMITIVE_PROPERTY (RTPSessionStatus, Status);

private:
    class ClientTcpObserver;
    class LostedPacket
    {
    public:
        __ORI_PRIMITIVE_PROPERTY (uint64_t, Timestamp);
        __ORI_PROPERTY (Base::Vector <uint16_t>, Indices);
    };
    // ORI_EXPORT for using in tests
    ORI_EXPORT bool SendWithTimestamp (const uint8_t* thePacket,
                                       int thePacketLenght,
                                       uint64_t theTs,
                                       bool theIsKey);

    ORI_EXPORT bool DoSend (const uint8_t* thePacket, size_t thePacketLenght);
    ORI_EXPORT void PrepareExtendedBuffer (uint16_t theNumOfSubPackets,
                                           size_t thePacketLenght,
                                           bool theIsKey,
                                           uint64_t theTs);
    
    ORI_EXPORT void InsertSubIndex (uint16_t theSubIndex);
    ORI_EXPORT void InsertIsLast (uint8_t theIsLast);
    ORI_EXPORT void CachePacket (const uint8_t* thePacket, size_t thePacketLenght, uint64_t theTs, bool theIsKey);
    void RecordLostedPacket (uint64_t theTs);
    void RecordLostedSubPackets (uint64_t theTs, const uint16_t* theData, int theLenght);
    ORI_EXPORT void FlushLostedPackets();

    friend Ori::NetTestLib::Helper;
    friend ClientTcpObserver;

private:
    std::shared_ptr <jrtplib::RTPSession>                            myRTPSession;
    std::shared_ptr <TcpClient>                                      myTcpClient;
    std::shared_ptr <ClientTcpObserver>                              myTcpObserver;
    std::vector <std::shared_ptr <Base::ByteArray>>                  myFreePacketBuffer;
    std::vector <LostedPacket>                                       myLostedPackets;
    std::unordered_map <uint64_t, std::shared_ptr <Base::ByteArray>> myCachedPacketMap;
    Base::ByteArray                                                  myExtendedDataBuffer;
    Base::Logger                                                     myLogger;
    uint64_t                                                         myTimestamp;
    bool                                                             myIsSessionActive;

};

}}


#endif