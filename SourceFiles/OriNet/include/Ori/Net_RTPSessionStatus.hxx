#ifndef _Ori_Net_SessionStatus_HeaderFile
#define _Ori_Net_SessionStatus_HeaderFile

namespace Ori {
namespace Net {

enum class RTPSessionStatus
{
    Success,
    Fail
};

}}

#endif