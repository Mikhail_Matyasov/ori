#ifndef _Ori_NetWin_NetworkInfoImpl_HeaderFile
#define _Ori_NetWin_NetworkInfoImpl_HeaderFile

#include <Ori/Base_Vector.hxx>
#include <Ori/Net_WiFiHotSpot.hxx>

namespace Ori {
namespace NetWin {

class NetworkInfoImpl
{
public:
    static Base::Vector <Net::WiFiHotSpot> AvaliableHotSpots();
};

}}


#endif
