#ifndef _Ori_Net_RTPEndPoint_HeaderFile
#define _Ori_Net_RTPEndPoint_HeaderFile

#include <Ori/Base_String.hxx>

namespace Ori {
namespace Net {

class RTPEndPoint
{
public:
    RTPEndPoint() :
        myIPAdress (""),
        myTcpPort (-1),
        myUdpPort (-1)
    {}

    RTPEndPoint (const Base::String& theIPAdress,
                 int theTcpPort,
                 int theUdpPort) :
        myIPAdress (theIPAdress),
        myTcpPort (theTcpPort),
        myUdpPort (theUdpPort)
    {}

    __ORI_PROPERTY (Base::String, IPAdress)
    __ORI_PRIMITIVE_PROPERTY (int, TcpPort)
    __ORI_PRIMITIVE_PROPERTY (int, UdpPort)
    
};

}}


#endif
