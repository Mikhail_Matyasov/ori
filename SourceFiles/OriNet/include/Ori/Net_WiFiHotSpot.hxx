#ifndef _Ori_Net_WiFiHotSpot_HeaderFile
#define _Ori_Net_WiFiHotSpot_HeaderFile

#include <Ori/NetBase_Macros.hxx>
#include <Ori/Base_String.hxx>
#include <memory>

__ORI_NET_DEFINE_WIN_ANDROID_IMPL (WiFiHotSpot);

namespace Ori {
namespace Net {

class WiFiHotSpot
{
public:
    ORI_EXPORT WiFiHotSpot (const std::shared_ptr <WiFiHotSpotImpl>& theImpl = nullptr);
    ORI_EXPORT const Base::String SSID() const;
    ORI_EXPORT static void OpenWiFiSettings();

private:
    std::shared_ptr <WiFiHotSpotImpl> myImpl;
};

}}


#endif
