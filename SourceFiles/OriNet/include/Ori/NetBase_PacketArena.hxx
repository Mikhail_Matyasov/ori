#ifndef _Ori_NetBase_PacketArena_HeaderFile
#define _Ori_NetBase_PacketArena_HeaderFile

#include <Ori/Net_RTPListener.hxx>
#include <Ori/NetBase_RTPPacket.hxx>
#include <Ori/NetBase_TcpListener.hxx>
#include <Ori/Base_Vector.hxx>
#include <Ori/Base_Logger.hxx>
#include <unordered_map>

namespace jrtplib {
class RTPPacket;
}

namespace Ori {
namespace NetBase {
class RTPSession;


// Packet arena implemented as - unordered_map <timestamp, RTPPacket>
//I ---------------------------------------- || - Each row it is an instance of RTPBase_RTPPacket
//P -----------x-x---xx---x-xx-------------- ||
//P ---------------------------------------- ||
//P ----x------x------x---------------x----- ||  Key farme interval |
//P -----------------------------x---------- ||----------------------
//P ---------------------------------------- ||
//P -------------------x-------------------- ||
//I ----------------------------------------
//P ----------------------------------------
//P -----------------------------x----------
//P ----------------------------------------
//P ----------------------------------------
//P ----------------------------------------
//P ----------------------------------------

// x - received subpackets

// As soon as one row is filled, we make a decision, if need give a signal about new RTPPacket.
// Signal is occured if
//   1) Timestamp of filled RTPPacket match with myExpectedTimestamp.
//   2) Filled packet stores "KEY" frame. It is an 'I' on the figure above.

// If two rows with timestamp > myExpectedTimestamp filled, we suppose, that some subpacket from "Expected" RTPPacket is losted.
// In that case we ask client to resend losted subpacket(s).
// In case if resended packet came early then next 'I' frame, we give a signal about new received packet, otherwise
// we drop all packets that has came before 'I frame' AND belong other "key frame interval"

// Pay attention! We should not send 'P' packet with timestamp == 2, before packet with timestamp 1,
// because decoder in browser drop it any way, but afterwards may come expected packet, which will be processed by decoder,
// but we hasn't next packet (decoder always dropped it). This scenario leeds to video artifacts.

class PacketArena
{
public:
    PacketArena (Net::RTPListener::Observer& theObserver,
                 NetBase::RTPSession& theSession,
                 const Base::Logger& theLogger,
                 int theTcpPort);
    void ProcessRTPPacket (jrtplib::RTPPacket* thePacket);

private:
    void ProcessLastSubPacket (uint8_t* theExtendedData,
                               uint8_t* thePlayloadData,
                               size_t   theDataLenght,
                               uint64_t theTimeStamp);

    void ProcessSubPacket (uint8_t* theExtendedData,
                           uint8_t* thePlayloadData,
                           size_t   theDataLenght,
                           uint64_t theTimeStamp);

    void InsertFirstSubPacket (uint8_t* theExtendedData,
                               uint8_t* thePlayloadData,
                               size_t   theDataLenght,
                               uint64_t theTimeStamp,
                               uint16_t theSubIndex,
                               uint16_t theNumOfSubPackets);

    void InsertSubPacket (uint8_t* thePlayloadData,
                          size_t   theDataLenght,
                          uint64_t theTimeStamp,
                          uint16_t theSubIndex);

    void ProcessFilledPacket (uint64_t theTimeStamp);

    void DropOutdatedPackets (uint64_t theKeyPacketTimeStamp);
    void GiveSignal (uint64_t theTimeStamp, const std::shared_ptr <RTPPacket>& thePacket);
    void ProcessNextPacket();
    void RequireLostedSubpackets();
    void ResetPacket (uint64_t theTimeStamp, const std::shared_ptr <RTPPacket>& thePacket);

    void DoRequireLostedSubpackets (uint64_t theTimestamp,
                                    const Base::Vector <uint16_t>& theIndices);
    void RequireLostedPacket (uint64_t theTimestamp);

private:
    Net::RTPListener::Observer& myObserver;
    NetBase::RTPSession& mySession;
    TcpListener myListener;
    std::unordered_map <uint64_t, std::shared_ptr <RTPPacket>> myPacketMap;
    std::unordered_map <uint64_t, std::shared_ptr <RTPPacket>> myFreePackets;
    Base::Vector <uint64_t> myRecuiredPackets;
    Base::Logger myLogger;
    uint64_t myExpectedTimestamp;
    int myNumberOfUnexpectedPackets;
};

}}


#endif