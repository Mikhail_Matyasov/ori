#ifndef _Ori_NetBase_IPv4Header_HeaderFile
#define _Ori_NetBase_IPv4Header_HeaderFile

#include <boost/asio/streambuf.hpp>
#include <boost/asio/ip/address_v4.hpp>

namespace Ori {
namespace NetBase {

class IPv4Header
{
// Packet header for IPv4.
//
// The wire format of an IPv4 header is:
// 
// 0               8               16                             31
// +-------+-------+---------------+------------------------------+      ---
// |       |       |               |                              |       ^
// |version|header |    type of    |    total length in bytes     |       |
// |  (4)  | length|    service    |                              |       |
// +-------+-------+---------------+-+-+-+------------------------+       |
// |                               | | | |                        |       |
// |        identification         |0|D|M|    fragment offset     |       |
// |                               | |F|F|                        |       |
// +---------------+---------------+-+-+-+------------------------+       |
// |               |               |                              |       |
// | time to live  |   protocol    |       header checksum        |   20 bytes
// |               |               |                              |       |
// +---------------+---------------+------------------------------+       |
// |                                                              |       |
// |                      source IPv4 address                     |       |
// |                                                              |       |
// +--------------------------------------------------------------+       |
// |                                                              |       |
// |                   destination IPv4 address                   |       |
// |                                                              |       v
// +--------------------------------------------------------------+      ---
// |                                                              |       ^
// |                                                              |       |
// /                        options (if any)                      /    0 - 40
// /                                                              /     bytes
// |                                                              |       |
// |                                                              |       v
// +--------------------------------------------------------------+      ---

public:
    IPv4Header (boost::asio::streambuf& theBuf);
    int Version() const;
    int HeaderLenght() const;
    uint8_t TypeOfService() const;
    uint16_t TotalLength() const;
    uint16_t Identification() const;
    bool DontFragment() const;
    bool MoreFragments() const;
    uint16_t FragmentOffset() const;
    uint32_t TimeToLive() const;
    uint8_t Protocol() const;
    uint16_t HeaderChecksum() const;
    boost::asio::ip::address_v4 SourceAdress() const;
    boost::asio::ip::address_v4 DestinationAdress() const;

private:
    uint8_t myBuffer[60];
};

}}

#endif