#ifndef _Ori_NetBase_TcpHelper_HeaderFile
#define _Ori_NetBase_TcpHelper_HeaderFile

#include <Ori/NetBase_TcpBuffer.hxx>

#include <boost/asio.hpp>
#include <boost/thread.hpp>

namespace Ori {
namespace Net {
class TcpObserver;
}
namespace NetBase {

class TcpHelper
{
public:
    TcpHelper (boost::asio::io_context& theContext, boost::asio::ip::tcp::socket& theSocket);
    void OnLenghtAvaliable();
    void OnBufferAvaliable();
    void RegisterObserver (Net::TcpObserver* theObserver);
    void Listen();
    void RunContext();
    void Stop();
    bool Send (const uint8_t* theData, int theLenght);

private:
    boost::asio::io_context& myContext;
    boost::asio::ip::tcp::socket& mySocket;
    Net::TcpObserver* myObserver;
    bool myIsActive;
    bool myIsReading;
    TcpBuffer myBuffer;
    std::shared_ptr <boost::thread> myContextThread;
    std::shared_ptr <boost::thread> myListenThread;

};

}}


#endif