#ifndef _Ori_Net_TcpClient_HeaderFile
#define _Ori_Net_TcpClient_HeaderFile

#include <Ori/NetBase_Macros.hxx>
#include <Ori/Net_IPEndPoint.hxx>
#include <Ori/Base_ByteArray.hxx>
#include <memory>

__ORI_NET_DEFINE_WIN_UNIX_IMPL(TcpClient);

namespace Ori {
namespace Net {
class TcpObserver;

class TcpClient
{
public:

    ORI_EXPORT TcpClient (const IPEndPoint& theEndPoint = IPEndPoint());
    ORI_EXPORT bool Connect (const IPEndPoint& theEndPoint = IPEndPoint());
    ORI_EXPORT bool Connected() const;
    ORI_EXPORT bool Send (const uint8_t* theData, int theLength);
    ORI_EXPORT void RegisterObserver (TcpObserver* theObserver);
    ORI_EXPORT const IPEndPoint& EndPoint() const;
    ORI_EXPORT IPEndPoint& EndPoint();

private:
    std::shared_ptr <TcpClientImpl> myImpl;
};

}}


#endif
