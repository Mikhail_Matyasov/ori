#ifndef _Ori_NetWin_TcpClientImpl_HeaderFile
#define _Ori_NetWin_TcpClientImpl_HeaderFile

#include <Ori/Net_TcpClient.hxx>
#include <Ori/Base_ByteArray.hxx>

namespace Ori {
namespace NetWin {

class TcpClientImpl
{
public:
    TcpClientImpl (const Net::IPEndPoint& theEndPoint);
    ~TcpClientImpl();
    bool Connect (const Net::IPEndPoint& theEndPoint);
    bool Connected() const;
    bool Send (const uint8_t* theData, int theLength);
    void RegisterObserver (Net::TcpObserver* theObserver);

    __ORI_PROPERTY (Net::IPEndPoint, IPEndPoint);

private:
    bool DoConnect();
    void Destroy();

private:
    Net::TcpObserver* myObserver;
    uint64_t mySocket;
    int myError;
};

}}


#endif
