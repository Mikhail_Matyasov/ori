#ifndef _Ori_NetBase_ConnectionCheckerImpl_HeaderFile
#define _Ori_NetBase_ConnectionCheckerImpl_HeaderFile

#include <Ori/Base_String.hxx>
#include <Ori/Net_ConnectionChecker.hxx>

#include <boost/asio.hpp>
#include <boost/thread.hpp>

using namespace boost::asio::ip;
using namespace boost::asio;

namespace Ori {
namespace NetBase {

class ConnectionCheckerImpl
{
public:
    ConnectionCheckerImpl (const Base::String& theHost);
    ~ConnectionCheckerImpl();
    void RegisterObserver (Net::ConnectionChecker::Observer* theObserver);
    void SetHost (const Base::String& theHost);
    const Base::String& Host() const;

private:
    void StartSend();
    void StartReceive();
    void OnTimeout();
    void OnReply (std::size_t theLength);

private:
    io_context                        myContext;
    icmp::endpoint                    myEndPoint;
    icmp::socket                      mySocket;
    steady_timer                      myTimer;
    int                               mySequenceNumber;
    int                               myNumberOfReplies;
    chrono::steady_clock::time_point  mySentTime;
    streambuf                         myBuffer;
    Net::ConnectionChecker::Observer* myObserver;
    Base::String                      myHost;
    std::shared_ptr <boost::thread>   myContextThread;
};

}}

#endif