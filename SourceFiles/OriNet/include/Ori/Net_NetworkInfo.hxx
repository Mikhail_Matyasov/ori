#ifndef _Ori_Net_NetworkInfo_HeaderFile
#define _Ori_Net_NetworkInfo_HeaderFile

#include <Ori/Base_Vector.hxx>
#include <Ori/Net_WiFiHotSpot.hxx>

namespace Ori {
namespace Net {

class NetworkInfo
{
public:
    ORI_EXPORT static Base::Vector <WiFiHotSpot> AvaliableHotSpots();
};

}}


#endif
