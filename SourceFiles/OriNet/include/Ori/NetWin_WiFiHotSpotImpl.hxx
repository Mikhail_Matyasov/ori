#ifndef _Ori_NetWin_WiFiHotSpotImpl_HeaderFile
#define _Ori_NetWin_WiFiHotSpotImpl_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Base_String.hxx>

#include <wtypes.h>
#include <wlanapi.h>

namespace Ori {
namespace NetWin {

class WiFiHotSpotImpl
{
public:
    WiFiHotSpotImpl (const Base::String& theSSID);

    static void OpenWiFiSettings();
    bool IsConnected() const;

    __ORI_PROPERTY (Base::String, SSID);

private:
    bool DoConnect (const WLAN_AVAILABLE_NETWORK_V2& theNetwork,
                    HANDLE theClient,
                    GUID theGuid,
                    const Base::String& thePassword) const;
};

}}


#endif
