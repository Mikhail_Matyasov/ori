#ifndef _Ori_NetAndroid_HotSpotImpl_HeaderFile
#define _Ori_NetAndroid_HotSpotImpl_HeaderFile

#include <Ori/Base_String.hxx>

namespace Ori {
namespace NetAndroid {

class WiFiHotSpotImpl
{
public:
    WiFiHotSpotImpl (const Base::String& theSSID);

    static void OpenWiFiSettings();

    __ORI_PROPERTY (Base::String, SSID);

};

}}


#endif
