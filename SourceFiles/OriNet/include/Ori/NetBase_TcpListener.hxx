#ifndef _Ori_NetBase_TcpListener_HeaderFile
#define _Ori_NetBase_TcpListener_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/NetBase_TcpHelper.hxx>
#include <boost/asio.hpp>

namespace Ori {
namespace Net {
class TcpObserver;
}
namespace NetBase {

class TcpListener
{
public:

    ORI_EXPORT TcpListener (int thePort);
    ORI_EXPORT void RegisterObserver (Net::TcpObserver* theObserver);
    ORI_EXPORT bool Send (const uint8_t* theData, int theLenght);
    ORI_EXPORT ~TcpListener();

private:
    void OnAccepted (const boost::system::error_code& theError);
    void Accept();

private:
    boost::asio::io_context myContext;
    boost::asio::ip::tcp::socket mySocket;
    boost::asio::ip::tcp::endpoint myEndPoint;
    boost::asio::ip::tcp::acceptor myAcceptor;

    TcpHelper myHelper;

};

}}

#endif