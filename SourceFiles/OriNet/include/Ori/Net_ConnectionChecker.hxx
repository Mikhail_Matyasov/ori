#ifndef _Ori_Net_ConnectionChecker_HeaderFile
#define _Ori_Net_ConnectionChecker_HeaderFile

#include <Ori/Base_Macros.hxx>
#include <Ori/Net_ConnectionStatistics.hxx>
#include <memory>

namespace Ori {
namespace Base {
class String;
}
namespace NetBase {
class ConnectionCheckerImpl;
}
namespace Net {

class ConnectionChecker
{
public:
    class Observer
    {
    public:
        virtual void OnStatistics (const ConnectionStatistics&) {}
        virtual void OnTimeout() {}
    };

    ORI_EXPORT ConnectionChecker (const Base::String& theHost);
    ORI_EXPORT void RegisterObserver (ConnectionChecker::Observer* theObserver);
    ORI_EXPORT void SetHost (Base::String& theHost);
    ORI_EXPORT const Base::String& Host() const;

private:
    std::shared_ptr <NetBase::ConnectionCheckerImpl> myImpl;
};

}}


#endif