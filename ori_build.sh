
SUPPORTED_TARGETS="codecjs"
SUPPORTED_COMPILERS="em"
SUPPORTED_MODES="debug release"
SUPPORTED_ARCH="x86 x86_64"
USAGE=$"ori_build.sh [target] [compiler] [mode] [arch]
        target: ${SUPPORTED_TARGETS}
        compiler: ${SUPPORTED_COMPILERS}
        mode: ${SUPPORTED_MODES}
        arch: ${SUPPORTED_ARCH}"
  
export TARGET=$1
export COMPILER=$2
export MODE=$3
export ARCH=$4
  
if [ $TARGET == "--help" ]; then
    echo "${USAGE}"
    exit 0
fi

./build_${TARGET}.sh
