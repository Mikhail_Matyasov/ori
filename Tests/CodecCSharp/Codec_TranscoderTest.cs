﻿using Xunit;
using Ori;
using System;
using System.IO;
using System.Collections.Generic;

namespace Ori.CodecCSharpTest
{
public class Codec_CSharpTest
{
    static Codec_CSharpTest()
    {
        string anOriHome = Environment.GetEnvironmentVariable("ORI_HOME");
        Core.Helpers.DllLoader.Load ($@"{anOriHome}\\DLLs\\Codec\\debug");
    }

    [Fact]
    public void Codec_PacketTest()
    {
        ByteStream aData = new ByteStream { 1, 2, 3, 4 };
        Codec.Packet aPacket = new Codec.Packet (aData);
        Assert.True (aPacket.Data().Count == 4);
    }

    [Fact]
    public void Codec_ParametersTest() 
    {
        Codec.Parameters aParameters = new Codec.Parameters();
        aParameters.SetHeight (240);
        aParameters.SetWidth (320);
        aParameters.SetPixelFormat (AVPixelFormat.AV_PIX_FMT_YUV420P);
        aParameters.SetEncoderType (Codec.Parameters.CodecType.Jpeg);
        aParameters.SetDecoderType (Codec.Parameters.CodecType.Mpeg);

        Assert.False (aParameters.EncoderType() == Codec.Parameters.CodecType.Mpeg);
    }

    byte[] ReadFile() 
    {
        var anOriHome = Environment.GetEnvironmentVariable ("ORI_HOME");
        string aFileName =  $@"{anOriHome}\Tests\CodecCSharp\Resources\Source.jpg";

        BinaryReader aReader = new BinaryReader (File.Open (aFileName, FileMode.Open));
        var aRes = aReader.ReadBytes ((int) aReader.BaseStream.Length);
        return aRes;
    }

    [Fact]
    public void Codec_TranscoderTest()
    {
        Codec.Parameters aParameters = new Codec.Parameters();
        aParameters.SetWidth (1366);
        aParameters.SetHeight (768);
        aParameters.SetPixelFormat (AVPixelFormat.AV_PIX_FMT_YUV420P);
        aParameters.SetDecoderType (Codec.Parameters.CodecType.Jpeg);
        aParameters.SetEncoderType (Codec.Parameters.CodecType.Mpeg);

        Codec.Transcoder aTranscoder = new Codec.Transcoder (aParameters);
        byte[] aSourceData = ReadFile();
        ByteStream aStreamData = new ByteStream (aSourceData);
        Assert.True (aSourceData.Length == 727777);

        Codec.Packet aSourcePacket = new Codec.Packet (aStreamData);
        List <byte> aStream = new List <byte>();
        for (int i = 0; i < 25; i++) {
            var aRes = aTranscoder.Transcode (aSourcePacket);
            if (!aRes) {
                continue;
            }

            Codec.Packet aResPacket = new Codec.Packet();
            while (aTranscoder.GetNextPacket (aResPacket)) {
                aStream.AddRange (aResPacket.Data().ToArray());
            }
        }

        Assert.True (aStream.Count == 2417770);

        int aCheckIndex = 131313;
        int aValue = aStream[aCheckIndex];
        Assert.True (aValue == 201);
    }
}
}
