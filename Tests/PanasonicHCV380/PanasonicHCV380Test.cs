﻿using Microsoft.AspNetCore.SignalR.Client;
using Ori.Core;
using Ori.PanasonicHCV380;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Ori.PanasonicHCV380Test
{
    public class PanasonicHCV380Test_TestEnvironment
    {
        public readonly static string Server = Core_Base.HOST_IP;

        public static string AccessPoint {
            get {
                return Core_Base.HOST_ACCESSPOINT;
            }
        }
    }
    public class PanasonicHCV380Test_HttpManager
    {

        [Fact]
        public void CreateRequest()
        {
            PanasonicHCV380_HttpManager aManager = new PanasonicHCV380_HttpManager 
                (PanasonicHCV380Test_TestEnvironment.Server);

            string aRequest = $"cam.cgi?mode=getstate";
            var aRes = aManager.CreateRequest (aRequest);
            Assert.True (aRes);
        }

        [Fact]
        public void CreateRequestAndWaitResponse()
        {
            PanasonicHCV380_HttpManager aManager = new PanasonicHCV380_HttpManager 
                (/*PanasonicHCV380Test_TestEnvironment.Server*/"192.168.56.1");

            string aRequest = $"cam.cgi?mode=getstate";
            var aRes = aManager.CreateRequestAndWaitResponse (aRequest);
            Assert.NotNull (aRes);
        }
    }

    public class PanasonicHCV380Test_StreamSender : PanasonicHCV380_StreamSender
    {
        private void CloseConnection(PanasonicHCV380_StreamSender theSender)
        {
            theSender.HubConnection.SendAsync("CloseCurrentRecord");
            theSender.HubConnection.StopAsync();
        }

        [Fact]
        public void Connect_Remote()
        {
            PanasonicHCV380_StreamSender aSender = new PanasonicHCV380_StreamSender();
            aSender.Connect (Core_Base.REMOTEHOST + "/hub_frame_manager");

            Assert.True (aSender.HubConnection.State == HubConnectionState.Connected);
        }

        [Fact]
        public void Connect_Local()
        {
            PanasonicHCV380_StreamSender aSender = new PanasonicHCV380_StreamSender();
            aSender.Connect ($"{Core_Base.VIRTUALMACHINE_IIS}/hub_frame_manager");
            CloseConnection (aSender);

            Assert.True(aSender.HubConnection.State == HubConnectionState.Connected);
        }

        [Fact]
        public void ConnectQueue_Local()
        {
            for(int i = 0; i < 1000; i++)
            {
                PanasonicHCV380_StreamSender aSender = new PanasonicHCV380_StreamSender();
                aSender.Connect($"{Core_Base.LOCALHOST}/hubreceiver");
                CloseConnection (aSender);

                Assert.True (aSender.HubConnection.State == HubConnectionState.Connected);                
            }
        }

        [Fact]
        public void CreateRecordWithName()
        {
            PanasonicHCV380_StreamSender aSender = new PanasonicHCV380_StreamSender();
            aSender.Connect ($"{Core_Base.LOCALHOST}/hubreceiver", "New Record 1");
            CloseConnection (aSender);
        }

        string GenerateString (int theSize) {
            string aRes = null;
            Random aRand = new Random();
            for (int i = 0; i < theSize; i++) {
                aRes += aRand.Next() % 10;
            }
            return aRes;
        }
        [Fact]
        public void SpliteFrame()
        {
            string[] aSource = new string[3];
            aSource[0] = GenerateString (1400);
            aSource[1] = GenerateString (1400);
            aSource[2] = GenerateString (200);
            string aSourceString = string.Join("", aSource);
            string[] aSplitedString = base.SplitFrame (aSourceString);

            for (int i = 0; i < aSource.Length; i++) {
                Assert.Equal (aSource[i].Length, aSplitedString[i].Length);
                Assert.Equal (aSource[i], aSplitedString[i]);
            }
        }
    }

    public class PanasonicHCV380Test_NetworkExplorer : PanasonicHCV380_NetworkExplorer
    {
        [Fact]
        public void ExtractServerPrefix_Test()
        {
            string[] aServers = { "192.168.0.103", "243.0.0.0", "248.0.123.0" };
            string[] anExpectedPrefs = { "192.168.0.", "243.0.0.", "248.0.123." };

            Assert.Equal (anExpectedPrefs[0],  base.ExtractServerPrefix (aServers[0]));
            Assert.Equal (anExpectedPrefs[1],  base.ExtractServerPrefix (aServers[1]));
            Assert.Equal (anExpectedPrefs[2],  base.ExtractServerPrefix (aServers[2]));
        }

        [Fact]
        public void IsLocalIp_Test()
        {
            string[] anIps =       { "192.168.43.85", "243.0.0.0", "192.0.123.0", "248.168.0.103" };
            bool[]   anExpecteds = { true, false, false, false };

            Assert.Equal (anExpecteds[0], base.IsLocalIp (anIps[0]));
            Assert.Equal (anExpecteds[1], base.IsLocalIp (anIps[1]));
            Assert.Equal (anExpecteds[2], base.IsLocalIp (anIps[2]));
            Assert.Equal (anExpecteds[3], base.IsLocalIp (anIps[3]));
        }

        [Fact]
        public void FindAccessPoint_Test()
        {
            base.FindAccessPoint();
            Assert.True (base.ErrorCode == Core_Base.ErrorCodes.OK);
        }

        [Fact]
        public void FindServer_Test()
        {
            base.AccessPoint = PanasonicHCV380Test_TestEnvironment.AccessPoint;
            Assert.True (base.ErrorCode == Core_Base.ErrorCodes.OK);

            Task.WaitAll (FindServer());
            Assert.Equal (PanasonicHCV380Test_TestEnvironment.Server, base.Server);
            Assert.True (base.ErrorCode == Core_Base.ErrorCodes.OK);
        }
    }

    public class PanasonicHCV380Test_StreamReader : PanasonicHCV380_StreamReader {

        [Fact]
        public void Init_Test()
        {
            base.AccessPoint = Core_Base.HOST_ACCESSPOINT;
            base.Init();
            
            Assert.True (base.myErrorCode == Core_Base.ErrorCodes.OK);
            Assert.True (base.AccessPoint != "");
            Assert.Equal (base.myHttpManager.Server, PanasonicHCV380Test_TestEnvironment.Server);
        }

        [Fact]
        public void SetAccCtrl_Test()
        {
            base.AccessPoint = Core_Base.HOST_ACCESSPOINT;
            base.Init();
            base.SetAccCtrl();
        }

        [Fact]
        public void SetCapability_Test()
        {
            base.AccessPoint = Core_Base.HOST_ACCESSPOINT;
            base.Init();
            base.SetAccCtrl();
            base.SetCapability();
        }

        [Fact]
        public void SetRecMode_Test()
        {
            base.AccessPoint = Core_Base.HOST_ACCESSPOINT;
            base.Init();
            base.SetAccCtrl();
            base.SetCapability();
            base.SetRecMode();
        }

        [Fact]
        public void SetStartStream_Test()
        {
            base.AccessPoint = Core_Base.HOST_ACCESSPOINT;
            base.Init();
            base.SetAccCtrl();
            base.SetCapability();
            base.SetRecMode();
            base.SetStartStream();
        }

        [Fact]
        public void GetTagValue_Test()
        {
            string anExpectedTagValue = "Hello World !";

            string aSourceTag1 = "<tag1>value1<tag2><tag3>"+anExpectedTagValue+"</tag3></tag2></tag1>";
            string aSourceTag2 = "<tag1>tag3<tag2><tag3>"+anExpectedTagValue+"</tag3></tag2></tag1>";
            string aTagValue1 = base.GetTagValue (aSourceTag1, "tag3");
            string aTagValue2 = base.GetTagValue (aSourceTag2, "tag3");

            Assert.True (aTagValue1 == anExpectedTagValue);
            Assert.True (aTagValue2 == anExpectedTagValue);
        }

        [Fact]
        public void ReadStream_Test()
        {
            base.AccessPoint = Core_Base.HOST_ACCESSPOINT;
            try {
                base.ReadStream();
            } catch (Exception e) {
                if (e.Message != "Требуемый адрес для своего контекста неверен") {
                    throw new ArgumentException (e.Message);
                }
            }
            base.Stop();
        }
    }
}
