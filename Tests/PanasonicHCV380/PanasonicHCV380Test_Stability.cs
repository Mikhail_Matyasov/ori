﻿using Ori.Core;
using Ori.PanasonicHCV380;
using System.Threading;
using Xunit;

namespace Ori.PanasonicHCV380Test
{
    public class PanasonicHCV380Test_Stability
    {
        private void Run (int theTimeout, int theExpectedFrames)
        {
            PanasonicHCV380_StreamReader aReader = new PanasonicHCV380_StreamReader();

            int aNumberFrames = 0;
            aReader.OnReceiveFrameEvent += new Core_BaseCameraStreamReader.OnReceiveFrameDelegate
                ((theFrame)=>
                {
                    aNumberFrames++;
                });
            aReader.ReadStream();

            Thread.Sleep (theTimeout);
            aReader.Stop();

            if (aNumberFrames > theExpectedFrames) {
                Assert.True (aNumberFrames > theExpectedFrames);
            } else {
                throw new System.ArgumentException ("Number Frames: " + aNumberFrames);
            }
        }

        [Fact]
        public void Basic()
        {
            Run (10000, 20);
        }

        [Fact]
        public void Main()
        {
            Run (10000, 90);
        }

        [Fact]
        public void Extra()
        {
            Run (10000, 200);
        }

        [Fact]
        public void ExtraHigh_10s()
        {
            Run (10000, 240);
        }

        [Fact]
        public void ExtraHigh_30s()
        {
            Run (30000, 720);
        }

        [Fact]
        public void ExtraHigh_60s()
        {
            Run (60000, 1440);
        }

        [Fact]
        public void ExtraHigh_300s()
        {
            Run (300000, 7200);
        }

        [Fact]
        public void ExtraHigh_600s()
        {
            Run (600000, 14400);
        }

    }
}
